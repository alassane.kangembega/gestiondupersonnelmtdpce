import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { INaturePositionIrreguliere } from '../nature-position-irreguliere.model';

@Component({
  selector: 'jhi-nature-position-irreguliere-detail',
  templateUrl: './nature-position-irreguliere-detail.component.html',
})
export class NaturePositionIrreguliereDetailComponent implements OnInit {
  naturePositionIrreguliere: INaturePositionIrreguliere | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ naturePositionIrreguliere }) => {
      this.naturePositionIrreguliere = naturePositionIrreguliere;
    });
  }

  previousState(): void {
    window.history.back();
  }
}

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { NaturePositionIrreguliereDetailComponent } from './nature-position-irreguliere-detail.component';

describe('NaturePositionIrreguliere Management Detail Component', () => {
  let comp: NaturePositionIrreguliereDetailComponent;
  let fixture: ComponentFixture<NaturePositionIrreguliereDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [NaturePositionIrreguliereDetailComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: { data: of({ naturePositionIrreguliere: { id: 123 } }) },
        },
      ],
    })
      .overrideTemplate(NaturePositionIrreguliereDetailComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(NaturePositionIrreguliereDetailComponent);
    comp = fixture.componentInstance;
  });

  describe('OnInit', () => {
    it('Should load naturePositionIrreguliere on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(comp.naturePositionIrreguliere).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});

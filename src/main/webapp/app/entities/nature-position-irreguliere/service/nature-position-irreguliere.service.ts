import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { INaturePositionIrreguliere, NewNaturePositionIrreguliere } from '../nature-position-irreguliere.model';

export type PartialUpdateNaturePositionIrreguliere = Partial<INaturePositionIrreguliere> & Pick<INaturePositionIrreguliere, 'id'>;

export type EntityResponseType = HttpResponse<INaturePositionIrreguliere>;
export type EntityArrayResponseType = HttpResponse<INaturePositionIrreguliere[]>;

@Injectable({ providedIn: 'root' })
export class NaturePositionIrreguliereService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/nature-position-irregulieres');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(naturePositionIrreguliere: NewNaturePositionIrreguliere): Observable<EntityResponseType> {
    return this.http.post<INaturePositionIrreguliere>(this.resourceUrl, naturePositionIrreguliere, { observe: 'response' });
  }

  update(naturePositionIrreguliere: INaturePositionIrreguliere): Observable<EntityResponseType> {
    return this.http.put<INaturePositionIrreguliere>(
      `${this.resourceUrl}/${this.getNaturePositionIrreguliereIdentifier(naturePositionIrreguliere)}`,
      naturePositionIrreguliere,
      { observe: 'response' }
    );
  }

  partialUpdate(naturePositionIrreguliere: PartialUpdateNaturePositionIrreguliere): Observable<EntityResponseType> {
    return this.http.patch<INaturePositionIrreguliere>(
      `${this.resourceUrl}/${this.getNaturePositionIrreguliereIdentifier(naturePositionIrreguliere)}`,
      naturePositionIrreguliere,
      { observe: 'response' }
    );
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<INaturePositionIrreguliere>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<INaturePositionIrreguliere[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  getNaturePositionIrreguliereIdentifier(naturePositionIrreguliere: Pick<INaturePositionIrreguliere, 'id'>): number {
    return naturePositionIrreguliere.id;
  }

  compareNaturePositionIrreguliere(
    o1: Pick<INaturePositionIrreguliere, 'id'> | null,
    o2: Pick<INaturePositionIrreguliere, 'id'> | null
  ): boolean {
    return o1 && o2 ? this.getNaturePositionIrreguliereIdentifier(o1) === this.getNaturePositionIrreguliereIdentifier(o2) : o1 === o2;
  }

  addNaturePositionIrreguliereToCollectionIfMissing<Type extends Pick<INaturePositionIrreguliere, 'id'>>(
    naturePositionIrreguliereCollection: Type[],
    ...naturePositionIrregulieresToCheck: (Type | null | undefined)[]
  ): Type[] {
    const naturePositionIrregulieres: Type[] = naturePositionIrregulieresToCheck.filter(isPresent);
    if (naturePositionIrregulieres.length > 0) {
      const naturePositionIrreguliereCollectionIdentifiers = naturePositionIrreguliereCollection.map(
        naturePositionIrreguliereItem => this.getNaturePositionIrreguliereIdentifier(naturePositionIrreguliereItem)!
      );
      const naturePositionIrregulieresToAdd = naturePositionIrregulieres.filter(naturePositionIrreguliereItem => {
        const naturePositionIrreguliereIdentifier = this.getNaturePositionIrreguliereIdentifier(naturePositionIrreguliereItem);
        if (naturePositionIrreguliereCollectionIdentifiers.includes(naturePositionIrreguliereIdentifier)) {
          return false;
        }
        naturePositionIrreguliereCollectionIdentifiers.push(naturePositionIrreguliereIdentifier);
        return true;
      });
      return [...naturePositionIrregulieresToAdd, ...naturePositionIrreguliereCollection];
    }
    return naturePositionIrreguliereCollection;
  }
}

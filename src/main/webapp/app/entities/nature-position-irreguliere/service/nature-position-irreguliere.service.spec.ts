import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { INaturePositionIrreguliere } from '../nature-position-irreguliere.model';
import {
  sampleWithRequiredData,
  sampleWithNewData,
  sampleWithPartialData,
  sampleWithFullData,
} from '../nature-position-irreguliere.test-samples';

import { NaturePositionIrreguliereService } from './nature-position-irreguliere.service';

const requireRestSample: INaturePositionIrreguliere = {
  ...sampleWithRequiredData,
};

describe('NaturePositionIrreguliere Service', () => {
  let service: NaturePositionIrreguliereService;
  let httpMock: HttpTestingController;
  let expectedResult: INaturePositionIrreguliere | INaturePositionIrreguliere[] | boolean | null;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(NaturePositionIrreguliereService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should create a NaturePositionIrreguliere', () => {
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      const naturePositionIrreguliere = { ...sampleWithNewData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.create(naturePositionIrreguliere).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a NaturePositionIrreguliere', () => {
      const naturePositionIrreguliere = { ...sampleWithRequiredData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.update(naturePositionIrreguliere).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a NaturePositionIrreguliere', () => {
      const patchObject = { ...sampleWithPartialData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of NaturePositionIrreguliere', () => {
      const returnedFromService = { ...requireRestSample };

      const expected = { ...sampleWithRequiredData };

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toMatchObject([expected]);
    });

    it('should delete a NaturePositionIrreguliere', () => {
      const expected = true;

      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult).toBe(expected);
    });

    describe('addNaturePositionIrreguliereToCollectionIfMissing', () => {
      it('should add a NaturePositionIrreguliere to an empty array', () => {
        const naturePositionIrreguliere: INaturePositionIrreguliere = sampleWithRequiredData;
        expectedResult = service.addNaturePositionIrreguliereToCollectionIfMissing([], naturePositionIrreguliere);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(naturePositionIrreguliere);
      });

      it('should not add a NaturePositionIrreguliere to an array that contains it', () => {
        const naturePositionIrreguliere: INaturePositionIrreguliere = sampleWithRequiredData;
        const naturePositionIrreguliereCollection: INaturePositionIrreguliere[] = [
          {
            ...naturePositionIrreguliere,
          },
          sampleWithPartialData,
        ];
        expectedResult = service.addNaturePositionIrreguliereToCollectionIfMissing(
          naturePositionIrreguliereCollection,
          naturePositionIrreguliere
        );
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a NaturePositionIrreguliere to an array that doesn't contain it", () => {
        const naturePositionIrreguliere: INaturePositionIrreguliere = sampleWithRequiredData;
        const naturePositionIrreguliereCollection: INaturePositionIrreguliere[] = [sampleWithPartialData];
        expectedResult = service.addNaturePositionIrreguliereToCollectionIfMissing(
          naturePositionIrreguliereCollection,
          naturePositionIrreguliere
        );
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(naturePositionIrreguliere);
      });

      it('should add only unique NaturePositionIrreguliere to an array', () => {
        const naturePositionIrreguliereArray: INaturePositionIrreguliere[] = [
          sampleWithRequiredData,
          sampleWithPartialData,
          sampleWithFullData,
        ];
        const naturePositionIrreguliereCollection: INaturePositionIrreguliere[] = [sampleWithRequiredData];
        expectedResult = service.addNaturePositionIrreguliereToCollectionIfMissing(
          naturePositionIrreguliereCollection,
          ...naturePositionIrreguliereArray
        );
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const naturePositionIrreguliere: INaturePositionIrreguliere = sampleWithRequiredData;
        const naturePositionIrreguliere2: INaturePositionIrreguliere = sampleWithPartialData;
        expectedResult = service.addNaturePositionIrreguliereToCollectionIfMissing(
          [],
          naturePositionIrreguliere,
          naturePositionIrreguliere2
        );
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(naturePositionIrreguliere);
        expect(expectedResult).toContain(naturePositionIrreguliere2);
      });

      it('should accept null and undefined values', () => {
        const naturePositionIrreguliere: INaturePositionIrreguliere = sampleWithRequiredData;
        expectedResult = service.addNaturePositionIrreguliereToCollectionIfMissing([], null, naturePositionIrreguliere, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(naturePositionIrreguliere);
      });

      it('should return initial array if no NaturePositionIrreguliere is added', () => {
        const naturePositionIrreguliereCollection: INaturePositionIrreguliere[] = [sampleWithRequiredData];
        expectedResult = service.addNaturePositionIrreguliereToCollectionIfMissing(naturePositionIrreguliereCollection, undefined, null);
        expect(expectedResult).toEqual(naturePositionIrreguliereCollection);
      });
    });

    describe('compareNaturePositionIrreguliere', () => {
      it('Should return true if both entities are null', () => {
        const entity1 = null;
        const entity2 = null;

        const compareResult = service.compareNaturePositionIrreguliere(entity1, entity2);

        expect(compareResult).toEqual(true);
      });

      it('Should return false if one entity is null', () => {
        const entity1 = { id: 123 };
        const entity2 = null;

        const compareResult1 = service.compareNaturePositionIrreguliere(entity1, entity2);
        const compareResult2 = service.compareNaturePositionIrreguliere(entity2, entity1);

        expect(compareResult1).toEqual(false);
        expect(compareResult2).toEqual(false);
      });

      it('Should return false if primaryKey differs', () => {
        const entity1 = { id: 123 };
        const entity2 = { id: 456 };

        const compareResult1 = service.compareNaturePositionIrreguliere(entity1, entity2);
        const compareResult2 = service.compareNaturePositionIrreguliere(entity2, entity1);

        expect(compareResult1).toEqual(false);
        expect(compareResult2).toEqual(false);
      });

      it('Should return false if primaryKey matches', () => {
        const entity1 = { id: 123 };
        const entity2 = { id: 123 };

        const compareResult1 = service.compareNaturePositionIrreguliere(entity1, entity2);
        const compareResult2 = service.compareNaturePositionIrreguliere(entity2, entity1);

        expect(compareResult1).toEqual(true);
        expect(compareResult2).toEqual(true);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});

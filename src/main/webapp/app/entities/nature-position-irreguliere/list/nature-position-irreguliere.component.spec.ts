import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';

import { NaturePositionIrreguliereService } from '../service/nature-position-irreguliere.service';

import { NaturePositionIrreguliereComponent } from './nature-position-irreguliere.component';

describe('NaturePositionIrreguliere Management Component', () => {
  let comp: NaturePositionIrreguliereComponent;
  let fixture: ComponentFixture<NaturePositionIrreguliereComponent>;
  let service: NaturePositionIrreguliereService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule.withRoutes([{ path: 'nature-position-irreguliere', component: NaturePositionIrreguliereComponent }]),
        HttpClientTestingModule,
      ],
      declarations: [NaturePositionIrreguliereComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            data: of({
              defaultSort: 'id,asc',
            }),
            queryParamMap: of(
              jest.requireActual('@angular/router').convertToParamMap({
                page: '1',
                size: '1',
                sort: 'id,desc',
              })
            ),
            snapshot: { queryParams: {} },
          },
        },
      ],
    })
      .overrideTemplate(NaturePositionIrreguliereComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(NaturePositionIrreguliereComponent);
    comp = fixture.componentInstance;
    service = TestBed.inject(NaturePositionIrreguliereService);

    const headers = new HttpHeaders();
    jest.spyOn(service, 'query').mockReturnValue(
      of(
        new HttpResponse({
          body: [{ id: 123 }],
          headers,
        })
      )
    );
  });

  it('Should call load all on init', () => {
    // WHEN
    comp.ngOnInit();

    // THEN
    expect(service.query).toHaveBeenCalled();
    expect(comp.naturePositionIrregulieres?.[0]).toEqual(expect.objectContaining({ id: 123 }));
  });

  describe('trackId', () => {
    it('Should forward to naturePositionIrreguliereService', () => {
      const entity = { id: 123 };
      jest.spyOn(service, 'getNaturePositionIrreguliereIdentifier');
      const id = comp.trackId(0, entity);
      expect(service.getNaturePositionIrreguliereIdentifier).toHaveBeenCalledWith(entity);
      expect(id).toBe(entity.id);
    });
  });
});

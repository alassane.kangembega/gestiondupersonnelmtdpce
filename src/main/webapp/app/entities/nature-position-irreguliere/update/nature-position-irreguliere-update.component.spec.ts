import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { NaturePositionIrreguliereFormService } from './nature-position-irreguliere-form.service';
import { NaturePositionIrreguliereService } from '../service/nature-position-irreguliere.service';
import { INaturePositionIrreguliere } from '../nature-position-irreguliere.model';

import { NaturePositionIrreguliereUpdateComponent } from './nature-position-irreguliere-update.component';

describe('NaturePositionIrreguliere Management Update Component', () => {
  let comp: NaturePositionIrreguliereUpdateComponent;
  let fixture: ComponentFixture<NaturePositionIrreguliereUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let naturePositionIrreguliereFormService: NaturePositionIrreguliereFormService;
  let naturePositionIrreguliereService: NaturePositionIrreguliereService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      declarations: [NaturePositionIrreguliereUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(NaturePositionIrreguliereUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(NaturePositionIrreguliereUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    naturePositionIrreguliereFormService = TestBed.inject(NaturePositionIrreguliereFormService);
    naturePositionIrreguliereService = TestBed.inject(NaturePositionIrreguliereService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should update editForm', () => {
      const naturePositionIrreguliere: INaturePositionIrreguliere = { id: 456 };

      activatedRoute.data = of({ naturePositionIrreguliere });
      comp.ngOnInit();

      expect(comp.naturePositionIrreguliere).toEqual(naturePositionIrreguliere);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<INaturePositionIrreguliere>>();
      const naturePositionIrreguliere = { id: 123 };
      jest.spyOn(naturePositionIrreguliereFormService, 'getNaturePositionIrreguliere').mockReturnValue(naturePositionIrreguliere);
      jest.spyOn(naturePositionIrreguliereService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ naturePositionIrreguliere });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: naturePositionIrreguliere }));
      saveSubject.complete();

      // THEN
      expect(naturePositionIrreguliereFormService.getNaturePositionIrreguliere).toHaveBeenCalled();
      expect(comp.previousState).toHaveBeenCalled();
      expect(naturePositionIrreguliereService.update).toHaveBeenCalledWith(expect.objectContaining(naturePositionIrreguliere));
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<INaturePositionIrreguliere>>();
      const naturePositionIrreguliere = { id: 123 };
      jest.spyOn(naturePositionIrreguliereFormService, 'getNaturePositionIrreguliere').mockReturnValue({ id: null });
      jest.spyOn(naturePositionIrreguliereService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ naturePositionIrreguliere: null });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: naturePositionIrreguliere }));
      saveSubject.complete();

      // THEN
      expect(naturePositionIrreguliereFormService.getNaturePositionIrreguliere).toHaveBeenCalled();
      expect(naturePositionIrreguliereService.create).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<INaturePositionIrreguliere>>();
      const naturePositionIrreguliere = { id: 123 };
      jest.spyOn(naturePositionIrreguliereService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ naturePositionIrreguliere });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(naturePositionIrreguliereService.update).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });
});

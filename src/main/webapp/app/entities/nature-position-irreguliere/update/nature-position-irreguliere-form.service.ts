import { Injectable } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { INaturePositionIrreguliere, NewNaturePositionIrreguliere } from '../nature-position-irreguliere.model';

/**
 * A partial Type with required key is used as form input.
 */
type PartialWithRequiredKeyOf<T extends { id: unknown }> = Partial<Omit<T, 'id'>> & { id: T['id'] };

/**
 * Type for createFormGroup and resetForm argument.
 * It accepts INaturePositionIrreguliere for edit and NewNaturePositionIrreguliereFormGroupInput for create.
 */
type NaturePositionIrreguliereFormGroupInput = INaturePositionIrreguliere | PartialWithRequiredKeyOf<NewNaturePositionIrreguliere>;

type NaturePositionIrreguliereFormDefaults = Pick<NewNaturePositionIrreguliere, 'id'>;

type NaturePositionIrreguliereFormGroupContent = {
  id: FormControl<INaturePositionIrreguliere['id'] | NewNaturePositionIrreguliere['id']>;
  naturePositionIrreguliere: FormControl<INaturePositionIrreguliere['naturePositionIrreguliere']>;
};

export type NaturePositionIrreguliereFormGroup = FormGroup<NaturePositionIrreguliereFormGroupContent>;

@Injectable({ providedIn: 'root' })
export class NaturePositionIrreguliereFormService {
  createNaturePositionIrreguliereFormGroup(
    naturePositionIrreguliere: NaturePositionIrreguliereFormGroupInput = { id: null }
  ): NaturePositionIrreguliereFormGroup {
    const naturePositionIrreguliereRawValue = {
      ...this.getFormDefaults(),
      ...naturePositionIrreguliere,
    };
    return new FormGroup<NaturePositionIrreguliereFormGroupContent>({
      id: new FormControl(
        { value: naturePositionIrreguliereRawValue.id, disabled: true },
        {
          nonNullable: true,
          validators: [Validators.required],
        }
      ),
      naturePositionIrreguliere: new FormControl(naturePositionIrreguliereRawValue.naturePositionIrreguliere),
    });
  }

  getNaturePositionIrreguliere(form: NaturePositionIrreguliereFormGroup): INaturePositionIrreguliere | NewNaturePositionIrreguliere {
    return form.getRawValue() as INaturePositionIrreguliere | NewNaturePositionIrreguliere;
  }

  resetForm(form: NaturePositionIrreguliereFormGroup, naturePositionIrreguliere: NaturePositionIrreguliereFormGroupInput): void {
    const naturePositionIrreguliereRawValue = { ...this.getFormDefaults(), ...naturePositionIrreguliere };
    form.reset(
      {
        ...naturePositionIrreguliereRawValue,
        id: { value: naturePositionIrreguliereRawValue.id, disabled: true },
      } as any /* cast to workaround https://github.com/angular/angular/issues/46458 */
    );
  }

  private getFormDefaults(): NaturePositionIrreguliereFormDefaults {
    return {
      id: null,
    };
  }
}

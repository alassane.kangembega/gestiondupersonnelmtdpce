import { TestBed } from '@angular/core/testing';

import { sampleWithRequiredData, sampleWithNewData } from '../nature-position-irreguliere.test-samples';

import { NaturePositionIrreguliereFormService } from './nature-position-irreguliere-form.service';

describe('NaturePositionIrreguliere Form Service', () => {
  let service: NaturePositionIrreguliereFormService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(NaturePositionIrreguliereFormService);
  });

  describe('Service methods', () => {
    describe('createNaturePositionIrreguliereFormGroup', () => {
      it('should create a new form with FormControl', () => {
        const formGroup = service.createNaturePositionIrreguliereFormGroup();

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            naturePositionIrreguliere: expect.any(Object),
          })
        );
      });

      it('passing INaturePositionIrreguliere should create a new form with FormGroup', () => {
        const formGroup = service.createNaturePositionIrreguliereFormGroup(sampleWithRequiredData);

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            naturePositionIrreguliere: expect.any(Object),
          })
        );
      });
    });

    describe('getNaturePositionIrreguliere', () => {
      it('should return NewNaturePositionIrreguliere for default NaturePositionIrreguliere initial value', () => {
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        const formGroup = service.createNaturePositionIrreguliereFormGroup(sampleWithNewData);

        const naturePositionIrreguliere = service.getNaturePositionIrreguliere(formGroup) as any;

        expect(naturePositionIrreguliere).toMatchObject(sampleWithNewData);
      });

      it('should return NewNaturePositionIrreguliere for empty NaturePositionIrreguliere initial value', () => {
        const formGroup = service.createNaturePositionIrreguliereFormGroup();

        const naturePositionIrreguliere = service.getNaturePositionIrreguliere(formGroup) as any;

        expect(naturePositionIrreguliere).toMatchObject({});
      });

      it('should return INaturePositionIrreguliere', () => {
        const formGroup = service.createNaturePositionIrreguliereFormGroup(sampleWithRequiredData);

        const naturePositionIrreguliere = service.getNaturePositionIrreguliere(formGroup) as any;

        expect(naturePositionIrreguliere).toMatchObject(sampleWithRequiredData);
      });
    });

    describe('resetForm', () => {
      it('passing INaturePositionIrreguliere should not enable id FormControl', () => {
        const formGroup = service.createNaturePositionIrreguliereFormGroup();
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, sampleWithRequiredData);

        expect(formGroup.controls.id.disabled).toBe(true);
      });

      it('passing NewNaturePositionIrreguliere should disable id FormControl', () => {
        const formGroup = service.createNaturePositionIrreguliereFormGroup(sampleWithRequiredData);
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, { id: null });

        expect(formGroup.controls.id.disabled).toBe(true);
      });
    });
  });
});

import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

import { NaturePositionIrreguliereFormService, NaturePositionIrreguliereFormGroup } from './nature-position-irreguliere-form.service';
import { INaturePositionIrreguliere } from '../nature-position-irreguliere.model';
import { NaturePositionIrreguliereService } from '../service/nature-position-irreguliere.service';

@Component({
  selector: 'jhi-nature-position-irreguliere-update',
  templateUrl: './nature-position-irreguliere-update.component.html',
})
export class NaturePositionIrreguliereUpdateComponent implements OnInit {
  isSaving = false;
  naturePositionIrreguliere: INaturePositionIrreguliere | null = null;

  editForm: NaturePositionIrreguliereFormGroup = this.naturePositionIrreguliereFormService.createNaturePositionIrreguliereFormGroup();

  constructor(
    protected naturePositionIrreguliereService: NaturePositionIrreguliereService,
    protected naturePositionIrreguliereFormService: NaturePositionIrreguliereFormService,
    protected activatedRoute: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ naturePositionIrreguliere }) => {
      this.naturePositionIrreguliere = naturePositionIrreguliere;
      if (naturePositionIrreguliere) {
        this.updateForm(naturePositionIrreguliere);
      }
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const naturePositionIrreguliere = this.naturePositionIrreguliereFormService.getNaturePositionIrreguliere(this.editForm);
    if (naturePositionIrreguliere.id !== null) {
      this.subscribeToSaveResponse(this.naturePositionIrreguliereService.update(naturePositionIrreguliere));
    } else {
      this.subscribeToSaveResponse(this.naturePositionIrreguliereService.create(naturePositionIrreguliere));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<INaturePositionIrreguliere>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(naturePositionIrreguliere: INaturePositionIrreguliere): void {
    this.naturePositionIrreguliere = naturePositionIrreguliere;
    this.naturePositionIrreguliereFormService.resetForm(this.editForm, naturePositionIrreguliere);
  }
}

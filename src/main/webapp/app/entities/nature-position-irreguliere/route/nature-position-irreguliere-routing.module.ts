import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { NaturePositionIrreguliereComponent } from '../list/nature-position-irreguliere.component';
import { NaturePositionIrreguliereDetailComponent } from '../detail/nature-position-irreguliere-detail.component';
import { NaturePositionIrreguliereUpdateComponent } from '../update/nature-position-irreguliere-update.component';
import { NaturePositionIrreguliereRoutingResolveService } from './nature-position-irreguliere-routing-resolve.service';
import { ASC } from 'app/config/navigation.constants';

const naturePositionIrreguliereRoute: Routes = [
  {
    path: '',
    component: NaturePositionIrreguliereComponent,
    data: {
      defaultSort: 'id,' + ASC,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: NaturePositionIrreguliereDetailComponent,
    resolve: {
      naturePositionIrreguliere: NaturePositionIrreguliereRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: NaturePositionIrreguliereUpdateComponent,
    resolve: {
      naturePositionIrreguliere: NaturePositionIrreguliereRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: NaturePositionIrreguliereUpdateComponent,
    resolve: {
      naturePositionIrreguliere: NaturePositionIrreguliereRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(naturePositionIrreguliereRoute)],
  exports: [RouterModule],
})
export class NaturePositionIrreguliereRoutingModule {}

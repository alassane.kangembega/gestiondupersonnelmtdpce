import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { INaturePositionIrreguliere } from '../nature-position-irreguliere.model';
import { NaturePositionIrreguliereService } from '../service/nature-position-irreguliere.service';

@Injectable({ providedIn: 'root' })
export class NaturePositionIrreguliereRoutingResolveService implements Resolve<INaturePositionIrreguliere | null> {
  constructor(protected service: NaturePositionIrreguliereService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<INaturePositionIrreguliere | null | never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((naturePositionIrreguliere: HttpResponse<INaturePositionIrreguliere>) => {
          if (naturePositionIrreguliere.body) {
            return of(naturePositionIrreguliere.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(null);
  }
}

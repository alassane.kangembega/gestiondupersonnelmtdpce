export interface INaturePositionIrreguliere {
  id: number;
  naturePositionIrreguliere?: string | null;
}

export type NewNaturePositionIrreguliere = Omit<INaturePositionIrreguliere, 'id'> & { id: null };

import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { INaturePositionIrreguliere } from '../nature-position-irreguliere.model';
import { NaturePositionIrreguliereService } from '../service/nature-position-irreguliere.service';
import { ITEM_DELETED_EVENT } from 'app/config/navigation.constants';

@Component({
  templateUrl: './nature-position-irreguliere-delete-dialog.component.html',
})
export class NaturePositionIrreguliereDeleteDialogComponent {
  naturePositionIrreguliere?: INaturePositionIrreguliere;

  constructor(protected naturePositionIrreguliereService: NaturePositionIrreguliereService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.naturePositionIrreguliereService.delete(id).subscribe(() => {
      this.activeModal.close(ITEM_DELETED_EVENT);
    });
  }
}

import { INaturePositionIrreguliere, NewNaturePositionIrreguliere } from './nature-position-irreguliere.model';

export const sampleWithRequiredData: INaturePositionIrreguliere = {
  id: 99174,
};

export const sampleWithPartialData: INaturePositionIrreguliere = {
  id: 39625,
};

export const sampleWithFullData: INaturePositionIrreguliere = {
  id: 57243,
  naturePositionIrreguliere: 'Lorraine invoice Guatemala',
};

export const sampleWithNewData: NewNaturePositionIrreguliere = {
  id: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);

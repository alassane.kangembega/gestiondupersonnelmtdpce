import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { NaturePositionIrreguliereComponent } from './list/nature-position-irreguliere.component';
import { NaturePositionIrreguliereDetailComponent } from './detail/nature-position-irreguliere-detail.component';
import { NaturePositionIrreguliereUpdateComponent } from './update/nature-position-irreguliere-update.component';
import { NaturePositionIrreguliereDeleteDialogComponent } from './delete/nature-position-irreguliere-delete-dialog.component';
import { NaturePositionIrreguliereRoutingModule } from './route/nature-position-irreguliere-routing.module';

@NgModule({
  imports: [SharedModule, NaturePositionIrreguliereRoutingModule],
  declarations: [
    NaturePositionIrreguliereComponent,
    NaturePositionIrreguliereDetailComponent,
    NaturePositionIrreguliereUpdateComponent,
    NaturePositionIrreguliereDeleteDialogComponent,
  ],
})
export class NaturePositionIrreguliereModule {}

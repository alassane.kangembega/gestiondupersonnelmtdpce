import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { AvancementClasseDetailComponent } from './avancement-classe-detail.component';

describe('AvancementClasse Management Detail Component', () => {
  let comp: AvancementClasseDetailComponent;
  let fixture: ComponentFixture<AvancementClasseDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [AvancementClasseDetailComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: { data: of({ avancementClasse: { id: 123 } }) },
        },
      ],
    })
      .overrideTemplate(AvancementClasseDetailComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(AvancementClasseDetailComponent);
    comp = fixture.componentInstance;
  });

  describe('OnInit', () => {
    it('Should load avancementClasse on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(comp.avancementClasse).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});

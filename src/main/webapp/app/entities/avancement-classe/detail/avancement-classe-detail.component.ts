import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IAvancementClasse } from '../avancement-classe.model';

@Component({
  selector: 'jhi-avancement-classe-detail',
  templateUrl: './avancement-classe-detail.component.html',
})
export class AvancementClasseDetailComponent implements OnInit {
  avancementClasse: IAvancementClasse | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ avancementClasse }) => {
      this.avancementClasse = avancementClasse;
    });
  }

  previousState(): void {
    window.history.back();
  }
}

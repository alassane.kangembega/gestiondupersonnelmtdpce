import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';

import { AvancementClasseService } from '../service/avancement-classe.service';

import { AvancementClasseComponent } from './avancement-classe.component';

describe('AvancementClasse Management Component', () => {
  let comp: AvancementClasseComponent;
  let fixture: ComponentFixture<AvancementClasseComponent>;
  let service: AvancementClasseService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule.withRoutes([{ path: 'avancement-classe', component: AvancementClasseComponent }]),
        HttpClientTestingModule,
      ],
      declarations: [AvancementClasseComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            data: of({
              defaultSort: 'id,asc',
            }),
            queryParamMap: of(
              jest.requireActual('@angular/router').convertToParamMap({
                page: '1',
                size: '1',
                sort: 'id,desc',
              })
            ),
            snapshot: { queryParams: {} },
          },
        },
      ],
    })
      .overrideTemplate(AvancementClasseComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(AvancementClasseComponent);
    comp = fixture.componentInstance;
    service = TestBed.inject(AvancementClasseService);

    const headers = new HttpHeaders();
    jest.spyOn(service, 'query').mockReturnValue(
      of(
        new HttpResponse({
          body: [{ id: 123 }],
          headers,
        })
      )
    );
  });

  it('Should call load all on init', () => {
    // WHEN
    comp.ngOnInit();

    // THEN
    expect(service.query).toHaveBeenCalled();
    expect(comp.avancementClasses?.[0]).toEqual(expect.objectContaining({ id: 123 }));
  });

  describe('trackId', () => {
    it('Should forward to avancementClasseService', () => {
      const entity = { id: 123 };
      jest.spyOn(service, 'getAvancementClasseIdentifier');
      const id = comp.trackId(0, entity);
      expect(service.getAvancementClasseIdentifier).toHaveBeenCalledWith(entity);
      expect(id).toBe(entity.id);
    });
  });
});

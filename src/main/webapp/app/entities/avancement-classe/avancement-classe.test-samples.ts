import dayjs from 'dayjs/esm';

import { IAvancementClasse, NewAvancementClasse } from './avancement-classe.model';

export const sampleWithRequiredData: IAvancementClasse = {
  id: 15603,
};

export const sampleWithPartialData: IAvancementClasse = {
  id: 28105,
  dateAvancement: dayjs('2022-09-06'),
};

export const sampleWithFullData: IAvancementClasse = {
  id: 57209,
  dateAvancement: dayjs('2022-09-07'),
};

export const sampleWithNewData: NewAvancementClasse = {
  id: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);

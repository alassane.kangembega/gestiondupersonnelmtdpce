import dayjs from 'dayjs/esm';
import { IClasse } from 'app/entities/classe/classe.model';
import { IEmploye } from 'app/entities/employe/employe.model';

export interface IAvancementClasse {
  id: number;
  dateAvancement?: dayjs.Dayjs | null;
  classe?: Pick<IClasse, 'id'> | null;
  employe?: Pick<IEmploye, 'id'> | null;
}

export type NewAvancementClasse = Omit<IAvancementClasse, 'id'> & { id: null };

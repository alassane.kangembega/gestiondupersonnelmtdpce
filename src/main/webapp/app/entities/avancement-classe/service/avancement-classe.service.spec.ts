import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { DATE_FORMAT } from 'app/config/input.constants';
import { IAvancementClasse } from '../avancement-classe.model';
import { sampleWithRequiredData, sampleWithNewData, sampleWithPartialData, sampleWithFullData } from '../avancement-classe.test-samples';

import { AvancementClasseService, RestAvancementClasse } from './avancement-classe.service';

const requireRestSample: RestAvancementClasse = {
  ...sampleWithRequiredData,
  dateAvancement: sampleWithRequiredData.dateAvancement?.format(DATE_FORMAT),
};

describe('AvancementClasse Service', () => {
  let service: AvancementClasseService;
  let httpMock: HttpTestingController;
  let expectedResult: IAvancementClasse | IAvancementClasse[] | boolean | null;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(AvancementClasseService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should create a AvancementClasse', () => {
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      const avancementClasse = { ...sampleWithNewData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.create(avancementClasse).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a AvancementClasse', () => {
      const avancementClasse = { ...sampleWithRequiredData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.update(avancementClasse).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a AvancementClasse', () => {
      const patchObject = { ...sampleWithPartialData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of AvancementClasse', () => {
      const returnedFromService = { ...requireRestSample };

      const expected = { ...sampleWithRequiredData };

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toMatchObject([expected]);
    });

    it('should delete a AvancementClasse', () => {
      const expected = true;

      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult).toBe(expected);
    });

    describe('addAvancementClasseToCollectionIfMissing', () => {
      it('should add a AvancementClasse to an empty array', () => {
        const avancementClasse: IAvancementClasse = sampleWithRequiredData;
        expectedResult = service.addAvancementClasseToCollectionIfMissing([], avancementClasse);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(avancementClasse);
      });

      it('should not add a AvancementClasse to an array that contains it', () => {
        const avancementClasse: IAvancementClasse = sampleWithRequiredData;
        const avancementClasseCollection: IAvancementClasse[] = [
          {
            ...avancementClasse,
          },
          sampleWithPartialData,
        ];
        expectedResult = service.addAvancementClasseToCollectionIfMissing(avancementClasseCollection, avancementClasse);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a AvancementClasse to an array that doesn't contain it", () => {
        const avancementClasse: IAvancementClasse = sampleWithRequiredData;
        const avancementClasseCollection: IAvancementClasse[] = [sampleWithPartialData];
        expectedResult = service.addAvancementClasseToCollectionIfMissing(avancementClasseCollection, avancementClasse);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(avancementClasse);
      });

      it('should add only unique AvancementClasse to an array', () => {
        const avancementClasseArray: IAvancementClasse[] = [sampleWithRequiredData, sampleWithPartialData, sampleWithFullData];
        const avancementClasseCollection: IAvancementClasse[] = [sampleWithRequiredData];
        expectedResult = service.addAvancementClasseToCollectionIfMissing(avancementClasseCollection, ...avancementClasseArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const avancementClasse: IAvancementClasse = sampleWithRequiredData;
        const avancementClasse2: IAvancementClasse = sampleWithPartialData;
        expectedResult = service.addAvancementClasseToCollectionIfMissing([], avancementClasse, avancementClasse2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(avancementClasse);
        expect(expectedResult).toContain(avancementClasse2);
      });

      it('should accept null and undefined values', () => {
        const avancementClasse: IAvancementClasse = sampleWithRequiredData;
        expectedResult = service.addAvancementClasseToCollectionIfMissing([], null, avancementClasse, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(avancementClasse);
      });

      it('should return initial array if no AvancementClasse is added', () => {
        const avancementClasseCollection: IAvancementClasse[] = [sampleWithRequiredData];
        expectedResult = service.addAvancementClasseToCollectionIfMissing(avancementClasseCollection, undefined, null);
        expect(expectedResult).toEqual(avancementClasseCollection);
      });
    });

    describe('compareAvancementClasse', () => {
      it('Should return true if both entities are null', () => {
        const entity1 = null;
        const entity2 = null;

        const compareResult = service.compareAvancementClasse(entity1, entity2);

        expect(compareResult).toEqual(true);
      });

      it('Should return false if one entity is null', () => {
        const entity1 = { id: 123 };
        const entity2 = null;

        const compareResult1 = service.compareAvancementClasse(entity1, entity2);
        const compareResult2 = service.compareAvancementClasse(entity2, entity1);

        expect(compareResult1).toEqual(false);
        expect(compareResult2).toEqual(false);
      });

      it('Should return false if primaryKey differs', () => {
        const entity1 = { id: 123 };
        const entity2 = { id: 456 };

        const compareResult1 = service.compareAvancementClasse(entity1, entity2);
        const compareResult2 = service.compareAvancementClasse(entity2, entity1);

        expect(compareResult1).toEqual(false);
        expect(compareResult2).toEqual(false);
      });

      it('Should return false if primaryKey matches', () => {
        const entity1 = { id: 123 };
        const entity2 = { id: 123 };

        const compareResult1 = service.compareAvancementClasse(entity1, entity2);
        const compareResult2 = service.compareAvancementClasse(entity2, entity1);

        expect(compareResult1).toEqual(true);
        expect(compareResult2).toEqual(true);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});

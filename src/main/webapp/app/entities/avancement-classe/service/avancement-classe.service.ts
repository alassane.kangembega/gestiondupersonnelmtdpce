import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import dayjs from 'dayjs/esm';

import { isPresent } from 'app/core/util/operators';
import { DATE_FORMAT } from 'app/config/input.constants';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IAvancementClasse, NewAvancementClasse } from '../avancement-classe.model';

export type PartialUpdateAvancementClasse = Partial<IAvancementClasse> & Pick<IAvancementClasse, 'id'>;

type RestOf<T extends IAvancementClasse | NewAvancementClasse> = Omit<T, 'dateAvancement'> & {
  dateAvancement?: string | null;
};

export type RestAvancementClasse = RestOf<IAvancementClasse>;

export type NewRestAvancementClasse = RestOf<NewAvancementClasse>;

export type PartialUpdateRestAvancementClasse = RestOf<PartialUpdateAvancementClasse>;

export type EntityResponseType = HttpResponse<IAvancementClasse>;
export type EntityArrayResponseType = HttpResponse<IAvancementClasse[]>;

@Injectable({ providedIn: 'root' })
export class AvancementClasseService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/avancement-classes');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(avancementClasse: NewAvancementClasse): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(avancementClasse);
    return this.http
      .post<RestAvancementClasse>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  update(avancementClasse: IAvancementClasse): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(avancementClasse);
    return this.http
      .put<RestAvancementClasse>(`${this.resourceUrl}/${this.getAvancementClasseIdentifier(avancementClasse)}`, copy, {
        observe: 'response',
      })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  partialUpdate(avancementClasse: PartialUpdateAvancementClasse): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(avancementClasse);
    return this.http
      .patch<RestAvancementClasse>(`${this.resourceUrl}/${this.getAvancementClasseIdentifier(avancementClasse)}`, copy, {
        observe: 'response',
      })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<RestAvancementClasse>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<RestAvancementClasse[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map(res => this.convertResponseArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  getAvancementClasseIdentifier(avancementClasse: Pick<IAvancementClasse, 'id'>): number {
    return avancementClasse.id;
  }

  compareAvancementClasse(o1: Pick<IAvancementClasse, 'id'> | null, o2: Pick<IAvancementClasse, 'id'> | null): boolean {
    return o1 && o2 ? this.getAvancementClasseIdentifier(o1) === this.getAvancementClasseIdentifier(o2) : o1 === o2;
  }

  addAvancementClasseToCollectionIfMissing<Type extends Pick<IAvancementClasse, 'id'>>(
    avancementClasseCollection: Type[],
    ...avancementClassesToCheck: (Type | null | undefined)[]
  ): Type[] {
    const avancementClasses: Type[] = avancementClassesToCheck.filter(isPresent);
    if (avancementClasses.length > 0) {
      const avancementClasseCollectionIdentifiers = avancementClasseCollection.map(
        avancementClasseItem => this.getAvancementClasseIdentifier(avancementClasseItem)!
      );
      const avancementClassesToAdd = avancementClasses.filter(avancementClasseItem => {
        const avancementClasseIdentifier = this.getAvancementClasseIdentifier(avancementClasseItem);
        if (avancementClasseCollectionIdentifiers.includes(avancementClasseIdentifier)) {
          return false;
        }
        avancementClasseCollectionIdentifiers.push(avancementClasseIdentifier);
        return true;
      });
      return [...avancementClassesToAdd, ...avancementClasseCollection];
    }
    return avancementClasseCollection;
  }

  protected convertDateFromClient<T extends IAvancementClasse | NewAvancementClasse | PartialUpdateAvancementClasse>(
    avancementClasse: T
  ): RestOf<T> {
    return {
      ...avancementClasse,
      dateAvancement: avancementClasse.dateAvancement?.format(DATE_FORMAT) ?? null,
    };
  }

  protected convertDateFromServer(restAvancementClasse: RestAvancementClasse): IAvancementClasse {
    return {
      ...restAvancementClasse,
      dateAvancement: restAvancementClasse.dateAvancement ? dayjs(restAvancementClasse.dateAvancement) : undefined,
    };
  }

  protected convertResponseFromServer(res: HttpResponse<RestAvancementClasse>): HttpResponse<IAvancementClasse> {
    return res.clone({
      body: res.body ? this.convertDateFromServer(res.body) : null,
    });
  }

  protected convertResponseArrayFromServer(res: HttpResponse<RestAvancementClasse[]>): HttpResponse<IAvancementClasse[]> {
    return res.clone({
      body: res.body ? res.body.map(item => this.convertDateFromServer(item)) : null,
    });
  }
}

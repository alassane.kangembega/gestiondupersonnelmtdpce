import { TestBed } from '@angular/core/testing';

import { sampleWithRequiredData, sampleWithNewData } from '../avancement-classe.test-samples';

import { AvancementClasseFormService } from './avancement-classe-form.service';

describe('AvancementClasse Form Service', () => {
  let service: AvancementClasseFormService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AvancementClasseFormService);
  });

  describe('Service methods', () => {
    describe('createAvancementClasseFormGroup', () => {
      it('should create a new form with FormControl', () => {
        const formGroup = service.createAvancementClasseFormGroup();

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            dateAvancement: expect.any(Object),
            classe: expect.any(Object),
            employe: expect.any(Object),
          })
        );
      });

      it('passing IAvancementClasse should create a new form with FormGroup', () => {
        const formGroup = service.createAvancementClasseFormGroup(sampleWithRequiredData);

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            dateAvancement: expect.any(Object),
            classe: expect.any(Object),
            employe: expect.any(Object),
          })
        );
      });
    });

    describe('getAvancementClasse', () => {
      it('should return NewAvancementClasse for default AvancementClasse initial value', () => {
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        const formGroup = service.createAvancementClasseFormGroup(sampleWithNewData);

        const avancementClasse = service.getAvancementClasse(formGroup) as any;

        expect(avancementClasse).toMatchObject(sampleWithNewData);
      });

      it('should return NewAvancementClasse for empty AvancementClasse initial value', () => {
        const formGroup = service.createAvancementClasseFormGroup();

        const avancementClasse = service.getAvancementClasse(formGroup) as any;

        expect(avancementClasse).toMatchObject({});
      });

      it('should return IAvancementClasse', () => {
        const formGroup = service.createAvancementClasseFormGroup(sampleWithRequiredData);

        const avancementClasse = service.getAvancementClasse(formGroup) as any;

        expect(avancementClasse).toMatchObject(sampleWithRequiredData);
      });
    });

    describe('resetForm', () => {
      it('passing IAvancementClasse should not enable id FormControl', () => {
        const formGroup = service.createAvancementClasseFormGroup();
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, sampleWithRequiredData);

        expect(formGroup.controls.id.disabled).toBe(true);
      });

      it('passing NewAvancementClasse should disable id FormControl', () => {
        const formGroup = service.createAvancementClasseFormGroup(sampleWithRequiredData);
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, { id: null });

        expect(formGroup.controls.id.disabled).toBe(true);
      });
    });
  });
});

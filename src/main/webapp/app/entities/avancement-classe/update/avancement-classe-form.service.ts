import { Injectable } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { IAvancementClasse, NewAvancementClasse } from '../avancement-classe.model';

/**
 * A partial Type with required key is used as form input.
 */
type PartialWithRequiredKeyOf<T extends { id: unknown }> = Partial<Omit<T, 'id'>> & { id: T['id'] };

/**
 * Type for createFormGroup and resetForm argument.
 * It accepts IAvancementClasse for edit and NewAvancementClasseFormGroupInput for create.
 */
type AvancementClasseFormGroupInput = IAvancementClasse | PartialWithRequiredKeyOf<NewAvancementClasse>;

type AvancementClasseFormDefaults = Pick<NewAvancementClasse, 'id'>;

type AvancementClasseFormGroupContent = {
  id: FormControl<IAvancementClasse['id'] | NewAvancementClasse['id']>;
  dateAvancement: FormControl<IAvancementClasse['dateAvancement']>;
  classe: FormControl<IAvancementClasse['classe']>;
  employe: FormControl<IAvancementClasse['employe']>;
};

export type AvancementClasseFormGroup = FormGroup<AvancementClasseFormGroupContent>;

@Injectable({ providedIn: 'root' })
export class AvancementClasseFormService {
  createAvancementClasseFormGroup(avancementClasse: AvancementClasseFormGroupInput = { id: null }): AvancementClasseFormGroup {
    const avancementClasseRawValue = {
      ...this.getFormDefaults(),
      ...avancementClasse,
    };
    return new FormGroup<AvancementClasseFormGroupContent>({
      id: new FormControl(
        { value: avancementClasseRawValue.id, disabled: true },
        {
          nonNullable: true,
          validators: [Validators.required],
        }
      ),
      dateAvancement: new FormControl(avancementClasseRawValue.dateAvancement),
      classe: new FormControl(avancementClasseRawValue.classe),
      employe: new FormControl(avancementClasseRawValue.employe),
    });
  }

  getAvancementClasse(form: AvancementClasseFormGroup): IAvancementClasse | NewAvancementClasse {
    return form.getRawValue() as IAvancementClasse | NewAvancementClasse;
  }

  resetForm(form: AvancementClasseFormGroup, avancementClasse: AvancementClasseFormGroupInput): void {
    const avancementClasseRawValue = { ...this.getFormDefaults(), ...avancementClasse };
    form.reset(
      {
        ...avancementClasseRawValue,
        id: { value: avancementClasseRawValue.id, disabled: true },
      } as any /* cast to workaround https://github.com/angular/angular/issues/46458 */
    );
  }

  private getFormDefaults(): AvancementClasseFormDefaults {
    return {
      id: null,
    };
  }
}

import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { AvancementClasseFormService, AvancementClasseFormGroup } from './avancement-classe-form.service';
import { IAvancementClasse } from '../avancement-classe.model';
import { AvancementClasseService } from '../service/avancement-classe.service';
import { IClasse } from 'app/entities/classe/classe.model';
import { ClasseService } from 'app/entities/classe/service/classe.service';
import { IEmploye } from 'app/entities/employe/employe.model';
import { EmployeService } from 'app/entities/employe/service/employe.service';

@Component({
  selector: 'jhi-avancement-classe-update',
  templateUrl: './avancement-classe-update.component.html',
})
export class AvancementClasseUpdateComponent implements OnInit {
  isSaving = false;
  avancementClasse: IAvancementClasse | null = null;

  classesSharedCollection: IClasse[] = [];
  employesSharedCollection: IEmploye[] = [];

  editForm: AvancementClasseFormGroup = this.avancementClasseFormService.createAvancementClasseFormGroup();

  constructor(
    protected avancementClasseService: AvancementClasseService,
    protected avancementClasseFormService: AvancementClasseFormService,
    protected classeService: ClasseService,
    protected employeService: EmployeService,
    protected activatedRoute: ActivatedRoute
  ) {}

  compareClasse = (o1: IClasse | null, o2: IClasse | null): boolean => this.classeService.compareClasse(o1, o2);

  compareEmploye = (o1: IEmploye | null, o2: IEmploye | null): boolean => this.employeService.compareEmploye(o1, o2);

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ avancementClasse }) => {
      this.avancementClasse = avancementClasse;
      if (avancementClasse) {
        this.updateForm(avancementClasse);
      }

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const avancementClasse = this.avancementClasseFormService.getAvancementClasse(this.editForm);
    if (avancementClasse.id !== null) {
      this.subscribeToSaveResponse(this.avancementClasseService.update(avancementClasse));
    } else {
      this.subscribeToSaveResponse(this.avancementClasseService.create(avancementClasse));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IAvancementClasse>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(avancementClasse: IAvancementClasse): void {
    this.avancementClasse = avancementClasse;
    this.avancementClasseFormService.resetForm(this.editForm, avancementClasse);

    this.classesSharedCollection = this.classeService.addClasseToCollectionIfMissing<IClasse>(
      this.classesSharedCollection,
      avancementClasse.classe
    );
    this.employesSharedCollection = this.employeService.addEmployeToCollectionIfMissing<IEmploye>(
      this.employesSharedCollection,
      avancementClasse.employe
    );
  }

  protected loadRelationshipsOptions(): void {
    this.classeService
      .query()
      .pipe(map((res: HttpResponse<IClasse[]>) => res.body ?? []))
      .pipe(map((classes: IClasse[]) => this.classeService.addClasseToCollectionIfMissing<IClasse>(classes, this.avancementClasse?.classe)))
      .subscribe((classes: IClasse[]) => (this.classesSharedCollection = classes));

    this.employeService
      .query()
      .pipe(map((res: HttpResponse<IEmploye[]>) => res.body ?? []))
      .pipe(
        map((employes: IEmploye[]) =>
          this.employeService.addEmployeToCollectionIfMissing<IEmploye>(employes, this.avancementClasse?.employe)
        )
      )
      .subscribe((employes: IEmploye[]) => (this.employesSharedCollection = employes));
  }
}

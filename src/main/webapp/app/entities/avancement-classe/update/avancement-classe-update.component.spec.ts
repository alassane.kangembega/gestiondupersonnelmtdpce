import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { AvancementClasseFormService } from './avancement-classe-form.service';
import { AvancementClasseService } from '../service/avancement-classe.service';
import { IAvancementClasse } from '../avancement-classe.model';
import { IClasse } from 'app/entities/classe/classe.model';
import { ClasseService } from 'app/entities/classe/service/classe.service';
import { IEmploye } from 'app/entities/employe/employe.model';
import { EmployeService } from 'app/entities/employe/service/employe.service';

import { AvancementClasseUpdateComponent } from './avancement-classe-update.component';

describe('AvancementClasse Management Update Component', () => {
  let comp: AvancementClasseUpdateComponent;
  let fixture: ComponentFixture<AvancementClasseUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let avancementClasseFormService: AvancementClasseFormService;
  let avancementClasseService: AvancementClasseService;
  let classeService: ClasseService;
  let employeService: EmployeService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      declarations: [AvancementClasseUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(AvancementClasseUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(AvancementClasseUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    avancementClasseFormService = TestBed.inject(AvancementClasseFormService);
    avancementClasseService = TestBed.inject(AvancementClasseService);
    classeService = TestBed.inject(ClasseService);
    employeService = TestBed.inject(EmployeService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call Classe query and add missing value', () => {
      const avancementClasse: IAvancementClasse = { id: 456 };
      const classe: IClasse = { id: 80332 };
      avancementClasse.classe = classe;

      const classeCollection: IClasse[] = [{ id: 33059 }];
      jest.spyOn(classeService, 'query').mockReturnValue(of(new HttpResponse({ body: classeCollection })));
      const additionalClasses = [classe];
      const expectedCollection: IClasse[] = [...additionalClasses, ...classeCollection];
      jest.spyOn(classeService, 'addClasseToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ avancementClasse });
      comp.ngOnInit();

      expect(classeService.query).toHaveBeenCalled();
      expect(classeService.addClasseToCollectionIfMissing).toHaveBeenCalledWith(
        classeCollection,
        ...additionalClasses.map(expect.objectContaining)
      );
      expect(comp.classesSharedCollection).toEqual(expectedCollection);
    });

    it('Should call Employe query and add missing value', () => {
      const avancementClasse: IAvancementClasse = { id: 456 };
      const employe: IEmploye = { id: 85878 };
      avancementClasse.employe = employe;

      const employeCollection: IEmploye[] = [{ id: 77178 }];
      jest.spyOn(employeService, 'query').mockReturnValue(of(new HttpResponse({ body: employeCollection })));
      const additionalEmployes = [employe];
      const expectedCollection: IEmploye[] = [...additionalEmployes, ...employeCollection];
      jest.spyOn(employeService, 'addEmployeToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ avancementClasse });
      comp.ngOnInit();

      expect(employeService.query).toHaveBeenCalled();
      expect(employeService.addEmployeToCollectionIfMissing).toHaveBeenCalledWith(
        employeCollection,
        ...additionalEmployes.map(expect.objectContaining)
      );
      expect(comp.employesSharedCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const avancementClasse: IAvancementClasse = { id: 456 };
      const classe: IClasse = { id: 15763 };
      avancementClasse.classe = classe;
      const employe: IEmploye = { id: 30131 };
      avancementClasse.employe = employe;

      activatedRoute.data = of({ avancementClasse });
      comp.ngOnInit();

      expect(comp.classesSharedCollection).toContain(classe);
      expect(comp.employesSharedCollection).toContain(employe);
      expect(comp.avancementClasse).toEqual(avancementClasse);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IAvancementClasse>>();
      const avancementClasse = { id: 123 };
      jest.spyOn(avancementClasseFormService, 'getAvancementClasse').mockReturnValue(avancementClasse);
      jest.spyOn(avancementClasseService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ avancementClasse });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: avancementClasse }));
      saveSubject.complete();

      // THEN
      expect(avancementClasseFormService.getAvancementClasse).toHaveBeenCalled();
      expect(comp.previousState).toHaveBeenCalled();
      expect(avancementClasseService.update).toHaveBeenCalledWith(expect.objectContaining(avancementClasse));
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IAvancementClasse>>();
      const avancementClasse = { id: 123 };
      jest.spyOn(avancementClasseFormService, 'getAvancementClasse').mockReturnValue({ id: null });
      jest.spyOn(avancementClasseService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ avancementClasse: null });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: avancementClasse }));
      saveSubject.complete();

      // THEN
      expect(avancementClasseFormService.getAvancementClasse).toHaveBeenCalled();
      expect(avancementClasseService.create).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IAvancementClasse>>();
      const avancementClasse = { id: 123 };
      jest.spyOn(avancementClasseService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ avancementClasse });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(avancementClasseService.update).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Compare relationships', () => {
    describe('compareClasse', () => {
      it('Should forward to classeService', () => {
        const entity = { id: 123 };
        const entity2 = { id: 456 };
        jest.spyOn(classeService, 'compareClasse');
        comp.compareClasse(entity, entity2);
        expect(classeService.compareClasse).toHaveBeenCalledWith(entity, entity2);
      });
    });

    describe('compareEmploye', () => {
      it('Should forward to employeService', () => {
        const entity = { id: 123 };
        const entity2 = { id: 456 };
        jest.spyOn(employeService, 'compareEmploye');
        comp.compareEmploye(entity, entity2);
        expect(employeService.compareEmploye).toHaveBeenCalledWith(entity, entity2);
      });
    });
  });
});

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { AvancementClasseComponent } from '../list/avancement-classe.component';
import { AvancementClasseDetailComponent } from '../detail/avancement-classe-detail.component';
import { AvancementClasseUpdateComponent } from '../update/avancement-classe-update.component';
import { AvancementClasseRoutingResolveService } from './avancement-classe-routing-resolve.service';
import { ASC } from 'app/config/navigation.constants';

const avancementClasseRoute: Routes = [
  {
    path: '',
    component: AvancementClasseComponent,
    data: {
      defaultSort: 'id,' + ASC,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: AvancementClasseDetailComponent,
    resolve: {
      avancementClasse: AvancementClasseRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: AvancementClasseUpdateComponent,
    resolve: {
      avancementClasse: AvancementClasseRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: AvancementClasseUpdateComponent,
    resolve: {
      avancementClasse: AvancementClasseRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(avancementClasseRoute)],
  exports: [RouterModule],
})
export class AvancementClasseRoutingModule {}

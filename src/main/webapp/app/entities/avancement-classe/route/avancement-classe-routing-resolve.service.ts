import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IAvancementClasse } from '../avancement-classe.model';
import { AvancementClasseService } from '../service/avancement-classe.service';

@Injectable({ providedIn: 'root' })
export class AvancementClasseRoutingResolveService implements Resolve<IAvancementClasse | null> {
  constructor(protected service: AvancementClasseService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IAvancementClasse | null | never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((avancementClasse: HttpResponse<IAvancementClasse>) => {
          if (avancementClasse.body) {
            return of(avancementClasse.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(null);
  }
}

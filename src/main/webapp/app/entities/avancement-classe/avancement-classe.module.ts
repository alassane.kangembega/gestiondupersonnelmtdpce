import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { AvancementClasseComponent } from './list/avancement-classe.component';
import { AvancementClasseDetailComponent } from './detail/avancement-classe-detail.component';
import { AvancementClasseUpdateComponent } from './update/avancement-classe-update.component';
import { AvancementClasseDeleteDialogComponent } from './delete/avancement-classe-delete-dialog.component';
import { AvancementClasseRoutingModule } from './route/avancement-classe-routing.module';

@NgModule({
  imports: [SharedModule, AvancementClasseRoutingModule],
  declarations: [
    AvancementClasseComponent,
    AvancementClasseDetailComponent,
    AvancementClasseUpdateComponent,
    AvancementClasseDeleteDialogComponent,
  ],
})
export class AvancementClasseModule {}

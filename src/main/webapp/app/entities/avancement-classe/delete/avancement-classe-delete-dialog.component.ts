import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IAvancementClasse } from '../avancement-classe.model';
import { AvancementClasseService } from '../service/avancement-classe.service';
import { ITEM_DELETED_EVENT } from 'app/config/navigation.constants';

@Component({
  templateUrl: './avancement-classe-delete-dialog.component.html',
})
export class AvancementClasseDeleteDialogComponent {
  avancementClasse?: IAvancementClasse;

  constructor(protected avancementClasseService: AvancementClasseService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.avancementClasseService.delete(id).subscribe(() => {
      this.activeModal.close(ITEM_DELETED_EVENT);
    });
  }
}

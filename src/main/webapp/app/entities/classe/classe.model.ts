export interface IClasse {
  id: number;
  numero?: number | null;
}

export type NewClasse = Omit<IClasse, 'id'> & { id: null };

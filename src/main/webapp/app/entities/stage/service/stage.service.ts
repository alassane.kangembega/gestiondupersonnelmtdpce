import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import dayjs from 'dayjs/esm';

import { isPresent } from 'app/core/util/operators';
import { DATE_FORMAT } from 'app/config/input.constants';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IStage, NewStage } from '../stage.model';

export type PartialUpdateStage = Partial<IStage> & Pick<IStage, 'id'>;

type RestOf<T extends IStage | NewStage> = Omit<T, 'dateDebut' | 'dateFin'> & {
  dateDebut?: string | null;
  dateFin?: string | null;
};

export type RestStage = RestOf<IStage>;

export type NewRestStage = RestOf<NewStage>;

export type PartialUpdateRestStage = RestOf<PartialUpdateStage>;

export type EntityResponseType = HttpResponse<IStage>;
export type EntityArrayResponseType = HttpResponse<IStage[]>;

@Injectable({ providedIn: 'root' })
export class StageService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/stages');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(stage: NewStage): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(stage);
    return this.http.post<RestStage>(this.resourceUrl, copy, { observe: 'response' }).pipe(map(res => this.convertResponseFromServer(res)));
  }

  update(stage: IStage): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(stage);
    return this.http
      .put<RestStage>(`${this.resourceUrl}/${this.getStageIdentifier(stage)}`, copy, { observe: 'response' })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  partialUpdate(stage: PartialUpdateStage): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(stage);
    return this.http
      .patch<RestStage>(`${this.resourceUrl}/${this.getStageIdentifier(stage)}`, copy, { observe: 'response' })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<RestStage>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<RestStage[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map(res => this.convertResponseArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  getStageIdentifier(stage: Pick<IStage, 'id'>): number {
    return stage.id;
  }

  compareStage(o1: Pick<IStage, 'id'> | null, o2: Pick<IStage, 'id'> | null): boolean {
    return o1 && o2 ? this.getStageIdentifier(o1) === this.getStageIdentifier(o2) : o1 === o2;
  }

  addStageToCollectionIfMissing<Type extends Pick<IStage, 'id'>>(
    stageCollection: Type[],
    ...stagesToCheck: (Type | null | undefined)[]
  ): Type[] {
    const stages: Type[] = stagesToCheck.filter(isPresent);
    if (stages.length > 0) {
      const stageCollectionIdentifiers = stageCollection.map(stageItem => this.getStageIdentifier(stageItem)!);
      const stagesToAdd = stages.filter(stageItem => {
        const stageIdentifier = this.getStageIdentifier(stageItem);
        if (stageCollectionIdentifiers.includes(stageIdentifier)) {
          return false;
        }
        stageCollectionIdentifiers.push(stageIdentifier);
        return true;
      });
      return [...stagesToAdd, ...stageCollection];
    }
    return stageCollection;
  }

  protected convertDateFromClient<T extends IStage | NewStage | PartialUpdateStage>(stage: T): RestOf<T> {
    return {
      ...stage,
      dateDebut: stage.dateDebut?.format(DATE_FORMAT) ?? null,
      dateFin: stage.dateFin?.format(DATE_FORMAT) ?? null,
    };
  }

  protected convertDateFromServer(restStage: RestStage): IStage {
    return {
      ...restStage,
      dateDebut: restStage.dateDebut ? dayjs(restStage.dateDebut) : undefined,
      dateFin: restStage.dateFin ? dayjs(restStage.dateFin) : undefined,
    };
  }

  protected convertResponseFromServer(res: HttpResponse<RestStage>): HttpResponse<IStage> {
    return res.clone({
      body: res.body ? this.convertDateFromServer(res.body) : null,
    });
  }

  protected convertResponseArrayFromServer(res: HttpResponse<RestStage[]>): HttpResponse<IStage[]> {
    return res.clone({
      body: res.body ? res.body.map(item => this.convertDateFromServer(item)) : null,
    });
  }
}

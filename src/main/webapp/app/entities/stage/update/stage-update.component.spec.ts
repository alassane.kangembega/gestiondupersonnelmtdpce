import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { StageFormService } from './stage-form.service';
import { StageService } from '../service/stage.service';
import { IStage } from '../stage.model';
import { INatureStage } from 'app/entities/nature-stage/nature-stage.model';
import { NatureStageService } from 'app/entities/nature-stage/service/nature-stage.service';
import { IEmploye } from 'app/entities/employe/employe.model';
import { EmployeService } from 'app/entities/employe/service/employe.service';

import { StageUpdateComponent } from './stage-update.component';

describe('Stage Management Update Component', () => {
  let comp: StageUpdateComponent;
  let fixture: ComponentFixture<StageUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let stageFormService: StageFormService;
  let stageService: StageService;
  let natureStageService: NatureStageService;
  let employeService: EmployeService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      declarations: [StageUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(StageUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(StageUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    stageFormService = TestBed.inject(StageFormService);
    stageService = TestBed.inject(StageService);
    natureStageService = TestBed.inject(NatureStageService);
    employeService = TestBed.inject(EmployeService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call NatureStage query and add missing value', () => {
      const stage: IStage = { id: 456 };
      const natureStage: INatureStage = { id: 53296 };
      stage.natureStage = natureStage;

      const natureStageCollection: INatureStage[] = [{ id: 61617 }];
      jest.spyOn(natureStageService, 'query').mockReturnValue(of(new HttpResponse({ body: natureStageCollection })));
      const additionalNatureStages = [natureStage];
      const expectedCollection: INatureStage[] = [...additionalNatureStages, ...natureStageCollection];
      jest.spyOn(natureStageService, 'addNatureStageToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ stage });
      comp.ngOnInit();

      expect(natureStageService.query).toHaveBeenCalled();
      expect(natureStageService.addNatureStageToCollectionIfMissing).toHaveBeenCalledWith(
        natureStageCollection,
        ...additionalNatureStages.map(expect.objectContaining)
      );
      expect(comp.natureStagesSharedCollection).toEqual(expectedCollection);
    });

    it('Should call Employe query and add missing value', () => {
      const stage: IStage = { id: 456 };
      const employe: IEmploye = { id: 40249 };
      stage.employe = employe;

      const employeCollection: IEmploye[] = [{ id: 11122 }];
      jest.spyOn(employeService, 'query').mockReturnValue(of(new HttpResponse({ body: employeCollection })));
      const additionalEmployes = [employe];
      const expectedCollection: IEmploye[] = [...additionalEmployes, ...employeCollection];
      jest.spyOn(employeService, 'addEmployeToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ stage });
      comp.ngOnInit();

      expect(employeService.query).toHaveBeenCalled();
      expect(employeService.addEmployeToCollectionIfMissing).toHaveBeenCalledWith(
        employeCollection,
        ...additionalEmployes.map(expect.objectContaining)
      );
      expect(comp.employesSharedCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const stage: IStage = { id: 456 };
      const natureStage: INatureStage = { id: 43326 };
      stage.natureStage = natureStage;
      const employe: IEmploye = { id: 38115 };
      stage.employe = employe;

      activatedRoute.data = of({ stage });
      comp.ngOnInit();

      expect(comp.natureStagesSharedCollection).toContain(natureStage);
      expect(comp.employesSharedCollection).toContain(employe);
      expect(comp.stage).toEqual(stage);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IStage>>();
      const stage = { id: 123 };
      jest.spyOn(stageFormService, 'getStage').mockReturnValue(stage);
      jest.spyOn(stageService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ stage });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: stage }));
      saveSubject.complete();

      // THEN
      expect(stageFormService.getStage).toHaveBeenCalled();
      expect(comp.previousState).toHaveBeenCalled();
      expect(stageService.update).toHaveBeenCalledWith(expect.objectContaining(stage));
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IStage>>();
      const stage = { id: 123 };
      jest.spyOn(stageFormService, 'getStage').mockReturnValue({ id: null });
      jest.spyOn(stageService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ stage: null });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: stage }));
      saveSubject.complete();

      // THEN
      expect(stageFormService.getStage).toHaveBeenCalled();
      expect(stageService.create).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IStage>>();
      const stage = { id: 123 };
      jest.spyOn(stageService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ stage });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(stageService.update).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Compare relationships', () => {
    describe('compareNatureStage', () => {
      it('Should forward to natureStageService', () => {
        const entity = { id: 123 };
        const entity2 = { id: 456 };
        jest.spyOn(natureStageService, 'compareNatureStage');
        comp.compareNatureStage(entity, entity2);
        expect(natureStageService.compareNatureStage).toHaveBeenCalledWith(entity, entity2);
      });
    });

    describe('compareEmploye', () => {
      it('Should forward to employeService', () => {
        const entity = { id: 123 };
        const entity2 = { id: 456 };
        jest.spyOn(employeService, 'compareEmploye');
        comp.compareEmploye(entity, entity2);
        expect(employeService.compareEmploye).toHaveBeenCalledWith(entity, entity2);
      });
    });
  });
});

import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { StageFormService, StageFormGroup } from './stage-form.service';
import { IStage } from '../stage.model';
import { StageService } from '../service/stage.service';
import { INatureStage } from 'app/entities/nature-stage/nature-stage.model';
import { NatureStageService } from 'app/entities/nature-stage/service/nature-stage.service';
import { IEmploye } from 'app/entities/employe/employe.model';
import { EmployeService } from 'app/entities/employe/service/employe.service';

@Component({
  selector: 'jhi-stage-update',
  templateUrl: './stage-update.component.html',
})
export class StageUpdateComponent implements OnInit {
  isSaving = false;
  stage: IStage | null = null;

  natureStagesSharedCollection: INatureStage[] = [];
  employesSharedCollection: IEmploye[] = [];

  editForm: StageFormGroup = this.stageFormService.createStageFormGroup();

  constructor(
    protected stageService: StageService,
    protected stageFormService: StageFormService,
    protected natureStageService: NatureStageService,
    protected employeService: EmployeService,
    protected activatedRoute: ActivatedRoute
  ) {}

  compareNatureStage = (o1: INatureStage | null, o2: INatureStage | null): boolean => this.natureStageService.compareNatureStage(o1, o2);

  compareEmploye = (o1: IEmploye | null, o2: IEmploye | null): boolean => this.employeService.compareEmploye(o1, o2);

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ stage }) => {
      this.stage = stage;
      if (stage) {
        this.updateForm(stage);
      }

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const stage = this.stageFormService.getStage(this.editForm);
    if (stage.id !== null) {
      this.subscribeToSaveResponse(this.stageService.update(stage));
    } else {
      this.subscribeToSaveResponse(this.stageService.create(stage));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IStage>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(stage: IStage): void {
    this.stage = stage;
    this.stageFormService.resetForm(this.editForm, stage);

    this.natureStagesSharedCollection = this.natureStageService.addNatureStageToCollectionIfMissing<INatureStage>(
      this.natureStagesSharedCollection,
      stage.natureStage
    );
    this.employesSharedCollection = this.employeService.addEmployeToCollectionIfMissing<IEmploye>(
      this.employesSharedCollection,
      stage.employe
    );
  }

  protected loadRelationshipsOptions(): void {
    this.natureStageService
      .query()
      .pipe(map((res: HttpResponse<INatureStage[]>) => res.body ?? []))
      .pipe(
        map((natureStages: INatureStage[]) =>
          this.natureStageService.addNatureStageToCollectionIfMissing<INatureStage>(natureStages, this.stage?.natureStage)
        )
      )
      .subscribe((natureStages: INatureStage[]) => (this.natureStagesSharedCollection = natureStages));

    this.employeService
      .query()
      .pipe(map((res: HttpResponse<IEmploye[]>) => res.body ?? []))
      .pipe(map((employes: IEmploye[]) => this.employeService.addEmployeToCollectionIfMissing<IEmploye>(employes, this.stage?.employe)))
      .subscribe((employes: IEmploye[]) => (this.employesSharedCollection = employes));
  }
}

import dayjs from 'dayjs/esm';
import { INatureStage } from 'app/entities/nature-stage/nature-stage.model';
import { IEmploye } from 'app/entities/employe/employe.model';

export interface IStage {
  id: number;
  dateDebut?: dayjs.Dayjs | null;
  dateFin?: dayjs.Dayjs | null;
  natureStage?: Pick<INatureStage, 'id'> | null;
  employe?: Pick<IEmploye, 'id'> | null;
}

export type NewStage = Omit<IStage, 'id'> & { id: null };

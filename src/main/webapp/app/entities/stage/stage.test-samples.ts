import dayjs from 'dayjs/esm';

import { IStage, NewStage } from './stage.model';

export const sampleWithRequiredData: IStage = {
  id: 86802,
};

export const sampleWithPartialData: IStage = {
  id: 53943,
};

export const sampleWithFullData: IStage = {
  id: 96589,
  dateDebut: dayjs('2022-09-06'),
  dateFin: dayjs('2022-09-07'),
};

export const sampleWithNewData: NewStage = {
  id: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);

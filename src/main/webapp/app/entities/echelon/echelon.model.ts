export interface IEchelon {
  id: number;
  numero?: number | null;
}

export type NewEchelon = Omit<IEchelon, 'id'> & { id: null };

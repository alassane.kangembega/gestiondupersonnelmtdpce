import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

import { EchelonFormService, EchelonFormGroup } from './echelon-form.service';
import { IEchelon } from '../echelon.model';
import { EchelonService } from '../service/echelon.service';

@Component({
  selector: 'jhi-echelon-update',
  templateUrl: './echelon-update.component.html',
})
export class EchelonUpdateComponent implements OnInit {
  isSaving = false;
  echelon: IEchelon | null = null;

  editForm: EchelonFormGroup = this.echelonFormService.createEchelonFormGroup();

  constructor(
    protected echelonService: EchelonService,
    protected echelonFormService: EchelonFormService,
    protected activatedRoute: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ echelon }) => {
      this.echelon = echelon;
      if (echelon) {
        this.updateForm(echelon);
      }
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const echelon = this.echelonFormService.getEchelon(this.editForm);
    if (echelon.id !== null) {
      this.subscribeToSaveResponse(this.echelonService.update(echelon));
    } else {
      this.subscribeToSaveResponse(this.echelonService.create(echelon));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IEchelon>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(echelon: IEchelon): void {
    this.echelon = echelon;
    this.echelonFormService.resetForm(this.editForm, echelon);
  }
}

import { TestBed } from '@angular/core/testing';

import { sampleWithRequiredData, sampleWithNewData } from '../echelon.test-samples';

import { EchelonFormService } from './echelon-form.service';

describe('Echelon Form Service', () => {
  let service: EchelonFormService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EchelonFormService);
  });

  describe('Service methods', () => {
    describe('createEchelonFormGroup', () => {
      it('should create a new form with FormControl', () => {
        const formGroup = service.createEchelonFormGroup();

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            numero: expect.any(Object),
          })
        );
      });

      it('passing IEchelon should create a new form with FormGroup', () => {
        const formGroup = service.createEchelonFormGroup(sampleWithRequiredData);

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            numero: expect.any(Object),
          })
        );
      });
    });

    describe('getEchelon', () => {
      it('should return NewEchelon for default Echelon initial value', () => {
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        const formGroup = service.createEchelonFormGroup(sampleWithNewData);

        const echelon = service.getEchelon(formGroup) as any;

        expect(echelon).toMatchObject(sampleWithNewData);
      });

      it('should return NewEchelon for empty Echelon initial value', () => {
        const formGroup = service.createEchelonFormGroup();

        const echelon = service.getEchelon(formGroup) as any;

        expect(echelon).toMatchObject({});
      });

      it('should return IEchelon', () => {
        const formGroup = service.createEchelonFormGroup(sampleWithRequiredData);

        const echelon = service.getEchelon(formGroup) as any;

        expect(echelon).toMatchObject(sampleWithRequiredData);
      });
    });

    describe('resetForm', () => {
      it('passing IEchelon should not enable id FormControl', () => {
        const formGroup = service.createEchelonFormGroup();
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, sampleWithRequiredData);

        expect(formGroup.controls.id.disabled).toBe(true);
      });

      it('passing NewEchelon should disable id FormControl', () => {
        const formGroup = service.createEchelonFormGroup(sampleWithRequiredData);
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, { id: null });

        expect(formGroup.controls.id.disabled).toBe(true);
      });
    });
  });
});

import { Injectable } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { IEchelon, NewEchelon } from '../echelon.model';

/**
 * A partial Type with required key is used as form input.
 */
type PartialWithRequiredKeyOf<T extends { id: unknown }> = Partial<Omit<T, 'id'>> & { id: T['id'] };

/**
 * Type for createFormGroup and resetForm argument.
 * It accepts IEchelon for edit and NewEchelonFormGroupInput for create.
 */
type EchelonFormGroupInput = IEchelon | PartialWithRequiredKeyOf<NewEchelon>;

type EchelonFormDefaults = Pick<NewEchelon, 'id'>;

type EchelonFormGroupContent = {
  id: FormControl<IEchelon['id'] | NewEchelon['id']>;
  numero: FormControl<IEchelon['numero']>;
};

export type EchelonFormGroup = FormGroup<EchelonFormGroupContent>;

@Injectable({ providedIn: 'root' })
export class EchelonFormService {
  createEchelonFormGroup(echelon: EchelonFormGroupInput = { id: null }): EchelonFormGroup {
    const echelonRawValue = {
      ...this.getFormDefaults(),
      ...echelon,
    };
    return new FormGroup<EchelonFormGroupContent>({
      id: new FormControl(
        { value: echelonRawValue.id, disabled: true },
        {
          nonNullable: true,
          validators: [Validators.required],
        }
      ),
      numero: new FormControl(echelonRawValue.numero),
    });
  }

  getEchelon(form: EchelonFormGroup): IEchelon | NewEchelon {
    return form.getRawValue() as IEchelon | NewEchelon;
  }

  resetForm(form: EchelonFormGroup, echelon: EchelonFormGroupInput): void {
    const echelonRawValue = { ...this.getFormDefaults(), ...echelon };
    form.reset(
      {
        ...echelonRawValue,
        id: { value: echelonRawValue.id, disabled: true },
      } as any /* cast to workaround https://github.com/angular/angular/issues/46458 */
    );
  }

  private getFormDefaults(): EchelonFormDefaults {
    return {
      id: null,
    };
  }
}

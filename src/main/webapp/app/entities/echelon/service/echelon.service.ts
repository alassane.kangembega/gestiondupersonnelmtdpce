import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IEchelon, NewEchelon } from '../echelon.model';

export type PartialUpdateEchelon = Partial<IEchelon> & Pick<IEchelon, 'id'>;

export type EntityResponseType = HttpResponse<IEchelon>;
export type EntityArrayResponseType = HttpResponse<IEchelon[]>;

@Injectable({ providedIn: 'root' })
export class EchelonService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/echelons');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(echelon: NewEchelon): Observable<EntityResponseType> {
    return this.http.post<IEchelon>(this.resourceUrl, echelon, { observe: 'response' });
  }

  update(echelon: IEchelon): Observable<EntityResponseType> {
    return this.http.put<IEchelon>(`${this.resourceUrl}/${this.getEchelonIdentifier(echelon)}`, echelon, { observe: 'response' });
  }

  partialUpdate(echelon: PartialUpdateEchelon): Observable<EntityResponseType> {
    return this.http.patch<IEchelon>(`${this.resourceUrl}/${this.getEchelonIdentifier(echelon)}`, echelon, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IEchelon>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IEchelon[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  getEchelonIdentifier(echelon: Pick<IEchelon, 'id'>): number {
    return echelon.id;
  }

  compareEchelon(o1: Pick<IEchelon, 'id'> | null, o2: Pick<IEchelon, 'id'> | null): boolean {
    return o1 && o2 ? this.getEchelonIdentifier(o1) === this.getEchelonIdentifier(o2) : o1 === o2;
  }

  addEchelonToCollectionIfMissing<Type extends Pick<IEchelon, 'id'>>(
    echelonCollection: Type[],
    ...echelonsToCheck: (Type | null | undefined)[]
  ): Type[] {
    const echelons: Type[] = echelonsToCheck.filter(isPresent);
    if (echelons.length > 0) {
      const echelonCollectionIdentifiers = echelonCollection.map(echelonItem => this.getEchelonIdentifier(echelonItem)!);
      const echelonsToAdd = echelons.filter(echelonItem => {
        const echelonIdentifier = this.getEchelonIdentifier(echelonItem);
        if (echelonCollectionIdentifiers.includes(echelonIdentifier)) {
          return false;
        }
        echelonCollectionIdentifiers.push(echelonIdentifier);
        return true;
      });
      return [...echelonsToAdd, ...echelonCollection];
    }
    return echelonCollection;
  }
}

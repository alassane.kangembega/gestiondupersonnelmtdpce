import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { IEchelon } from '../echelon.model';
import { sampleWithRequiredData, sampleWithNewData, sampleWithPartialData, sampleWithFullData } from '../echelon.test-samples';

import { EchelonService } from './echelon.service';

const requireRestSample: IEchelon = {
  ...sampleWithRequiredData,
};

describe('Echelon Service', () => {
  let service: EchelonService;
  let httpMock: HttpTestingController;
  let expectedResult: IEchelon | IEchelon[] | boolean | null;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(EchelonService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should create a Echelon', () => {
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      const echelon = { ...sampleWithNewData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.create(echelon).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a Echelon', () => {
      const echelon = { ...sampleWithRequiredData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.update(echelon).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a Echelon', () => {
      const patchObject = { ...sampleWithPartialData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of Echelon', () => {
      const returnedFromService = { ...requireRestSample };

      const expected = { ...sampleWithRequiredData };

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toMatchObject([expected]);
    });

    it('should delete a Echelon', () => {
      const expected = true;

      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult).toBe(expected);
    });

    describe('addEchelonToCollectionIfMissing', () => {
      it('should add a Echelon to an empty array', () => {
        const echelon: IEchelon = sampleWithRequiredData;
        expectedResult = service.addEchelonToCollectionIfMissing([], echelon);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(echelon);
      });

      it('should not add a Echelon to an array that contains it', () => {
        const echelon: IEchelon = sampleWithRequiredData;
        const echelonCollection: IEchelon[] = [
          {
            ...echelon,
          },
          sampleWithPartialData,
        ];
        expectedResult = service.addEchelonToCollectionIfMissing(echelonCollection, echelon);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a Echelon to an array that doesn't contain it", () => {
        const echelon: IEchelon = sampleWithRequiredData;
        const echelonCollection: IEchelon[] = [sampleWithPartialData];
        expectedResult = service.addEchelonToCollectionIfMissing(echelonCollection, echelon);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(echelon);
      });

      it('should add only unique Echelon to an array', () => {
        const echelonArray: IEchelon[] = [sampleWithRequiredData, sampleWithPartialData, sampleWithFullData];
        const echelonCollection: IEchelon[] = [sampleWithRequiredData];
        expectedResult = service.addEchelonToCollectionIfMissing(echelonCollection, ...echelonArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const echelon: IEchelon = sampleWithRequiredData;
        const echelon2: IEchelon = sampleWithPartialData;
        expectedResult = service.addEchelonToCollectionIfMissing([], echelon, echelon2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(echelon);
        expect(expectedResult).toContain(echelon2);
      });

      it('should accept null and undefined values', () => {
        const echelon: IEchelon = sampleWithRequiredData;
        expectedResult = service.addEchelonToCollectionIfMissing([], null, echelon, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(echelon);
      });

      it('should return initial array if no Echelon is added', () => {
        const echelonCollection: IEchelon[] = [sampleWithRequiredData];
        expectedResult = service.addEchelonToCollectionIfMissing(echelonCollection, undefined, null);
        expect(expectedResult).toEqual(echelonCollection);
      });
    });

    describe('compareEchelon', () => {
      it('Should return true if both entities are null', () => {
        const entity1 = null;
        const entity2 = null;

        const compareResult = service.compareEchelon(entity1, entity2);

        expect(compareResult).toEqual(true);
      });

      it('Should return false if one entity is null', () => {
        const entity1 = { id: 123 };
        const entity2 = null;

        const compareResult1 = service.compareEchelon(entity1, entity2);
        const compareResult2 = service.compareEchelon(entity2, entity1);

        expect(compareResult1).toEqual(false);
        expect(compareResult2).toEqual(false);
      });

      it('Should return false if primaryKey differs', () => {
        const entity1 = { id: 123 };
        const entity2 = { id: 456 };

        const compareResult1 = service.compareEchelon(entity1, entity2);
        const compareResult2 = service.compareEchelon(entity2, entity1);

        expect(compareResult1).toEqual(false);
        expect(compareResult2).toEqual(false);
      });

      it('Should return false if primaryKey matches', () => {
        const entity1 = { id: 123 };
        const entity2 = { id: 123 };

        const compareResult1 = service.compareEchelon(entity1, entity2);
        const compareResult2 = service.compareEchelon(entity2, entity1);

        expect(compareResult1).toEqual(true);
        expect(compareResult2).toEqual(true);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});

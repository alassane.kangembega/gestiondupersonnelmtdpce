import { IEchelon, NewEchelon } from './echelon.model';

export const sampleWithRequiredData: IEchelon = {
  id: 19832,
};

export const sampleWithPartialData: IEchelon = {
  id: 32829,
  numero: 11150,
};

export const sampleWithFullData: IEchelon = {
  id: 18822,
  numero: 81375,
};

export const sampleWithNewData: NewEchelon = {
  id: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);

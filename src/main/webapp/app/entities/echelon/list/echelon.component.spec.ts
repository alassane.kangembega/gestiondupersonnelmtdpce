import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';

import { EchelonService } from '../service/echelon.service';

import { EchelonComponent } from './echelon.component';

describe('Echelon Management Component', () => {
  let comp: EchelonComponent;
  let fixture: ComponentFixture<EchelonComponent>;
  let service: EchelonService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule.withRoutes([{ path: 'echelon', component: EchelonComponent }]), HttpClientTestingModule],
      declarations: [EchelonComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            data: of({
              defaultSort: 'id,asc',
            }),
            queryParamMap: of(
              jest.requireActual('@angular/router').convertToParamMap({
                page: '1',
                size: '1',
                sort: 'id,desc',
              })
            ),
            snapshot: { queryParams: {} },
          },
        },
      ],
    })
      .overrideTemplate(EchelonComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(EchelonComponent);
    comp = fixture.componentInstance;
    service = TestBed.inject(EchelonService);

    const headers = new HttpHeaders();
    jest.spyOn(service, 'query').mockReturnValue(
      of(
        new HttpResponse({
          body: [{ id: 123 }],
          headers,
        })
      )
    );
  });

  it('Should call load all on init', () => {
    // WHEN
    comp.ngOnInit();

    // THEN
    expect(service.query).toHaveBeenCalled();
    expect(comp.echelons?.[0]).toEqual(expect.objectContaining({ id: 123 }));
  });

  describe('trackId', () => {
    it('Should forward to echelonService', () => {
      const entity = { id: 123 };
      jest.spyOn(service, 'getEchelonIdentifier');
      const id = comp.trackId(0, entity);
      expect(service.getEchelonIdentifier).toHaveBeenCalledWith(entity);
      expect(id).toBe(entity.id);
    });
  });
});

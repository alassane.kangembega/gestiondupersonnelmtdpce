import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import dayjs from 'dayjs/esm';

import { isPresent } from 'app/core/util/operators';
import { DATE_FORMAT } from 'app/config/input.constants';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IEntree, NewEntree } from '../entree.model';

export type PartialUpdateEntree = Partial<IEntree> & Pick<IEntree, 'id'>;

type RestOf<T extends IEntree | NewEntree> = Omit<T, 'dateEntree'> & {
  dateEntree?: string | null;
};

export type RestEntree = RestOf<IEntree>;

export type NewRestEntree = RestOf<NewEntree>;

export type PartialUpdateRestEntree = RestOf<PartialUpdateEntree>;

export type EntityResponseType = HttpResponse<IEntree>;
export type EntityArrayResponseType = HttpResponse<IEntree[]>;

@Injectable({ providedIn: 'root' })
export class EntreeService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/entrees');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(entree: NewEntree): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(entree);
    return this.http
      .post<RestEntree>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  update(entree: IEntree): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(entree);
    return this.http
      .put<RestEntree>(`${this.resourceUrl}/${this.getEntreeIdentifier(entree)}`, copy, { observe: 'response' })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  partialUpdate(entree: PartialUpdateEntree): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(entree);
    return this.http
      .patch<RestEntree>(`${this.resourceUrl}/${this.getEntreeIdentifier(entree)}`, copy, { observe: 'response' })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<RestEntree>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<RestEntree[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map(res => this.convertResponseArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  getEntreeIdentifier(entree: Pick<IEntree, 'id'>): number {
    return entree.id;
  }

  compareEntree(o1: Pick<IEntree, 'id'> | null, o2: Pick<IEntree, 'id'> | null): boolean {
    return o1 && o2 ? this.getEntreeIdentifier(o1) === this.getEntreeIdentifier(o2) : o1 === o2;
  }

  addEntreeToCollectionIfMissing<Type extends Pick<IEntree, 'id'>>(
    entreeCollection: Type[],
    ...entreesToCheck: (Type | null | undefined)[]
  ): Type[] {
    const entrees: Type[] = entreesToCheck.filter(isPresent);
    if (entrees.length > 0) {
      const entreeCollectionIdentifiers = entreeCollection.map(entreeItem => this.getEntreeIdentifier(entreeItem)!);
      const entreesToAdd = entrees.filter(entreeItem => {
        const entreeIdentifier = this.getEntreeIdentifier(entreeItem);
        if (entreeCollectionIdentifiers.includes(entreeIdentifier)) {
          return false;
        }
        entreeCollectionIdentifiers.push(entreeIdentifier);
        return true;
      });
      return [...entreesToAdd, ...entreeCollection];
    }
    return entreeCollection;
  }

  protected convertDateFromClient<T extends IEntree | NewEntree | PartialUpdateEntree>(entree: T): RestOf<T> {
    return {
      ...entree,
      dateEntree: entree.dateEntree?.format(DATE_FORMAT) ?? null,
    };
  }

  protected convertDateFromServer(restEntree: RestEntree): IEntree {
    return {
      ...restEntree,
      dateEntree: restEntree.dateEntree ? dayjs(restEntree.dateEntree) : undefined,
    };
  }

  protected convertResponseFromServer(res: HttpResponse<RestEntree>): HttpResponse<IEntree> {
    return res.clone({
      body: res.body ? this.convertDateFromServer(res.body) : null,
    });
  }

  protected convertResponseArrayFromServer(res: HttpResponse<RestEntree[]>): HttpResponse<IEntree[]> {
    return res.clone({
      body: res.body ? res.body.map(item => this.convertDateFromServer(item)) : null,
    });
  }
}

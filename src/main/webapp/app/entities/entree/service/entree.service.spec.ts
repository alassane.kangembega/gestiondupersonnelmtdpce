import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { DATE_FORMAT } from 'app/config/input.constants';
import { IEntree } from '../entree.model';
import { sampleWithRequiredData, sampleWithNewData, sampleWithPartialData, sampleWithFullData } from '../entree.test-samples';

import { EntreeService, RestEntree } from './entree.service';

const requireRestSample: RestEntree = {
  ...sampleWithRequiredData,
  dateEntree: sampleWithRequiredData.dateEntree?.format(DATE_FORMAT),
};

describe('Entree Service', () => {
  let service: EntreeService;
  let httpMock: HttpTestingController;
  let expectedResult: IEntree | IEntree[] | boolean | null;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(EntreeService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should create a Entree', () => {
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      const entree = { ...sampleWithNewData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.create(entree).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a Entree', () => {
      const entree = { ...sampleWithRequiredData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.update(entree).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a Entree', () => {
      const patchObject = { ...sampleWithPartialData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of Entree', () => {
      const returnedFromService = { ...requireRestSample };

      const expected = { ...sampleWithRequiredData };

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toMatchObject([expected]);
    });

    it('should delete a Entree', () => {
      const expected = true;

      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult).toBe(expected);
    });

    describe('addEntreeToCollectionIfMissing', () => {
      it('should add a Entree to an empty array', () => {
        const entree: IEntree = sampleWithRequiredData;
        expectedResult = service.addEntreeToCollectionIfMissing([], entree);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(entree);
      });

      it('should not add a Entree to an array that contains it', () => {
        const entree: IEntree = sampleWithRequiredData;
        const entreeCollection: IEntree[] = [
          {
            ...entree,
          },
          sampleWithPartialData,
        ];
        expectedResult = service.addEntreeToCollectionIfMissing(entreeCollection, entree);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a Entree to an array that doesn't contain it", () => {
        const entree: IEntree = sampleWithRequiredData;
        const entreeCollection: IEntree[] = [sampleWithPartialData];
        expectedResult = service.addEntreeToCollectionIfMissing(entreeCollection, entree);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(entree);
      });

      it('should add only unique Entree to an array', () => {
        const entreeArray: IEntree[] = [sampleWithRequiredData, sampleWithPartialData, sampleWithFullData];
        const entreeCollection: IEntree[] = [sampleWithRequiredData];
        expectedResult = service.addEntreeToCollectionIfMissing(entreeCollection, ...entreeArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const entree: IEntree = sampleWithRequiredData;
        const entree2: IEntree = sampleWithPartialData;
        expectedResult = service.addEntreeToCollectionIfMissing([], entree, entree2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(entree);
        expect(expectedResult).toContain(entree2);
      });

      it('should accept null and undefined values', () => {
        const entree: IEntree = sampleWithRequiredData;
        expectedResult = service.addEntreeToCollectionIfMissing([], null, entree, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(entree);
      });

      it('should return initial array if no Entree is added', () => {
        const entreeCollection: IEntree[] = [sampleWithRequiredData];
        expectedResult = service.addEntreeToCollectionIfMissing(entreeCollection, undefined, null);
        expect(expectedResult).toEqual(entreeCollection);
      });
    });

    describe('compareEntree', () => {
      it('Should return true if both entities are null', () => {
        const entity1 = null;
        const entity2 = null;

        const compareResult = service.compareEntree(entity1, entity2);

        expect(compareResult).toEqual(true);
      });

      it('Should return false if one entity is null', () => {
        const entity1 = { id: 123 };
        const entity2 = null;

        const compareResult1 = service.compareEntree(entity1, entity2);
        const compareResult2 = service.compareEntree(entity2, entity1);

        expect(compareResult1).toEqual(false);
        expect(compareResult2).toEqual(false);
      });

      it('Should return false if primaryKey differs', () => {
        const entity1 = { id: 123 };
        const entity2 = { id: 456 };

        const compareResult1 = service.compareEntree(entity1, entity2);
        const compareResult2 = service.compareEntree(entity2, entity1);

        expect(compareResult1).toEqual(false);
        expect(compareResult2).toEqual(false);
      });

      it('Should return false if primaryKey matches', () => {
        const entity1 = { id: 123 };
        const entity2 = { id: 123 };

        const compareResult1 = service.compareEntree(entity1, entity2);
        const compareResult2 = service.compareEntree(entity2, entity1);

        expect(compareResult1).toEqual(true);
        expect(compareResult2).toEqual(true);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});

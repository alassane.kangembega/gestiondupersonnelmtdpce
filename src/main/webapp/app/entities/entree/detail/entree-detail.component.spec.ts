import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { EntreeDetailComponent } from './entree-detail.component';

describe('Entree Management Detail Component', () => {
  let comp: EntreeDetailComponent;
  let fixture: ComponentFixture<EntreeDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [EntreeDetailComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: { data: of({ entree: { id: 123 } }) },
        },
      ],
    })
      .overrideTemplate(EntreeDetailComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(EntreeDetailComponent);
    comp = fixture.componentInstance;
  });

  describe('OnInit', () => {
    it('Should load entree on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(comp.entree).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});

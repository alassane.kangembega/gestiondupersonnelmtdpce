import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IEntree } from '../entree.model';

@Component({
  selector: 'jhi-entree-detail',
  templateUrl: './entree-detail.component.html',
})
export class EntreeDetailComponent implements OnInit {
  entree: IEntree | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ entree }) => {
      this.entree = entree;
    });
  }

  previousState(): void {
    window.history.back();
  }
}

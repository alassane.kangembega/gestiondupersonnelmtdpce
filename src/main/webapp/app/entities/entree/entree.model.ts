import dayjs from 'dayjs/esm';
import { IEmploye } from 'app/entities/employe/employe.model';
import { INatureEntree } from 'app/entities/nature-entree/nature-entree.model';

export interface IEntree {
  id: number;
  dateEntree?: dayjs.Dayjs | null;
  employe?: Pick<IEmploye, 'id'> | null;
  natureEntree?: Pick<INatureEntree, 'id'> | null;
}

export type NewEntree = Omit<IEntree, 'id'> & { id: null };

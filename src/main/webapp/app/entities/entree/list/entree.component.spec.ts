import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';

import { EntreeService } from '../service/entree.service';

import { EntreeComponent } from './entree.component';

describe('Entree Management Component', () => {
  let comp: EntreeComponent;
  let fixture: ComponentFixture<EntreeComponent>;
  let service: EntreeService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule.withRoutes([{ path: 'entree', component: EntreeComponent }]), HttpClientTestingModule],
      declarations: [EntreeComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            data: of({
              defaultSort: 'id,asc',
            }),
            queryParamMap: of(
              jest.requireActual('@angular/router').convertToParamMap({
                page: '1',
                size: '1',
                sort: 'id,desc',
              })
            ),
            snapshot: { queryParams: {} },
          },
        },
      ],
    })
      .overrideTemplate(EntreeComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(EntreeComponent);
    comp = fixture.componentInstance;
    service = TestBed.inject(EntreeService);

    const headers = new HttpHeaders();
    jest.spyOn(service, 'query').mockReturnValue(
      of(
        new HttpResponse({
          body: [{ id: 123 }],
          headers,
        })
      )
    );
  });

  it('Should call load all on init', () => {
    // WHEN
    comp.ngOnInit();

    // THEN
    expect(service.query).toHaveBeenCalled();
    expect(comp.entrees?.[0]).toEqual(expect.objectContaining({ id: 123 }));
  });

  describe('trackId', () => {
    it('Should forward to entreeService', () => {
      const entity = { id: 123 };
      jest.spyOn(service, 'getEntreeIdentifier');
      const id = comp.trackId(0, entity);
      expect(service.getEntreeIdentifier).toHaveBeenCalledWith(entity);
      expect(id).toBe(entity.id);
    });
  });
});

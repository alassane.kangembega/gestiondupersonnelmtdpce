import dayjs from 'dayjs/esm';

import { IEntree, NewEntree } from './entree.model';

export const sampleWithRequiredData: IEntree = {
  id: 18031,
};

export const sampleWithPartialData: IEntree = {
  id: 78135,
  dateEntree: dayjs('2022-09-07'),
};

export const sampleWithFullData: IEntree = {
  id: 20385,
  dateEntree: dayjs('2022-09-07'),
};

export const sampleWithNewData: NewEntree = {
  id: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);

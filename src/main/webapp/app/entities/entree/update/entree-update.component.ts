import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { EntreeFormService, EntreeFormGroup } from './entree-form.service';
import { IEntree } from '../entree.model';
import { EntreeService } from '../service/entree.service';
import { IEmploye } from 'app/entities/employe/employe.model';
import { EmployeService } from 'app/entities/employe/service/employe.service';
import { INatureEntree } from 'app/entities/nature-entree/nature-entree.model';
import { NatureEntreeService } from 'app/entities/nature-entree/service/nature-entree.service';

@Component({
  selector: 'jhi-entree-update',
  templateUrl: './entree-update.component.html',
})
export class EntreeUpdateComponent implements OnInit {
  isSaving = false;
  entree: IEntree | null = null;

  employesSharedCollection: IEmploye[] = [];
  natureEntreesSharedCollection: INatureEntree[] = [];

  editForm: EntreeFormGroup = this.entreeFormService.createEntreeFormGroup();

  constructor(
    protected entreeService: EntreeService,
    protected entreeFormService: EntreeFormService,
    protected employeService: EmployeService,
    protected natureEntreeService: NatureEntreeService,
    protected activatedRoute: ActivatedRoute
  ) {}

  compareEmploye = (o1: IEmploye | null, o2: IEmploye | null): boolean => this.employeService.compareEmploye(o1, o2);

  compareNatureEntree = (o1: INatureEntree | null, o2: INatureEntree | null): boolean =>
    this.natureEntreeService.compareNatureEntree(o1, o2);

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ entree }) => {
      this.entree = entree;
      if (entree) {
        this.updateForm(entree);
      }

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const entree = this.entreeFormService.getEntree(this.editForm);
    if (entree.id !== null) {
      this.subscribeToSaveResponse(this.entreeService.update(entree));
    } else {
      this.subscribeToSaveResponse(this.entreeService.create(entree));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IEntree>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(entree: IEntree): void {
    this.entree = entree;
    this.entreeFormService.resetForm(this.editForm, entree);

    this.employesSharedCollection = this.employeService.addEmployeToCollectionIfMissing<IEmploye>(
      this.employesSharedCollection,
      entree.employe
    );
    this.natureEntreesSharedCollection = this.natureEntreeService.addNatureEntreeToCollectionIfMissing<INatureEntree>(
      this.natureEntreesSharedCollection,
      entree.natureEntree
    );
  }

  protected loadRelationshipsOptions(): void {
    this.employeService
      .query()
      .pipe(map((res: HttpResponse<IEmploye[]>) => res.body ?? []))
      .pipe(map((employes: IEmploye[]) => this.employeService.addEmployeToCollectionIfMissing<IEmploye>(employes, this.entree?.employe)))
      .subscribe((employes: IEmploye[]) => (this.employesSharedCollection = employes));

    this.natureEntreeService
      .query()
      .pipe(map((res: HttpResponse<INatureEntree[]>) => res.body ?? []))
      .pipe(
        map((natureEntrees: INatureEntree[]) =>
          this.natureEntreeService.addNatureEntreeToCollectionIfMissing<INatureEntree>(natureEntrees, this.entree?.natureEntree)
        )
      )
      .subscribe((natureEntrees: INatureEntree[]) => (this.natureEntreesSharedCollection = natureEntrees));
  }
}

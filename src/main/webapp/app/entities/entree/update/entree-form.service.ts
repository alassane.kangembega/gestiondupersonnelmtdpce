import { Injectable } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { IEntree, NewEntree } from '../entree.model';

/**
 * A partial Type with required key is used as form input.
 */
type PartialWithRequiredKeyOf<T extends { id: unknown }> = Partial<Omit<T, 'id'>> & { id: T['id'] };

/**
 * Type for createFormGroup and resetForm argument.
 * It accepts IEntree for edit and NewEntreeFormGroupInput for create.
 */
type EntreeFormGroupInput = IEntree | PartialWithRequiredKeyOf<NewEntree>;

type EntreeFormDefaults = Pick<NewEntree, 'id'>;

type EntreeFormGroupContent = {
  id: FormControl<IEntree['id'] | NewEntree['id']>;
  dateEntree: FormControl<IEntree['dateEntree']>;
  employe: FormControl<IEntree['employe']>;
  natureEntree: FormControl<IEntree['natureEntree']>;
};

export type EntreeFormGroup = FormGroup<EntreeFormGroupContent>;

@Injectable({ providedIn: 'root' })
export class EntreeFormService {
  createEntreeFormGroup(entree: EntreeFormGroupInput = { id: null }): EntreeFormGroup {
    const entreeRawValue = {
      ...this.getFormDefaults(),
      ...entree,
    };
    return new FormGroup<EntreeFormGroupContent>({
      id: new FormControl(
        { value: entreeRawValue.id, disabled: true },
        {
          nonNullable: true,
          validators: [Validators.required],
        }
      ),
      dateEntree: new FormControl(entreeRawValue.dateEntree),
      employe: new FormControl(entreeRawValue.employe),
      natureEntree: new FormControl(entreeRawValue.natureEntree),
    });
  }

  getEntree(form: EntreeFormGroup): IEntree | NewEntree {
    return form.getRawValue() as IEntree | NewEntree;
  }

  resetForm(form: EntreeFormGroup, entree: EntreeFormGroupInput): void {
    const entreeRawValue = { ...this.getFormDefaults(), ...entree };
    form.reset(
      {
        ...entreeRawValue,
        id: { value: entreeRawValue.id, disabled: true },
      } as any /* cast to workaround https://github.com/angular/angular/issues/46458 */
    );
  }

  private getFormDefaults(): EntreeFormDefaults {
    return {
      id: null,
    };
  }
}

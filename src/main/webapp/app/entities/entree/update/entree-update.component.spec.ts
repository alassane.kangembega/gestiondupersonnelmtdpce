import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { EntreeFormService } from './entree-form.service';
import { EntreeService } from '../service/entree.service';
import { IEntree } from '../entree.model';
import { IEmploye } from 'app/entities/employe/employe.model';
import { EmployeService } from 'app/entities/employe/service/employe.service';
import { INatureEntree } from 'app/entities/nature-entree/nature-entree.model';
import { NatureEntreeService } from 'app/entities/nature-entree/service/nature-entree.service';

import { EntreeUpdateComponent } from './entree-update.component';

describe('Entree Management Update Component', () => {
  let comp: EntreeUpdateComponent;
  let fixture: ComponentFixture<EntreeUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let entreeFormService: EntreeFormService;
  let entreeService: EntreeService;
  let employeService: EmployeService;
  let natureEntreeService: NatureEntreeService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      declarations: [EntreeUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(EntreeUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(EntreeUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    entreeFormService = TestBed.inject(EntreeFormService);
    entreeService = TestBed.inject(EntreeService);
    employeService = TestBed.inject(EmployeService);
    natureEntreeService = TestBed.inject(NatureEntreeService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call Employe query and add missing value', () => {
      const entree: IEntree = { id: 456 };
      const employe: IEmploye = { id: 22131 };
      entree.employe = employe;

      const employeCollection: IEmploye[] = [{ id: 7629 }];
      jest.spyOn(employeService, 'query').mockReturnValue(of(new HttpResponse({ body: employeCollection })));
      const additionalEmployes = [employe];
      const expectedCollection: IEmploye[] = [...additionalEmployes, ...employeCollection];
      jest.spyOn(employeService, 'addEmployeToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ entree });
      comp.ngOnInit();

      expect(employeService.query).toHaveBeenCalled();
      expect(employeService.addEmployeToCollectionIfMissing).toHaveBeenCalledWith(
        employeCollection,
        ...additionalEmployes.map(expect.objectContaining)
      );
      expect(comp.employesSharedCollection).toEqual(expectedCollection);
    });

    it('Should call NatureEntree query and add missing value', () => {
      const entree: IEntree = { id: 456 };
      const natureEntree: INatureEntree = { id: 6373 };
      entree.natureEntree = natureEntree;

      const natureEntreeCollection: INatureEntree[] = [{ id: 17895 }];
      jest.spyOn(natureEntreeService, 'query').mockReturnValue(of(new HttpResponse({ body: natureEntreeCollection })));
      const additionalNatureEntrees = [natureEntree];
      const expectedCollection: INatureEntree[] = [...additionalNatureEntrees, ...natureEntreeCollection];
      jest.spyOn(natureEntreeService, 'addNatureEntreeToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ entree });
      comp.ngOnInit();

      expect(natureEntreeService.query).toHaveBeenCalled();
      expect(natureEntreeService.addNatureEntreeToCollectionIfMissing).toHaveBeenCalledWith(
        natureEntreeCollection,
        ...additionalNatureEntrees.map(expect.objectContaining)
      );
      expect(comp.natureEntreesSharedCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const entree: IEntree = { id: 456 };
      const employe: IEmploye = { id: 31652 };
      entree.employe = employe;
      const natureEntree: INatureEntree = { id: 48105 };
      entree.natureEntree = natureEntree;

      activatedRoute.data = of({ entree });
      comp.ngOnInit();

      expect(comp.employesSharedCollection).toContain(employe);
      expect(comp.natureEntreesSharedCollection).toContain(natureEntree);
      expect(comp.entree).toEqual(entree);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IEntree>>();
      const entree = { id: 123 };
      jest.spyOn(entreeFormService, 'getEntree').mockReturnValue(entree);
      jest.spyOn(entreeService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ entree });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: entree }));
      saveSubject.complete();

      // THEN
      expect(entreeFormService.getEntree).toHaveBeenCalled();
      expect(comp.previousState).toHaveBeenCalled();
      expect(entreeService.update).toHaveBeenCalledWith(expect.objectContaining(entree));
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IEntree>>();
      const entree = { id: 123 };
      jest.spyOn(entreeFormService, 'getEntree').mockReturnValue({ id: null });
      jest.spyOn(entreeService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ entree: null });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: entree }));
      saveSubject.complete();

      // THEN
      expect(entreeFormService.getEntree).toHaveBeenCalled();
      expect(entreeService.create).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IEntree>>();
      const entree = { id: 123 };
      jest.spyOn(entreeService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ entree });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(entreeService.update).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Compare relationships', () => {
    describe('compareEmploye', () => {
      it('Should forward to employeService', () => {
        const entity = { id: 123 };
        const entity2 = { id: 456 };
        jest.spyOn(employeService, 'compareEmploye');
        comp.compareEmploye(entity, entity2);
        expect(employeService.compareEmploye).toHaveBeenCalledWith(entity, entity2);
      });
    });

    describe('compareNatureEntree', () => {
      it('Should forward to natureEntreeService', () => {
        const entity = { id: 123 };
        const entity2 = { id: 456 };
        jest.spyOn(natureEntreeService, 'compareNatureEntree');
        comp.compareNatureEntree(entity, entity2);
        expect(natureEntreeService.compareNatureEntree).toHaveBeenCalledWith(entity, entity2);
      });
    });
  });
});

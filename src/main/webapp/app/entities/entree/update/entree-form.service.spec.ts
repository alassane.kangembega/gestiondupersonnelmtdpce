import { TestBed } from '@angular/core/testing';

import { sampleWithRequiredData, sampleWithNewData } from '../entree.test-samples';

import { EntreeFormService } from './entree-form.service';

describe('Entree Form Service', () => {
  let service: EntreeFormService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EntreeFormService);
  });

  describe('Service methods', () => {
    describe('createEntreeFormGroup', () => {
      it('should create a new form with FormControl', () => {
        const formGroup = service.createEntreeFormGroup();

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            dateEntree: expect.any(Object),
            employe: expect.any(Object),
            natureEntree: expect.any(Object),
          })
        );
      });

      it('passing IEntree should create a new form with FormGroup', () => {
        const formGroup = service.createEntreeFormGroup(sampleWithRequiredData);

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            dateEntree: expect.any(Object),
            employe: expect.any(Object),
            natureEntree: expect.any(Object),
          })
        );
      });
    });

    describe('getEntree', () => {
      it('should return NewEntree for default Entree initial value', () => {
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        const formGroup = service.createEntreeFormGroup(sampleWithNewData);

        const entree = service.getEntree(formGroup) as any;

        expect(entree).toMatchObject(sampleWithNewData);
      });

      it('should return NewEntree for empty Entree initial value', () => {
        const formGroup = service.createEntreeFormGroup();

        const entree = service.getEntree(formGroup) as any;

        expect(entree).toMatchObject({});
      });

      it('should return IEntree', () => {
        const formGroup = service.createEntreeFormGroup(sampleWithRequiredData);

        const entree = service.getEntree(formGroup) as any;

        expect(entree).toMatchObject(sampleWithRequiredData);
      });
    });

    describe('resetForm', () => {
      it('passing IEntree should not enable id FormControl', () => {
        const formGroup = service.createEntreeFormGroup();
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, sampleWithRequiredData);

        expect(formGroup.controls.id.disabled).toBe(true);
      });

      it('passing NewEntree should disable id FormControl', () => {
        const formGroup = service.createEntreeFormGroup(sampleWithRequiredData);
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, { id: null });

        expect(formGroup.controls.id.disabled).toBe(true);
      });
    });
  });
});

import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import dayjs from 'dayjs/esm';

import { isPresent } from 'app/core/util/operators';
import { DATE_FORMAT } from 'app/config/input.constants';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { ISortie, NewSortie } from '../sortie.model';

export type PartialUpdateSortie = Partial<ISortie> & Pick<ISortie, 'id'>;

type RestOf<T extends ISortie | NewSortie> = Omit<T, 'dateSortie'> & {
  dateSortie?: string | null;
};

export type RestSortie = RestOf<ISortie>;

export type NewRestSortie = RestOf<NewSortie>;

export type PartialUpdateRestSortie = RestOf<PartialUpdateSortie>;

export type EntityResponseType = HttpResponse<ISortie>;
export type EntityArrayResponseType = HttpResponse<ISortie[]>;

@Injectable({ providedIn: 'root' })
export class SortieService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/sorties');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(sortie: NewSortie): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(sortie);
    return this.http
      .post<RestSortie>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  update(sortie: ISortie): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(sortie);
    return this.http
      .put<RestSortie>(`${this.resourceUrl}/${this.getSortieIdentifier(sortie)}`, copy, { observe: 'response' })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  partialUpdate(sortie: PartialUpdateSortie): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(sortie);
    return this.http
      .patch<RestSortie>(`${this.resourceUrl}/${this.getSortieIdentifier(sortie)}`, copy, { observe: 'response' })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<RestSortie>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<RestSortie[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map(res => this.convertResponseArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  getSortieIdentifier(sortie: Pick<ISortie, 'id'>): number {
    return sortie.id;
  }

  compareSortie(o1: Pick<ISortie, 'id'> | null, o2: Pick<ISortie, 'id'> | null): boolean {
    return o1 && o2 ? this.getSortieIdentifier(o1) === this.getSortieIdentifier(o2) : o1 === o2;
  }

  addSortieToCollectionIfMissing<Type extends Pick<ISortie, 'id'>>(
    sortieCollection: Type[],
    ...sortiesToCheck: (Type | null | undefined)[]
  ): Type[] {
    const sorties: Type[] = sortiesToCheck.filter(isPresent);
    if (sorties.length > 0) {
      const sortieCollectionIdentifiers = sortieCollection.map(sortieItem => this.getSortieIdentifier(sortieItem)!);
      const sortiesToAdd = sorties.filter(sortieItem => {
        const sortieIdentifier = this.getSortieIdentifier(sortieItem);
        if (sortieCollectionIdentifiers.includes(sortieIdentifier)) {
          return false;
        }
        sortieCollectionIdentifiers.push(sortieIdentifier);
        return true;
      });
      return [...sortiesToAdd, ...sortieCollection];
    }
    return sortieCollection;
  }

  protected convertDateFromClient<T extends ISortie | NewSortie | PartialUpdateSortie>(sortie: T): RestOf<T> {
    return {
      ...sortie,
      dateSortie: sortie.dateSortie?.format(DATE_FORMAT) ?? null,
    };
  }

  protected convertDateFromServer(restSortie: RestSortie): ISortie {
    return {
      ...restSortie,
      dateSortie: restSortie.dateSortie ? dayjs(restSortie.dateSortie) : undefined,
    };
  }

  protected convertResponseFromServer(res: HttpResponse<RestSortie>): HttpResponse<ISortie> {
    return res.clone({
      body: res.body ? this.convertDateFromServer(res.body) : null,
    });
  }

  protected convertResponseArrayFromServer(res: HttpResponse<RestSortie[]>): HttpResponse<ISortie[]> {
    return res.clone({
      body: res.body ? res.body.map(item => this.convertDateFromServer(item)) : null,
    });
  }
}

import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { DATE_FORMAT } from 'app/config/input.constants';
import { ISortie } from '../sortie.model';
import { sampleWithRequiredData, sampleWithNewData, sampleWithPartialData, sampleWithFullData } from '../sortie.test-samples';

import { SortieService, RestSortie } from './sortie.service';

const requireRestSample: RestSortie = {
  ...sampleWithRequiredData,
  dateSortie: sampleWithRequiredData.dateSortie?.format(DATE_FORMAT),
};

describe('Sortie Service', () => {
  let service: SortieService;
  let httpMock: HttpTestingController;
  let expectedResult: ISortie | ISortie[] | boolean | null;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(SortieService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should create a Sortie', () => {
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      const sortie = { ...sampleWithNewData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.create(sortie).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a Sortie', () => {
      const sortie = { ...sampleWithRequiredData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.update(sortie).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a Sortie', () => {
      const patchObject = { ...sampleWithPartialData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of Sortie', () => {
      const returnedFromService = { ...requireRestSample };

      const expected = { ...sampleWithRequiredData };

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toMatchObject([expected]);
    });

    it('should delete a Sortie', () => {
      const expected = true;

      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult).toBe(expected);
    });

    describe('addSortieToCollectionIfMissing', () => {
      it('should add a Sortie to an empty array', () => {
        const sortie: ISortie = sampleWithRequiredData;
        expectedResult = service.addSortieToCollectionIfMissing([], sortie);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(sortie);
      });

      it('should not add a Sortie to an array that contains it', () => {
        const sortie: ISortie = sampleWithRequiredData;
        const sortieCollection: ISortie[] = [
          {
            ...sortie,
          },
          sampleWithPartialData,
        ];
        expectedResult = service.addSortieToCollectionIfMissing(sortieCollection, sortie);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a Sortie to an array that doesn't contain it", () => {
        const sortie: ISortie = sampleWithRequiredData;
        const sortieCollection: ISortie[] = [sampleWithPartialData];
        expectedResult = service.addSortieToCollectionIfMissing(sortieCollection, sortie);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(sortie);
      });

      it('should add only unique Sortie to an array', () => {
        const sortieArray: ISortie[] = [sampleWithRequiredData, sampleWithPartialData, sampleWithFullData];
        const sortieCollection: ISortie[] = [sampleWithRequiredData];
        expectedResult = service.addSortieToCollectionIfMissing(sortieCollection, ...sortieArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const sortie: ISortie = sampleWithRequiredData;
        const sortie2: ISortie = sampleWithPartialData;
        expectedResult = service.addSortieToCollectionIfMissing([], sortie, sortie2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(sortie);
        expect(expectedResult).toContain(sortie2);
      });

      it('should accept null and undefined values', () => {
        const sortie: ISortie = sampleWithRequiredData;
        expectedResult = service.addSortieToCollectionIfMissing([], null, sortie, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(sortie);
      });

      it('should return initial array if no Sortie is added', () => {
        const sortieCollection: ISortie[] = [sampleWithRequiredData];
        expectedResult = service.addSortieToCollectionIfMissing(sortieCollection, undefined, null);
        expect(expectedResult).toEqual(sortieCollection);
      });
    });

    describe('compareSortie', () => {
      it('Should return true if both entities are null', () => {
        const entity1 = null;
        const entity2 = null;

        const compareResult = service.compareSortie(entity1, entity2);

        expect(compareResult).toEqual(true);
      });

      it('Should return false if one entity is null', () => {
        const entity1 = { id: 123 };
        const entity2 = null;

        const compareResult1 = service.compareSortie(entity1, entity2);
        const compareResult2 = service.compareSortie(entity2, entity1);

        expect(compareResult1).toEqual(false);
        expect(compareResult2).toEqual(false);
      });

      it('Should return false if primaryKey differs', () => {
        const entity1 = { id: 123 };
        const entity2 = { id: 456 };

        const compareResult1 = service.compareSortie(entity1, entity2);
        const compareResult2 = service.compareSortie(entity2, entity1);

        expect(compareResult1).toEqual(false);
        expect(compareResult2).toEqual(false);
      });

      it('Should return false if primaryKey matches', () => {
        const entity1 = { id: 123 };
        const entity2 = { id: 123 };

        const compareResult1 = service.compareSortie(entity1, entity2);
        const compareResult2 = service.compareSortie(entity2, entity1);

        expect(compareResult1).toEqual(true);
        expect(compareResult2).toEqual(true);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});

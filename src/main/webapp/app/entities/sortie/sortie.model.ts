import dayjs from 'dayjs/esm';
import { INatureSortie } from 'app/entities/nature-sortie/nature-sortie.model';
import { IEmploye } from 'app/entities/employe/employe.model';

export interface ISortie {
  id: number;
  dateSortie?: dayjs.Dayjs | null;
  natureSortie?: Pick<INatureSortie, 'id'> | null;
  employe?: Pick<IEmploye, 'id'> | null;
}

export type NewSortie = Omit<ISortie, 'id'> & { id: null };

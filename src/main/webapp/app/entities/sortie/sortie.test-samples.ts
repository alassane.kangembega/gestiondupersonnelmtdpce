import dayjs from 'dayjs/esm';

import { ISortie, NewSortie } from './sortie.model';

export const sampleWithRequiredData: ISortie = {
  id: 88726,
};

export const sampleWithPartialData: ISortie = {
  id: 33239,
  dateSortie: dayjs('2022-09-07'),
};

export const sampleWithFullData: ISortie = {
  id: 97945,
  dateSortie: dayjs('2022-09-07'),
};

export const sampleWithNewData: NewSortie = {
  id: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);

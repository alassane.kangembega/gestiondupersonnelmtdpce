import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { SortieFormService, SortieFormGroup } from './sortie-form.service';
import { ISortie } from '../sortie.model';
import { SortieService } from '../service/sortie.service';
import { INatureSortie } from 'app/entities/nature-sortie/nature-sortie.model';
import { NatureSortieService } from 'app/entities/nature-sortie/service/nature-sortie.service';
import { IEmploye } from 'app/entities/employe/employe.model';
import { EmployeService } from 'app/entities/employe/service/employe.service';

@Component({
  selector: 'jhi-sortie-update',
  templateUrl: './sortie-update.component.html',
})
export class SortieUpdateComponent implements OnInit {
  isSaving = false;
  sortie: ISortie | null = null;

  natureSortiesSharedCollection: INatureSortie[] = [];
  employesSharedCollection: IEmploye[] = [];

  editForm: SortieFormGroup = this.sortieFormService.createSortieFormGroup();

  constructor(
    protected sortieService: SortieService,
    protected sortieFormService: SortieFormService,
    protected natureSortieService: NatureSortieService,
    protected employeService: EmployeService,
    protected activatedRoute: ActivatedRoute
  ) {}

  compareNatureSortie = (o1: INatureSortie | null, o2: INatureSortie | null): boolean =>
    this.natureSortieService.compareNatureSortie(o1, o2);

  compareEmploye = (o1: IEmploye | null, o2: IEmploye | null): boolean => this.employeService.compareEmploye(o1, o2);

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ sortie }) => {
      this.sortie = sortie;
      if (sortie) {
        this.updateForm(sortie);
      }

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const sortie = this.sortieFormService.getSortie(this.editForm);
    if (sortie.id !== null) {
      this.subscribeToSaveResponse(this.sortieService.update(sortie));
    } else {
      this.subscribeToSaveResponse(this.sortieService.create(sortie));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ISortie>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(sortie: ISortie): void {
    this.sortie = sortie;
    this.sortieFormService.resetForm(this.editForm, sortie);

    this.natureSortiesSharedCollection = this.natureSortieService.addNatureSortieToCollectionIfMissing<INatureSortie>(
      this.natureSortiesSharedCollection,
      sortie.natureSortie
    );
    this.employesSharedCollection = this.employeService.addEmployeToCollectionIfMissing<IEmploye>(
      this.employesSharedCollection,
      sortie.employe
    );
  }

  protected loadRelationshipsOptions(): void {
    this.natureSortieService
      .query()
      .pipe(map((res: HttpResponse<INatureSortie[]>) => res.body ?? []))
      .pipe(
        map((natureSorties: INatureSortie[]) =>
          this.natureSortieService.addNatureSortieToCollectionIfMissing<INatureSortie>(natureSorties, this.sortie?.natureSortie)
        )
      )
      .subscribe((natureSorties: INatureSortie[]) => (this.natureSortiesSharedCollection = natureSorties));

    this.employeService
      .query()
      .pipe(map((res: HttpResponse<IEmploye[]>) => res.body ?? []))
      .pipe(map((employes: IEmploye[]) => this.employeService.addEmployeToCollectionIfMissing<IEmploye>(employes, this.sortie?.employe)))
      .subscribe((employes: IEmploye[]) => (this.employesSharedCollection = employes));
  }
}

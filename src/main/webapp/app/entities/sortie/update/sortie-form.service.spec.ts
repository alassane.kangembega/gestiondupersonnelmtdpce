import { TestBed } from '@angular/core/testing';

import { sampleWithRequiredData, sampleWithNewData } from '../sortie.test-samples';

import { SortieFormService } from './sortie-form.service';

describe('Sortie Form Service', () => {
  let service: SortieFormService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SortieFormService);
  });

  describe('Service methods', () => {
    describe('createSortieFormGroup', () => {
      it('should create a new form with FormControl', () => {
        const formGroup = service.createSortieFormGroup();

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            dateSortie: expect.any(Object),
            natureSortie: expect.any(Object),
            employe: expect.any(Object),
          })
        );
      });

      it('passing ISortie should create a new form with FormGroup', () => {
        const formGroup = service.createSortieFormGroup(sampleWithRequiredData);

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            dateSortie: expect.any(Object),
            natureSortie: expect.any(Object),
            employe: expect.any(Object),
          })
        );
      });
    });

    describe('getSortie', () => {
      it('should return NewSortie for default Sortie initial value', () => {
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        const formGroup = service.createSortieFormGroup(sampleWithNewData);

        const sortie = service.getSortie(formGroup) as any;

        expect(sortie).toMatchObject(sampleWithNewData);
      });

      it('should return NewSortie for empty Sortie initial value', () => {
        const formGroup = service.createSortieFormGroup();

        const sortie = service.getSortie(formGroup) as any;

        expect(sortie).toMatchObject({});
      });

      it('should return ISortie', () => {
        const formGroup = service.createSortieFormGroup(sampleWithRequiredData);

        const sortie = service.getSortie(formGroup) as any;

        expect(sortie).toMatchObject(sampleWithRequiredData);
      });
    });

    describe('resetForm', () => {
      it('passing ISortie should not enable id FormControl', () => {
        const formGroup = service.createSortieFormGroup();
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, sampleWithRequiredData);

        expect(formGroup.controls.id.disabled).toBe(true);
      });

      it('passing NewSortie should disable id FormControl', () => {
        const formGroup = service.createSortieFormGroup(sampleWithRequiredData);
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, { id: null });

        expect(formGroup.controls.id.disabled).toBe(true);
      });
    });
  });
});

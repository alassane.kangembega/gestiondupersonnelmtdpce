import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { SortieFormService } from './sortie-form.service';
import { SortieService } from '../service/sortie.service';
import { ISortie } from '../sortie.model';
import { INatureSortie } from 'app/entities/nature-sortie/nature-sortie.model';
import { NatureSortieService } from 'app/entities/nature-sortie/service/nature-sortie.service';
import { IEmploye } from 'app/entities/employe/employe.model';
import { EmployeService } from 'app/entities/employe/service/employe.service';

import { SortieUpdateComponent } from './sortie-update.component';

describe('Sortie Management Update Component', () => {
  let comp: SortieUpdateComponent;
  let fixture: ComponentFixture<SortieUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let sortieFormService: SortieFormService;
  let sortieService: SortieService;
  let natureSortieService: NatureSortieService;
  let employeService: EmployeService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      declarations: [SortieUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(SortieUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(SortieUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    sortieFormService = TestBed.inject(SortieFormService);
    sortieService = TestBed.inject(SortieService);
    natureSortieService = TestBed.inject(NatureSortieService);
    employeService = TestBed.inject(EmployeService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call NatureSortie query and add missing value', () => {
      const sortie: ISortie = { id: 456 };
      const natureSortie: INatureSortie = { id: 76466 };
      sortie.natureSortie = natureSortie;

      const natureSortieCollection: INatureSortie[] = [{ id: 69494 }];
      jest.spyOn(natureSortieService, 'query').mockReturnValue(of(new HttpResponse({ body: natureSortieCollection })));
      const additionalNatureSorties = [natureSortie];
      const expectedCollection: INatureSortie[] = [...additionalNatureSorties, ...natureSortieCollection];
      jest.spyOn(natureSortieService, 'addNatureSortieToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ sortie });
      comp.ngOnInit();

      expect(natureSortieService.query).toHaveBeenCalled();
      expect(natureSortieService.addNatureSortieToCollectionIfMissing).toHaveBeenCalledWith(
        natureSortieCollection,
        ...additionalNatureSorties.map(expect.objectContaining)
      );
      expect(comp.natureSortiesSharedCollection).toEqual(expectedCollection);
    });

    it('Should call Employe query and add missing value', () => {
      const sortie: ISortie = { id: 456 };
      const employe: IEmploye = { id: 96656 };
      sortie.employe = employe;

      const employeCollection: IEmploye[] = [{ id: 88728 }];
      jest.spyOn(employeService, 'query').mockReturnValue(of(new HttpResponse({ body: employeCollection })));
      const additionalEmployes = [employe];
      const expectedCollection: IEmploye[] = [...additionalEmployes, ...employeCollection];
      jest.spyOn(employeService, 'addEmployeToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ sortie });
      comp.ngOnInit();

      expect(employeService.query).toHaveBeenCalled();
      expect(employeService.addEmployeToCollectionIfMissing).toHaveBeenCalledWith(
        employeCollection,
        ...additionalEmployes.map(expect.objectContaining)
      );
      expect(comp.employesSharedCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const sortie: ISortie = { id: 456 };
      const natureSortie: INatureSortie = { id: 40774 };
      sortie.natureSortie = natureSortie;
      const employe: IEmploye = { id: 14667 };
      sortie.employe = employe;

      activatedRoute.data = of({ sortie });
      comp.ngOnInit();

      expect(comp.natureSortiesSharedCollection).toContain(natureSortie);
      expect(comp.employesSharedCollection).toContain(employe);
      expect(comp.sortie).toEqual(sortie);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<ISortie>>();
      const sortie = { id: 123 };
      jest.spyOn(sortieFormService, 'getSortie').mockReturnValue(sortie);
      jest.spyOn(sortieService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ sortie });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: sortie }));
      saveSubject.complete();

      // THEN
      expect(sortieFormService.getSortie).toHaveBeenCalled();
      expect(comp.previousState).toHaveBeenCalled();
      expect(sortieService.update).toHaveBeenCalledWith(expect.objectContaining(sortie));
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<ISortie>>();
      const sortie = { id: 123 };
      jest.spyOn(sortieFormService, 'getSortie').mockReturnValue({ id: null });
      jest.spyOn(sortieService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ sortie: null });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: sortie }));
      saveSubject.complete();

      // THEN
      expect(sortieFormService.getSortie).toHaveBeenCalled();
      expect(sortieService.create).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<ISortie>>();
      const sortie = { id: 123 };
      jest.spyOn(sortieService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ sortie });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(sortieService.update).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Compare relationships', () => {
    describe('compareNatureSortie', () => {
      it('Should forward to natureSortieService', () => {
        const entity = { id: 123 };
        const entity2 = { id: 456 };
        jest.spyOn(natureSortieService, 'compareNatureSortie');
        comp.compareNatureSortie(entity, entity2);
        expect(natureSortieService.compareNatureSortie).toHaveBeenCalledWith(entity, entity2);
      });
    });

    describe('compareEmploye', () => {
      it('Should forward to employeService', () => {
        const entity = { id: 123 };
        const entity2 = { id: 456 };
        jest.spyOn(employeService, 'compareEmploye');
        comp.compareEmploye(entity, entity2);
        expect(employeService.compareEmploye).toHaveBeenCalledWith(entity, entity2);
      });
    });
  });
});

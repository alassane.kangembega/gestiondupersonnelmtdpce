import { Injectable } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { ISortie, NewSortie } from '../sortie.model';

/**
 * A partial Type with required key is used as form input.
 */
type PartialWithRequiredKeyOf<T extends { id: unknown }> = Partial<Omit<T, 'id'>> & { id: T['id'] };

/**
 * Type for createFormGroup and resetForm argument.
 * It accepts ISortie for edit and NewSortieFormGroupInput for create.
 */
type SortieFormGroupInput = ISortie | PartialWithRequiredKeyOf<NewSortie>;

type SortieFormDefaults = Pick<NewSortie, 'id'>;

type SortieFormGroupContent = {
  id: FormControl<ISortie['id'] | NewSortie['id']>;
  dateSortie: FormControl<ISortie['dateSortie']>;
  natureSortie: FormControl<ISortie['natureSortie']>;
  employe: FormControl<ISortie['employe']>;
};

export type SortieFormGroup = FormGroup<SortieFormGroupContent>;

@Injectable({ providedIn: 'root' })
export class SortieFormService {
  createSortieFormGroup(sortie: SortieFormGroupInput = { id: null }): SortieFormGroup {
    const sortieRawValue = {
      ...this.getFormDefaults(),
      ...sortie,
    };
    return new FormGroup<SortieFormGroupContent>({
      id: new FormControl(
        { value: sortieRawValue.id, disabled: true },
        {
          nonNullable: true,
          validators: [Validators.required],
        }
      ),
      dateSortie: new FormControl(sortieRawValue.dateSortie),
      natureSortie: new FormControl(sortieRawValue.natureSortie),
      employe: new FormControl(sortieRawValue.employe),
    });
  }

  getSortie(form: SortieFormGroup): ISortie | NewSortie {
    return form.getRawValue() as ISortie | NewSortie;
  }

  resetForm(form: SortieFormGroup, sortie: SortieFormGroupInput): void {
    const sortieRawValue = { ...this.getFormDefaults(), ...sortie };
    form.reset(
      {
        ...sortieRawValue,
        id: { value: sortieRawValue.id, disabled: true },
      } as any /* cast to workaround https://github.com/angular/angular/issues/46458 */
    );
  }

  private getFormDefaults(): SortieFormDefaults {
    return {
      id: null,
    };
  }
}

import { Injectable } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { IStatut, NewStatut } from '../statut.model';

/**
 * A partial Type with required key is used as form input.
 */
type PartialWithRequiredKeyOf<T extends { id: unknown }> = Partial<Omit<T, 'id'>> & { id: T['id'] };

/**
 * Type for createFormGroup and resetForm argument.
 * It accepts IStatut for edit and NewStatutFormGroupInput for create.
 */
type StatutFormGroupInput = IStatut | PartialWithRequiredKeyOf<NewStatut>;

type StatutFormDefaults = Pick<NewStatut, 'id'>;

type StatutFormGroupContent = {
  id: FormControl<IStatut['id'] | NewStatut['id']>;
  statutAgent: FormControl<IStatut['statutAgent']>;
};

export type StatutFormGroup = FormGroup<StatutFormGroupContent>;

@Injectable({ providedIn: 'root' })
export class StatutFormService {
  createStatutFormGroup(statut: StatutFormGroupInput = { id: null }): StatutFormGroup {
    const statutRawValue = {
      ...this.getFormDefaults(),
      ...statut,
    };
    return new FormGroup<StatutFormGroupContent>({
      id: new FormControl(
        { value: statutRawValue.id, disabled: true },
        {
          nonNullable: true,
          validators: [Validators.required],
        }
      ),
      statutAgent: new FormControl(statutRawValue.statutAgent),
    });
  }

  getStatut(form: StatutFormGroup): IStatut | NewStatut {
    return form.getRawValue() as IStatut | NewStatut;
  }

  resetForm(form: StatutFormGroup, statut: StatutFormGroupInput): void {
    const statutRawValue = { ...this.getFormDefaults(), ...statut };
    form.reset(
      {
        ...statutRawValue,
        id: { value: statutRawValue.id, disabled: true },
      } as any /* cast to workaround https://github.com/angular/angular/issues/46458 */
    );
  }

  private getFormDefaults(): StatutFormDefaults {
    return {
      id: null,
    };
  }
}

import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

import { StatutFormService, StatutFormGroup } from './statut-form.service';
import { IStatut } from '../statut.model';
import { StatutService } from '../service/statut.service';

@Component({
  selector: 'jhi-statut-update',
  templateUrl: './statut-update.component.html',
})
export class StatutUpdateComponent implements OnInit {
  isSaving = false;
  statut: IStatut | null = null;

  editForm: StatutFormGroup = this.statutFormService.createStatutFormGroup();

  constructor(
    protected statutService: StatutService,
    protected statutFormService: StatutFormService,
    protected activatedRoute: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ statut }) => {
      this.statut = statut;
      if (statut) {
        this.updateForm(statut);
      }
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const statut = this.statutFormService.getStatut(this.editForm);
    if (statut.id !== null) {
      this.subscribeToSaveResponse(this.statutService.update(statut));
    } else {
      this.subscribeToSaveResponse(this.statutService.create(statut));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IStatut>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(statut: IStatut): void {
    this.statut = statut;
    this.statutFormService.resetForm(this.editForm, statut);
  }
}

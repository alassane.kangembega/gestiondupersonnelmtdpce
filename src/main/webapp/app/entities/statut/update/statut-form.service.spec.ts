import { TestBed } from '@angular/core/testing';

import { sampleWithRequiredData, sampleWithNewData } from '../statut.test-samples';

import { StatutFormService } from './statut-form.service';

describe('Statut Form Service', () => {
  let service: StatutFormService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(StatutFormService);
  });

  describe('Service methods', () => {
    describe('createStatutFormGroup', () => {
      it('should create a new form with FormControl', () => {
        const formGroup = service.createStatutFormGroup();

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            statutAgent: expect.any(Object),
          })
        );
      });

      it('passing IStatut should create a new form with FormGroup', () => {
        const formGroup = service.createStatutFormGroup(sampleWithRequiredData);

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            statutAgent: expect.any(Object),
          })
        );
      });
    });

    describe('getStatut', () => {
      it('should return NewStatut for default Statut initial value', () => {
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        const formGroup = service.createStatutFormGroup(sampleWithNewData);

        const statut = service.getStatut(formGroup) as any;

        expect(statut).toMatchObject(sampleWithNewData);
      });

      it('should return NewStatut for empty Statut initial value', () => {
        const formGroup = service.createStatutFormGroup();

        const statut = service.getStatut(formGroup) as any;

        expect(statut).toMatchObject({});
      });

      it('should return IStatut', () => {
        const formGroup = service.createStatutFormGroup(sampleWithRequiredData);

        const statut = service.getStatut(formGroup) as any;

        expect(statut).toMatchObject(sampleWithRequiredData);
      });
    });

    describe('resetForm', () => {
      it('passing IStatut should not enable id FormControl', () => {
        const formGroup = service.createStatutFormGroup();
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, sampleWithRequiredData);

        expect(formGroup.controls.id.disabled).toBe(true);
      });

      it('passing NewStatut should disable id FormControl', () => {
        const formGroup = service.createStatutFormGroup(sampleWithRequiredData);
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, { id: null });

        expect(formGroup.controls.id.disabled).toBe(true);
      });
    });
  });
});

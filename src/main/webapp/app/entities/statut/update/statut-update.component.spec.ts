import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { StatutFormService } from './statut-form.service';
import { StatutService } from '../service/statut.service';
import { IStatut } from '../statut.model';

import { StatutUpdateComponent } from './statut-update.component';

describe('Statut Management Update Component', () => {
  let comp: StatutUpdateComponent;
  let fixture: ComponentFixture<StatutUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let statutFormService: StatutFormService;
  let statutService: StatutService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      declarations: [StatutUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(StatutUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(StatutUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    statutFormService = TestBed.inject(StatutFormService);
    statutService = TestBed.inject(StatutService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should update editForm', () => {
      const statut: IStatut = { id: 456 };

      activatedRoute.data = of({ statut });
      comp.ngOnInit();

      expect(comp.statut).toEqual(statut);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IStatut>>();
      const statut = { id: 123 };
      jest.spyOn(statutFormService, 'getStatut').mockReturnValue(statut);
      jest.spyOn(statutService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ statut });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: statut }));
      saveSubject.complete();

      // THEN
      expect(statutFormService.getStatut).toHaveBeenCalled();
      expect(comp.previousState).toHaveBeenCalled();
      expect(statutService.update).toHaveBeenCalledWith(expect.objectContaining(statut));
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IStatut>>();
      const statut = { id: 123 };
      jest.spyOn(statutFormService, 'getStatut').mockReturnValue({ id: null });
      jest.spyOn(statutService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ statut: null });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: statut }));
      saveSubject.complete();

      // THEN
      expect(statutFormService.getStatut).toHaveBeenCalled();
      expect(statutService.create).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IStatut>>();
      const statut = { id: 123 };
      jest.spyOn(statutService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ statut });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(statutService.update).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });
});

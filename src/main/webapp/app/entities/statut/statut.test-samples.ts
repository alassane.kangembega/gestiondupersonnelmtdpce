import { IStatut, NewStatut } from './statut.model';

export const sampleWithRequiredData: IStatut = {
  id: 16980,
};

export const sampleWithPartialData: IStatut = {
  id: 33490,
};

export const sampleWithFullData: IStatut = {
  id: 99690,
  statutAgent: 'Open-source Joubert Sausages',
};

export const sampleWithNewData: NewStatut = {
  id: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);

export interface IStatut {
  id: number;
  statutAgent?: string | null;
}

export type NewStatut = Omit<IStatut, 'id'> & { id: null };

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { PositionIrreguliereDetailComponent } from './position-irreguliere-detail.component';

describe('PositionIrreguliere Management Detail Component', () => {
  let comp: PositionIrreguliereDetailComponent;
  let fixture: ComponentFixture<PositionIrreguliereDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [PositionIrreguliereDetailComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: { data: of({ positionIrreguliere: { id: 123 } }) },
        },
      ],
    })
      .overrideTemplate(PositionIrreguliereDetailComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(PositionIrreguliereDetailComponent);
    comp = fixture.componentInstance;
  });

  describe('OnInit', () => {
    it('Should load positionIrreguliere on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(comp.positionIrreguliere).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});

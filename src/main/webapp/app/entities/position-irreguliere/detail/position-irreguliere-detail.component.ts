import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IPositionIrreguliere } from '../position-irreguliere.model';

@Component({
  selector: 'jhi-position-irreguliere-detail',
  templateUrl: './position-irreguliere-detail.component.html',
})
export class PositionIrreguliereDetailComponent implements OnInit {
  positionIrreguliere: IPositionIrreguliere | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ positionIrreguliere }) => {
      this.positionIrreguliere = positionIrreguliere;
    });
  }

  previousState(): void {
    window.history.back();
  }
}

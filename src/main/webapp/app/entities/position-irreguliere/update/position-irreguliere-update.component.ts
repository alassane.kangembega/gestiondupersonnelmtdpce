import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { PositionIrreguliereFormService, PositionIrreguliereFormGroup } from './position-irreguliere-form.service';
import { IPositionIrreguliere } from '../position-irreguliere.model';
import { PositionIrreguliereService } from '../service/position-irreguliere.service';
import { INaturePositionIrreguliere } from 'app/entities/nature-position-irreguliere/nature-position-irreguliere.model';
import { NaturePositionIrreguliereService } from 'app/entities/nature-position-irreguliere/service/nature-position-irreguliere.service';
import { IEmploye } from 'app/entities/employe/employe.model';
import { EmployeService } from 'app/entities/employe/service/employe.service';

@Component({
  selector: 'jhi-position-irreguliere-update',
  templateUrl: './position-irreguliere-update.component.html',
})
export class PositionIrreguliereUpdateComponent implements OnInit {
  isSaving = false;
  positionIrreguliere: IPositionIrreguliere | null = null;

  naturePositionIrregulieresSharedCollection: INaturePositionIrreguliere[] = [];
  employesSharedCollection: IEmploye[] = [];

  editForm: PositionIrreguliereFormGroup = this.positionIrreguliereFormService.createPositionIrreguliereFormGroup();

  constructor(
    protected positionIrreguliereService: PositionIrreguliereService,
    protected positionIrreguliereFormService: PositionIrreguliereFormService,
    protected naturePositionIrreguliereService: NaturePositionIrreguliereService,
    protected employeService: EmployeService,
    protected activatedRoute: ActivatedRoute
  ) {}

  compareNaturePositionIrreguliere = (o1: INaturePositionIrreguliere | null, o2: INaturePositionIrreguliere | null): boolean =>
    this.naturePositionIrreguliereService.compareNaturePositionIrreguliere(o1, o2);

  compareEmploye = (o1: IEmploye | null, o2: IEmploye | null): boolean => this.employeService.compareEmploye(o1, o2);

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ positionIrreguliere }) => {
      this.positionIrreguliere = positionIrreguliere;
      if (positionIrreguliere) {
        this.updateForm(positionIrreguliere);
      }

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const positionIrreguliere = this.positionIrreguliereFormService.getPositionIrreguliere(this.editForm);
    if (positionIrreguliere.id !== null) {
      this.subscribeToSaveResponse(this.positionIrreguliereService.update(positionIrreguliere));
    } else {
      this.subscribeToSaveResponse(this.positionIrreguliereService.create(positionIrreguliere));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IPositionIrreguliere>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(positionIrreguliere: IPositionIrreguliere): void {
    this.positionIrreguliere = positionIrreguliere;
    this.positionIrreguliereFormService.resetForm(this.editForm, positionIrreguliere);

    this.naturePositionIrregulieresSharedCollection =
      this.naturePositionIrreguliereService.addNaturePositionIrreguliereToCollectionIfMissing<INaturePositionIrreguliere>(
        this.naturePositionIrregulieresSharedCollection,
        positionIrreguliere.naturePositionIrreguliere
      );
    this.employesSharedCollection = this.employeService.addEmployeToCollectionIfMissing<IEmploye>(
      this.employesSharedCollection,
      positionIrreguliere.employe
    );
  }

  protected loadRelationshipsOptions(): void {
    this.naturePositionIrreguliereService
      .query()
      .pipe(map((res: HttpResponse<INaturePositionIrreguliere[]>) => res.body ?? []))
      .pipe(
        map((naturePositionIrregulieres: INaturePositionIrreguliere[]) =>
          this.naturePositionIrreguliereService.addNaturePositionIrreguliereToCollectionIfMissing<INaturePositionIrreguliere>(
            naturePositionIrregulieres,
            this.positionIrreguliere?.naturePositionIrreguliere
          )
        )
      )
      .subscribe(
        (naturePositionIrregulieres: INaturePositionIrreguliere[]) =>
          (this.naturePositionIrregulieresSharedCollection = naturePositionIrregulieres)
      );

    this.employeService
      .query()
      .pipe(map((res: HttpResponse<IEmploye[]>) => res.body ?? []))
      .pipe(
        map((employes: IEmploye[]) =>
          this.employeService.addEmployeToCollectionIfMissing<IEmploye>(employes, this.positionIrreguliere?.employe)
        )
      )
      .subscribe((employes: IEmploye[]) => (this.employesSharedCollection = employes));
  }
}

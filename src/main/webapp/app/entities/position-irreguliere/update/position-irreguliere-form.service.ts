import { Injectable } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { IPositionIrreguliere, NewPositionIrreguliere } from '../position-irreguliere.model';

/**
 * A partial Type with required key is used as form input.
 */
type PartialWithRequiredKeyOf<T extends { id: unknown }> = Partial<Omit<T, 'id'>> & { id: T['id'] };

/**
 * Type for createFormGroup and resetForm argument.
 * It accepts IPositionIrreguliere for edit and NewPositionIrreguliereFormGroupInput for create.
 */
type PositionIrreguliereFormGroupInput = IPositionIrreguliere | PartialWithRequiredKeyOf<NewPositionIrreguliere>;

type PositionIrreguliereFormDefaults = Pick<NewPositionIrreguliere, 'id'>;

type PositionIrreguliereFormGroupContent = {
  id: FormControl<IPositionIrreguliere['id'] | NewPositionIrreguliere['id']>;
  dateDebut: FormControl<IPositionIrreguliere['dateDebut']>;
  dateFin: FormControl<IPositionIrreguliere['dateFin']>;
  naturePositionIrreguliere: FormControl<IPositionIrreguliere['naturePositionIrreguliere']>;
  employe: FormControl<IPositionIrreguliere['employe']>;
};

export type PositionIrreguliereFormGroup = FormGroup<PositionIrreguliereFormGroupContent>;

@Injectable({ providedIn: 'root' })
export class PositionIrreguliereFormService {
  createPositionIrreguliereFormGroup(positionIrreguliere: PositionIrreguliereFormGroupInput = { id: null }): PositionIrreguliereFormGroup {
    const positionIrreguliereRawValue = {
      ...this.getFormDefaults(),
      ...positionIrreguliere,
    };
    return new FormGroup<PositionIrreguliereFormGroupContent>({
      id: new FormControl(
        { value: positionIrreguliereRawValue.id, disabled: true },
        {
          nonNullable: true,
          validators: [Validators.required],
        }
      ),
      dateDebut: new FormControl(positionIrreguliereRawValue.dateDebut),
      dateFin: new FormControl(positionIrreguliereRawValue.dateFin),
      naturePositionIrreguliere: new FormControl(positionIrreguliereRawValue.naturePositionIrreguliere),
      employe: new FormControl(positionIrreguliereRawValue.employe),
    });
  }

  getPositionIrreguliere(form: PositionIrreguliereFormGroup): IPositionIrreguliere | NewPositionIrreguliere {
    return form.getRawValue() as IPositionIrreguliere | NewPositionIrreguliere;
  }

  resetForm(form: PositionIrreguliereFormGroup, positionIrreguliere: PositionIrreguliereFormGroupInput): void {
    const positionIrreguliereRawValue = { ...this.getFormDefaults(), ...positionIrreguliere };
    form.reset(
      {
        ...positionIrreguliereRawValue,
        id: { value: positionIrreguliereRawValue.id, disabled: true },
      } as any /* cast to workaround https://github.com/angular/angular/issues/46458 */
    );
  }

  private getFormDefaults(): PositionIrreguliereFormDefaults {
    return {
      id: null,
    };
  }
}

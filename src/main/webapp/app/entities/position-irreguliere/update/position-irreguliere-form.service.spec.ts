import { TestBed } from '@angular/core/testing';

import { sampleWithRequiredData, sampleWithNewData } from '../position-irreguliere.test-samples';

import { PositionIrreguliereFormService } from './position-irreguliere-form.service';

describe('PositionIrreguliere Form Service', () => {
  let service: PositionIrreguliereFormService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PositionIrreguliereFormService);
  });

  describe('Service methods', () => {
    describe('createPositionIrreguliereFormGroup', () => {
      it('should create a new form with FormControl', () => {
        const formGroup = service.createPositionIrreguliereFormGroup();

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            dateDebut: expect.any(Object),
            dateFin: expect.any(Object),
            naturePositionIrreguliere: expect.any(Object),
            employe: expect.any(Object),
          })
        );
      });

      it('passing IPositionIrreguliere should create a new form with FormGroup', () => {
        const formGroup = service.createPositionIrreguliereFormGroup(sampleWithRequiredData);

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            dateDebut: expect.any(Object),
            dateFin: expect.any(Object),
            naturePositionIrreguliere: expect.any(Object),
            employe: expect.any(Object),
          })
        );
      });
    });

    describe('getPositionIrreguliere', () => {
      it('should return NewPositionIrreguliere for default PositionIrreguliere initial value', () => {
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        const formGroup = service.createPositionIrreguliereFormGroup(sampleWithNewData);

        const positionIrreguliere = service.getPositionIrreguliere(formGroup) as any;

        expect(positionIrreguliere).toMatchObject(sampleWithNewData);
      });

      it('should return NewPositionIrreguliere for empty PositionIrreguliere initial value', () => {
        const formGroup = service.createPositionIrreguliereFormGroup();

        const positionIrreguliere = service.getPositionIrreguliere(formGroup) as any;

        expect(positionIrreguliere).toMatchObject({});
      });

      it('should return IPositionIrreguliere', () => {
        const formGroup = service.createPositionIrreguliereFormGroup(sampleWithRequiredData);

        const positionIrreguliere = service.getPositionIrreguliere(formGroup) as any;

        expect(positionIrreguliere).toMatchObject(sampleWithRequiredData);
      });
    });

    describe('resetForm', () => {
      it('passing IPositionIrreguliere should not enable id FormControl', () => {
        const formGroup = service.createPositionIrreguliereFormGroup();
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, sampleWithRequiredData);

        expect(formGroup.controls.id.disabled).toBe(true);
      });

      it('passing NewPositionIrreguliere should disable id FormControl', () => {
        const formGroup = service.createPositionIrreguliereFormGroup(sampleWithRequiredData);
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, { id: null });

        expect(formGroup.controls.id.disabled).toBe(true);
      });
    });
  });
});

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { PositionIrreguliereFormService } from './position-irreguliere-form.service';
import { PositionIrreguliereService } from '../service/position-irreguliere.service';
import { IPositionIrreguliere } from '../position-irreguliere.model';
import { INaturePositionIrreguliere } from 'app/entities/nature-position-irreguliere/nature-position-irreguliere.model';
import { NaturePositionIrreguliereService } from 'app/entities/nature-position-irreguliere/service/nature-position-irreguliere.service';
import { IEmploye } from 'app/entities/employe/employe.model';
import { EmployeService } from 'app/entities/employe/service/employe.service';

import { PositionIrreguliereUpdateComponent } from './position-irreguliere-update.component';

describe('PositionIrreguliere Management Update Component', () => {
  let comp: PositionIrreguliereUpdateComponent;
  let fixture: ComponentFixture<PositionIrreguliereUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let positionIrreguliereFormService: PositionIrreguliereFormService;
  let positionIrreguliereService: PositionIrreguliereService;
  let naturePositionIrreguliereService: NaturePositionIrreguliereService;
  let employeService: EmployeService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      declarations: [PositionIrreguliereUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(PositionIrreguliereUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(PositionIrreguliereUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    positionIrreguliereFormService = TestBed.inject(PositionIrreguliereFormService);
    positionIrreguliereService = TestBed.inject(PositionIrreguliereService);
    naturePositionIrreguliereService = TestBed.inject(NaturePositionIrreguliereService);
    employeService = TestBed.inject(EmployeService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call NaturePositionIrreguliere query and add missing value', () => {
      const positionIrreguliere: IPositionIrreguliere = { id: 456 };
      const naturePositionIrreguliere: INaturePositionIrreguliere = { id: 61996 };
      positionIrreguliere.naturePositionIrreguliere = naturePositionIrreguliere;

      const naturePositionIrreguliereCollection: INaturePositionIrreguliere[] = [{ id: 69329 }];
      jest
        .spyOn(naturePositionIrreguliereService, 'query')
        .mockReturnValue(of(new HttpResponse({ body: naturePositionIrreguliereCollection })));
      const additionalNaturePositionIrregulieres = [naturePositionIrreguliere];
      const expectedCollection: INaturePositionIrreguliere[] = [
        ...additionalNaturePositionIrregulieres,
        ...naturePositionIrreguliereCollection,
      ];
      jest.spyOn(naturePositionIrreguliereService, 'addNaturePositionIrreguliereToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ positionIrreguliere });
      comp.ngOnInit();

      expect(naturePositionIrreguliereService.query).toHaveBeenCalled();
      expect(naturePositionIrreguliereService.addNaturePositionIrreguliereToCollectionIfMissing).toHaveBeenCalledWith(
        naturePositionIrreguliereCollection,
        ...additionalNaturePositionIrregulieres.map(expect.objectContaining)
      );
      expect(comp.naturePositionIrregulieresSharedCollection).toEqual(expectedCollection);
    });

    it('Should call Employe query and add missing value', () => {
      const positionIrreguliere: IPositionIrreguliere = { id: 456 };
      const employe: IEmploye = { id: 52550 };
      positionIrreguliere.employe = employe;

      const employeCollection: IEmploye[] = [{ id: 75051 }];
      jest.spyOn(employeService, 'query').mockReturnValue(of(new HttpResponse({ body: employeCollection })));
      const additionalEmployes = [employe];
      const expectedCollection: IEmploye[] = [...additionalEmployes, ...employeCollection];
      jest.spyOn(employeService, 'addEmployeToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ positionIrreguliere });
      comp.ngOnInit();

      expect(employeService.query).toHaveBeenCalled();
      expect(employeService.addEmployeToCollectionIfMissing).toHaveBeenCalledWith(
        employeCollection,
        ...additionalEmployes.map(expect.objectContaining)
      );
      expect(comp.employesSharedCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const positionIrreguliere: IPositionIrreguliere = { id: 456 };
      const naturePositionIrreguliere: INaturePositionIrreguliere = { id: 30715 };
      positionIrreguliere.naturePositionIrreguliere = naturePositionIrreguliere;
      const employe: IEmploye = { id: 96861 };
      positionIrreguliere.employe = employe;

      activatedRoute.data = of({ positionIrreguliere });
      comp.ngOnInit();

      expect(comp.naturePositionIrregulieresSharedCollection).toContain(naturePositionIrreguliere);
      expect(comp.employesSharedCollection).toContain(employe);
      expect(comp.positionIrreguliere).toEqual(positionIrreguliere);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IPositionIrreguliere>>();
      const positionIrreguliere = { id: 123 };
      jest.spyOn(positionIrreguliereFormService, 'getPositionIrreguliere').mockReturnValue(positionIrreguliere);
      jest.spyOn(positionIrreguliereService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ positionIrreguliere });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: positionIrreguliere }));
      saveSubject.complete();

      // THEN
      expect(positionIrreguliereFormService.getPositionIrreguliere).toHaveBeenCalled();
      expect(comp.previousState).toHaveBeenCalled();
      expect(positionIrreguliereService.update).toHaveBeenCalledWith(expect.objectContaining(positionIrreguliere));
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IPositionIrreguliere>>();
      const positionIrreguliere = { id: 123 };
      jest.spyOn(positionIrreguliereFormService, 'getPositionIrreguliere').mockReturnValue({ id: null });
      jest.spyOn(positionIrreguliereService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ positionIrreguliere: null });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: positionIrreguliere }));
      saveSubject.complete();

      // THEN
      expect(positionIrreguliereFormService.getPositionIrreguliere).toHaveBeenCalled();
      expect(positionIrreguliereService.create).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IPositionIrreguliere>>();
      const positionIrreguliere = { id: 123 };
      jest.spyOn(positionIrreguliereService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ positionIrreguliere });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(positionIrreguliereService.update).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Compare relationships', () => {
    describe('compareNaturePositionIrreguliere', () => {
      it('Should forward to naturePositionIrreguliereService', () => {
        const entity = { id: 123 };
        const entity2 = { id: 456 };
        jest.spyOn(naturePositionIrreguliereService, 'compareNaturePositionIrreguliere');
        comp.compareNaturePositionIrreguliere(entity, entity2);
        expect(naturePositionIrreguliereService.compareNaturePositionIrreguliere).toHaveBeenCalledWith(entity, entity2);
      });
    });

    describe('compareEmploye', () => {
      it('Should forward to employeService', () => {
        const entity = { id: 123 };
        const entity2 = { id: 456 };
        jest.spyOn(employeService, 'compareEmploye');
        comp.compareEmploye(entity, entity2);
        expect(employeService.compareEmploye).toHaveBeenCalledWith(entity, entity2);
      });
    });
  });
});

import dayjs from 'dayjs/esm';

import { IPositionIrreguliere, NewPositionIrreguliere } from './position-irreguliere.model';

export const sampleWithRequiredData: IPositionIrreguliere = {
  id: 88715,
};

export const sampleWithPartialData: IPositionIrreguliere = {
  id: 72256,
  dateDebut: dayjs('2022-09-07'),
  dateFin: dayjs('2022-09-07'),
};

export const sampleWithFullData: IPositionIrreguliere = {
  id: 1146,
  dateDebut: dayjs('2022-09-06'),
  dateFin: dayjs('2022-09-06'),
};

export const sampleWithNewData: NewPositionIrreguliere = {
  id: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);

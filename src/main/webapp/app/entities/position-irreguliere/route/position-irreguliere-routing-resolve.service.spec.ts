import { TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRouteSnapshot, ActivatedRoute, Router, convertToParamMap } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';

import { IPositionIrreguliere } from '../position-irreguliere.model';
import { PositionIrreguliereService } from '../service/position-irreguliere.service';

import { PositionIrreguliereRoutingResolveService } from './position-irreguliere-routing-resolve.service';

describe('PositionIrreguliere routing resolve service', () => {
  let mockRouter: Router;
  let mockActivatedRouteSnapshot: ActivatedRouteSnapshot;
  let routingResolveService: PositionIrreguliereRoutingResolveService;
  let service: PositionIrreguliereService;
  let resultPositionIrreguliere: IPositionIrreguliere | null | undefined;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            snapshot: {
              paramMap: convertToParamMap({}),
            },
          },
        },
      ],
    });
    mockRouter = TestBed.inject(Router);
    jest.spyOn(mockRouter, 'navigate').mockImplementation(() => Promise.resolve(true));
    mockActivatedRouteSnapshot = TestBed.inject(ActivatedRoute).snapshot;
    routingResolveService = TestBed.inject(PositionIrreguliereRoutingResolveService);
    service = TestBed.inject(PositionIrreguliereService);
    resultPositionIrreguliere = undefined;
  });

  describe('resolve', () => {
    it('should return IPositionIrreguliere returned by find', () => {
      // GIVEN
      service.find = jest.fn(id => of(new HttpResponse({ body: { id } })));
      mockActivatedRouteSnapshot.params = { id: 123 };

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultPositionIrreguliere = result;
      });

      // THEN
      expect(service.find).toBeCalledWith(123);
      expect(resultPositionIrreguliere).toEqual({ id: 123 });
    });

    it('should return null if id is not provided', () => {
      // GIVEN
      service.find = jest.fn();
      mockActivatedRouteSnapshot.params = {};

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultPositionIrreguliere = result;
      });

      // THEN
      expect(service.find).not.toBeCalled();
      expect(resultPositionIrreguliere).toEqual(null);
    });

    it('should route to 404 page if data not found in server', () => {
      // GIVEN
      jest.spyOn(service, 'find').mockReturnValue(of(new HttpResponse<IPositionIrreguliere>({ body: null })));
      mockActivatedRouteSnapshot.params = { id: 123 };

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultPositionIrreguliere = result;
      });

      // THEN
      expect(service.find).toBeCalledWith(123);
      expect(resultPositionIrreguliere).toEqual(undefined);
      expect(mockRouter.navigate).toHaveBeenCalledWith(['404']);
    });
  });
});

import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IPositionIrreguliere } from '../position-irreguliere.model';
import { PositionIrreguliereService } from '../service/position-irreguliere.service';

@Injectable({ providedIn: 'root' })
export class PositionIrreguliereRoutingResolveService implements Resolve<IPositionIrreguliere | null> {
  constructor(protected service: PositionIrreguliereService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IPositionIrreguliere | null | never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((positionIrreguliere: HttpResponse<IPositionIrreguliere>) => {
          if (positionIrreguliere.body) {
            return of(positionIrreguliere.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(null);
  }
}

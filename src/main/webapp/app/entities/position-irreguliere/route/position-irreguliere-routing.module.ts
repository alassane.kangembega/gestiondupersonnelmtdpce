import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { PositionIrreguliereComponent } from '../list/position-irreguliere.component';
import { PositionIrreguliereDetailComponent } from '../detail/position-irreguliere-detail.component';
import { PositionIrreguliereUpdateComponent } from '../update/position-irreguliere-update.component';
import { PositionIrreguliereRoutingResolveService } from './position-irreguliere-routing-resolve.service';
import { ASC } from 'app/config/navigation.constants';

const positionIrreguliereRoute: Routes = [
  {
    path: '',
    component: PositionIrreguliereComponent,
    data: {
      defaultSort: 'id,' + ASC,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: PositionIrreguliereDetailComponent,
    resolve: {
      positionIrreguliere: PositionIrreguliereRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: PositionIrreguliereUpdateComponent,
    resolve: {
      positionIrreguliere: PositionIrreguliereRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: PositionIrreguliereUpdateComponent,
    resolve: {
      positionIrreguliere: PositionIrreguliereRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(positionIrreguliereRoute)],
  exports: [RouterModule],
})
export class PositionIrreguliereRoutingModule {}

import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { DATE_FORMAT } from 'app/config/input.constants';
import { IPositionIrreguliere } from '../position-irreguliere.model';
import { sampleWithRequiredData, sampleWithNewData, sampleWithPartialData, sampleWithFullData } from '../position-irreguliere.test-samples';

import { PositionIrreguliereService, RestPositionIrreguliere } from './position-irreguliere.service';

const requireRestSample: RestPositionIrreguliere = {
  ...sampleWithRequiredData,
  dateDebut: sampleWithRequiredData.dateDebut?.format(DATE_FORMAT),
  dateFin: sampleWithRequiredData.dateFin?.format(DATE_FORMAT),
};

describe('PositionIrreguliere Service', () => {
  let service: PositionIrreguliereService;
  let httpMock: HttpTestingController;
  let expectedResult: IPositionIrreguliere | IPositionIrreguliere[] | boolean | null;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(PositionIrreguliereService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should create a PositionIrreguliere', () => {
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      const positionIrreguliere = { ...sampleWithNewData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.create(positionIrreguliere).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a PositionIrreguliere', () => {
      const positionIrreguliere = { ...sampleWithRequiredData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.update(positionIrreguliere).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a PositionIrreguliere', () => {
      const patchObject = { ...sampleWithPartialData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of PositionIrreguliere', () => {
      const returnedFromService = { ...requireRestSample };

      const expected = { ...sampleWithRequiredData };

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toMatchObject([expected]);
    });

    it('should delete a PositionIrreguliere', () => {
      const expected = true;

      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult).toBe(expected);
    });

    describe('addPositionIrreguliereToCollectionIfMissing', () => {
      it('should add a PositionIrreguliere to an empty array', () => {
        const positionIrreguliere: IPositionIrreguliere = sampleWithRequiredData;
        expectedResult = service.addPositionIrreguliereToCollectionIfMissing([], positionIrreguliere);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(positionIrreguliere);
      });

      it('should not add a PositionIrreguliere to an array that contains it', () => {
        const positionIrreguliere: IPositionIrreguliere = sampleWithRequiredData;
        const positionIrreguliereCollection: IPositionIrreguliere[] = [
          {
            ...positionIrreguliere,
          },
          sampleWithPartialData,
        ];
        expectedResult = service.addPositionIrreguliereToCollectionIfMissing(positionIrreguliereCollection, positionIrreguliere);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a PositionIrreguliere to an array that doesn't contain it", () => {
        const positionIrreguliere: IPositionIrreguliere = sampleWithRequiredData;
        const positionIrreguliereCollection: IPositionIrreguliere[] = [sampleWithPartialData];
        expectedResult = service.addPositionIrreguliereToCollectionIfMissing(positionIrreguliereCollection, positionIrreguliere);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(positionIrreguliere);
      });

      it('should add only unique PositionIrreguliere to an array', () => {
        const positionIrreguliereArray: IPositionIrreguliere[] = [sampleWithRequiredData, sampleWithPartialData, sampleWithFullData];
        const positionIrreguliereCollection: IPositionIrreguliere[] = [sampleWithRequiredData];
        expectedResult = service.addPositionIrreguliereToCollectionIfMissing(positionIrreguliereCollection, ...positionIrreguliereArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const positionIrreguliere: IPositionIrreguliere = sampleWithRequiredData;
        const positionIrreguliere2: IPositionIrreguliere = sampleWithPartialData;
        expectedResult = service.addPositionIrreguliereToCollectionIfMissing([], positionIrreguliere, positionIrreguliere2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(positionIrreguliere);
        expect(expectedResult).toContain(positionIrreguliere2);
      });

      it('should accept null and undefined values', () => {
        const positionIrreguliere: IPositionIrreguliere = sampleWithRequiredData;
        expectedResult = service.addPositionIrreguliereToCollectionIfMissing([], null, positionIrreguliere, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(positionIrreguliere);
      });

      it('should return initial array if no PositionIrreguliere is added', () => {
        const positionIrreguliereCollection: IPositionIrreguliere[] = [sampleWithRequiredData];
        expectedResult = service.addPositionIrreguliereToCollectionIfMissing(positionIrreguliereCollection, undefined, null);
        expect(expectedResult).toEqual(positionIrreguliereCollection);
      });
    });

    describe('comparePositionIrreguliere', () => {
      it('Should return true if both entities are null', () => {
        const entity1 = null;
        const entity2 = null;

        const compareResult = service.comparePositionIrreguliere(entity1, entity2);

        expect(compareResult).toEqual(true);
      });

      it('Should return false if one entity is null', () => {
        const entity1 = { id: 123 };
        const entity2 = null;

        const compareResult1 = service.comparePositionIrreguliere(entity1, entity2);
        const compareResult2 = service.comparePositionIrreguliere(entity2, entity1);

        expect(compareResult1).toEqual(false);
        expect(compareResult2).toEqual(false);
      });

      it('Should return false if primaryKey differs', () => {
        const entity1 = { id: 123 };
        const entity2 = { id: 456 };

        const compareResult1 = service.comparePositionIrreguliere(entity1, entity2);
        const compareResult2 = service.comparePositionIrreguliere(entity2, entity1);

        expect(compareResult1).toEqual(false);
        expect(compareResult2).toEqual(false);
      });

      it('Should return false if primaryKey matches', () => {
        const entity1 = { id: 123 };
        const entity2 = { id: 123 };

        const compareResult1 = service.comparePositionIrreguliere(entity1, entity2);
        const compareResult2 = service.comparePositionIrreguliere(entity2, entity1);

        expect(compareResult1).toEqual(true);
        expect(compareResult2).toEqual(true);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});

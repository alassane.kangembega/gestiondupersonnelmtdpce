import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import dayjs from 'dayjs/esm';

import { isPresent } from 'app/core/util/operators';
import { DATE_FORMAT } from 'app/config/input.constants';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IPositionIrreguliere, NewPositionIrreguliere } from '../position-irreguliere.model';

export type PartialUpdatePositionIrreguliere = Partial<IPositionIrreguliere> & Pick<IPositionIrreguliere, 'id'>;

type RestOf<T extends IPositionIrreguliere | NewPositionIrreguliere> = Omit<T, 'dateDebut' | 'dateFin'> & {
  dateDebut?: string | null;
  dateFin?: string | null;
};

export type RestPositionIrreguliere = RestOf<IPositionIrreguliere>;

export type NewRestPositionIrreguliere = RestOf<NewPositionIrreguliere>;

export type PartialUpdateRestPositionIrreguliere = RestOf<PartialUpdatePositionIrreguliere>;

export type EntityResponseType = HttpResponse<IPositionIrreguliere>;
export type EntityArrayResponseType = HttpResponse<IPositionIrreguliere[]>;

@Injectable({ providedIn: 'root' })
export class PositionIrreguliereService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/position-irregulieres');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(positionIrreguliere: NewPositionIrreguliere): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(positionIrreguliere);
    return this.http
      .post<RestPositionIrreguliere>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  update(positionIrreguliere: IPositionIrreguliere): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(positionIrreguliere);
    return this.http
      .put<RestPositionIrreguliere>(`${this.resourceUrl}/${this.getPositionIrreguliereIdentifier(positionIrreguliere)}`, copy, {
        observe: 'response',
      })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  partialUpdate(positionIrreguliere: PartialUpdatePositionIrreguliere): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(positionIrreguliere);
    return this.http
      .patch<RestPositionIrreguliere>(`${this.resourceUrl}/${this.getPositionIrreguliereIdentifier(positionIrreguliere)}`, copy, {
        observe: 'response',
      })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<RestPositionIrreguliere>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<RestPositionIrreguliere[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map(res => this.convertResponseArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  getPositionIrreguliereIdentifier(positionIrreguliere: Pick<IPositionIrreguliere, 'id'>): number {
    return positionIrreguliere.id;
  }

  comparePositionIrreguliere(o1: Pick<IPositionIrreguliere, 'id'> | null, o2: Pick<IPositionIrreguliere, 'id'> | null): boolean {
    return o1 && o2 ? this.getPositionIrreguliereIdentifier(o1) === this.getPositionIrreguliereIdentifier(o2) : o1 === o2;
  }

  addPositionIrreguliereToCollectionIfMissing<Type extends Pick<IPositionIrreguliere, 'id'>>(
    positionIrreguliereCollection: Type[],
    ...positionIrregulieresToCheck: (Type | null | undefined)[]
  ): Type[] {
    const positionIrregulieres: Type[] = positionIrregulieresToCheck.filter(isPresent);
    if (positionIrregulieres.length > 0) {
      const positionIrreguliereCollectionIdentifiers = positionIrreguliereCollection.map(
        positionIrreguliereItem => this.getPositionIrreguliereIdentifier(positionIrreguliereItem)!
      );
      const positionIrregulieresToAdd = positionIrregulieres.filter(positionIrreguliereItem => {
        const positionIrreguliereIdentifier = this.getPositionIrreguliereIdentifier(positionIrreguliereItem);
        if (positionIrreguliereCollectionIdentifiers.includes(positionIrreguliereIdentifier)) {
          return false;
        }
        positionIrreguliereCollectionIdentifiers.push(positionIrreguliereIdentifier);
        return true;
      });
      return [...positionIrregulieresToAdd, ...positionIrreguliereCollection];
    }
    return positionIrreguliereCollection;
  }

  protected convertDateFromClient<T extends IPositionIrreguliere | NewPositionIrreguliere | PartialUpdatePositionIrreguliere>(
    positionIrreguliere: T
  ): RestOf<T> {
    return {
      ...positionIrreguliere,
      dateDebut: positionIrreguliere.dateDebut?.format(DATE_FORMAT) ?? null,
      dateFin: positionIrreguliere.dateFin?.format(DATE_FORMAT) ?? null,
    };
  }

  protected convertDateFromServer(restPositionIrreguliere: RestPositionIrreguliere): IPositionIrreguliere {
    return {
      ...restPositionIrreguliere,
      dateDebut: restPositionIrreguliere.dateDebut ? dayjs(restPositionIrreguliere.dateDebut) : undefined,
      dateFin: restPositionIrreguliere.dateFin ? dayjs(restPositionIrreguliere.dateFin) : undefined,
    };
  }

  protected convertResponseFromServer(res: HttpResponse<RestPositionIrreguliere>): HttpResponse<IPositionIrreguliere> {
    return res.clone({
      body: res.body ? this.convertDateFromServer(res.body) : null,
    });
  }

  protected convertResponseArrayFromServer(res: HttpResponse<RestPositionIrreguliere[]>): HttpResponse<IPositionIrreguliere[]> {
    return res.clone({
      body: res.body ? res.body.map(item => this.convertDateFromServer(item)) : null,
    });
  }
}

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';

import { PositionIrreguliereService } from '../service/position-irreguliere.service';

import { PositionIrreguliereComponent } from './position-irreguliere.component';

describe('PositionIrreguliere Management Component', () => {
  let comp: PositionIrreguliereComponent;
  let fixture: ComponentFixture<PositionIrreguliereComponent>;
  let service: PositionIrreguliereService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule.withRoutes([{ path: 'position-irreguliere', component: PositionIrreguliereComponent }]),
        HttpClientTestingModule,
      ],
      declarations: [PositionIrreguliereComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            data: of({
              defaultSort: 'id,asc',
            }),
            queryParamMap: of(
              jest.requireActual('@angular/router').convertToParamMap({
                page: '1',
                size: '1',
                sort: 'id,desc',
              })
            ),
            snapshot: { queryParams: {} },
          },
        },
      ],
    })
      .overrideTemplate(PositionIrreguliereComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(PositionIrreguliereComponent);
    comp = fixture.componentInstance;
    service = TestBed.inject(PositionIrreguliereService);

    const headers = new HttpHeaders();
    jest.spyOn(service, 'query').mockReturnValue(
      of(
        new HttpResponse({
          body: [{ id: 123 }],
          headers,
        })
      )
    );
  });

  it('Should call load all on init', () => {
    // WHEN
    comp.ngOnInit();

    // THEN
    expect(service.query).toHaveBeenCalled();
    expect(comp.positionIrregulieres?.[0]).toEqual(expect.objectContaining({ id: 123 }));
  });

  describe('trackId', () => {
    it('Should forward to positionIrreguliereService', () => {
      const entity = { id: 123 };
      jest.spyOn(service, 'getPositionIrreguliereIdentifier');
      const id = comp.trackId(0, entity);
      expect(service.getPositionIrreguliereIdentifier).toHaveBeenCalledWith(entity);
      expect(id).toBe(entity.id);
    });
  });
});

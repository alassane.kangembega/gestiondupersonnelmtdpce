jest.mock('@ng-bootstrap/ng-bootstrap');

import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of } from 'rxjs';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { PositionIrreguliereService } from '../service/position-irreguliere.service';

import { PositionIrreguliereDeleteDialogComponent } from './position-irreguliere-delete-dialog.component';

describe('PositionIrreguliere Management Delete Component', () => {
  let comp: PositionIrreguliereDeleteDialogComponent;
  let fixture: ComponentFixture<PositionIrreguliereDeleteDialogComponent>;
  let service: PositionIrreguliereService;
  let mockActiveModal: NgbActiveModal;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [PositionIrreguliereDeleteDialogComponent],
      providers: [NgbActiveModal],
    })
      .overrideTemplate(PositionIrreguliereDeleteDialogComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(PositionIrreguliereDeleteDialogComponent);
    comp = fixture.componentInstance;
    service = TestBed.inject(PositionIrreguliereService);
    mockActiveModal = TestBed.inject(NgbActiveModal);
  });

  describe('confirmDelete', () => {
    it('Should call delete service on confirmDelete', inject(
      [],
      fakeAsync(() => {
        // GIVEN
        jest.spyOn(service, 'delete').mockReturnValue(of(new HttpResponse({ body: {} })));

        // WHEN
        comp.confirmDelete(123);
        tick();

        // THEN
        expect(service.delete).toHaveBeenCalledWith(123);
        expect(mockActiveModal.close).toHaveBeenCalledWith('deleted');
      })
    ));

    it('Should not call delete service on clear', () => {
      // GIVEN
      jest.spyOn(service, 'delete');

      // WHEN
      comp.cancel();

      // THEN
      expect(service.delete).not.toHaveBeenCalled();
      expect(mockActiveModal.close).not.toHaveBeenCalled();
      expect(mockActiveModal.dismiss).toHaveBeenCalled();
    });
  });
});

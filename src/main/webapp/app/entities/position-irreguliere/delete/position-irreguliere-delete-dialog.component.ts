import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IPositionIrreguliere } from '../position-irreguliere.model';
import { PositionIrreguliereService } from '../service/position-irreguliere.service';
import { ITEM_DELETED_EVENT } from 'app/config/navigation.constants';

@Component({
  templateUrl: './position-irreguliere-delete-dialog.component.html',
})
export class PositionIrreguliereDeleteDialogComponent {
  positionIrreguliere?: IPositionIrreguliere;

  constructor(protected positionIrreguliereService: PositionIrreguliereService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.positionIrreguliereService.delete(id).subscribe(() => {
      this.activeModal.close(ITEM_DELETED_EVENT);
    });
  }
}

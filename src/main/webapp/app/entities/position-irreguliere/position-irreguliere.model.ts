import dayjs from 'dayjs/esm';
import { INaturePositionIrreguliere } from 'app/entities/nature-position-irreguliere/nature-position-irreguliere.model';
import { IEmploye } from 'app/entities/employe/employe.model';

export interface IPositionIrreguliere {
  id: number;
  dateDebut?: dayjs.Dayjs | null;
  dateFin?: dayjs.Dayjs | null;
  naturePositionIrreguliere?: Pick<INaturePositionIrreguliere, 'id'> | null;
  employe?: Pick<IEmploye, 'id'> | null;
}

export type NewPositionIrreguliere = Omit<IPositionIrreguliere, 'id'> & { id: null };

import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { PositionIrreguliereComponent } from './list/position-irreguliere.component';
import { PositionIrreguliereDetailComponent } from './detail/position-irreguliere-detail.component';
import { PositionIrreguliereUpdateComponent } from './update/position-irreguliere-update.component';
import { PositionIrreguliereDeleteDialogComponent } from './delete/position-irreguliere-delete-dialog.component';
import { PositionIrreguliereRoutingModule } from './route/position-irreguliere-routing.module';

@NgModule({
  imports: [SharedModule, PositionIrreguliereRoutingModule],
  declarations: [
    PositionIrreguliereComponent,
    PositionIrreguliereDetailComponent,
    PositionIrreguliereUpdateComponent,
    PositionIrreguliereDeleteDialogComponent,
  ],
})
export class PositionIrreguliereModule {}

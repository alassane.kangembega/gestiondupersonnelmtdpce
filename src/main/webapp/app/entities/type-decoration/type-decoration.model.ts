export interface ITypeDecoration {
  id: number;
  libelle?: string | null;
}

export type NewTypeDecoration = Omit<ITypeDecoration, 'id'> & { id: null };

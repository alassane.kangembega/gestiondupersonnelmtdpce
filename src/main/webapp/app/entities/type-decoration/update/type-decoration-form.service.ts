import { Injectable } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { ITypeDecoration, NewTypeDecoration } from '../type-decoration.model';

/**
 * A partial Type with required key is used as form input.
 */
type PartialWithRequiredKeyOf<T extends { id: unknown }> = Partial<Omit<T, 'id'>> & { id: T['id'] };

/**
 * Type for createFormGroup and resetForm argument.
 * It accepts ITypeDecoration for edit and NewTypeDecorationFormGroupInput for create.
 */
type TypeDecorationFormGroupInput = ITypeDecoration | PartialWithRequiredKeyOf<NewTypeDecoration>;

type TypeDecorationFormDefaults = Pick<NewTypeDecoration, 'id'>;

type TypeDecorationFormGroupContent = {
  id: FormControl<ITypeDecoration['id'] | NewTypeDecoration['id']>;
  libelle: FormControl<ITypeDecoration['libelle']>;
};

export type TypeDecorationFormGroup = FormGroup<TypeDecorationFormGroupContent>;

@Injectable({ providedIn: 'root' })
export class TypeDecorationFormService {
  createTypeDecorationFormGroup(typeDecoration: TypeDecorationFormGroupInput = { id: null }): TypeDecorationFormGroup {
    const typeDecorationRawValue = {
      ...this.getFormDefaults(),
      ...typeDecoration,
    };
    return new FormGroup<TypeDecorationFormGroupContent>({
      id: new FormControl(
        { value: typeDecorationRawValue.id, disabled: true },
        {
          nonNullable: true,
          validators: [Validators.required],
        }
      ),
      libelle: new FormControl(typeDecorationRawValue.libelle),
    });
  }

  getTypeDecoration(form: TypeDecorationFormGroup): ITypeDecoration | NewTypeDecoration {
    return form.getRawValue() as ITypeDecoration | NewTypeDecoration;
  }

  resetForm(form: TypeDecorationFormGroup, typeDecoration: TypeDecorationFormGroupInput): void {
    const typeDecorationRawValue = { ...this.getFormDefaults(), ...typeDecoration };
    form.reset(
      {
        ...typeDecorationRawValue,
        id: { value: typeDecorationRawValue.id, disabled: true },
      } as any /* cast to workaround https://github.com/angular/angular/issues/46458 */
    );
  }

  private getFormDefaults(): TypeDecorationFormDefaults {
    return {
      id: null,
    };
  }
}

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { TypeDecorationFormService } from './type-decoration-form.service';
import { TypeDecorationService } from '../service/type-decoration.service';
import { ITypeDecoration } from '../type-decoration.model';

import { TypeDecorationUpdateComponent } from './type-decoration-update.component';

describe('TypeDecoration Management Update Component', () => {
  let comp: TypeDecorationUpdateComponent;
  let fixture: ComponentFixture<TypeDecorationUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let typeDecorationFormService: TypeDecorationFormService;
  let typeDecorationService: TypeDecorationService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      declarations: [TypeDecorationUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(TypeDecorationUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(TypeDecorationUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    typeDecorationFormService = TestBed.inject(TypeDecorationFormService);
    typeDecorationService = TestBed.inject(TypeDecorationService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should update editForm', () => {
      const typeDecoration: ITypeDecoration = { id: 456 };

      activatedRoute.data = of({ typeDecoration });
      comp.ngOnInit();

      expect(comp.typeDecoration).toEqual(typeDecoration);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<ITypeDecoration>>();
      const typeDecoration = { id: 123 };
      jest.spyOn(typeDecorationFormService, 'getTypeDecoration').mockReturnValue(typeDecoration);
      jest.spyOn(typeDecorationService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ typeDecoration });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: typeDecoration }));
      saveSubject.complete();

      // THEN
      expect(typeDecorationFormService.getTypeDecoration).toHaveBeenCalled();
      expect(comp.previousState).toHaveBeenCalled();
      expect(typeDecorationService.update).toHaveBeenCalledWith(expect.objectContaining(typeDecoration));
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<ITypeDecoration>>();
      const typeDecoration = { id: 123 };
      jest.spyOn(typeDecorationFormService, 'getTypeDecoration').mockReturnValue({ id: null });
      jest.spyOn(typeDecorationService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ typeDecoration: null });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: typeDecoration }));
      saveSubject.complete();

      // THEN
      expect(typeDecorationFormService.getTypeDecoration).toHaveBeenCalled();
      expect(typeDecorationService.create).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<ITypeDecoration>>();
      const typeDecoration = { id: 123 };
      jest.spyOn(typeDecorationService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ typeDecoration });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(typeDecorationService.update).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });
});

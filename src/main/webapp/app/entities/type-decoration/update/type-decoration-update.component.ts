import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

import { TypeDecorationFormService, TypeDecorationFormGroup } from './type-decoration-form.service';
import { ITypeDecoration } from '../type-decoration.model';
import { TypeDecorationService } from '../service/type-decoration.service';

@Component({
  selector: 'jhi-type-decoration-update',
  templateUrl: './type-decoration-update.component.html',
})
export class TypeDecorationUpdateComponent implements OnInit {
  isSaving = false;
  typeDecoration: ITypeDecoration | null = null;

  editForm: TypeDecorationFormGroup = this.typeDecorationFormService.createTypeDecorationFormGroup();

  constructor(
    protected typeDecorationService: TypeDecorationService,
    protected typeDecorationFormService: TypeDecorationFormService,
    protected activatedRoute: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ typeDecoration }) => {
      this.typeDecoration = typeDecoration;
      if (typeDecoration) {
        this.updateForm(typeDecoration);
      }
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const typeDecoration = this.typeDecorationFormService.getTypeDecoration(this.editForm);
    if (typeDecoration.id !== null) {
      this.subscribeToSaveResponse(this.typeDecorationService.update(typeDecoration));
    } else {
      this.subscribeToSaveResponse(this.typeDecorationService.create(typeDecoration));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ITypeDecoration>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(typeDecoration: ITypeDecoration): void {
    this.typeDecoration = typeDecoration;
    this.typeDecorationFormService.resetForm(this.editForm, typeDecoration);
  }
}

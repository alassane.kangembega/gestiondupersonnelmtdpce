import { TestBed } from '@angular/core/testing';

import { sampleWithRequiredData, sampleWithNewData } from '../type-decoration.test-samples';

import { TypeDecorationFormService } from './type-decoration-form.service';

describe('TypeDecoration Form Service', () => {
  let service: TypeDecorationFormService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TypeDecorationFormService);
  });

  describe('Service methods', () => {
    describe('createTypeDecorationFormGroup', () => {
      it('should create a new form with FormControl', () => {
        const formGroup = service.createTypeDecorationFormGroup();

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            libelle: expect.any(Object),
          })
        );
      });

      it('passing ITypeDecoration should create a new form with FormGroup', () => {
        const formGroup = service.createTypeDecorationFormGroup(sampleWithRequiredData);

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            libelle: expect.any(Object),
          })
        );
      });
    });

    describe('getTypeDecoration', () => {
      it('should return NewTypeDecoration for default TypeDecoration initial value', () => {
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        const formGroup = service.createTypeDecorationFormGroup(sampleWithNewData);

        const typeDecoration = service.getTypeDecoration(formGroup) as any;

        expect(typeDecoration).toMatchObject(sampleWithNewData);
      });

      it('should return NewTypeDecoration for empty TypeDecoration initial value', () => {
        const formGroup = service.createTypeDecorationFormGroup();

        const typeDecoration = service.getTypeDecoration(formGroup) as any;

        expect(typeDecoration).toMatchObject({});
      });

      it('should return ITypeDecoration', () => {
        const formGroup = service.createTypeDecorationFormGroup(sampleWithRequiredData);

        const typeDecoration = service.getTypeDecoration(formGroup) as any;

        expect(typeDecoration).toMatchObject(sampleWithRequiredData);
      });
    });

    describe('resetForm', () => {
      it('passing ITypeDecoration should not enable id FormControl', () => {
        const formGroup = service.createTypeDecorationFormGroup();
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, sampleWithRequiredData);

        expect(formGroup.controls.id.disabled).toBe(true);
      });

      it('passing NewTypeDecoration should disable id FormControl', () => {
        const formGroup = service.createTypeDecorationFormGroup(sampleWithRequiredData);
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, { id: null });

        expect(formGroup.controls.id.disabled).toBe(true);
      });
    });
  });
});

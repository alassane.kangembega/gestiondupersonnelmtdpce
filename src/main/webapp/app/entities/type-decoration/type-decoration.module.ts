import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { TypeDecorationComponent } from './list/type-decoration.component';
import { TypeDecorationDetailComponent } from './detail/type-decoration-detail.component';
import { TypeDecorationUpdateComponent } from './update/type-decoration-update.component';
import { TypeDecorationDeleteDialogComponent } from './delete/type-decoration-delete-dialog.component';
import { TypeDecorationRoutingModule } from './route/type-decoration-routing.module';

@NgModule({
  imports: [SharedModule, TypeDecorationRoutingModule],
  declarations: [
    TypeDecorationComponent,
    TypeDecorationDetailComponent,
    TypeDecorationUpdateComponent,
    TypeDecorationDeleteDialogComponent,
  ],
})
export class TypeDecorationModule {}

import { ITypeDecoration, NewTypeDecoration } from './type-decoration.model';

export const sampleWithRequiredData: ITypeDecoration = {
  id: 66938,
};

export const sampleWithPartialData: ITypeDecoration = {
  id: 42684,
};

export const sampleWithFullData: ITypeDecoration = {
  id: 88788,
  libelle: 'Avon',
};

export const sampleWithNewData: NewTypeDecoration = {
  id: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);

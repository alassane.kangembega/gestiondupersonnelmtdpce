import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';

import { TypeDecorationService } from '../service/type-decoration.service';

import { TypeDecorationComponent } from './type-decoration.component';

describe('TypeDecoration Management Component', () => {
  let comp: TypeDecorationComponent;
  let fixture: ComponentFixture<TypeDecorationComponent>;
  let service: TypeDecorationService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule.withRoutes([{ path: 'type-decoration', component: TypeDecorationComponent }]), HttpClientTestingModule],
      declarations: [TypeDecorationComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            data: of({
              defaultSort: 'id,asc',
            }),
            queryParamMap: of(
              jest.requireActual('@angular/router').convertToParamMap({
                page: '1',
                size: '1',
                sort: 'id,desc',
              })
            ),
            snapshot: { queryParams: {} },
          },
        },
      ],
    })
      .overrideTemplate(TypeDecorationComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(TypeDecorationComponent);
    comp = fixture.componentInstance;
    service = TestBed.inject(TypeDecorationService);

    const headers = new HttpHeaders();
    jest.spyOn(service, 'query').mockReturnValue(
      of(
        new HttpResponse({
          body: [{ id: 123 }],
          headers,
        })
      )
    );
  });

  it('Should call load all on init', () => {
    // WHEN
    comp.ngOnInit();

    // THEN
    expect(service.query).toHaveBeenCalled();
    expect(comp.typeDecorations?.[0]).toEqual(expect.objectContaining({ id: 123 }));
  });

  describe('trackId', () => {
    it('Should forward to typeDecorationService', () => {
      const entity = { id: 123 };
      jest.spyOn(service, 'getTypeDecorationIdentifier');
      const id = comp.trackId(0, entity);
      expect(service.getTypeDecorationIdentifier).toHaveBeenCalledWith(entity);
      expect(id).toBe(entity.id);
    });
  });
});

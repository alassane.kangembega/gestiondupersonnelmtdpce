import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { ITypeDecoration } from '../type-decoration.model';
import { TypeDecorationService } from '../service/type-decoration.service';

@Injectable({ providedIn: 'root' })
export class TypeDecorationRoutingResolveService implements Resolve<ITypeDecoration | null> {
  constructor(protected service: TypeDecorationService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<ITypeDecoration | null | never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((typeDecoration: HttpResponse<ITypeDecoration>) => {
          if (typeDecoration.body) {
            return of(typeDecoration.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(null);
  }
}

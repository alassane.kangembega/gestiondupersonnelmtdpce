import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { TypeDecorationComponent } from '../list/type-decoration.component';
import { TypeDecorationDetailComponent } from '../detail/type-decoration-detail.component';
import { TypeDecorationUpdateComponent } from '../update/type-decoration-update.component';
import { TypeDecorationRoutingResolveService } from './type-decoration-routing-resolve.service';
import { ASC } from 'app/config/navigation.constants';

const typeDecorationRoute: Routes = [
  {
    path: '',
    component: TypeDecorationComponent,
    data: {
      defaultSort: 'id,' + ASC,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: TypeDecorationDetailComponent,
    resolve: {
      typeDecoration: TypeDecorationRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: TypeDecorationUpdateComponent,
    resolve: {
      typeDecoration: TypeDecorationRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: TypeDecorationUpdateComponent,
    resolve: {
      typeDecoration: TypeDecorationRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(typeDecorationRoute)],
  exports: [RouterModule],
})
export class TypeDecorationRoutingModule {}

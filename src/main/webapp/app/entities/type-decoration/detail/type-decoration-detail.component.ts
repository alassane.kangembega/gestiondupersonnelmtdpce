import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ITypeDecoration } from '../type-decoration.model';

@Component({
  selector: 'jhi-type-decoration-detail',
  templateUrl: './type-decoration-detail.component.html',
})
export class TypeDecorationDetailComponent implements OnInit {
  typeDecoration: ITypeDecoration | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ typeDecoration }) => {
      this.typeDecoration = typeDecoration;
    });
  }

  previousState(): void {
    window.history.back();
  }
}

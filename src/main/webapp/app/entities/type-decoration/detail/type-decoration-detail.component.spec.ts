import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { TypeDecorationDetailComponent } from './type-decoration-detail.component';

describe('TypeDecoration Management Detail Component', () => {
  let comp: TypeDecorationDetailComponent;
  let fixture: ComponentFixture<TypeDecorationDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [TypeDecorationDetailComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: { data: of({ typeDecoration: { id: 123 } }) },
        },
      ],
    })
      .overrideTemplate(TypeDecorationDetailComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(TypeDecorationDetailComponent);
    comp = fixture.componentInstance;
  });

  describe('OnInit', () => {
    it('Should load typeDecoration on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(comp.typeDecoration).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});

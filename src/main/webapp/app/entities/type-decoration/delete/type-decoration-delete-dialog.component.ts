import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { ITypeDecoration } from '../type-decoration.model';
import { TypeDecorationService } from '../service/type-decoration.service';
import { ITEM_DELETED_EVENT } from 'app/config/navigation.constants';

@Component({
  templateUrl: './type-decoration-delete-dialog.component.html',
})
export class TypeDecorationDeleteDialogComponent {
  typeDecoration?: ITypeDecoration;

  constructor(protected typeDecorationService: TypeDecorationService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.typeDecorationService.delete(id).subscribe(() => {
      this.activeModal.close(ITEM_DELETED_EVENT);
    });
  }
}

import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { ITypeDecoration, NewTypeDecoration } from '../type-decoration.model';

export type PartialUpdateTypeDecoration = Partial<ITypeDecoration> & Pick<ITypeDecoration, 'id'>;

export type EntityResponseType = HttpResponse<ITypeDecoration>;
export type EntityArrayResponseType = HttpResponse<ITypeDecoration[]>;

@Injectable({ providedIn: 'root' })
export class TypeDecorationService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/type-decorations');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(typeDecoration: NewTypeDecoration): Observable<EntityResponseType> {
    return this.http.post<ITypeDecoration>(this.resourceUrl, typeDecoration, { observe: 'response' });
  }

  update(typeDecoration: ITypeDecoration): Observable<EntityResponseType> {
    return this.http.put<ITypeDecoration>(`${this.resourceUrl}/${this.getTypeDecorationIdentifier(typeDecoration)}`, typeDecoration, {
      observe: 'response',
    });
  }

  partialUpdate(typeDecoration: PartialUpdateTypeDecoration): Observable<EntityResponseType> {
    return this.http.patch<ITypeDecoration>(`${this.resourceUrl}/${this.getTypeDecorationIdentifier(typeDecoration)}`, typeDecoration, {
      observe: 'response',
    });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<ITypeDecoration>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<ITypeDecoration[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  getTypeDecorationIdentifier(typeDecoration: Pick<ITypeDecoration, 'id'>): number {
    return typeDecoration.id;
  }

  compareTypeDecoration(o1: Pick<ITypeDecoration, 'id'> | null, o2: Pick<ITypeDecoration, 'id'> | null): boolean {
    return o1 && o2 ? this.getTypeDecorationIdentifier(o1) === this.getTypeDecorationIdentifier(o2) : o1 === o2;
  }

  addTypeDecorationToCollectionIfMissing<Type extends Pick<ITypeDecoration, 'id'>>(
    typeDecorationCollection: Type[],
    ...typeDecorationsToCheck: (Type | null | undefined)[]
  ): Type[] {
    const typeDecorations: Type[] = typeDecorationsToCheck.filter(isPresent);
    if (typeDecorations.length > 0) {
      const typeDecorationCollectionIdentifiers = typeDecorationCollection.map(
        typeDecorationItem => this.getTypeDecorationIdentifier(typeDecorationItem)!
      );
      const typeDecorationsToAdd = typeDecorations.filter(typeDecorationItem => {
        const typeDecorationIdentifier = this.getTypeDecorationIdentifier(typeDecorationItem);
        if (typeDecorationCollectionIdentifiers.includes(typeDecorationIdentifier)) {
          return false;
        }
        typeDecorationCollectionIdentifiers.push(typeDecorationIdentifier);
        return true;
      });
      return [...typeDecorationsToAdd, ...typeDecorationCollection];
    }
    return typeDecorationCollection;
  }
}

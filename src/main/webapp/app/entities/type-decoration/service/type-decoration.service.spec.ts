import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { ITypeDecoration } from '../type-decoration.model';
import { sampleWithRequiredData, sampleWithNewData, sampleWithPartialData, sampleWithFullData } from '../type-decoration.test-samples';

import { TypeDecorationService } from './type-decoration.service';

const requireRestSample: ITypeDecoration = {
  ...sampleWithRequiredData,
};

describe('TypeDecoration Service', () => {
  let service: TypeDecorationService;
  let httpMock: HttpTestingController;
  let expectedResult: ITypeDecoration | ITypeDecoration[] | boolean | null;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(TypeDecorationService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should create a TypeDecoration', () => {
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      const typeDecoration = { ...sampleWithNewData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.create(typeDecoration).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a TypeDecoration', () => {
      const typeDecoration = { ...sampleWithRequiredData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.update(typeDecoration).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a TypeDecoration', () => {
      const patchObject = { ...sampleWithPartialData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of TypeDecoration', () => {
      const returnedFromService = { ...requireRestSample };

      const expected = { ...sampleWithRequiredData };

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toMatchObject([expected]);
    });

    it('should delete a TypeDecoration', () => {
      const expected = true;

      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult).toBe(expected);
    });

    describe('addTypeDecorationToCollectionIfMissing', () => {
      it('should add a TypeDecoration to an empty array', () => {
        const typeDecoration: ITypeDecoration = sampleWithRequiredData;
        expectedResult = service.addTypeDecorationToCollectionIfMissing([], typeDecoration);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(typeDecoration);
      });

      it('should not add a TypeDecoration to an array that contains it', () => {
        const typeDecoration: ITypeDecoration = sampleWithRequiredData;
        const typeDecorationCollection: ITypeDecoration[] = [
          {
            ...typeDecoration,
          },
          sampleWithPartialData,
        ];
        expectedResult = service.addTypeDecorationToCollectionIfMissing(typeDecorationCollection, typeDecoration);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a TypeDecoration to an array that doesn't contain it", () => {
        const typeDecoration: ITypeDecoration = sampleWithRequiredData;
        const typeDecorationCollection: ITypeDecoration[] = [sampleWithPartialData];
        expectedResult = service.addTypeDecorationToCollectionIfMissing(typeDecorationCollection, typeDecoration);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(typeDecoration);
      });

      it('should add only unique TypeDecoration to an array', () => {
        const typeDecorationArray: ITypeDecoration[] = [sampleWithRequiredData, sampleWithPartialData, sampleWithFullData];
        const typeDecorationCollection: ITypeDecoration[] = [sampleWithRequiredData];
        expectedResult = service.addTypeDecorationToCollectionIfMissing(typeDecorationCollection, ...typeDecorationArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const typeDecoration: ITypeDecoration = sampleWithRequiredData;
        const typeDecoration2: ITypeDecoration = sampleWithPartialData;
        expectedResult = service.addTypeDecorationToCollectionIfMissing([], typeDecoration, typeDecoration2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(typeDecoration);
        expect(expectedResult).toContain(typeDecoration2);
      });

      it('should accept null and undefined values', () => {
        const typeDecoration: ITypeDecoration = sampleWithRequiredData;
        expectedResult = service.addTypeDecorationToCollectionIfMissing([], null, typeDecoration, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(typeDecoration);
      });

      it('should return initial array if no TypeDecoration is added', () => {
        const typeDecorationCollection: ITypeDecoration[] = [sampleWithRequiredData];
        expectedResult = service.addTypeDecorationToCollectionIfMissing(typeDecorationCollection, undefined, null);
        expect(expectedResult).toEqual(typeDecorationCollection);
      });
    });

    describe('compareTypeDecoration', () => {
      it('Should return true if both entities are null', () => {
        const entity1 = null;
        const entity2 = null;

        const compareResult = service.compareTypeDecoration(entity1, entity2);

        expect(compareResult).toEqual(true);
      });

      it('Should return false if one entity is null', () => {
        const entity1 = { id: 123 };
        const entity2 = null;

        const compareResult1 = service.compareTypeDecoration(entity1, entity2);
        const compareResult2 = service.compareTypeDecoration(entity2, entity1);

        expect(compareResult1).toEqual(false);
        expect(compareResult2).toEqual(false);
      });

      it('Should return false if primaryKey differs', () => {
        const entity1 = { id: 123 };
        const entity2 = { id: 456 };

        const compareResult1 = service.compareTypeDecoration(entity1, entity2);
        const compareResult2 = service.compareTypeDecoration(entity2, entity1);

        expect(compareResult1).toEqual(false);
        expect(compareResult2).toEqual(false);
      });

      it('Should return false if primaryKey matches', () => {
        const entity1 = { id: 123 };
        const entity2 = { id: 123 };

        const compareResult1 = service.compareTypeDecoration(entity1, entity2);
        const compareResult2 = service.compareTypeDecoration(entity2, entity1);

        expect(compareResult1).toEqual(true);
        expect(compareResult2).toEqual(true);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});

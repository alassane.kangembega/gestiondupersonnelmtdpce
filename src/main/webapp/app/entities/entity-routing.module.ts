import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'employe',
        data: { pageTitle: 'gestionPersonnel3App.employe.home.title' },
        loadChildren: () => import('./employe/employe.module').then(m => m.EmployeModule),
      },
      {
        path: 'structure',
        data: { pageTitle: 'gestionPersonnel3App.structure.home.title' },
        loadChildren: () => import('./structure/structure.module').then(m => m.StructureModule),
      },
      {
        path: 'decoration',
        data: { pageTitle: 'gestionPersonnel3App.decoration.home.title' },
        loadChildren: () => import('./decoration/decoration.module').then(m => m.DecorationModule),
      },
      {
        path: 'type-decoration',
        data: { pageTitle: 'gestionPersonnel3App.typeDecoration.home.title' },
        loadChildren: () => import('./type-decoration/type-decoration.module').then(m => m.TypeDecorationModule),
      },
      {
        path: 'emploi',
        data: { pageTitle: 'gestionPersonnel3App.emploi.home.title' },
        loadChildren: () => import('./emploi/emploi.module').then(m => m.EmploiModule),
      },
      {
        path: 'fonction',
        data: { pageTitle: 'gestionPersonnel3App.fonction.home.title' },
        loadChildren: () => import('./fonction/fonction.module').then(m => m.FonctionModule),
      },
      {
        path: 'categorie',
        data: { pageTitle: 'gestionPersonnel3App.categorie.home.title' },
        loadChildren: () => import('./categorie/categorie.module').then(m => m.CategorieModule),
      },
      {
        path: 'classe',
        data: { pageTitle: 'gestionPersonnel3App.classe.home.title' },
        loadChildren: () => import('./classe/classe.module').then(m => m.ClasseModule),
      },
      {
        path: 'echelon',
        data: { pageTitle: 'gestionPersonnel3App.echelon.home.title' },
        loadChildren: () => import('./echelon/echelon.module').then(m => m.EchelonModule),
      },
      {
        path: 'changement-emploi',
        data: { pageTitle: 'gestionPersonnel3App.changementEmploi.home.title' },
        loadChildren: () => import('./changement-emploi/changement-emploi.module').then(m => m.ChangementEmploiModule),
      },
      {
        path: 'affectation',
        data: { pageTitle: 'gestionPersonnel3App.affectation.home.title' },
        loadChildren: () => import('./affectation/affectation.module').then(m => m.AffectationModule),
      },
      {
        path: 'avancement-categorie',
        data: { pageTitle: 'gestionPersonnel3App.avancementCategorie.home.title' },
        loadChildren: () => import('./avancement-categorie/avancement-categorie.module').then(m => m.AvancementCategorieModule),
      },
      {
        path: 'avancement-classe',
        data: { pageTitle: 'gestionPersonnel3App.avancementClasse.home.title' },
        loadChildren: () => import('./avancement-classe/avancement-classe.module').then(m => m.AvancementClasseModule),
      },
      {
        path: 'avancement-echelon',
        data: { pageTitle: 'gestionPersonnel3App.avancementEchelon.home.title' },
        loadChildren: () => import('./avancement-echelon/avancement-echelon.module').then(m => m.AvancementEchelonModule),
      },
      {
        path: 'indemnite',
        data: { pageTitle: 'gestionPersonnel3App.indemnite.home.title' },
        loadChildren: () => import('./indemnite/indemnite.module').then(m => m.IndemniteModule),
      },
      {
        path: 'entree',
        data: { pageTitle: 'gestionPersonnel3App.entree.home.title' },
        loadChildren: () => import('./entree/entree.module').then(m => m.EntreeModule),
      },
      {
        path: 'nature-entree',
        data: { pageTitle: 'gestionPersonnel3App.natureEntree.home.title' },
        loadChildren: () => import('./nature-entree/nature-entree.module').then(m => m.NatureEntreeModule),
      },
      {
        path: 'nature-recrutement',
        data: { pageTitle: 'gestionPersonnel3App.natureRecrutement.home.title' },
        loadChildren: () => import('./nature-recrutement/nature-recrutement.module').then(m => m.NatureRecrutementModule),
      },
      {
        path: 'statut',
        data: { pageTitle: 'gestionPersonnel3App.statut.home.title' },
        loadChildren: () => import('./statut/statut.module').then(m => m.StatutModule),
      },
      {
        path: 'sortie',
        data: { pageTitle: 'gestionPersonnel3App.sortie.home.title' },
        loadChildren: () => import('./sortie/sortie.module').then(m => m.SortieModule),
      },
      {
        path: 'nature-sortie',
        data: { pageTitle: 'gestionPersonnel3App.natureSortie.home.title' },
        loadChildren: () => import('./nature-sortie/nature-sortie.module').then(m => m.NatureSortieModule),
      },
      {
        path: 'stage',
        data: { pageTitle: 'gestionPersonnel3App.stage.home.title' },
        loadChildren: () => import('./stage/stage.module').then(m => m.StageModule),
      },
      {
        path: 'nature-stage',
        data: { pageTitle: 'gestionPersonnel3App.natureStage.home.title' },
        loadChildren: () => import('./nature-stage/nature-stage.module').then(m => m.NatureStageModule),
      },
      {
        path: 'nature-position-irreguliere',
        data: { pageTitle: 'gestionPersonnel3App.naturePositionIrreguliere.home.title' },
        loadChildren: () =>
          import('./nature-position-irreguliere/nature-position-irreguliere.module').then(m => m.NaturePositionIrreguliereModule),
      },
      {
        path: 'position-irreguliere',
        data: { pageTitle: 'gestionPersonnel3App.positionIrreguliere.home.title' },
        loadChildren: () => import('./position-irreguliere/position-irreguliere.module').then(m => m.PositionIrreguliereModule),
      },
      {
        path: 'region',
        data: { pageTitle: 'gestionPersonnel3App.region.home.title' },
        loadChildren: () => import('./region/region.module').then(m => m.RegionModule),
      },
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ]),
  ],
})
export class EntityRoutingModule {}

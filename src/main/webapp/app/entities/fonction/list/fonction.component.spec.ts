import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';

import { FonctionService } from '../service/fonction.service';

import { FonctionComponent } from './fonction.component';

describe('Fonction Management Component', () => {
  let comp: FonctionComponent;
  let fixture: ComponentFixture<FonctionComponent>;
  let service: FonctionService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule.withRoutes([{ path: 'fonction', component: FonctionComponent }]), HttpClientTestingModule],
      declarations: [FonctionComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            data: of({
              defaultSort: 'id,asc',
            }),
            queryParamMap: of(
              jest.requireActual('@angular/router').convertToParamMap({
                page: '1',
                size: '1',
                sort: 'id,desc',
              })
            ),
            snapshot: { queryParams: {} },
          },
        },
      ],
    })
      .overrideTemplate(FonctionComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(FonctionComponent);
    comp = fixture.componentInstance;
    service = TestBed.inject(FonctionService);

    const headers = new HttpHeaders();
    jest.spyOn(service, 'query').mockReturnValue(
      of(
        new HttpResponse({
          body: [{ id: 123 }],
          headers,
        })
      )
    );
  });

  it('Should call load all on init', () => {
    // WHEN
    comp.ngOnInit();

    // THEN
    expect(service.query).toHaveBeenCalled();
    expect(comp.fonctions?.[0]).toEqual(expect.objectContaining({ id: 123 }));
  });

  describe('trackId', () => {
    it('Should forward to fonctionService', () => {
      const entity = { id: 123 };
      jest.spyOn(service, 'getFonctionIdentifier');
      const id = comp.trackId(0, entity);
      expect(service.getFonctionIdentifier).toHaveBeenCalledWith(entity);
      expect(id).toBe(entity.id);
    });
  });
});

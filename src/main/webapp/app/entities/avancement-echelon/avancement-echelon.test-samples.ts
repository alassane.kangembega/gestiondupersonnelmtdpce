import dayjs from 'dayjs/esm';

import { IAvancementEchelon, NewAvancementEchelon } from './avancement-echelon.model';

export const sampleWithRequiredData: IAvancementEchelon = {
  id: 80992,
};

export const sampleWithPartialData: IAvancementEchelon = {
  id: 45937,
  dateAvancement: dayjs('2022-09-07'),
};

export const sampleWithFullData: IAvancementEchelon = {
  id: 38991,
  dateAvancement: dayjs('2022-09-06'),
};

export const sampleWithNewData: NewAvancementEchelon = {
  id: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);

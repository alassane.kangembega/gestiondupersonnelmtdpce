import { Injectable } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { IAvancementEchelon, NewAvancementEchelon } from '../avancement-echelon.model';

/**
 * A partial Type with required key is used as form input.
 */
type PartialWithRequiredKeyOf<T extends { id: unknown }> = Partial<Omit<T, 'id'>> & { id: T['id'] };

/**
 * Type for createFormGroup and resetForm argument.
 * It accepts IAvancementEchelon for edit and NewAvancementEchelonFormGroupInput for create.
 */
type AvancementEchelonFormGroupInput = IAvancementEchelon | PartialWithRequiredKeyOf<NewAvancementEchelon>;

type AvancementEchelonFormDefaults = Pick<NewAvancementEchelon, 'id'>;

type AvancementEchelonFormGroupContent = {
  id: FormControl<IAvancementEchelon['id'] | NewAvancementEchelon['id']>;
  dateAvancement: FormControl<IAvancementEchelon['dateAvancement']>;
  echelon: FormControl<IAvancementEchelon['echelon']>;
  employe: FormControl<IAvancementEchelon['employe']>;
};

export type AvancementEchelonFormGroup = FormGroup<AvancementEchelonFormGroupContent>;

@Injectable({ providedIn: 'root' })
export class AvancementEchelonFormService {
  createAvancementEchelonFormGroup(avancementEchelon: AvancementEchelonFormGroupInput = { id: null }): AvancementEchelonFormGroup {
    const avancementEchelonRawValue = {
      ...this.getFormDefaults(),
      ...avancementEchelon,
    };
    return new FormGroup<AvancementEchelonFormGroupContent>({
      id: new FormControl(
        { value: avancementEchelonRawValue.id, disabled: true },
        {
          nonNullable: true,
          validators: [Validators.required],
        }
      ),
      dateAvancement: new FormControl(avancementEchelonRawValue.dateAvancement),
      echelon: new FormControl(avancementEchelonRawValue.echelon),
      employe: new FormControl(avancementEchelonRawValue.employe),
    });
  }

  getAvancementEchelon(form: AvancementEchelonFormGroup): IAvancementEchelon | NewAvancementEchelon {
    return form.getRawValue() as IAvancementEchelon | NewAvancementEchelon;
  }

  resetForm(form: AvancementEchelonFormGroup, avancementEchelon: AvancementEchelonFormGroupInput): void {
    const avancementEchelonRawValue = { ...this.getFormDefaults(), ...avancementEchelon };
    form.reset(
      {
        ...avancementEchelonRawValue,
        id: { value: avancementEchelonRawValue.id, disabled: true },
      } as any /* cast to workaround https://github.com/angular/angular/issues/46458 */
    );
  }

  private getFormDefaults(): AvancementEchelonFormDefaults {
    return {
      id: null,
    };
  }
}

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { AvancementEchelonFormService } from './avancement-echelon-form.service';
import { AvancementEchelonService } from '../service/avancement-echelon.service';
import { IAvancementEchelon } from '../avancement-echelon.model';
import { IEchelon } from 'app/entities/echelon/echelon.model';
import { EchelonService } from 'app/entities/echelon/service/echelon.service';
import { IEmploye } from 'app/entities/employe/employe.model';
import { EmployeService } from 'app/entities/employe/service/employe.service';

import { AvancementEchelonUpdateComponent } from './avancement-echelon-update.component';

describe('AvancementEchelon Management Update Component', () => {
  let comp: AvancementEchelonUpdateComponent;
  let fixture: ComponentFixture<AvancementEchelonUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let avancementEchelonFormService: AvancementEchelonFormService;
  let avancementEchelonService: AvancementEchelonService;
  let echelonService: EchelonService;
  let employeService: EmployeService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      declarations: [AvancementEchelonUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(AvancementEchelonUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(AvancementEchelonUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    avancementEchelonFormService = TestBed.inject(AvancementEchelonFormService);
    avancementEchelonService = TestBed.inject(AvancementEchelonService);
    echelonService = TestBed.inject(EchelonService);
    employeService = TestBed.inject(EmployeService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call Echelon query and add missing value', () => {
      const avancementEchelon: IAvancementEchelon = { id: 456 };
      const echelon: IEchelon = { id: 21191 };
      avancementEchelon.echelon = echelon;

      const echelonCollection: IEchelon[] = [{ id: 52923 }];
      jest.spyOn(echelonService, 'query').mockReturnValue(of(new HttpResponse({ body: echelonCollection })));
      const additionalEchelons = [echelon];
      const expectedCollection: IEchelon[] = [...additionalEchelons, ...echelonCollection];
      jest.spyOn(echelonService, 'addEchelonToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ avancementEchelon });
      comp.ngOnInit();

      expect(echelonService.query).toHaveBeenCalled();
      expect(echelonService.addEchelonToCollectionIfMissing).toHaveBeenCalledWith(
        echelonCollection,
        ...additionalEchelons.map(expect.objectContaining)
      );
      expect(comp.echelonsSharedCollection).toEqual(expectedCollection);
    });

    it('Should call Employe query and add missing value', () => {
      const avancementEchelon: IAvancementEchelon = { id: 456 };
      const employe: IEmploye = { id: 4212 };
      avancementEchelon.employe = employe;

      const employeCollection: IEmploye[] = [{ id: 58318 }];
      jest.spyOn(employeService, 'query').mockReturnValue(of(new HttpResponse({ body: employeCollection })));
      const additionalEmployes = [employe];
      const expectedCollection: IEmploye[] = [...additionalEmployes, ...employeCollection];
      jest.spyOn(employeService, 'addEmployeToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ avancementEchelon });
      comp.ngOnInit();

      expect(employeService.query).toHaveBeenCalled();
      expect(employeService.addEmployeToCollectionIfMissing).toHaveBeenCalledWith(
        employeCollection,
        ...additionalEmployes.map(expect.objectContaining)
      );
      expect(comp.employesSharedCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const avancementEchelon: IAvancementEchelon = { id: 456 };
      const echelon: IEchelon = { id: 85657 };
      avancementEchelon.echelon = echelon;
      const employe: IEmploye = { id: 2033 };
      avancementEchelon.employe = employe;

      activatedRoute.data = of({ avancementEchelon });
      comp.ngOnInit();

      expect(comp.echelonsSharedCollection).toContain(echelon);
      expect(comp.employesSharedCollection).toContain(employe);
      expect(comp.avancementEchelon).toEqual(avancementEchelon);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IAvancementEchelon>>();
      const avancementEchelon = { id: 123 };
      jest.spyOn(avancementEchelonFormService, 'getAvancementEchelon').mockReturnValue(avancementEchelon);
      jest.spyOn(avancementEchelonService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ avancementEchelon });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: avancementEchelon }));
      saveSubject.complete();

      // THEN
      expect(avancementEchelonFormService.getAvancementEchelon).toHaveBeenCalled();
      expect(comp.previousState).toHaveBeenCalled();
      expect(avancementEchelonService.update).toHaveBeenCalledWith(expect.objectContaining(avancementEchelon));
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IAvancementEchelon>>();
      const avancementEchelon = { id: 123 };
      jest.spyOn(avancementEchelonFormService, 'getAvancementEchelon').mockReturnValue({ id: null });
      jest.spyOn(avancementEchelonService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ avancementEchelon: null });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: avancementEchelon }));
      saveSubject.complete();

      // THEN
      expect(avancementEchelonFormService.getAvancementEchelon).toHaveBeenCalled();
      expect(avancementEchelonService.create).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IAvancementEchelon>>();
      const avancementEchelon = { id: 123 };
      jest.spyOn(avancementEchelonService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ avancementEchelon });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(avancementEchelonService.update).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Compare relationships', () => {
    describe('compareEchelon', () => {
      it('Should forward to echelonService', () => {
        const entity = { id: 123 };
        const entity2 = { id: 456 };
        jest.spyOn(echelonService, 'compareEchelon');
        comp.compareEchelon(entity, entity2);
        expect(echelonService.compareEchelon).toHaveBeenCalledWith(entity, entity2);
      });
    });

    describe('compareEmploye', () => {
      it('Should forward to employeService', () => {
        const entity = { id: 123 };
        const entity2 = { id: 456 };
        jest.spyOn(employeService, 'compareEmploye');
        comp.compareEmploye(entity, entity2);
        expect(employeService.compareEmploye).toHaveBeenCalledWith(entity, entity2);
      });
    });
  });
});

import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { AvancementEchelonFormService, AvancementEchelonFormGroup } from './avancement-echelon-form.service';
import { IAvancementEchelon } from '../avancement-echelon.model';
import { AvancementEchelonService } from '../service/avancement-echelon.service';
import { IEchelon } from 'app/entities/echelon/echelon.model';
import { EchelonService } from 'app/entities/echelon/service/echelon.service';
import { IEmploye } from 'app/entities/employe/employe.model';
import { EmployeService } from 'app/entities/employe/service/employe.service';

@Component({
  selector: 'jhi-avancement-echelon-update',
  templateUrl: './avancement-echelon-update.component.html',
})
export class AvancementEchelonUpdateComponent implements OnInit {
  isSaving = false;
  avancementEchelon: IAvancementEchelon | null = null;

  echelonsSharedCollection: IEchelon[] = [];
  employesSharedCollection: IEmploye[] = [];

  editForm: AvancementEchelonFormGroup = this.avancementEchelonFormService.createAvancementEchelonFormGroup();

  constructor(
    protected avancementEchelonService: AvancementEchelonService,
    protected avancementEchelonFormService: AvancementEchelonFormService,
    protected echelonService: EchelonService,
    protected employeService: EmployeService,
    protected activatedRoute: ActivatedRoute
  ) {}

  compareEchelon = (o1: IEchelon | null, o2: IEchelon | null): boolean => this.echelonService.compareEchelon(o1, o2);

  compareEmploye = (o1: IEmploye | null, o2: IEmploye | null): boolean => this.employeService.compareEmploye(o1, o2);

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ avancementEchelon }) => {
      this.avancementEchelon = avancementEchelon;
      if (avancementEchelon) {
        this.updateForm(avancementEchelon);
      }

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const avancementEchelon = this.avancementEchelonFormService.getAvancementEchelon(this.editForm);
    if (avancementEchelon.id !== null) {
      this.subscribeToSaveResponse(this.avancementEchelonService.update(avancementEchelon));
    } else {
      this.subscribeToSaveResponse(this.avancementEchelonService.create(avancementEchelon));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IAvancementEchelon>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(avancementEchelon: IAvancementEchelon): void {
    this.avancementEchelon = avancementEchelon;
    this.avancementEchelonFormService.resetForm(this.editForm, avancementEchelon);

    this.echelonsSharedCollection = this.echelonService.addEchelonToCollectionIfMissing<IEchelon>(
      this.echelonsSharedCollection,
      avancementEchelon.echelon
    );
    this.employesSharedCollection = this.employeService.addEmployeToCollectionIfMissing<IEmploye>(
      this.employesSharedCollection,
      avancementEchelon.employe
    );
  }

  protected loadRelationshipsOptions(): void {
    this.echelonService
      .query()
      .pipe(map((res: HttpResponse<IEchelon[]>) => res.body ?? []))
      .pipe(
        map((echelons: IEchelon[]) =>
          this.echelonService.addEchelonToCollectionIfMissing<IEchelon>(echelons, this.avancementEchelon?.echelon)
        )
      )
      .subscribe((echelons: IEchelon[]) => (this.echelonsSharedCollection = echelons));

    this.employeService
      .query()
      .pipe(map((res: HttpResponse<IEmploye[]>) => res.body ?? []))
      .pipe(
        map((employes: IEmploye[]) =>
          this.employeService.addEmployeToCollectionIfMissing<IEmploye>(employes, this.avancementEchelon?.employe)
        )
      )
      .subscribe((employes: IEmploye[]) => (this.employesSharedCollection = employes));
  }
}

import { TestBed } from '@angular/core/testing';

import { sampleWithRequiredData, sampleWithNewData } from '../avancement-echelon.test-samples';

import { AvancementEchelonFormService } from './avancement-echelon-form.service';

describe('AvancementEchelon Form Service', () => {
  let service: AvancementEchelonFormService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AvancementEchelonFormService);
  });

  describe('Service methods', () => {
    describe('createAvancementEchelonFormGroup', () => {
      it('should create a new form with FormControl', () => {
        const formGroup = service.createAvancementEchelonFormGroup();

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            dateAvancement: expect.any(Object),
            echelon: expect.any(Object),
            employe: expect.any(Object),
          })
        );
      });

      it('passing IAvancementEchelon should create a new form with FormGroup', () => {
        const formGroup = service.createAvancementEchelonFormGroup(sampleWithRequiredData);

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            dateAvancement: expect.any(Object),
            echelon: expect.any(Object),
            employe: expect.any(Object),
          })
        );
      });
    });

    describe('getAvancementEchelon', () => {
      it('should return NewAvancementEchelon for default AvancementEchelon initial value', () => {
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        const formGroup = service.createAvancementEchelonFormGroup(sampleWithNewData);

        const avancementEchelon = service.getAvancementEchelon(formGroup) as any;

        expect(avancementEchelon).toMatchObject(sampleWithNewData);
      });

      it('should return NewAvancementEchelon for empty AvancementEchelon initial value', () => {
        const formGroup = service.createAvancementEchelonFormGroup();

        const avancementEchelon = service.getAvancementEchelon(formGroup) as any;

        expect(avancementEchelon).toMatchObject({});
      });

      it('should return IAvancementEchelon', () => {
        const formGroup = service.createAvancementEchelonFormGroup(sampleWithRequiredData);

        const avancementEchelon = service.getAvancementEchelon(formGroup) as any;

        expect(avancementEchelon).toMatchObject(sampleWithRequiredData);
      });
    });

    describe('resetForm', () => {
      it('passing IAvancementEchelon should not enable id FormControl', () => {
        const formGroup = service.createAvancementEchelonFormGroup();
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, sampleWithRequiredData);

        expect(formGroup.controls.id.disabled).toBe(true);
      });

      it('passing NewAvancementEchelon should disable id FormControl', () => {
        const formGroup = service.createAvancementEchelonFormGroup(sampleWithRequiredData);
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, { id: null });

        expect(formGroup.controls.id.disabled).toBe(true);
      });
    });
  });
});

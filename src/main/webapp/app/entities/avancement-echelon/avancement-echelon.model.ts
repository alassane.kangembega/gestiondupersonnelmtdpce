import dayjs from 'dayjs/esm';
import { IEchelon } from 'app/entities/echelon/echelon.model';
import { IEmploye } from 'app/entities/employe/employe.model';

export interface IAvancementEchelon {
  id: number;
  dateAvancement?: dayjs.Dayjs | null;
  echelon?: Pick<IEchelon, 'id'> | null;
  employe?: Pick<IEmploye, 'id'> | null;
}

export type NewAvancementEchelon = Omit<IAvancementEchelon, 'id'> & { id: null };

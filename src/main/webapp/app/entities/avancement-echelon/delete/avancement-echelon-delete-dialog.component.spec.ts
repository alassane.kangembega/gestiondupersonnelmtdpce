jest.mock('@ng-bootstrap/ng-bootstrap');

import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of } from 'rxjs';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { AvancementEchelonService } from '../service/avancement-echelon.service';

import { AvancementEchelonDeleteDialogComponent } from './avancement-echelon-delete-dialog.component';

describe('AvancementEchelon Management Delete Component', () => {
  let comp: AvancementEchelonDeleteDialogComponent;
  let fixture: ComponentFixture<AvancementEchelonDeleteDialogComponent>;
  let service: AvancementEchelonService;
  let mockActiveModal: NgbActiveModal;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [AvancementEchelonDeleteDialogComponent],
      providers: [NgbActiveModal],
    })
      .overrideTemplate(AvancementEchelonDeleteDialogComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(AvancementEchelonDeleteDialogComponent);
    comp = fixture.componentInstance;
    service = TestBed.inject(AvancementEchelonService);
    mockActiveModal = TestBed.inject(NgbActiveModal);
  });

  describe('confirmDelete', () => {
    it('Should call delete service on confirmDelete', inject(
      [],
      fakeAsync(() => {
        // GIVEN
        jest.spyOn(service, 'delete').mockReturnValue(of(new HttpResponse({ body: {} })));

        // WHEN
        comp.confirmDelete(123);
        tick();

        // THEN
        expect(service.delete).toHaveBeenCalledWith(123);
        expect(mockActiveModal.close).toHaveBeenCalledWith('deleted');
      })
    ));

    it('Should not call delete service on clear', () => {
      // GIVEN
      jest.spyOn(service, 'delete');

      // WHEN
      comp.cancel();

      // THEN
      expect(service.delete).not.toHaveBeenCalled();
      expect(mockActiveModal.close).not.toHaveBeenCalled();
      expect(mockActiveModal.dismiss).toHaveBeenCalled();
    });
  });
});

import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IAvancementEchelon } from '../avancement-echelon.model';
import { AvancementEchelonService } from '../service/avancement-echelon.service';
import { ITEM_DELETED_EVENT } from 'app/config/navigation.constants';

@Component({
  templateUrl: './avancement-echelon-delete-dialog.component.html',
})
export class AvancementEchelonDeleteDialogComponent {
  avancementEchelon?: IAvancementEchelon;

  constructor(protected avancementEchelonService: AvancementEchelonService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.avancementEchelonService.delete(id).subscribe(() => {
      this.activeModal.close(ITEM_DELETED_EVENT);
    });
  }
}

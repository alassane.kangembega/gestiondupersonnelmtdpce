import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IAvancementEchelon } from '../avancement-echelon.model';
import { AvancementEchelonService } from '../service/avancement-echelon.service';

@Injectable({ providedIn: 'root' })
export class AvancementEchelonRoutingResolveService implements Resolve<IAvancementEchelon | null> {
  constructor(protected service: AvancementEchelonService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IAvancementEchelon | null | never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((avancementEchelon: HttpResponse<IAvancementEchelon>) => {
          if (avancementEchelon.body) {
            return of(avancementEchelon.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(null);
  }
}

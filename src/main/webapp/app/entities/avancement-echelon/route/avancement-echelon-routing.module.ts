import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { AvancementEchelonComponent } from '../list/avancement-echelon.component';
import { AvancementEchelonDetailComponent } from '../detail/avancement-echelon-detail.component';
import { AvancementEchelonUpdateComponent } from '../update/avancement-echelon-update.component';
import { AvancementEchelonRoutingResolveService } from './avancement-echelon-routing-resolve.service';
import { ASC } from 'app/config/navigation.constants';

const avancementEchelonRoute: Routes = [
  {
    path: '',
    component: AvancementEchelonComponent,
    data: {
      defaultSort: 'id,' + ASC,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: AvancementEchelonDetailComponent,
    resolve: {
      avancementEchelon: AvancementEchelonRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: AvancementEchelonUpdateComponent,
    resolve: {
      avancementEchelon: AvancementEchelonRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: AvancementEchelonUpdateComponent,
    resolve: {
      avancementEchelon: AvancementEchelonRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(avancementEchelonRoute)],
  exports: [RouterModule],
})
export class AvancementEchelonRoutingModule {}

import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { AvancementEchelonComponent } from './list/avancement-echelon.component';
import { AvancementEchelonDetailComponent } from './detail/avancement-echelon-detail.component';
import { AvancementEchelonUpdateComponent } from './update/avancement-echelon-update.component';
import { AvancementEchelonDeleteDialogComponent } from './delete/avancement-echelon-delete-dialog.component';
import { AvancementEchelonRoutingModule } from './route/avancement-echelon-routing.module';

@NgModule({
  imports: [SharedModule, AvancementEchelonRoutingModule],
  declarations: [
    AvancementEchelonComponent,
    AvancementEchelonDetailComponent,
    AvancementEchelonUpdateComponent,
    AvancementEchelonDeleteDialogComponent,
  ],
})
export class AvancementEchelonModule {}

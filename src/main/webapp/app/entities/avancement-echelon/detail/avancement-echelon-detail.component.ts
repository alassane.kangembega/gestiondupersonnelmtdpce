import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IAvancementEchelon } from '../avancement-echelon.model';

@Component({
  selector: 'jhi-avancement-echelon-detail',
  templateUrl: './avancement-echelon-detail.component.html',
})
export class AvancementEchelonDetailComponent implements OnInit {
  avancementEchelon: IAvancementEchelon | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ avancementEchelon }) => {
      this.avancementEchelon = avancementEchelon;
    });
  }

  previousState(): void {
    window.history.back();
  }
}

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { AvancementEchelonDetailComponent } from './avancement-echelon-detail.component';

describe('AvancementEchelon Management Detail Component', () => {
  let comp: AvancementEchelonDetailComponent;
  let fixture: ComponentFixture<AvancementEchelonDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [AvancementEchelonDetailComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: { data: of({ avancementEchelon: { id: 123 } }) },
        },
      ],
    })
      .overrideTemplate(AvancementEchelonDetailComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(AvancementEchelonDetailComponent);
    comp = fixture.componentInstance;
  });

  describe('OnInit', () => {
    it('Should load avancementEchelon on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(comp.avancementEchelon).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});

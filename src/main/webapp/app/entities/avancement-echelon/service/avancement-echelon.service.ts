import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import dayjs from 'dayjs/esm';

import { isPresent } from 'app/core/util/operators';
import { DATE_FORMAT } from 'app/config/input.constants';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IAvancementEchelon, NewAvancementEchelon } from '../avancement-echelon.model';

export type PartialUpdateAvancementEchelon = Partial<IAvancementEchelon> & Pick<IAvancementEchelon, 'id'>;

type RestOf<T extends IAvancementEchelon | NewAvancementEchelon> = Omit<T, 'dateAvancement'> & {
  dateAvancement?: string | null;
};

export type RestAvancementEchelon = RestOf<IAvancementEchelon>;

export type NewRestAvancementEchelon = RestOf<NewAvancementEchelon>;

export type PartialUpdateRestAvancementEchelon = RestOf<PartialUpdateAvancementEchelon>;

export type EntityResponseType = HttpResponse<IAvancementEchelon>;
export type EntityArrayResponseType = HttpResponse<IAvancementEchelon[]>;

@Injectable({ providedIn: 'root' })
export class AvancementEchelonService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/avancement-echelons');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(avancementEchelon: NewAvancementEchelon): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(avancementEchelon);
    return this.http
      .post<RestAvancementEchelon>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  update(avancementEchelon: IAvancementEchelon): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(avancementEchelon);
    return this.http
      .put<RestAvancementEchelon>(`${this.resourceUrl}/${this.getAvancementEchelonIdentifier(avancementEchelon)}`, copy, {
        observe: 'response',
      })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  partialUpdate(avancementEchelon: PartialUpdateAvancementEchelon): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(avancementEchelon);
    return this.http
      .patch<RestAvancementEchelon>(`${this.resourceUrl}/${this.getAvancementEchelonIdentifier(avancementEchelon)}`, copy, {
        observe: 'response',
      })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<RestAvancementEchelon>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<RestAvancementEchelon[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map(res => this.convertResponseArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  getAvancementEchelonIdentifier(avancementEchelon: Pick<IAvancementEchelon, 'id'>): number {
    return avancementEchelon.id;
  }

  compareAvancementEchelon(o1: Pick<IAvancementEchelon, 'id'> | null, o2: Pick<IAvancementEchelon, 'id'> | null): boolean {
    return o1 && o2 ? this.getAvancementEchelonIdentifier(o1) === this.getAvancementEchelonIdentifier(o2) : o1 === o2;
  }

  addAvancementEchelonToCollectionIfMissing<Type extends Pick<IAvancementEchelon, 'id'>>(
    avancementEchelonCollection: Type[],
    ...avancementEchelonsToCheck: (Type | null | undefined)[]
  ): Type[] {
    const avancementEchelons: Type[] = avancementEchelonsToCheck.filter(isPresent);
    if (avancementEchelons.length > 0) {
      const avancementEchelonCollectionIdentifiers = avancementEchelonCollection.map(
        avancementEchelonItem => this.getAvancementEchelonIdentifier(avancementEchelonItem)!
      );
      const avancementEchelonsToAdd = avancementEchelons.filter(avancementEchelonItem => {
        const avancementEchelonIdentifier = this.getAvancementEchelonIdentifier(avancementEchelonItem);
        if (avancementEchelonCollectionIdentifiers.includes(avancementEchelonIdentifier)) {
          return false;
        }
        avancementEchelonCollectionIdentifiers.push(avancementEchelonIdentifier);
        return true;
      });
      return [...avancementEchelonsToAdd, ...avancementEchelonCollection];
    }
    return avancementEchelonCollection;
  }

  protected convertDateFromClient<T extends IAvancementEchelon | NewAvancementEchelon | PartialUpdateAvancementEchelon>(
    avancementEchelon: T
  ): RestOf<T> {
    return {
      ...avancementEchelon,
      dateAvancement: avancementEchelon.dateAvancement?.format(DATE_FORMAT) ?? null,
    };
  }

  protected convertDateFromServer(restAvancementEchelon: RestAvancementEchelon): IAvancementEchelon {
    return {
      ...restAvancementEchelon,
      dateAvancement: restAvancementEchelon.dateAvancement ? dayjs(restAvancementEchelon.dateAvancement) : undefined,
    };
  }

  protected convertResponseFromServer(res: HttpResponse<RestAvancementEchelon>): HttpResponse<IAvancementEchelon> {
    return res.clone({
      body: res.body ? this.convertDateFromServer(res.body) : null,
    });
  }

  protected convertResponseArrayFromServer(res: HttpResponse<RestAvancementEchelon[]>): HttpResponse<IAvancementEchelon[]> {
    return res.clone({
      body: res.body ? res.body.map(item => this.convertDateFromServer(item)) : null,
    });
  }
}

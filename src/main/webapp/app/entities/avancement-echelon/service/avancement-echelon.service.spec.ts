import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { DATE_FORMAT } from 'app/config/input.constants';
import { IAvancementEchelon } from '../avancement-echelon.model';
import { sampleWithRequiredData, sampleWithNewData, sampleWithPartialData, sampleWithFullData } from '../avancement-echelon.test-samples';

import { AvancementEchelonService, RestAvancementEchelon } from './avancement-echelon.service';

const requireRestSample: RestAvancementEchelon = {
  ...sampleWithRequiredData,
  dateAvancement: sampleWithRequiredData.dateAvancement?.format(DATE_FORMAT),
};

describe('AvancementEchelon Service', () => {
  let service: AvancementEchelonService;
  let httpMock: HttpTestingController;
  let expectedResult: IAvancementEchelon | IAvancementEchelon[] | boolean | null;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(AvancementEchelonService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should create a AvancementEchelon', () => {
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      const avancementEchelon = { ...sampleWithNewData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.create(avancementEchelon).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a AvancementEchelon', () => {
      const avancementEchelon = { ...sampleWithRequiredData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.update(avancementEchelon).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a AvancementEchelon', () => {
      const patchObject = { ...sampleWithPartialData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of AvancementEchelon', () => {
      const returnedFromService = { ...requireRestSample };

      const expected = { ...sampleWithRequiredData };

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toMatchObject([expected]);
    });

    it('should delete a AvancementEchelon', () => {
      const expected = true;

      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult).toBe(expected);
    });

    describe('addAvancementEchelonToCollectionIfMissing', () => {
      it('should add a AvancementEchelon to an empty array', () => {
        const avancementEchelon: IAvancementEchelon = sampleWithRequiredData;
        expectedResult = service.addAvancementEchelonToCollectionIfMissing([], avancementEchelon);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(avancementEchelon);
      });

      it('should not add a AvancementEchelon to an array that contains it', () => {
        const avancementEchelon: IAvancementEchelon = sampleWithRequiredData;
        const avancementEchelonCollection: IAvancementEchelon[] = [
          {
            ...avancementEchelon,
          },
          sampleWithPartialData,
        ];
        expectedResult = service.addAvancementEchelonToCollectionIfMissing(avancementEchelonCollection, avancementEchelon);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a AvancementEchelon to an array that doesn't contain it", () => {
        const avancementEchelon: IAvancementEchelon = sampleWithRequiredData;
        const avancementEchelonCollection: IAvancementEchelon[] = [sampleWithPartialData];
        expectedResult = service.addAvancementEchelonToCollectionIfMissing(avancementEchelonCollection, avancementEchelon);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(avancementEchelon);
      });

      it('should add only unique AvancementEchelon to an array', () => {
        const avancementEchelonArray: IAvancementEchelon[] = [sampleWithRequiredData, sampleWithPartialData, sampleWithFullData];
        const avancementEchelonCollection: IAvancementEchelon[] = [sampleWithRequiredData];
        expectedResult = service.addAvancementEchelonToCollectionIfMissing(avancementEchelonCollection, ...avancementEchelonArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const avancementEchelon: IAvancementEchelon = sampleWithRequiredData;
        const avancementEchelon2: IAvancementEchelon = sampleWithPartialData;
        expectedResult = service.addAvancementEchelonToCollectionIfMissing([], avancementEchelon, avancementEchelon2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(avancementEchelon);
        expect(expectedResult).toContain(avancementEchelon2);
      });

      it('should accept null and undefined values', () => {
        const avancementEchelon: IAvancementEchelon = sampleWithRequiredData;
        expectedResult = service.addAvancementEchelonToCollectionIfMissing([], null, avancementEchelon, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(avancementEchelon);
      });

      it('should return initial array if no AvancementEchelon is added', () => {
        const avancementEchelonCollection: IAvancementEchelon[] = [sampleWithRequiredData];
        expectedResult = service.addAvancementEchelonToCollectionIfMissing(avancementEchelonCollection, undefined, null);
        expect(expectedResult).toEqual(avancementEchelonCollection);
      });
    });

    describe('compareAvancementEchelon', () => {
      it('Should return true if both entities are null', () => {
        const entity1 = null;
        const entity2 = null;

        const compareResult = service.compareAvancementEchelon(entity1, entity2);

        expect(compareResult).toEqual(true);
      });

      it('Should return false if one entity is null', () => {
        const entity1 = { id: 123 };
        const entity2 = null;

        const compareResult1 = service.compareAvancementEchelon(entity1, entity2);
        const compareResult2 = service.compareAvancementEchelon(entity2, entity1);

        expect(compareResult1).toEqual(false);
        expect(compareResult2).toEqual(false);
      });

      it('Should return false if primaryKey differs', () => {
        const entity1 = { id: 123 };
        const entity2 = { id: 456 };

        const compareResult1 = service.compareAvancementEchelon(entity1, entity2);
        const compareResult2 = service.compareAvancementEchelon(entity2, entity1);

        expect(compareResult1).toEqual(false);
        expect(compareResult2).toEqual(false);
      });

      it('Should return false if primaryKey matches', () => {
        const entity1 = { id: 123 };
        const entity2 = { id: 123 };

        const compareResult1 = service.compareAvancementEchelon(entity1, entity2);
        const compareResult2 = service.compareAvancementEchelon(entity2, entity1);

        expect(compareResult1).toEqual(true);
        expect(compareResult2).toEqual(true);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});

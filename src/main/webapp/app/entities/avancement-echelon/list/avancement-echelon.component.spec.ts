import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';

import { AvancementEchelonService } from '../service/avancement-echelon.service';

import { AvancementEchelonComponent } from './avancement-echelon.component';

describe('AvancementEchelon Management Component', () => {
  let comp: AvancementEchelonComponent;
  let fixture: ComponentFixture<AvancementEchelonComponent>;
  let service: AvancementEchelonService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule.withRoutes([{ path: 'avancement-echelon', component: AvancementEchelonComponent }]),
        HttpClientTestingModule,
      ],
      declarations: [AvancementEchelonComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            data: of({
              defaultSort: 'id,asc',
            }),
            queryParamMap: of(
              jest.requireActual('@angular/router').convertToParamMap({
                page: '1',
                size: '1',
                sort: 'id,desc',
              })
            ),
            snapshot: { queryParams: {} },
          },
        },
      ],
    })
      .overrideTemplate(AvancementEchelonComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(AvancementEchelonComponent);
    comp = fixture.componentInstance;
    service = TestBed.inject(AvancementEchelonService);

    const headers = new HttpHeaders();
    jest.spyOn(service, 'query').mockReturnValue(
      of(
        new HttpResponse({
          body: [{ id: 123 }],
          headers,
        })
      )
    );
  });

  it('Should call load all on init', () => {
    // WHEN
    comp.ngOnInit();

    // THEN
    expect(service.query).toHaveBeenCalled();
    expect(comp.avancementEchelons?.[0]).toEqual(expect.objectContaining({ id: 123 }));
  });

  describe('trackId', () => {
    it('Should forward to avancementEchelonService', () => {
      const entity = { id: 123 };
      jest.spyOn(service, 'getAvancementEchelonIdentifier');
      const id = comp.trackId(0, entity);
      expect(service.getAvancementEchelonIdentifier).toHaveBeenCalledWith(entity);
      expect(id).toBe(entity.id);
    });
  });
});

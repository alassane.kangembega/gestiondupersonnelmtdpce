import { TypeStructure } from 'app/entities/enumerations/type-structure.model';

import { IStructure, NewStructure } from './structure.model';

export const sampleWithRequiredData: IStructure = {
  id: 50985,
};

export const sampleWithPartialData: IStructure = {
  id: 98544,
  sigle: 'standardization calculate Cameroun',
  libelle: 'Rosiers',
};

export const sampleWithFullData: IStructure = {
  id: 37798,
  type: TypeStructure['DirectionTechnique'],
  sigle: 'SMTP bottom-line Bourgogne',
  libelle: 'primary hack',
};

export const sampleWithNewData: NewStructure = {
  id: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);

import { TypeStructure } from 'app/entities/enumerations/type-structure.model';

export interface IStructure {
  id: number;
  type?: TypeStructure | null;
  sigle?: string | null;
  libelle?: string | null;
  parentStructure?: Pick<IStructure, 'id'> | null;
}

export type NewStructure = Omit<IStructure, 'id'> & { id: null };

import { Injectable } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { IAffectation, NewAffectation } from '../affectation.model';

/**
 * A partial Type with required key is used as form input.
 */
type PartialWithRequiredKeyOf<T extends { id: unknown }> = Partial<Omit<T, 'id'>> & { id: T['id'] };

/**
 * Type for createFormGroup and resetForm argument.
 * It accepts IAffectation for edit and NewAffectationFormGroupInput for create.
 */
type AffectationFormGroupInput = IAffectation | PartialWithRequiredKeyOf<NewAffectation>;

type AffectationFormDefaults = Pick<NewAffectation, 'id'>;

type AffectationFormGroupContent = {
  id: FormControl<IAffectation['id'] | NewAffectation['id']>;
  dateAffectation: FormControl<IAffectation['dateAffectation']>;
  region: FormControl<IAffectation['region']>;
  fonction: FormControl<IAffectation['fonction']>;
  employe: FormControl<IAffectation['employe']>;
  structure: FormControl<IAffectation['structure']>;
};

export type AffectationFormGroup = FormGroup<AffectationFormGroupContent>;

@Injectable({ providedIn: 'root' })
export class AffectationFormService {
  createAffectationFormGroup(affectation: AffectationFormGroupInput = { id: null }): AffectationFormGroup {
    const affectationRawValue = {
      ...this.getFormDefaults(),
      ...affectation,
    };
    return new FormGroup<AffectationFormGroupContent>({
      id: new FormControl(
        { value: affectationRawValue.id, disabled: true },
        {
          nonNullable: true,
          validators: [Validators.required],
        }
      ),
      dateAffectation: new FormControl(affectationRawValue.dateAffectation),
      region: new FormControl(affectationRawValue.region),
      fonction: new FormControl(affectationRawValue.fonction),
      employe: new FormControl(affectationRawValue.employe),
      structure: new FormControl(affectationRawValue.structure),
    });
  }

  getAffectation(form: AffectationFormGroup): IAffectation | NewAffectation {
    return form.getRawValue() as IAffectation | NewAffectation;
  }

  resetForm(form: AffectationFormGroup, affectation: AffectationFormGroupInput): void {
    const affectationRawValue = { ...this.getFormDefaults(), ...affectation };
    form.reset(
      {
        ...affectationRawValue,
        id: { value: affectationRawValue.id, disabled: true },
      } as any /* cast to workaround https://github.com/angular/angular/issues/46458 */
    );
  }

  private getFormDefaults(): AffectationFormDefaults {
    return {
      id: null,
    };
  }
}

import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { AffectationFormService, AffectationFormGroup } from './affectation-form.service';
import { IAffectation } from '../affectation.model';
import { AffectationService } from '../service/affectation.service';
import { IRegion } from 'app/entities/region/region.model';
import { RegionService } from 'app/entities/region/service/region.service';
import { IFonction } from 'app/entities/fonction/fonction.model';
import { FonctionService } from 'app/entities/fonction/service/fonction.service';
import { IEmploye } from 'app/entities/employe/employe.model';
import { EmployeService } from 'app/entities/employe/service/employe.service';
import { IStructure } from 'app/entities/structure/structure.model';
import { StructureService } from 'app/entities/structure/service/structure.service';

@Component({
  selector: 'jhi-affectation-update',
  templateUrl: './affectation-update.component.html',
})
export class AffectationUpdateComponent implements OnInit {
  isSaving = false;
  affectation: IAffectation | null = null;

  regionsSharedCollection: IRegion[] = [];
  fonctionsSharedCollection: IFonction[] = [];
  employesSharedCollection: IEmploye[] = [];
  structuresSharedCollection: IStructure[] = [];

  editForm: AffectationFormGroup = this.affectationFormService.createAffectationFormGroup();

  constructor(
    protected affectationService: AffectationService,
    protected affectationFormService: AffectationFormService,
    protected regionService: RegionService,
    protected fonctionService: FonctionService,
    protected employeService: EmployeService,
    protected structureService: StructureService,
    protected activatedRoute: ActivatedRoute
  ) {}

  compareRegion = (o1: IRegion | null, o2: IRegion | null): boolean => this.regionService.compareRegion(o1, o2);

  compareFonction = (o1: IFonction | null, o2: IFonction | null): boolean => this.fonctionService.compareFonction(o1, o2);

  compareEmploye = (o1: IEmploye | null, o2: IEmploye | null): boolean => this.employeService.compareEmploye(o1, o2);

  compareStructure = (o1: IStructure | null, o2: IStructure | null): boolean => this.structureService.compareStructure(o1, o2);

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ affectation }) => {
      this.affectation = affectation;
      if (affectation) {
        this.updateForm(affectation);
      }

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const affectation = this.affectationFormService.getAffectation(this.editForm);
    if (affectation.id !== null) {
      this.subscribeToSaveResponse(this.affectationService.update(affectation));
    } else {
      this.subscribeToSaveResponse(this.affectationService.create(affectation));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IAffectation>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(affectation: IAffectation): void {
    this.affectation = affectation;
    this.affectationFormService.resetForm(this.editForm, affectation);

    this.regionsSharedCollection = this.regionService.addRegionToCollectionIfMissing<IRegion>(
      this.regionsSharedCollection,
      affectation.region
    );
    this.fonctionsSharedCollection = this.fonctionService.addFonctionToCollectionIfMissing<IFonction>(
      this.fonctionsSharedCollection,
      affectation.fonction
    );
    this.employesSharedCollection = this.employeService.addEmployeToCollectionIfMissing<IEmploye>(
      this.employesSharedCollection,
      affectation.employe
    );
    this.structuresSharedCollection = this.structureService.addStructureToCollectionIfMissing<IStructure>(
      this.structuresSharedCollection,
      affectation.structure
    );
  }

  protected loadRelationshipsOptions(): void {
    this.regionService
      .query()
      .pipe(map((res: HttpResponse<IRegion[]>) => res.body ?? []))
      .pipe(map((regions: IRegion[]) => this.regionService.addRegionToCollectionIfMissing<IRegion>(regions, this.affectation?.region)))
      .subscribe((regions: IRegion[]) => (this.regionsSharedCollection = regions));

    this.fonctionService
      .query()
      .pipe(map((res: HttpResponse<IFonction[]>) => res.body ?? []))
      .pipe(
        map((fonctions: IFonction[]) =>
          this.fonctionService.addFonctionToCollectionIfMissing<IFonction>(fonctions, this.affectation?.fonction)
        )
      )
      .subscribe((fonctions: IFonction[]) => (this.fonctionsSharedCollection = fonctions));

    this.employeService
      .query()
      .pipe(map((res: HttpResponse<IEmploye[]>) => res.body ?? []))
      .pipe(
        map((employes: IEmploye[]) => this.employeService.addEmployeToCollectionIfMissing<IEmploye>(employes, this.affectation?.employe))
      )
      .subscribe((employes: IEmploye[]) => (this.employesSharedCollection = employes));

    this.structureService
      .query()
      .pipe(map((res: HttpResponse<IStructure[]>) => res.body ?? []))
      .pipe(
        map((structures: IStructure[]) =>
          this.structureService.addStructureToCollectionIfMissing<IStructure>(structures, this.affectation?.structure)
        )
      )
      .subscribe((structures: IStructure[]) => (this.structuresSharedCollection = structures));
  }
}

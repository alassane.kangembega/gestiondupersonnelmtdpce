import dayjs from 'dayjs/esm';
import { IRegion } from 'app/entities/region/region.model';
import { IFonction } from 'app/entities/fonction/fonction.model';
import { IEmploye } from 'app/entities/employe/employe.model';
import { IStructure } from 'app/entities/structure/structure.model';

export interface IAffectation {
  id: number;
  dateAffectation?: dayjs.Dayjs | null;
  region?: Pick<IRegion, 'id'> | null;
  fonction?: Pick<IFonction, 'id'> | null;
  employe?: Pick<IEmploye, 'id'> | null;
  structure?: Pick<IStructure, 'id'> | null;
}

export type NewAffectation = Omit<IAffectation, 'id'> & { id: null };

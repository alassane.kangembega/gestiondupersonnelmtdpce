import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';

import { NatureRecrutementService } from '../service/nature-recrutement.service';

import { NatureRecrutementComponent } from './nature-recrutement.component';

describe('NatureRecrutement Management Component', () => {
  let comp: NatureRecrutementComponent;
  let fixture: ComponentFixture<NatureRecrutementComponent>;
  let service: NatureRecrutementService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule.withRoutes([{ path: 'nature-recrutement', component: NatureRecrutementComponent }]),
        HttpClientTestingModule,
      ],
      declarations: [NatureRecrutementComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            data: of({
              defaultSort: 'id,asc',
            }),
            queryParamMap: of(
              jest.requireActual('@angular/router').convertToParamMap({
                page: '1',
                size: '1',
                sort: 'id,desc',
              })
            ),
            snapshot: { queryParams: {} },
          },
        },
      ],
    })
      .overrideTemplate(NatureRecrutementComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(NatureRecrutementComponent);
    comp = fixture.componentInstance;
    service = TestBed.inject(NatureRecrutementService);

    const headers = new HttpHeaders();
    jest.spyOn(service, 'query').mockReturnValue(
      of(
        new HttpResponse({
          body: [{ id: 123 }],
          headers,
        })
      )
    );
  });

  it('Should call load all on init', () => {
    // WHEN
    comp.ngOnInit();

    // THEN
    expect(service.query).toHaveBeenCalled();
    expect(comp.natureRecrutements?.[0]).toEqual(expect.objectContaining({ id: 123 }));
  });

  describe('trackId', () => {
    it('Should forward to natureRecrutementService', () => {
      const entity = { id: 123 };
      jest.spyOn(service, 'getNatureRecrutementIdentifier');
      const id = comp.trackId(0, entity);
      expect(service.getNatureRecrutementIdentifier).toHaveBeenCalledWith(entity);
      expect(id).toBe(entity.id);
    });
  });
});

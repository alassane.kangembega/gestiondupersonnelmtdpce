import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { NatureRecrutementComponent } from '../list/nature-recrutement.component';
import { NatureRecrutementDetailComponent } from '../detail/nature-recrutement-detail.component';
import { NatureRecrutementUpdateComponent } from '../update/nature-recrutement-update.component';
import { NatureRecrutementRoutingResolveService } from './nature-recrutement-routing-resolve.service';
import { ASC } from 'app/config/navigation.constants';

const natureRecrutementRoute: Routes = [
  {
    path: '',
    component: NatureRecrutementComponent,
    data: {
      defaultSort: 'id,' + ASC,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: NatureRecrutementDetailComponent,
    resolve: {
      natureRecrutement: NatureRecrutementRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: NatureRecrutementUpdateComponent,
    resolve: {
      natureRecrutement: NatureRecrutementRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: NatureRecrutementUpdateComponent,
    resolve: {
      natureRecrutement: NatureRecrutementRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(natureRecrutementRoute)],
  exports: [RouterModule],
})
export class NatureRecrutementRoutingModule {}

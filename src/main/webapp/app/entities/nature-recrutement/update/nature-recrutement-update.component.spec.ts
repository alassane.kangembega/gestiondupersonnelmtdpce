import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { NatureRecrutementFormService } from './nature-recrutement-form.service';
import { NatureRecrutementService } from '../service/nature-recrutement.service';
import { INatureRecrutement } from '../nature-recrutement.model';

import { NatureRecrutementUpdateComponent } from './nature-recrutement-update.component';

describe('NatureRecrutement Management Update Component', () => {
  let comp: NatureRecrutementUpdateComponent;
  let fixture: ComponentFixture<NatureRecrutementUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let natureRecrutementFormService: NatureRecrutementFormService;
  let natureRecrutementService: NatureRecrutementService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      declarations: [NatureRecrutementUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(NatureRecrutementUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(NatureRecrutementUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    natureRecrutementFormService = TestBed.inject(NatureRecrutementFormService);
    natureRecrutementService = TestBed.inject(NatureRecrutementService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should update editForm', () => {
      const natureRecrutement: INatureRecrutement = { id: 456 };

      activatedRoute.data = of({ natureRecrutement });
      comp.ngOnInit();

      expect(comp.natureRecrutement).toEqual(natureRecrutement);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<INatureRecrutement>>();
      const natureRecrutement = { id: 123 };
      jest.spyOn(natureRecrutementFormService, 'getNatureRecrutement').mockReturnValue(natureRecrutement);
      jest.spyOn(natureRecrutementService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ natureRecrutement });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: natureRecrutement }));
      saveSubject.complete();

      // THEN
      expect(natureRecrutementFormService.getNatureRecrutement).toHaveBeenCalled();
      expect(comp.previousState).toHaveBeenCalled();
      expect(natureRecrutementService.update).toHaveBeenCalledWith(expect.objectContaining(natureRecrutement));
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<INatureRecrutement>>();
      const natureRecrutement = { id: 123 };
      jest.spyOn(natureRecrutementFormService, 'getNatureRecrutement').mockReturnValue({ id: null });
      jest.spyOn(natureRecrutementService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ natureRecrutement: null });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: natureRecrutement }));
      saveSubject.complete();

      // THEN
      expect(natureRecrutementFormService.getNatureRecrutement).toHaveBeenCalled();
      expect(natureRecrutementService.create).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<INatureRecrutement>>();
      const natureRecrutement = { id: 123 };
      jest.spyOn(natureRecrutementService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ natureRecrutement });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(natureRecrutementService.update).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });
});

import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

import { NatureRecrutementFormService, NatureRecrutementFormGroup } from './nature-recrutement-form.service';
import { INatureRecrutement } from '../nature-recrutement.model';
import { NatureRecrutementService } from '../service/nature-recrutement.service';

@Component({
  selector: 'jhi-nature-recrutement-update',
  templateUrl: './nature-recrutement-update.component.html',
})
export class NatureRecrutementUpdateComponent implements OnInit {
  isSaving = false;
  natureRecrutement: INatureRecrutement | null = null;

  editForm: NatureRecrutementFormGroup = this.natureRecrutementFormService.createNatureRecrutementFormGroup();

  constructor(
    protected natureRecrutementService: NatureRecrutementService,
    protected natureRecrutementFormService: NatureRecrutementFormService,
    protected activatedRoute: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ natureRecrutement }) => {
      this.natureRecrutement = natureRecrutement;
      if (natureRecrutement) {
        this.updateForm(natureRecrutement);
      }
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const natureRecrutement = this.natureRecrutementFormService.getNatureRecrutement(this.editForm);
    if (natureRecrutement.id !== null) {
      this.subscribeToSaveResponse(this.natureRecrutementService.update(natureRecrutement));
    } else {
      this.subscribeToSaveResponse(this.natureRecrutementService.create(natureRecrutement));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<INatureRecrutement>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(natureRecrutement: INatureRecrutement): void {
    this.natureRecrutement = natureRecrutement;
    this.natureRecrutementFormService.resetForm(this.editForm, natureRecrutement);
  }
}

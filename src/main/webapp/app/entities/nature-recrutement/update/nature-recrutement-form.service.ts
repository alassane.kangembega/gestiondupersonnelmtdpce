import { Injectable } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { INatureRecrutement, NewNatureRecrutement } from '../nature-recrutement.model';

/**
 * A partial Type with required key is used as form input.
 */
type PartialWithRequiredKeyOf<T extends { id: unknown }> = Partial<Omit<T, 'id'>> & { id: T['id'] };

/**
 * Type for createFormGroup and resetForm argument.
 * It accepts INatureRecrutement for edit and NewNatureRecrutementFormGroupInput for create.
 */
type NatureRecrutementFormGroupInput = INatureRecrutement | PartialWithRequiredKeyOf<NewNatureRecrutement>;

type NatureRecrutementFormDefaults = Pick<NewNatureRecrutement, 'id'>;

type NatureRecrutementFormGroupContent = {
  id: FormControl<INatureRecrutement['id'] | NewNatureRecrutement['id']>;
  natureRecrutement: FormControl<INatureRecrutement['natureRecrutement']>;
};

export type NatureRecrutementFormGroup = FormGroup<NatureRecrutementFormGroupContent>;

@Injectable({ providedIn: 'root' })
export class NatureRecrutementFormService {
  createNatureRecrutementFormGroup(natureRecrutement: NatureRecrutementFormGroupInput = { id: null }): NatureRecrutementFormGroup {
    const natureRecrutementRawValue = {
      ...this.getFormDefaults(),
      ...natureRecrutement,
    };
    return new FormGroup<NatureRecrutementFormGroupContent>({
      id: new FormControl(
        { value: natureRecrutementRawValue.id, disabled: true },
        {
          nonNullable: true,
          validators: [Validators.required],
        }
      ),
      natureRecrutement: new FormControl(natureRecrutementRawValue.natureRecrutement),
    });
  }

  getNatureRecrutement(form: NatureRecrutementFormGroup): INatureRecrutement | NewNatureRecrutement {
    return form.getRawValue() as INatureRecrutement | NewNatureRecrutement;
  }

  resetForm(form: NatureRecrutementFormGroup, natureRecrutement: NatureRecrutementFormGroupInput): void {
    const natureRecrutementRawValue = { ...this.getFormDefaults(), ...natureRecrutement };
    form.reset(
      {
        ...natureRecrutementRawValue,
        id: { value: natureRecrutementRawValue.id, disabled: true },
      } as any /* cast to workaround https://github.com/angular/angular/issues/46458 */
    );
  }

  private getFormDefaults(): NatureRecrutementFormDefaults {
    return {
      id: null,
    };
  }
}

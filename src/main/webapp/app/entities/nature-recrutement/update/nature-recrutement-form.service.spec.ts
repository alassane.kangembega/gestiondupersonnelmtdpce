import { TestBed } from '@angular/core/testing';

import { sampleWithRequiredData, sampleWithNewData } from '../nature-recrutement.test-samples';

import { NatureRecrutementFormService } from './nature-recrutement-form.service';

describe('NatureRecrutement Form Service', () => {
  let service: NatureRecrutementFormService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(NatureRecrutementFormService);
  });

  describe('Service methods', () => {
    describe('createNatureRecrutementFormGroup', () => {
      it('should create a new form with FormControl', () => {
        const formGroup = service.createNatureRecrutementFormGroup();

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            natureRecrutement: expect.any(Object),
          })
        );
      });

      it('passing INatureRecrutement should create a new form with FormGroup', () => {
        const formGroup = service.createNatureRecrutementFormGroup(sampleWithRequiredData);

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            natureRecrutement: expect.any(Object),
          })
        );
      });
    });

    describe('getNatureRecrutement', () => {
      it('should return NewNatureRecrutement for default NatureRecrutement initial value', () => {
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        const formGroup = service.createNatureRecrutementFormGroup(sampleWithNewData);

        const natureRecrutement = service.getNatureRecrutement(formGroup) as any;

        expect(natureRecrutement).toMatchObject(sampleWithNewData);
      });

      it('should return NewNatureRecrutement for empty NatureRecrutement initial value', () => {
        const formGroup = service.createNatureRecrutementFormGroup();

        const natureRecrutement = service.getNatureRecrutement(formGroup) as any;

        expect(natureRecrutement).toMatchObject({});
      });

      it('should return INatureRecrutement', () => {
        const formGroup = service.createNatureRecrutementFormGroup(sampleWithRequiredData);

        const natureRecrutement = service.getNatureRecrutement(formGroup) as any;

        expect(natureRecrutement).toMatchObject(sampleWithRequiredData);
      });
    });

    describe('resetForm', () => {
      it('passing INatureRecrutement should not enable id FormControl', () => {
        const formGroup = service.createNatureRecrutementFormGroup();
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, sampleWithRequiredData);

        expect(formGroup.controls.id.disabled).toBe(true);
      });

      it('passing NewNatureRecrutement should disable id FormControl', () => {
        const formGroup = service.createNatureRecrutementFormGroup(sampleWithRequiredData);
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, { id: null });

        expect(formGroup.controls.id.disabled).toBe(true);
      });
    });
  });
});

export interface INatureRecrutement {
  id: number;
  natureRecrutement?: string | null;
}

export type NewNatureRecrutement = Omit<INatureRecrutement, 'id'> & { id: null };

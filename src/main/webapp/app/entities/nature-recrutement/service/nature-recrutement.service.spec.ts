import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { INatureRecrutement } from '../nature-recrutement.model';
import { sampleWithRequiredData, sampleWithNewData, sampleWithPartialData, sampleWithFullData } from '../nature-recrutement.test-samples';

import { NatureRecrutementService } from './nature-recrutement.service';

const requireRestSample: INatureRecrutement = {
  ...sampleWithRequiredData,
};

describe('NatureRecrutement Service', () => {
  let service: NatureRecrutementService;
  let httpMock: HttpTestingController;
  let expectedResult: INatureRecrutement | INatureRecrutement[] | boolean | null;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(NatureRecrutementService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should create a NatureRecrutement', () => {
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      const natureRecrutement = { ...sampleWithNewData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.create(natureRecrutement).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a NatureRecrutement', () => {
      const natureRecrutement = { ...sampleWithRequiredData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.update(natureRecrutement).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a NatureRecrutement', () => {
      const patchObject = { ...sampleWithPartialData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of NatureRecrutement', () => {
      const returnedFromService = { ...requireRestSample };

      const expected = { ...sampleWithRequiredData };

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toMatchObject([expected]);
    });

    it('should delete a NatureRecrutement', () => {
      const expected = true;

      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult).toBe(expected);
    });

    describe('addNatureRecrutementToCollectionIfMissing', () => {
      it('should add a NatureRecrutement to an empty array', () => {
        const natureRecrutement: INatureRecrutement = sampleWithRequiredData;
        expectedResult = service.addNatureRecrutementToCollectionIfMissing([], natureRecrutement);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(natureRecrutement);
      });

      it('should not add a NatureRecrutement to an array that contains it', () => {
        const natureRecrutement: INatureRecrutement = sampleWithRequiredData;
        const natureRecrutementCollection: INatureRecrutement[] = [
          {
            ...natureRecrutement,
          },
          sampleWithPartialData,
        ];
        expectedResult = service.addNatureRecrutementToCollectionIfMissing(natureRecrutementCollection, natureRecrutement);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a NatureRecrutement to an array that doesn't contain it", () => {
        const natureRecrutement: INatureRecrutement = sampleWithRequiredData;
        const natureRecrutementCollection: INatureRecrutement[] = [sampleWithPartialData];
        expectedResult = service.addNatureRecrutementToCollectionIfMissing(natureRecrutementCollection, natureRecrutement);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(natureRecrutement);
      });

      it('should add only unique NatureRecrutement to an array', () => {
        const natureRecrutementArray: INatureRecrutement[] = [sampleWithRequiredData, sampleWithPartialData, sampleWithFullData];
        const natureRecrutementCollection: INatureRecrutement[] = [sampleWithRequiredData];
        expectedResult = service.addNatureRecrutementToCollectionIfMissing(natureRecrutementCollection, ...natureRecrutementArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const natureRecrutement: INatureRecrutement = sampleWithRequiredData;
        const natureRecrutement2: INatureRecrutement = sampleWithPartialData;
        expectedResult = service.addNatureRecrutementToCollectionIfMissing([], natureRecrutement, natureRecrutement2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(natureRecrutement);
        expect(expectedResult).toContain(natureRecrutement2);
      });

      it('should accept null and undefined values', () => {
        const natureRecrutement: INatureRecrutement = sampleWithRequiredData;
        expectedResult = service.addNatureRecrutementToCollectionIfMissing([], null, natureRecrutement, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(natureRecrutement);
      });

      it('should return initial array if no NatureRecrutement is added', () => {
        const natureRecrutementCollection: INatureRecrutement[] = [sampleWithRequiredData];
        expectedResult = service.addNatureRecrutementToCollectionIfMissing(natureRecrutementCollection, undefined, null);
        expect(expectedResult).toEqual(natureRecrutementCollection);
      });
    });

    describe('compareNatureRecrutement', () => {
      it('Should return true if both entities are null', () => {
        const entity1 = null;
        const entity2 = null;

        const compareResult = service.compareNatureRecrutement(entity1, entity2);

        expect(compareResult).toEqual(true);
      });

      it('Should return false if one entity is null', () => {
        const entity1 = { id: 123 };
        const entity2 = null;

        const compareResult1 = service.compareNatureRecrutement(entity1, entity2);
        const compareResult2 = service.compareNatureRecrutement(entity2, entity1);

        expect(compareResult1).toEqual(false);
        expect(compareResult2).toEqual(false);
      });

      it('Should return false if primaryKey differs', () => {
        const entity1 = { id: 123 };
        const entity2 = { id: 456 };

        const compareResult1 = service.compareNatureRecrutement(entity1, entity2);
        const compareResult2 = service.compareNatureRecrutement(entity2, entity1);

        expect(compareResult1).toEqual(false);
        expect(compareResult2).toEqual(false);
      });

      it('Should return false if primaryKey matches', () => {
        const entity1 = { id: 123 };
        const entity2 = { id: 123 };

        const compareResult1 = service.compareNatureRecrutement(entity1, entity2);
        const compareResult2 = service.compareNatureRecrutement(entity2, entity1);

        expect(compareResult1).toEqual(true);
        expect(compareResult2).toEqual(true);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});

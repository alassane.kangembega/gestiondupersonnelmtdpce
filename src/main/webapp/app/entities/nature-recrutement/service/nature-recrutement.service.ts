import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { INatureRecrutement, NewNatureRecrutement } from '../nature-recrutement.model';

export type PartialUpdateNatureRecrutement = Partial<INatureRecrutement> & Pick<INatureRecrutement, 'id'>;

export type EntityResponseType = HttpResponse<INatureRecrutement>;
export type EntityArrayResponseType = HttpResponse<INatureRecrutement[]>;

@Injectable({ providedIn: 'root' })
export class NatureRecrutementService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/nature-recrutements');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(natureRecrutement: NewNatureRecrutement): Observable<EntityResponseType> {
    return this.http.post<INatureRecrutement>(this.resourceUrl, natureRecrutement, { observe: 'response' });
  }

  update(natureRecrutement: INatureRecrutement): Observable<EntityResponseType> {
    return this.http.put<INatureRecrutement>(
      `${this.resourceUrl}/${this.getNatureRecrutementIdentifier(natureRecrutement)}`,
      natureRecrutement,
      { observe: 'response' }
    );
  }

  partialUpdate(natureRecrutement: PartialUpdateNatureRecrutement): Observable<EntityResponseType> {
    return this.http.patch<INatureRecrutement>(
      `${this.resourceUrl}/${this.getNatureRecrutementIdentifier(natureRecrutement)}`,
      natureRecrutement,
      { observe: 'response' }
    );
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<INatureRecrutement>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<INatureRecrutement[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  getNatureRecrutementIdentifier(natureRecrutement: Pick<INatureRecrutement, 'id'>): number {
    return natureRecrutement.id;
  }

  compareNatureRecrutement(o1: Pick<INatureRecrutement, 'id'> | null, o2: Pick<INatureRecrutement, 'id'> | null): boolean {
    return o1 && o2 ? this.getNatureRecrutementIdentifier(o1) === this.getNatureRecrutementIdentifier(o2) : o1 === o2;
  }

  addNatureRecrutementToCollectionIfMissing<Type extends Pick<INatureRecrutement, 'id'>>(
    natureRecrutementCollection: Type[],
    ...natureRecrutementsToCheck: (Type | null | undefined)[]
  ): Type[] {
    const natureRecrutements: Type[] = natureRecrutementsToCheck.filter(isPresent);
    if (natureRecrutements.length > 0) {
      const natureRecrutementCollectionIdentifiers = natureRecrutementCollection.map(
        natureRecrutementItem => this.getNatureRecrutementIdentifier(natureRecrutementItem)!
      );
      const natureRecrutementsToAdd = natureRecrutements.filter(natureRecrutementItem => {
        const natureRecrutementIdentifier = this.getNatureRecrutementIdentifier(natureRecrutementItem);
        if (natureRecrutementCollectionIdentifiers.includes(natureRecrutementIdentifier)) {
          return false;
        }
        natureRecrutementCollectionIdentifiers.push(natureRecrutementIdentifier);
        return true;
      });
      return [...natureRecrutementsToAdd, ...natureRecrutementCollection];
    }
    return natureRecrutementCollection;
  }
}

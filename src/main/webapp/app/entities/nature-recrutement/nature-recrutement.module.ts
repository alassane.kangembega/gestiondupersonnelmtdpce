import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { NatureRecrutementComponent } from './list/nature-recrutement.component';
import { NatureRecrutementDetailComponent } from './detail/nature-recrutement-detail.component';
import { NatureRecrutementUpdateComponent } from './update/nature-recrutement-update.component';
import { NatureRecrutementDeleteDialogComponent } from './delete/nature-recrutement-delete-dialog.component';
import { NatureRecrutementRoutingModule } from './route/nature-recrutement-routing.module';

@NgModule({
  imports: [SharedModule, NatureRecrutementRoutingModule],
  declarations: [
    NatureRecrutementComponent,
    NatureRecrutementDetailComponent,
    NatureRecrutementUpdateComponent,
    NatureRecrutementDeleteDialogComponent,
  ],
})
export class NatureRecrutementModule {}

import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { INatureRecrutement } from '../nature-recrutement.model';
import { NatureRecrutementService } from '../service/nature-recrutement.service';
import { ITEM_DELETED_EVENT } from 'app/config/navigation.constants';

@Component({
  templateUrl: './nature-recrutement-delete-dialog.component.html',
})
export class NatureRecrutementDeleteDialogComponent {
  natureRecrutement?: INatureRecrutement;

  constructor(protected natureRecrutementService: NatureRecrutementService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.natureRecrutementService.delete(id).subscribe(() => {
      this.activeModal.close(ITEM_DELETED_EVENT);
    });
  }
}

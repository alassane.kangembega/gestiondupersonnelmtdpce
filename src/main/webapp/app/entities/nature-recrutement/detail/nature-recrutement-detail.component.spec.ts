import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { NatureRecrutementDetailComponent } from './nature-recrutement-detail.component';

describe('NatureRecrutement Management Detail Component', () => {
  let comp: NatureRecrutementDetailComponent;
  let fixture: ComponentFixture<NatureRecrutementDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [NatureRecrutementDetailComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: { data: of({ natureRecrutement: { id: 123 } }) },
        },
      ],
    })
      .overrideTemplate(NatureRecrutementDetailComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(NatureRecrutementDetailComponent);
    comp = fixture.componentInstance;
  });

  describe('OnInit', () => {
    it('Should load natureRecrutement on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(comp.natureRecrutement).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});

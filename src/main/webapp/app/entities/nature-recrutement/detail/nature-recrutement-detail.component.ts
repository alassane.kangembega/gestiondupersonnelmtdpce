import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { INatureRecrutement } from '../nature-recrutement.model';

@Component({
  selector: 'jhi-nature-recrutement-detail',
  templateUrl: './nature-recrutement-detail.component.html',
})
export class NatureRecrutementDetailComponent implements OnInit {
  natureRecrutement: INatureRecrutement | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ natureRecrutement }) => {
      this.natureRecrutement = natureRecrutement;
    });
  }

  previousState(): void {
    window.history.back();
  }
}

import { INatureRecrutement, NewNatureRecrutement } from './nature-recrutement.model';

export const sampleWithRequiredData: INatureRecrutement = {
  id: 22761,
};

export const sampleWithPartialData: INatureRecrutement = {
  id: 35285,
};

export const sampleWithFullData: INatureRecrutement = {
  id: 29316,
  natureRecrutement: 'Bedfordshire Ergonomic alliance',
};

export const sampleWithNewData: NewNatureRecrutement = {
  id: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);

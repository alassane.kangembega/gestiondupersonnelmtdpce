import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { INatureSortie } from '../nature-sortie.model';
import { NatureSortieService } from '../service/nature-sortie.service';

@Injectable({ providedIn: 'root' })
export class NatureSortieRoutingResolveService implements Resolve<INatureSortie | null> {
  constructor(protected service: NatureSortieService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<INatureSortie | null | never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((natureSortie: HttpResponse<INatureSortie>) => {
          if (natureSortie.body) {
            return of(natureSortie.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(null);
  }
}

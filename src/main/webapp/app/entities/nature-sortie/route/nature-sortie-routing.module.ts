import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { NatureSortieComponent } from '../list/nature-sortie.component';
import { NatureSortieDetailComponent } from '../detail/nature-sortie-detail.component';
import { NatureSortieUpdateComponent } from '../update/nature-sortie-update.component';
import { NatureSortieRoutingResolveService } from './nature-sortie-routing-resolve.service';
import { ASC } from 'app/config/navigation.constants';

const natureSortieRoute: Routes = [
  {
    path: '',
    component: NatureSortieComponent,
    data: {
      defaultSort: 'id,' + ASC,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: NatureSortieDetailComponent,
    resolve: {
      natureSortie: NatureSortieRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: NatureSortieUpdateComponent,
    resolve: {
      natureSortie: NatureSortieRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: NatureSortieUpdateComponent,
    resolve: {
      natureSortie: NatureSortieRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(natureSortieRoute)],
  exports: [RouterModule],
})
export class NatureSortieRoutingModule {}

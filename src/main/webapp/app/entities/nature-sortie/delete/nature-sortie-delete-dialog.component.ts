import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { INatureSortie } from '../nature-sortie.model';
import { NatureSortieService } from '../service/nature-sortie.service';
import { ITEM_DELETED_EVENT } from 'app/config/navigation.constants';

@Component({
  templateUrl: './nature-sortie-delete-dialog.component.html',
})
export class NatureSortieDeleteDialogComponent {
  natureSortie?: INatureSortie;

  constructor(protected natureSortieService: NatureSortieService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.natureSortieService.delete(id).subscribe(() => {
      this.activeModal.close(ITEM_DELETED_EVENT);
    });
  }
}

import { TypeSortie } from 'app/entities/enumerations/type-sortie.model';

import { INatureSortie, NewNatureSortie } from './nature-sortie.model';

export const sampleWithRequiredData: INatureSortie = {
  id: 53855,
};

export const sampleWithPartialData: INatureSortie = {
  id: 91736,
  typeSortie: TypeSortie['Definitif'],
  natureSortie: 'Uzbekistan',
};

export const sampleWithFullData: INatureSortie = {
  id: 77165,
  typeSortie: TypeSortie['Temporaire'],
  natureSortie: 'Clothing turquoise transmitting',
};

export const sampleWithNewData: NewNatureSortie = {
  id: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);

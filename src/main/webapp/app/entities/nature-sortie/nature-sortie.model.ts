import { TypeSortie } from 'app/entities/enumerations/type-sortie.model';

export interface INatureSortie {
  id: number;
  typeSortie?: TypeSortie | null;
  natureSortie?: string | null;
}

export type NewNatureSortie = Omit<INatureSortie, 'id'> & { id: null };

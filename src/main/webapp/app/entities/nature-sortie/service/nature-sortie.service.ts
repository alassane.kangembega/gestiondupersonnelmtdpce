import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { INatureSortie, NewNatureSortie } from '../nature-sortie.model';

export type PartialUpdateNatureSortie = Partial<INatureSortie> & Pick<INatureSortie, 'id'>;

export type EntityResponseType = HttpResponse<INatureSortie>;
export type EntityArrayResponseType = HttpResponse<INatureSortie[]>;

@Injectable({ providedIn: 'root' })
export class NatureSortieService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/nature-sorties');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(natureSortie: NewNatureSortie): Observable<EntityResponseType> {
    return this.http.post<INatureSortie>(this.resourceUrl, natureSortie, { observe: 'response' });
  }

  update(natureSortie: INatureSortie): Observable<EntityResponseType> {
    return this.http.put<INatureSortie>(`${this.resourceUrl}/${this.getNatureSortieIdentifier(natureSortie)}`, natureSortie, {
      observe: 'response',
    });
  }

  partialUpdate(natureSortie: PartialUpdateNatureSortie): Observable<EntityResponseType> {
    return this.http.patch<INatureSortie>(`${this.resourceUrl}/${this.getNatureSortieIdentifier(natureSortie)}`, natureSortie, {
      observe: 'response',
    });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<INatureSortie>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<INatureSortie[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  getNatureSortieIdentifier(natureSortie: Pick<INatureSortie, 'id'>): number {
    return natureSortie.id;
  }

  compareNatureSortie(o1: Pick<INatureSortie, 'id'> | null, o2: Pick<INatureSortie, 'id'> | null): boolean {
    return o1 && o2 ? this.getNatureSortieIdentifier(o1) === this.getNatureSortieIdentifier(o2) : o1 === o2;
  }

  addNatureSortieToCollectionIfMissing<Type extends Pick<INatureSortie, 'id'>>(
    natureSortieCollection: Type[],
    ...natureSortiesToCheck: (Type | null | undefined)[]
  ): Type[] {
    const natureSorties: Type[] = natureSortiesToCheck.filter(isPresent);
    if (natureSorties.length > 0) {
      const natureSortieCollectionIdentifiers = natureSortieCollection.map(
        natureSortieItem => this.getNatureSortieIdentifier(natureSortieItem)!
      );
      const natureSortiesToAdd = natureSorties.filter(natureSortieItem => {
        const natureSortieIdentifier = this.getNatureSortieIdentifier(natureSortieItem);
        if (natureSortieCollectionIdentifiers.includes(natureSortieIdentifier)) {
          return false;
        }
        natureSortieCollectionIdentifiers.push(natureSortieIdentifier);
        return true;
      });
      return [...natureSortiesToAdd, ...natureSortieCollection];
    }
    return natureSortieCollection;
  }
}

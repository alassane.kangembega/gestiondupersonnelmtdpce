import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { INatureSortie } from '../nature-sortie.model';
import { sampleWithRequiredData, sampleWithNewData, sampleWithPartialData, sampleWithFullData } from '../nature-sortie.test-samples';

import { NatureSortieService } from './nature-sortie.service';

const requireRestSample: INatureSortie = {
  ...sampleWithRequiredData,
};

describe('NatureSortie Service', () => {
  let service: NatureSortieService;
  let httpMock: HttpTestingController;
  let expectedResult: INatureSortie | INatureSortie[] | boolean | null;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(NatureSortieService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should create a NatureSortie', () => {
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      const natureSortie = { ...sampleWithNewData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.create(natureSortie).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a NatureSortie', () => {
      const natureSortie = { ...sampleWithRequiredData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.update(natureSortie).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a NatureSortie', () => {
      const patchObject = { ...sampleWithPartialData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of NatureSortie', () => {
      const returnedFromService = { ...requireRestSample };

      const expected = { ...sampleWithRequiredData };

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toMatchObject([expected]);
    });

    it('should delete a NatureSortie', () => {
      const expected = true;

      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult).toBe(expected);
    });

    describe('addNatureSortieToCollectionIfMissing', () => {
      it('should add a NatureSortie to an empty array', () => {
        const natureSortie: INatureSortie = sampleWithRequiredData;
        expectedResult = service.addNatureSortieToCollectionIfMissing([], natureSortie);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(natureSortie);
      });

      it('should not add a NatureSortie to an array that contains it', () => {
        const natureSortie: INatureSortie = sampleWithRequiredData;
        const natureSortieCollection: INatureSortie[] = [
          {
            ...natureSortie,
          },
          sampleWithPartialData,
        ];
        expectedResult = service.addNatureSortieToCollectionIfMissing(natureSortieCollection, natureSortie);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a NatureSortie to an array that doesn't contain it", () => {
        const natureSortie: INatureSortie = sampleWithRequiredData;
        const natureSortieCollection: INatureSortie[] = [sampleWithPartialData];
        expectedResult = service.addNatureSortieToCollectionIfMissing(natureSortieCollection, natureSortie);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(natureSortie);
      });

      it('should add only unique NatureSortie to an array', () => {
        const natureSortieArray: INatureSortie[] = [sampleWithRequiredData, sampleWithPartialData, sampleWithFullData];
        const natureSortieCollection: INatureSortie[] = [sampleWithRequiredData];
        expectedResult = service.addNatureSortieToCollectionIfMissing(natureSortieCollection, ...natureSortieArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const natureSortie: INatureSortie = sampleWithRequiredData;
        const natureSortie2: INatureSortie = sampleWithPartialData;
        expectedResult = service.addNatureSortieToCollectionIfMissing([], natureSortie, natureSortie2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(natureSortie);
        expect(expectedResult).toContain(natureSortie2);
      });

      it('should accept null and undefined values', () => {
        const natureSortie: INatureSortie = sampleWithRequiredData;
        expectedResult = service.addNatureSortieToCollectionIfMissing([], null, natureSortie, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(natureSortie);
      });

      it('should return initial array if no NatureSortie is added', () => {
        const natureSortieCollection: INatureSortie[] = [sampleWithRequiredData];
        expectedResult = service.addNatureSortieToCollectionIfMissing(natureSortieCollection, undefined, null);
        expect(expectedResult).toEqual(natureSortieCollection);
      });
    });

    describe('compareNatureSortie', () => {
      it('Should return true if both entities are null', () => {
        const entity1 = null;
        const entity2 = null;

        const compareResult = service.compareNatureSortie(entity1, entity2);

        expect(compareResult).toEqual(true);
      });

      it('Should return false if one entity is null', () => {
        const entity1 = { id: 123 };
        const entity2 = null;

        const compareResult1 = service.compareNatureSortie(entity1, entity2);
        const compareResult2 = service.compareNatureSortie(entity2, entity1);

        expect(compareResult1).toEqual(false);
        expect(compareResult2).toEqual(false);
      });

      it('Should return false if primaryKey differs', () => {
        const entity1 = { id: 123 };
        const entity2 = { id: 456 };

        const compareResult1 = service.compareNatureSortie(entity1, entity2);
        const compareResult2 = service.compareNatureSortie(entity2, entity1);

        expect(compareResult1).toEqual(false);
        expect(compareResult2).toEqual(false);
      });

      it('Should return false if primaryKey matches', () => {
        const entity1 = { id: 123 };
        const entity2 = { id: 123 };

        const compareResult1 = service.compareNatureSortie(entity1, entity2);
        const compareResult2 = service.compareNatureSortie(entity2, entity1);

        expect(compareResult1).toEqual(true);
        expect(compareResult2).toEqual(true);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});

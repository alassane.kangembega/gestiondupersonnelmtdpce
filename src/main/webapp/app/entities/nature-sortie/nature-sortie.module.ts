import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { NatureSortieComponent } from './list/nature-sortie.component';
import { NatureSortieDetailComponent } from './detail/nature-sortie-detail.component';
import { NatureSortieUpdateComponent } from './update/nature-sortie-update.component';
import { NatureSortieDeleteDialogComponent } from './delete/nature-sortie-delete-dialog.component';
import { NatureSortieRoutingModule } from './route/nature-sortie-routing.module';

@NgModule({
  imports: [SharedModule, NatureSortieRoutingModule],
  declarations: [NatureSortieComponent, NatureSortieDetailComponent, NatureSortieUpdateComponent, NatureSortieDeleteDialogComponent],
})
export class NatureSortieModule {}

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { INatureSortie } from '../nature-sortie.model';

@Component({
  selector: 'jhi-nature-sortie-detail',
  templateUrl: './nature-sortie-detail.component.html',
})
export class NatureSortieDetailComponent implements OnInit {
  natureSortie: INatureSortie | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ natureSortie }) => {
      this.natureSortie = natureSortie;
    });
  }

  previousState(): void {
    window.history.back();
  }
}

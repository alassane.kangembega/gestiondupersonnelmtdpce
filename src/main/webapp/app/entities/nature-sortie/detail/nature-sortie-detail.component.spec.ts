import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { NatureSortieDetailComponent } from './nature-sortie-detail.component';

describe('NatureSortie Management Detail Component', () => {
  let comp: NatureSortieDetailComponent;
  let fixture: ComponentFixture<NatureSortieDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [NatureSortieDetailComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: { data: of({ natureSortie: { id: 123 } }) },
        },
      ],
    })
      .overrideTemplate(NatureSortieDetailComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(NatureSortieDetailComponent);
    comp = fixture.componentInstance;
  });

  describe('OnInit', () => {
    it('Should load natureSortie on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(comp.natureSortie).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});

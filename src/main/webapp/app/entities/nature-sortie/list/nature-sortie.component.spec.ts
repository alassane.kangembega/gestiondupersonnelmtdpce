import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';

import { NatureSortieService } from '../service/nature-sortie.service';

import { NatureSortieComponent } from './nature-sortie.component';

describe('NatureSortie Management Component', () => {
  let comp: NatureSortieComponent;
  let fixture: ComponentFixture<NatureSortieComponent>;
  let service: NatureSortieService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule.withRoutes([{ path: 'nature-sortie', component: NatureSortieComponent }]), HttpClientTestingModule],
      declarations: [NatureSortieComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            data: of({
              defaultSort: 'id,asc',
            }),
            queryParamMap: of(
              jest.requireActual('@angular/router').convertToParamMap({
                page: '1',
                size: '1',
                sort: 'id,desc',
              })
            ),
            snapshot: { queryParams: {} },
          },
        },
      ],
    })
      .overrideTemplate(NatureSortieComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(NatureSortieComponent);
    comp = fixture.componentInstance;
    service = TestBed.inject(NatureSortieService);

    const headers = new HttpHeaders();
    jest.spyOn(service, 'query').mockReturnValue(
      of(
        new HttpResponse({
          body: [{ id: 123 }],
          headers,
        })
      )
    );
  });

  it('Should call load all on init', () => {
    // WHEN
    comp.ngOnInit();

    // THEN
    expect(service.query).toHaveBeenCalled();
    expect(comp.natureSorties?.[0]).toEqual(expect.objectContaining({ id: 123 }));
  });

  describe('trackId', () => {
    it('Should forward to natureSortieService', () => {
      const entity = { id: 123 };
      jest.spyOn(service, 'getNatureSortieIdentifier');
      const id = comp.trackId(0, entity);
      expect(service.getNatureSortieIdentifier).toHaveBeenCalledWith(entity);
      expect(id).toBe(entity.id);
    });
  });
});

import { TestBed } from '@angular/core/testing';

import { sampleWithRequiredData, sampleWithNewData } from '../nature-sortie.test-samples';

import { NatureSortieFormService } from './nature-sortie-form.service';

describe('NatureSortie Form Service', () => {
  let service: NatureSortieFormService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(NatureSortieFormService);
  });

  describe('Service methods', () => {
    describe('createNatureSortieFormGroup', () => {
      it('should create a new form with FormControl', () => {
        const formGroup = service.createNatureSortieFormGroup();

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            typeSortie: expect.any(Object),
            natureSortie: expect.any(Object),
          })
        );
      });

      it('passing INatureSortie should create a new form with FormGroup', () => {
        const formGroup = service.createNatureSortieFormGroup(sampleWithRequiredData);

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            typeSortie: expect.any(Object),
            natureSortie: expect.any(Object),
          })
        );
      });
    });

    describe('getNatureSortie', () => {
      it('should return NewNatureSortie for default NatureSortie initial value', () => {
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        const formGroup = service.createNatureSortieFormGroup(sampleWithNewData);

        const natureSortie = service.getNatureSortie(formGroup) as any;

        expect(natureSortie).toMatchObject(sampleWithNewData);
      });

      it('should return NewNatureSortie for empty NatureSortie initial value', () => {
        const formGroup = service.createNatureSortieFormGroup();

        const natureSortie = service.getNatureSortie(formGroup) as any;

        expect(natureSortie).toMatchObject({});
      });

      it('should return INatureSortie', () => {
        const formGroup = service.createNatureSortieFormGroup(sampleWithRequiredData);

        const natureSortie = service.getNatureSortie(formGroup) as any;

        expect(natureSortie).toMatchObject(sampleWithRequiredData);
      });
    });

    describe('resetForm', () => {
      it('passing INatureSortie should not enable id FormControl', () => {
        const formGroup = service.createNatureSortieFormGroup();
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, sampleWithRequiredData);

        expect(formGroup.controls.id.disabled).toBe(true);
      });

      it('passing NewNatureSortie should disable id FormControl', () => {
        const formGroup = service.createNatureSortieFormGroup(sampleWithRequiredData);
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, { id: null });

        expect(formGroup.controls.id.disabled).toBe(true);
      });
    });
  });
});

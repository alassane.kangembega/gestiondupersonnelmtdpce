import { Injectable } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { INatureSortie, NewNatureSortie } from '../nature-sortie.model';

/**
 * A partial Type with required key is used as form input.
 */
type PartialWithRequiredKeyOf<T extends { id: unknown }> = Partial<Omit<T, 'id'>> & { id: T['id'] };

/**
 * Type for createFormGroup and resetForm argument.
 * It accepts INatureSortie for edit and NewNatureSortieFormGroupInput for create.
 */
type NatureSortieFormGroupInput = INatureSortie | PartialWithRequiredKeyOf<NewNatureSortie>;

type NatureSortieFormDefaults = Pick<NewNatureSortie, 'id'>;

type NatureSortieFormGroupContent = {
  id: FormControl<INatureSortie['id'] | NewNatureSortie['id']>;
  typeSortie: FormControl<INatureSortie['typeSortie']>;
  natureSortie: FormControl<INatureSortie['natureSortie']>;
};

export type NatureSortieFormGroup = FormGroup<NatureSortieFormGroupContent>;

@Injectable({ providedIn: 'root' })
export class NatureSortieFormService {
  createNatureSortieFormGroup(natureSortie: NatureSortieFormGroupInput = { id: null }): NatureSortieFormGroup {
    const natureSortieRawValue = {
      ...this.getFormDefaults(),
      ...natureSortie,
    };
    return new FormGroup<NatureSortieFormGroupContent>({
      id: new FormControl(
        { value: natureSortieRawValue.id, disabled: true },
        {
          nonNullable: true,
          validators: [Validators.required],
        }
      ),
      typeSortie: new FormControl(natureSortieRawValue.typeSortie),
      natureSortie: new FormControl(natureSortieRawValue.natureSortie),
    });
  }

  getNatureSortie(form: NatureSortieFormGroup): INatureSortie | NewNatureSortie {
    return form.getRawValue() as INatureSortie | NewNatureSortie;
  }

  resetForm(form: NatureSortieFormGroup, natureSortie: NatureSortieFormGroupInput): void {
    const natureSortieRawValue = { ...this.getFormDefaults(), ...natureSortie };
    form.reset(
      {
        ...natureSortieRawValue,
        id: { value: natureSortieRawValue.id, disabled: true },
      } as any /* cast to workaround https://github.com/angular/angular/issues/46458 */
    );
  }

  private getFormDefaults(): NatureSortieFormDefaults {
    return {
      id: null,
    };
  }
}

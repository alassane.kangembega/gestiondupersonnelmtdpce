import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { NatureSortieFormService } from './nature-sortie-form.service';
import { NatureSortieService } from '../service/nature-sortie.service';
import { INatureSortie } from '../nature-sortie.model';

import { NatureSortieUpdateComponent } from './nature-sortie-update.component';

describe('NatureSortie Management Update Component', () => {
  let comp: NatureSortieUpdateComponent;
  let fixture: ComponentFixture<NatureSortieUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let natureSortieFormService: NatureSortieFormService;
  let natureSortieService: NatureSortieService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      declarations: [NatureSortieUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(NatureSortieUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(NatureSortieUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    natureSortieFormService = TestBed.inject(NatureSortieFormService);
    natureSortieService = TestBed.inject(NatureSortieService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should update editForm', () => {
      const natureSortie: INatureSortie = { id: 456 };

      activatedRoute.data = of({ natureSortie });
      comp.ngOnInit();

      expect(comp.natureSortie).toEqual(natureSortie);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<INatureSortie>>();
      const natureSortie = { id: 123 };
      jest.spyOn(natureSortieFormService, 'getNatureSortie').mockReturnValue(natureSortie);
      jest.spyOn(natureSortieService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ natureSortie });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: natureSortie }));
      saveSubject.complete();

      // THEN
      expect(natureSortieFormService.getNatureSortie).toHaveBeenCalled();
      expect(comp.previousState).toHaveBeenCalled();
      expect(natureSortieService.update).toHaveBeenCalledWith(expect.objectContaining(natureSortie));
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<INatureSortie>>();
      const natureSortie = { id: 123 };
      jest.spyOn(natureSortieFormService, 'getNatureSortie').mockReturnValue({ id: null });
      jest.spyOn(natureSortieService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ natureSortie: null });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: natureSortie }));
      saveSubject.complete();

      // THEN
      expect(natureSortieFormService.getNatureSortie).toHaveBeenCalled();
      expect(natureSortieService.create).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<INatureSortie>>();
      const natureSortie = { id: 123 };
      jest.spyOn(natureSortieService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ natureSortie });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(natureSortieService.update).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });
});

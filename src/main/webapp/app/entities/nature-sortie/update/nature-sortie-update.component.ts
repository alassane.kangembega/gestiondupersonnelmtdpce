import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

import { NatureSortieFormService, NatureSortieFormGroup } from './nature-sortie-form.service';
import { INatureSortie } from '../nature-sortie.model';
import { NatureSortieService } from '../service/nature-sortie.service';
import { TypeSortie } from 'app/entities/enumerations/type-sortie.model';

@Component({
  selector: 'jhi-nature-sortie-update',
  templateUrl: './nature-sortie-update.component.html',
})
export class NatureSortieUpdateComponent implements OnInit {
  isSaving = false;
  natureSortie: INatureSortie | null = null;
  typeSortieValues = Object.keys(TypeSortie);

  editForm: NatureSortieFormGroup = this.natureSortieFormService.createNatureSortieFormGroup();

  constructor(
    protected natureSortieService: NatureSortieService,
    protected natureSortieFormService: NatureSortieFormService,
    protected activatedRoute: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ natureSortie }) => {
      this.natureSortie = natureSortie;
      if (natureSortie) {
        this.updateForm(natureSortie);
      }
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const natureSortie = this.natureSortieFormService.getNatureSortie(this.editForm);
    if (natureSortie.id !== null) {
      this.subscribeToSaveResponse(this.natureSortieService.update(natureSortie));
    } else {
      this.subscribeToSaveResponse(this.natureSortieService.create(natureSortie));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<INatureSortie>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(natureSortie: INatureSortie): void {
    this.natureSortie = natureSortie;
    this.natureSortieFormService.resetForm(this.editForm, natureSortie);
  }
}

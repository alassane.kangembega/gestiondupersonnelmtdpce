import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { INatureEntree } from '../nature-entree.model';
import { NatureEntreeService } from '../service/nature-entree.service';
import { ITEM_DELETED_EVENT } from 'app/config/navigation.constants';

@Component({
  templateUrl: './nature-entree-delete-dialog.component.html',
})
export class NatureEntreeDeleteDialogComponent {
  natureEntree?: INatureEntree;

  constructor(protected natureEntreeService: NatureEntreeService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.natureEntreeService.delete(id).subscribe(() => {
      this.activeModal.close(ITEM_DELETED_EVENT);
    });
  }
}

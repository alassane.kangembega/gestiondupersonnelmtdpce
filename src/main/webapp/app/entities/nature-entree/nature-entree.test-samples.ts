import { INatureEntree, NewNatureEntree } from './nature-entree.model';

export const sampleWithRequiredData: INatureEntree = {
  id: 27401,
};

export const sampleWithPartialData: INatureEntree = {
  id: 13210,
  natureEntree: 'virtual Bourgogne',
};

export const sampleWithFullData: INatureEntree = {
  id: 89436,
  natureEntree: 'b Up-sized',
};

export const sampleWithNewData: NewNatureEntree = {
  id: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);

import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { NatureEntreeComponent } from './list/nature-entree.component';
import { NatureEntreeDetailComponent } from './detail/nature-entree-detail.component';
import { NatureEntreeUpdateComponent } from './update/nature-entree-update.component';
import { NatureEntreeDeleteDialogComponent } from './delete/nature-entree-delete-dialog.component';
import { NatureEntreeRoutingModule } from './route/nature-entree-routing.module';

@NgModule({
  imports: [SharedModule, NatureEntreeRoutingModule],
  declarations: [NatureEntreeComponent, NatureEntreeDetailComponent, NatureEntreeUpdateComponent, NatureEntreeDeleteDialogComponent],
})
export class NatureEntreeModule {}

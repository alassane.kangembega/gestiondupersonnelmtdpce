import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { NatureEntreeComponent } from '../list/nature-entree.component';
import { NatureEntreeDetailComponent } from '../detail/nature-entree-detail.component';
import { NatureEntreeUpdateComponent } from '../update/nature-entree-update.component';
import { NatureEntreeRoutingResolveService } from './nature-entree-routing-resolve.service';
import { ASC } from 'app/config/navigation.constants';

const natureEntreeRoute: Routes = [
  {
    path: '',
    component: NatureEntreeComponent,
    data: {
      defaultSort: 'id,' + ASC,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: NatureEntreeDetailComponent,
    resolve: {
      natureEntree: NatureEntreeRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: NatureEntreeUpdateComponent,
    resolve: {
      natureEntree: NatureEntreeRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: NatureEntreeUpdateComponent,
    resolve: {
      natureEntree: NatureEntreeRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(natureEntreeRoute)],
  exports: [RouterModule],
})
export class NatureEntreeRoutingModule {}

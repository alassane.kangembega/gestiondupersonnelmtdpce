import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { INatureEntree } from '../nature-entree.model';
import { NatureEntreeService } from '../service/nature-entree.service';

@Injectable({ providedIn: 'root' })
export class NatureEntreeRoutingResolveService implements Resolve<INatureEntree | null> {
  constructor(protected service: NatureEntreeService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<INatureEntree | null | never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((natureEntree: HttpResponse<INatureEntree>) => {
          if (natureEntree.body) {
            return of(natureEntree.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(null);
  }
}

import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { INatureEntree, NewNatureEntree } from '../nature-entree.model';

export type PartialUpdateNatureEntree = Partial<INatureEntree> & Pick<INatureEntree, 'id'>;

export type EntityResponseType = HttpResponse<INatureEntree>;
export type EntityArrayResponseType = HttpResponse<INatureEntree[]>;

@Injectable({ providedIn: 'root' })
export class NatureEntreeService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/nature-entrees');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(natureEntree: NewNatureEntree): Observable<EntityResponseType> {
    return this.http.post<INatureEntree>(this.resourceUrl, natureEntree, { observe: 'response' });
  }

  update(natureEntree: INatureEntree): Observable<EntityResponseType> {
    return this.http.put<INatureEntree>(`${this.resourceUrl}/${this.getNatureEntreeIdentifier(natureEntree)}`, natureEntree, {
      observe: 'response',
    });
  }

  partialUpdate(natureEntree: PartialUpdateNatureEntree): Observable<EntityResponseType> {
    return this.http.patch<INatureEntree>(`${this.resourceUrl}/${this.getNatureEntreeIdentifier(natureEntree)}`, natureEntree, {
      observe: 'response',
    });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<INatureEntree>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<INatureEntree[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  getNatureEntreeIdentifier(natureEntree: Pick<INatureEntree, 'id'>): number {
    return natureEntree.id;
  }

  compareNatureEntree(o1: Pick<INatureEntree, 'id'> | null, o2: Pick<INatureEntree, 'id'> | null): boolean {
    return o1 && o2 ? this.getNatureEntreeIdentifier(o1) === this.getNatureEntreeIdentifier(o2) : o1 === o2;
  }

  addNatureEntreeToCollectionIfMissing<Type extends Pick<INatureEntree, 'id'>>(
    natureEntreeCollection: Type[],
    ...natureEntreesToCheck: (Type | null | undefined)[]
  ): Type[] {
    const natureEntrees: Type[] = natureEntreesToCheck.filter(isPresent);
    if (natureEntrees.length > 0) {
      const natureEntreeCollectionIdentifiers = natureEntreeCollection.map(
        natureEntreeItem => this.getNatureEntreeIdentifier(natureEntreeItem)!
      );
      const natureEntreesToAdd = natureEntrees.filter(natureEntreeItem => {
        const natureEntreeIdentifier = this.getNatureEntreeIdentifier(natureEntreeItem);
        if (natureEntreeCollectionIdentifiers.includes(natureEntreeIdentifier)) {
          return false;
        }
        natureEntreeCollectionIdentifiers.push(natureEntreeIdentifier);
        return true;
      });
      return [...natureEntreesToAdd, ...natureEntreeCollection];
    }
    return natureEntreeCollection;
  }
}

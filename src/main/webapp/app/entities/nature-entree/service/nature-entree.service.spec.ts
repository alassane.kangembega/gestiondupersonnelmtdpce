import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { INatureEntree } from '../nature-entree.model';
import { sampleWithRequiredData, sampleWithNewData, sampleWithPartialData, sampleWithFullData } from '../nature-entree.test-samples';

import { NatureEntreeService } from './nature-entree.service';

const requireRestSample: INatureEntree = {
  ...sampleWithRequiredData,
};

describe('NatureEntree Service', () => {
  let service: NatureEntreeService;
  let httpMock: HttpTestingController;
  let expectedResult: INatureEntree | INatureEntree[] | boolean | null;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(NatureEntreeService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should create a NatureEntree', () => {
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      const natureEntree = { ...sampleWithNewData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.create(natureEntree).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a NatureEntree', () => {
      const natureEntree = { ...sampleWithRequiredData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.update(natureEntree).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a NatureEntree', () => {
      const patchObject = { ...sampleWithPartialData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of NatureEntree', () => {
      const returnedFromService = { ...requireRestSample };

      const expected = { ...sampleWithRequiredData };

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toMatchObject([expected]);
    });

    it('should delete a NatureEntree', () => {
      const expected = true;

      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult).toBe(expected);
    });

    describe('addNatureEntreeToCollectionIfMissing', () => {
      it('should add a NatureEntree to an empty array', () => {
        const natureEntree: INatureEntree = sampleWithRequiredData;
        expectedResult = service.addNatureEntreeToCollectionIfMissing([], natureEntree);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(natureEntree);
      });

      it('should not add a NatureEntree to an array that contains it', () => {
        const natureEntree: INatureEntree = sampleWithRequiredData;
        const natureEntreeCollection: INatureEntree[] = [
          {
            ...natureEntree,
          },
          sampleWithPartialData,
        ];
        expectedResult = service.addNatureEntreeToCollectionIfMissing(natureEntreeCollection, natureEntree);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a NatureEntree to an array that doesn't contain it", () => {
        const natureEntree: INatureEntree = sampleWithRequiredData;
        const natureEntreeCollection: INatureEntree[] = [sampleWithPartialData];
        expectedResult = service.addNatureEntreeToCollectionIfMissing(natureEntreeCollection, natureEntree);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(natureEntree);
      });

      it('should add only unique NatureEntree to an array', () => {
        const natureEntreeArray: INatureEntree[] = [sampleWithRequiredData, sampleWithPartialData, sampleWithFullData];
        const natureEntreeCollection: INatureEntree[] = [sampleWithRequiredData];
        expectedResult = service.addNatureEntreeToCollectionIfMissing(natureEntreeCollection, ...natureEntreeArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const natureEntree: INatureEntree = sampleWithRequiredData;
        const natureEntree2: INatureEntree = sampleWithPartialData;
        expectedResult = service.addNatureEntreeToCollectionIfMissing([], natureEntree, natureEntree2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(natureEntree);
        expect(expectedResult).toContain(natureEntree2);
      });

      it('should accept null and undefined values', () => {
        const natureEntree: INatureEntree = sampleWithRequiredData;
        expectedResult = service.addNatureEntreeToCollectionIfMissing([], null, natureEntree, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(natureEntree);
      });

      it('should return initial array if no NatureEntree is added', () => {
        const natureEntreeCollection: INatureEntree[] = [sampleWithRequiredData];
        expectedResult = service.addNatureEntreeToCollectionIfMissing(natureEntreeCollection, undefined, null);
        expect(expectedResult).toEqual(natureEntreeCollection);
      });
    });

    describe('compareNatureEntree', () => {
      it('Should return true if both entities are null', () => {
        const entity1 = null;
        const entity2 = null;

        const compareResult = service.compareNatureEntree(entity1, entity2);

        expect(compareResult).toEqual(true);
      });

      it('Should return false if one entity is null', () => {
        const entity1 = { id: 123 };
        const entity2 = null;

        const compareResult1 = service.compareNatureEntree(entity1, entity2);
        const compareResult2 = service.compareNatureEntree(entity2, entity1);

        expect(compareResult1).toEqual(false);
        expect(compareResult2).toEqual(false);
      });

      it('Should return false if primaryKey differs', () => {
        const entity1 = { id: 123 };
        const entity2 = { id: 456 };

        const compareResult1 = service.compareNatureEntree(entity1, entity2);
        const compareResult2 = service.compareNatureEntree(entity2, entity1);

        expect(compareResult1).toEqual(false);
        expect(compareResult2).toEqual(false);
      });

      it('Should return false if primaryKey matches', () => {
        const entity1 = { id: 123 };
        const entity2 = { id: 123 };

        const compareResult1 = service.compareNatureEntree(entity1, entity2);
        const compareResult2 = service.compareNatureEntree(entity2, entity1);

        expect(compareResult1).toEqual(true);
        expect(compareResult2).toEqual(true);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { NatureEntreeDetailComponent } from './nature-entree-detail.component';

describe('NatureEntree Management Detail Component', () => {
  let comp: NatureEntreeDetailComponent;
  let fixture: ComponentFixture<NatureEntreeDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [NatureEntreeDetailComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: { data: of({ natureEntree: { id: 123 } }) },
        },
      ],
    })
      .overrideTemplate(NatureEntreeDetailComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(NatureEntreeDetailComponent);
    comp = fixture.componentInstance;
  });

  describe('OnInit', () => {
    it('Should load natureEntree on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(comp.natureEntree).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});

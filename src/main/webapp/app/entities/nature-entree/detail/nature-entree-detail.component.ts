import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { INatureEntree } from '../nature-entree.model';

@Component({
  selector: 'jhi-nature-entree-detail',
  templateUrl: './nature-entree-detail.component.html',
})
export class NatureEntreeDetailComponent implements OnInit {
  natureEntree: INatureEntree | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ natureEntree }) => {
      this.natureEntree = natureEntree;
    });
  }

  previousState(): void {
    window.history.back();
  }
}

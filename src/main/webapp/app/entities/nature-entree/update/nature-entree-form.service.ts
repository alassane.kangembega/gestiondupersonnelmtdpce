import { Injectable } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { INatureEntree, NewNatureEntree } from '../nature-entree.model';

/**
 * A partial Type with required key is used as form input.
 */
type PartialWithRequiredKeyOf<T extends { id: unknown }> = Partial<Omit<T, 'id'>> & { id: T['id'] };

/**
 * Type for createFormGroup and resetForm argument.
 * It accepts INatureEntree for edit and NewNatureEntreeFormGroupInput for create.
 */
type NatureEntreeFormGroupInput = INatureEntree | PartialWithRequiredKeyOf<NewNatureEntree>;

type NatureEntreeFormDefaults = Pick<NewNatureEntree, 'id'>;

type NatureEntreeFormGroupContent = {
  id: FormControl<INatureEntree['id'] | NewNatureEntree['id']>;
  natureEntree: FormControl<INatureEntree['natureEntree']>;
};

export type NatureEntreeFormGroup = FormGroup<NatureEntreeFormGroupContent>;

@Injectable({ providedIn: 'root' })
export class NatureEntreeFormService {
  createNatureEntreeFormGroup(natureEntree: NatureEntreeFormGroupInput = { id: null }): NatureEntreeFormGroup {
    const natureEntreeRawValue = {
      ...this.getFormDefaults(),
      ...natureEntree,
    };
    return new FormGroup<NatureEntreeFormGroupContent>({
      id: new FormControl(
        { value: natureEntreeRawValue.id, disabled: true },
        {
          nonNullable: true,
          validators: [Validators.required],
        }
      ),
      natureEntree: new FormControl(natureEntreeRawValue.natureEntree),
    });
  }

  getNatureEntree(form: NatureEntreeFormGroup): INatureEntree | NewNatureEntree {
    return form.getRawValue() as INatureEntree | NewNatureEntree;
  }

  resetForm(form: NatureEntreeFormGroup, natureEntree: NatureEntreeFormGroupInput): void {
    const natureEntreeRawValue = { ...this.getFormDefaults(), ...natureEntree };
    form.reset(
      {
        ...natureEntreeRawValue,
        id: { value: natureEntreeRawValue.id, disabled: true },
      } as any /* cast to workaround https://github.com/angular/angular/issues/46458 */
    );
  }

  private getFormDefaults(): NatureEntreeFormDefaults {
    return {
      id: null,
    };
  }
}

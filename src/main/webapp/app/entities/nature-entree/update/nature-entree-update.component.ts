import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

import { NatureEntreeFormService, NatureEntreeFormGroup } from './nature-entree-form.service';
import { INatureEntree } from '../nature-entree.model';
import { NatureEntreeService } from '../service/nature-entree.service';

@Component({
  selector: 'jhi-nature-entree-update',
  templateUrl: './nature-entree-update.component.html',
})
export class NatureEntreeUpdateComponent implements OnInit {
  isSaving = false;
  natureEntree: INatureEntree | null = null;

  editForm: NatureEntreeFormGroup = this.natureEntreeFormService.createNatureEntreeFormGroup();

  constructor(
    protected natureEntreeService: NatureEntreeService,
    protected natureEntreeFormService: NatureEntreeFormService,
    protected activatedRoute: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ natureEntree }) => {
      this.natureEntree = natureEntree;
      if (natureEntree) {
        this.updateForm(natureEntree);
      }
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const natureEntree = this.natureEntreeFormService.getNatureEntree(this.editForm);
    if (natureEntree.id !== null) {
      this.subscribeToSaveResponse(this.natureEntreeService.update(natureEntree));
    } else {
      this.subscribeToSaveResponse(this.natureEntreeService.create(natureEntree));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<INatureEntree>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(natureEntree: INatureEntree): void {
    this.natureEntree = natureEntree;
    this.natureEntreeFormService.resetForm(this.editForm, natureEntree);
  }
}

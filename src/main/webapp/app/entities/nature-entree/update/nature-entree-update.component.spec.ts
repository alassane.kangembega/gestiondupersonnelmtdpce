import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { NatureEntreeFormService } from './nature-entree-form.service';
import { NatureEntreeService } from '../service/nature-entree.service';
import { INatureEntree } from '../nature-entree.model';

import { NatureEntreeUpdateComponent } from './nature-entree-update.component';

describe('NatureEntree Management Update Component', () => {
  let comp: NatureEntreeUpdateComponent;
  let fixture: ComponentFixture<NatureEntreeUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let natureEntreeFormService: NatureEntreeFormService;
  let natureEntreeService: NatureEntreeService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      declarations: [NatureEntreeUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(NatureEntreeUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(NatureEntreeUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    natureEntreeFormService = TestBed.inject(NatureEntreeFormService);
    natureEntreeService = TestBed.inject(NatureEntreeService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should update editForm', () => {
      const natureEntree: INatureEntree = { id: 456 };

      activatedRoute.data = of({ natureEntree });
      comp.ngOnInit();

      expect(comp.natureEntree).toEqual(natureEntree);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<INatureEntree>>();
      const natureEntree = { id: 123 };
      jest.spyOn(natureEntreeFormService, 'getNatureEntree').mockReturnValue(natureEntree);
      jest.spyOn(natureEntreeService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ natureEntree });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: natureEntree }));
      saveSubject.complete();

      // THEN
      expect(natureEntreeFormService.getNatureEntree).toHaveBeenCalled();
      expect(comp.previousState).toHaveBeenCalled();
      expect(natureEntreeService.update).toHaveBeenCalledWith(expect.objectContaining(natureEntree));
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<INatureEntree>>();
      const natureEntree = { id: 123 };
      jest.spyOn(natureEntreeFormService, 'getNatureEntree').mockReturnValue({ id: null });
      jest.spyOn(natureEntreeService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ natureEntree: null });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: natureEntree }));
      saveSubject.complete();

      // THEN
      expect(natureEntreeFormService.getNatureEntree).toHaveBeenCalled();
      expect(natureEntreeService.create).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<INatureEntree>>();
      const natureEntree = { id: 123 };
      jest.spyOn(natureEntreeService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ natureEntree });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(natureEntreeService.update).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });
});

import { TestBed } from '@angular/core/testing';

import { sampleWithRequiredData, sampleWithNewData } from '../nature-entree.test-samples';

import { NatureEntreeFormService } from './nature-entree-form.service';

describe('NatureEntree Form Service', () => {
  let service: NatureEntreeFormService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(NatureEntreeFormService);
  });

  describe('Service methods', () => {
    describe('createNatureEntreeFormGroup', () => {
      it('should create a new form with FormControl', () => {
        const formGroup = service.createNatureEntreeFormGroup();

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            natureEntree: expect.any(Object),
          })
        );
      });

      it('passing INatureEntree should create a new form with FormGroup', () => {
        const formGroup = service.createNatureEntreeFormGroup(sampleWithRequiredData);

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            natureEntree: expect.any(Object),
          })
        );
      });
    });

    describe('getNatureEntree', () => {
      it('should return NewNatureEntree for default NatureEntree initial value', () => {
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        const formGroup = service.createNatureEntreeFormGroup(sampleWithNewData);

        const natureEntree = service.getNatureEntree(formGroup) as any;

        expect(natureEntree).toMatchObject(sampleWithNewData);
      });

      it('should return NewNatureEntree for empty NatureEntree initial value', () => {
        const formGroup = service.createNatureEntreeFormGroup();

        const natureEntree = service.getNatureEntree(formGroup) as any;

        expect(natureEntree).toMatchObject({});
      });

      it('should return INatureEntree', () => {
        const formGroup = service.createNatureEntreeFormGroup(sampleWithRequiredData);

        const natureEntree = service.getNatureEntree(formGroup) as any;

        expect(natureEntree).toMatchObject(sampleWithRequiredData);
      });
    });

    describe('resetForm', () => {
      it('passing INatureEntree should not enable id FormControl', () => {
        const formGroup = service.createNatureEntreeFormGroup();
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, sampleWithRequiredData);

        expect(formGroup.controls.id.disabled).toBe(true);
      });

      it('passing NewNatureEntree should disable id FormControl', () => {
        const formGroup = service.createNatureEntreeFormGroup(sampleWithRequiredData);
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, { id: null });

        expect(formGroup.controls.id.disabled).toBe(true);
      });
    });
  });
});

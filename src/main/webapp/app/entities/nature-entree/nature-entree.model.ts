export interface INatureEntree {
  id: number;
  natureEntree?: string | null;
}

export type NewNatureEntree = Omit<INatureEntree, 'id'> & { id: null };

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';

import { NatureEntreeService } from '../service/nature-entree.service';

import { NatureEntreeComponent } from './nature-entree.component';

describe('NatureEntree Management Component', () => {
  let comp: NatureEntreeComponent;
  let fixture: ComponentFixture<NatureEntreeComponent>;
  let service: NatureEntreeService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule.withRoutes([{ path: 'nature-entree', component: NatureEntreeComponent }]), HttpClientTestingModule],
      declarations: [NatureEntreeComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            data: of({
              defaultSort: 'id,asc',
            }),
            queryParamMap: of(
              jest.requireActual('@angular/router').convertToParamMap({
                page: '1',
                size: '1',
                sort: 'id,desc',
              })
            ),
            snapshot: { queryParams: {} },
          },
        },
      ],
    })
      .overrideTemplate(NatureEntreeComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(NatureEntreeComponent);
    comp = fixture.componentInstance;
    service = TestBed.inject(NatureEntreeService);

    const headers = new HttpHeaders();
    jest.spyOn(service, 'query').mockReturnValue(
      of(
        new HttpResponse({
          body: [{ id: 123 }],
          headers,
        })
      )
    );
  });

  it('Should call load all on init', () => {
    // WHEN
    comp.ngOnInit();

    // THEN
    expect(service.query).toHaveBeenCalled();
    expect(comp.natureEntrees?.[0]).toEqual(expect.objectContaining({ id: 123 }));
  });

  describe('trackId', () => {
    it('Should forward to natureEntreeService', () => {
      const entity = { id: 123 };
      jest.spyOn(service, 'getNatureEntreeIdentifier');
      const id = comp.trackId(0, entity);
      expect(service.getNatureEntreeIdentifier).toHaveBeenCalledWith(entity);
      expect(id).toBe(entity.id);
    });
  });
});

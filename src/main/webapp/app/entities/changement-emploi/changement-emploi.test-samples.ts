import dayjs from 'dayjs/esm';

import { IChangementEmploi, NewChangementEmploi } from './changement-emploi.model';

export const sampleWithRequiredData: IChangementEmploi = {
  id: 16600,
};

export const sampleWithPartialData: IChangementEmploi = {
  id: 84035,
};

export const sampleWithFullData: IChangementEmploi = {
  id: 32241,
  dateChangement: dayjs('2022-09-06'),
};

export const sampleWithNewData: NewChangementEmploi = {
  id: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);

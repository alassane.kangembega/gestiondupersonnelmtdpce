import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IChangementEmploi } from '../changement-emploi.model';
import { ChangementEmploiService } from '../service/changement-emploi.service';
import { ITEM_DELETED_EVENT } from 'app/config/navigation.constants';

@Component({
  templateUrl: './changement-emploi-delete-dialog.component.html',
})
export class ChangementEmploiDeleteDialogComponent {
  changementEmploi?: IChangementEmploi;

  constructor(protected changementEmploiService: ChangementEmploiService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.changementEmploiService.delete(id).subscribe(() => {
      this.activeModal.close(ITEM_DELETED_EVENT);
    });
  }
}

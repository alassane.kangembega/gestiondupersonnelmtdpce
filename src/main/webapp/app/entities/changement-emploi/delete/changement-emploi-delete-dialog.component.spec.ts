jest.mock('@ng-bootstrap/ng-bootstrap');

import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of } from 'rxjs';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { ChangementEmploiService } from '../service/changement-emploi.service';

import { ChangementEmploiDeleteDialogComponent } from './changement-emploi-delete-dialog.component';

describe('ChangementEmploi Management Delete Component', () => {
  let comp: ChangementEmploiDeleteDialogComponent;
  let fixture: ComponentFixture<ChangementEmploiDeleteDialogComponent>;
  let service: ChangementEmploiService;
  let mockActiveModal: NgbActiveModal;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [ChangementEmploiDeleteDialogComponent],
      providers: [NgbActiveModal],
    })
      .overrideTemplate(ChangementEmploiDeleteDialogComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(ChangementEmploiDeleteDialogComponent);
    comp = fixture.componentInstance;
    service = TestBed.inject(ChangementEmploiService);
    mockActiveModal = TestBed.inject(NgbActiveModal);
  });

  describe('confirmDelete', () => {
    it('Should call delete service on confirmDelete', inject(
      [],
      fakeAsync(() => {
        // GIVEN
        jest.spyOn(service, 'delete').mockReturnValue(of(new HttpResponse({ body: {} })));

        // WHEN
        comp.confirmDelete(123);
        tick();

        // THEN
        expect(service.delete).toHaveBeenCalledWith(123);
        expect(mockActiveModal.close).toHaveBeenCalledWith('deleted');
      })
    ));

    it('Should not call delete service on clear', () => {
      // GIVEN
      jest.spyOn(service, 'delete');

      // WHEN
      comp.cancel();

      // THEN
      expect(service.delete).not.toHaveBeenCalled();
      expect(mockActiveModal.close).not.toHaveBeenCalled();
      expect(mockActiveModal.dismiss).toHaveBeenCalled();
    });
  });
});

import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { ChangementEmploiComponent } from './list/changement-emploi.component';
import { ChangementEmploiDetailComponent } from './detail/changement-emploi-detail.component';
import { ChangementEmploiUpdateComponent } from './update/changement-emploi-update.component';
import { ChangementEmploiDeleteDialogComponent } from './delete/changement-emploi-delete-dialog.component';
import { ChangementEmploiRoutingModule } from './route/changement-emploi-routing.module';

@NgModule({
  imports: [SharedModule, ChangementEmploiRoutingModule],
  declarations: [
    ChangementEmploiComponent,
    ChangementEmploiDetailComponent,
    ChangementEmploiUpdateComponent,
    ChangementEmploiDeleteDialogComponent,
  ],
})
export class ChangementEmploiModule {}

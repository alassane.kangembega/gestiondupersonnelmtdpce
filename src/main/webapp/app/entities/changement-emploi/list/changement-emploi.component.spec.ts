import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';

import { ChangementEmploiService } from '../service/changement-emploi.service';

import { ChangementEmploiComponent } from './changement-emploi.component';

describe('ChangementEmploi Management Component', () => {
  let comp: ChangementEmploiComponent;
  let fixture: ComponentFixture<ChangementEmploiComponent>;
  let service: ChangementEmploiService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule.withRoutes([{ path: 'changement-emploi', component: ChangementEmploiComponent }]),
        HttpClientTestingModule,
      ],
      declarations: [ChangementEmploiComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            data: of({
              defaultSort: 'id,asc',
            }),
            queryParamMap: of(
              jest.requireActual('@angular/router').convertToParamMap({
                page: '1',
                size: '1',
                sort: 'id,desc',
              })
            ),
            snapshot: { queryParams: {} },
          },
        },
      ],
    })
      .overrideTemplate(ChangementEmploiComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(ChangementEmploiComponent);
    comp = fixture.componentInstance;
    service = TestBed.inject(ChangementEmploiService);

    const headers = new HttpHeaders();
    jest.spyOn(service, 'query').mockReturnValue(
      of(
        new HttpResponse({
          body: [{ id: 123 }],
          headers,
        })
      )
    );
  });

  it('Should call load all on init', () => {
    // WHEN
    comp.ngOnInit();

    // THEN
    expect(service.query).toHaveBeenCalled();
    expect(comp.changementEmplois?.[0]).toEqual(expect.objectContaining({ id: 123 }));
  });

  describe('trackId', () => {
    it('Should forward to changementEmploiService', () => {
      const entity = { id: 123 };
      jest.spyOn(service, 'getChangementEmploiIdentifier');
      const id = comp.trackId(0, entity);
      expect(service.getChangementEmploiIdentifier).toHaveBeenCalledWith(entity);
      expect(id).toBe(entity.id);
    });
  });
});

import dayjs from 'dayjs/esm';
import { IEmploi } from 'app/entities/emploi/emploi.model';
import { IEmploye } from 'app/entities/employe/employe.model';

export interface IChangementEmploi {
  id: number;
  dateChangement?: dayjs.Dayjs | null;
  emploi?: Pick<IEmploi, 'id'> | null;
  employe?: Pick<IEmploye, 'id'> | null;
}

export type NewChangementEmploi = Omit<IChangementEmploi, 'id'> & { id: null };

import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { DATE_FORMAT } from 'app/config/input.constants';
import { IChangementEmploi } from '../changement-emploi.model';
import { sampleWithRequiredData, sampleWithNewData, sampleWithPartialData, sampleWithFullData } from '../changement-emploi.test-samples';

import { ChangementEmploiService, RestChangementEmploi } from './changement-emploi.service';

const requireRestSample: RestChangementEmploi = {
  ...sampleWithRequiredData,
  dateChangement: sampleWithRequiredData.dateChangement?.format(DATE_FORMAT),
};

describe('ChangementEmploi Service', () => {
  let service: ChangementEmploiService;
  let httpMock: HttpTestingController;
  let expectedResult: IChangementEmploi | IChangementEmploi[] | boolean | null;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(ChangementEmploiService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should create a ChangementEmploi', () => {
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      const changementEmploi = { ...sampleWithNewData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.create(changementEmploi).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a ChangementEmploi', () => {
      const changementEmploi = { ...sampleWithRequiredData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.update(changementEmploi).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a ChangementEmploi', () => {
      const patchObject = { ...sampleWithPartialData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of ChangementEmploi', () => {
      const returnedFromService = { ...requireRestSample };

      const expected = { ...sampleWithRequiredData };

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toMatchObject([expected]);
    });

    it('should delete a ChangementEmploi', () => {
      const expected = true;

      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult).toBe(expected);
    });

    describe('addChangementEmploiToCollectionIfMissing', () => {
      it('should add a ChangementEmploi to an empty array', () => {
        const changementEmploi: IChangementEmploi = sampleWithRequiredData;
        expectedResult = service.addChangementEmploiToCollectionIfMissing([], changementEmploi);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(changementEmploi);
      });

      it('should not add a ChangementEmploi to an array that contains it', () => {
        const changementEmploi: IChangementEmploi = sampleWithRequiredData;
        const changementEmploiCollection: IChangementEmploi[] = [
          {
            ...changementEmploi,
          },
          sampleWithPartialData,
        ];
        expectedResult = service.addChangementEmploiToCollectionIfMissing(changementEmploiCollection, changementEmploi);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a ChangementEmploi to an array that doesn't contain it", () => {
        const changementEmploi: IChangementEmploi = sampleWithRequiredData;
        const changementEmploiCollection: IChangementEmploi[] = [sampleWithPartialData];
        expectedResult = service.addChangementEmploiToCollectionIfMissing(changementEmploiCollection, changementEmploi);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(changementEmploi);
      });

      it('should add only unique ChangementEmploi to an array', () => {
        const changementEmploiArray: IChangementEmploi[] = [sampleWithRequiredData, sampleWithPartialData, sampleWithFullData];
        const changementEmploiCollection: IChangementEmploi[] = [sampleWithRequiredData];
        expectedResult = service.addChangementEmploiToCollectionIfMissing(changementEmploiCollection, ...changementEmploiArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const changementEmploi: IChangementEmploi = sampleWithRequiredData;
        const changementEmploi2: IChangementEmploi = sampleWithPartialData;
        expectedResult = service.addChangementEmploiToCollectionIfMissing([], changementEmploi, changementEmploi2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(changementEmploi);
        expect(expectedResult).toContain(changementEmploi2);
      });

      it('should accept null and undefined values', () => {
        const changementEmploi: IChangementEmploi = sampleWithRequiredData;
        expectedResult = service.addChangementEmploiToCollectionIfMissing([], null, changementEmploi, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(changementEmploi);
      });

      it('should return initial array if no ChangementEmploi is added', () => {
        const changementEmploiCollection: IChangementEmploi[] = [sampleWithRequiredData];
        expectedResult = service.addChangementEmploiToCollectionIfMissing(changementEmploiCollection, undefined, null);
        expect(expectedResult).toEqual(changementEmploiCollection);
      });
    });

    describe('compareChangementEmploi', () => {
      it('Should return true if both entities are null', () => {
        const entity1 = null;
        const entity2 = null;

        const compareResult = service.compareChangementEmploi(entity1, entity2);

        expect(compareResult).toEqual(true);
      });

      it('Should return false if one entity is null', () => {
        const entity1 = { id: 123 };
        const entity2 = null;

        const compareResult1 = service.compareChangementEmploi(entity1, entity2);
        const compareResult2 = service.compareChangementEmploi(entity2, entity1);

        expect(compareResult1).toEqual(false);
        expect(compareResult2).toEqual(false);
      });

      it('Should return false if primaryKey differs', () => {
        const entity1 = { id: 123 };
        const entity2 = { id: 456 };

        const compareResult1 = service.compareChangementEmploi(entity1, entity2);
        const compareResult2 = service.compareChangementEmploi(entity2, entity1);

        expect(compareResult1).toEqual(false);
        expect(compareResult2).toEqual(false);
      });

      it('Should return false if primaryKey matches', () => {
        const entity1 = { id: 123 };
        const entity2 = { id: 123 };

        const compareResult1 = service.compareChangementEmploi(entity1, entity2);
        const compareResult2 = service.compareChangementEmploi(entity2, entity1);

        expect(compareResult1).toEqual(true);
        expect(compareResult2).toEqual(true);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});

import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import dayjs from 'dayjs/esm';

import { isPresent } from 'app/core/util/operators';
import { DATE_FORMAT } from 'app/config/input.constants';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IChangementEmploi, NewChangementEmploi } from '../changement-emploi.model';

export type PartialUpdateChangementEmploi = Partial<IChangementEmploi> & Pick<IChangementEmploi, 'id'>;

type RestOf<T extends IChangementEmploi | NewChangementEmploi> = Omit<T, 'dateChangement'> & {
  dateChangement?: string | null;
};

export type RestChangementEmploi = RestOf<IChangementEmploi>;

export type NewRestChangementEmploi = RestOf<NewChangementEmploi>;

export type PartialUpdateRestChangementEmploi = RestOf<PartialUpdateChangementEmploi>;

export type EntityResponseType = HttpResponse<IChangementEmploi>;
export type EntityArrayResponseType = HttpResponse<IChangementEmploi[]>;

@Injectable({ providedIn: 'root' })
export class ChangementEmploiService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/changement-emplois');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(changementEmploi: NewChangementEmploi): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(changementEmploi);
    return this.http
      .post<RestChangementEmploi>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  update(changementEmploi: IChangementEmploi): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(changementEmploi);
    return this.http
      .put<RestChangementEmploi>(`${this.resourceUrl}/${this.getChangementEmploiIdentifier(changementEmploi)}`, copy, {
        observe: 'response',
      })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  partialUpdate(changementEmploi: PartialUpdateChangementEmploi): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(changementEmploi);
    return this.http
      .patch<RestChangementEmploi>(`${this.resourceUrl}/${this.getChangementEmploiIdentifier(changementEmploi)}`, copy, {
        observe: 'response',
      })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<RestChangementEmploi>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<RestChangementEmploi[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map(res => this.convertResponseArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  getChangementEmploiIdentifier(changementEmploi: Pick<IChangementEmploi, 'id'>): number {
    return changementEmploi.id;
  }

  compareChangementEmploi(o1: Pick<IChangementEmploi, 'id'> | null, o2: Pick<IChangementEmploi, 'id'> | null): boolean {
    return o1 && o2 ? this.getChangementEmploiIdentifier(o1) === this.getChangementEmploiIdentifier(o2) : o1 === o2;
  }

  addChangementEmploiToCollectionIfMissing<Type extends Pick<IChangementEmploi, 'id'>>(
    changementEmploiCollection: Type[],
    ...changementEmploisToCheck: (Type | null | undefined)[]
  ): Type[] {
    const changementEmplois: Type[] = changementEmploisToCheck.filter(isPresent);
    if (changementEmplois.length > 0) {
      const changementEmploiCollectionIdentifiers = changementEmploiCollection.map(
        changementEmploiItem => this.getChangementEmploiIdentifier(changementEmploiItem)!
      );
      const changementEmploisToAdd = changementEmplois.filter(changementEmploiItem => {
        const changementEmploiIdentifier = this.getChangementEmploiIdentifier(changementEmploiItem);
        if (changementEmploiCollectionIdentifiers.includes(changementEmploiIdentifier)) {
          return false;
        }
        changementEmploiCollectionIdentifiers.push(changementEmploiIdentifier);
        return true;
      });
      return [...changementEmploisToAdd, ...changementEmploiCollection];
    }
    return changementEmploiCollection;
  }

  protected convertDateFromClient<T extends IChangementEmploi | NewChangementEmploi | PartialUpdateChangementEmploi>(
    changementEmploi: T
  ): RestOf<T> {
    return {
      ...changementEmploi,
      dateChangement: changementEmploi.dateChangement?.format(DATE_FORMAT) ?? null,
    };
  }

  protected convertDateFromServer(restChangementEmploi: RestChangementEmploi): IChangementEmploi {
    return {
      ...restChangementEmploi,
      dateChangement: restChangementEmploi.dateChangement ? dayjs(restChangementEmploi.dateChangement) : undefined,
    };
  }

  protected convertResponseFromServer(res: HttpResponse<RestChangementEmploi>): HttpResponse<IChangementEmploi> {
    return res.clone({
      body: res.body ? this.convertDateFromServer(res.body) : null,
    });
  }

  protected convertResponseArrayFromServer(res: HttpResponse<RestChangementEmploi[]>): HttpResponse<IChangementEmploi[]> {
    return res.clone({
      body: res.body ? res.body.map(item => this.convertDateFromServer(item)) : null,
    });
  }
}

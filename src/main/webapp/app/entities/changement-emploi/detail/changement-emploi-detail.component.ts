import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IChangementEmploi } from '../changement-emploi.model';

@Component({
  selector: 'jhi-changement-emploi-detail',
  templateUrl: './changement-emploi-detail.component.html',
})
export class ChangementEmploiDetailComponent implements OnInit {
  changementEmploi: IChangementEmploi | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ changementEmploi }) => {
      this.changementEmploi = changementEmploi;
    });
  }

  previousState(): void {
    window.history.back();
  }
}

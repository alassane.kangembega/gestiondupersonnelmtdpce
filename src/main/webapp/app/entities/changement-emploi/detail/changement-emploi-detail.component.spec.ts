import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { ChangementEmploiDetailComponent } from './changement-emploi-detail.component';

describe('ChangementEmploi Management Detail Component', () => {
  let comp: ChangementEmploiDetailComponent;
  let fixture: ComponentFixture<ChangementEmploiDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ChangementEmploiDetailComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: { data: of({ changementEmploi: { id: 123 } }) },
        },
      ],
    })
      .overrideTemplate(ChangementEmploiDetailComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(ChangementEmploiDetailComponent);
    comp = fixture.componentInstance;
  });

  describe('OnInit', () => {
    it('Should load changementEmploi on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(comp.changementEmploi).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});

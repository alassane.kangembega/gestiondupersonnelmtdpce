import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { ChangementEmploiFormService } from './changement-emploi-form.service';
import { ChangementEmploiService } from '../service/changement-emploi.service';
import { IChangementEmploi } from '../changement-emploi.model';
import { IEmploi } from 'app/entities/emploi/emploi.model';
import { EmploiService } from 'app/entities/emploi/service/emploi.service';
import { IEmploye } from 'app/entities/employe/employe.model';
import { EmployeService } from 'app/entities/employe/service/employe.service';

import { ChangementEmploiUpdateComponent } from './changement-emploi-update.component';

describe('ChangementEmploi Management Update Component', () => {
  let comp: ChangementEmploiUpdateComponent;
  let fixture: ComponentFixture<ChangementEmploiUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let changementEmploiFormService: ChangementEmploiFormService;
  let changementEmploiService: ChangementEmploiService;
  let emploiService: EmploiService;
  let employeService: EmployeService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      declarations: [ChangementEmploiUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(ChangementEmploiUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(ChangementEmploiUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    changementEmploiFormService = TestBed.inject(ChangementEmploiFormService);
    changementEmploiService = TestBed.inject(ChangementEmploiService);
    emploiService = TestBed.inject(EmploiService);
    employeService = TestBed.inject(EmployeService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call Emploi query and add missing value', () => {
      const changementEmploi: IChangementEmploi = { id: 456 };
      const emploi: IEmploi = { id: 82784 };
      changementEmploi.emploi = emploi;

      const emploiCollection: IEmploi[] = [{ id: 29481 }];
      jest.spyOn(emploiService, 'query').mockReturnValue(of(new HttpResponse({ body: emploiCollection })));
      const additionalEmplois = [emploi];
      const expectedCollection: IEmploi[] = [...additionalEmplois, ...emploiCollection];
      jest.spyOn(emploiService, 'addEmploiToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ changementEmploi });
      comp.ngOnInit();

      expect(emploiService.query).toHaveBeenCalled();
      expect(emploiService.addEmploiToCollectionIfMissing).toHaveBeenCalledWith(
        emploiCollection,
        ...additionalEmplois.map(expect.objectContaining)
      );
      expect(comp.emploisSharedCollection).toEqual(expectedCollection);
    });

    it('Should call Employe query and add missing value', () => {
      const changementEmploi: IChangementEmploi = { id: 456 };
      const employe: IEmploye = { id: 49618 };
      changementEmploi.employe = employe;

      const employeCollection: IEmploye[] = [{ id: 15292 }];
      jest.spyOn(employeService, 'query').mockReturnValue(of(new HttpResponse({ body: employeCollection })));
      const additionalEmployes = [employe];
      const expectedCollection: IEmploye[] = [...additionalEmployes, ...employeCollection];
      jest.spyOn(employeService, 'addEmployeToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ changementEmploi });
      comp.ngOnInit();

      expect(employeService.query).toHaveBeenCalled();
      expect(employeService.addEmployeToCollectionIfMissing).toHaveBeenCalledWith(
        employeCollection,
        ...additionalEmployes.map(expect.objectContaining)
      );
      expect(comp.employesSharedCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const changementEmploi: IChangementEmploi = { id: 456 };
      const emploi: IEmploi = { id: 87913 };
      changementEmploi.emploi = emploi;
      const employe: IEmploye = { id: 72711 };
      changementEmploi.employe = employe;

      activatedRoute.data = of({ changementEmploi });
      comp.ngOnInit();

      expect(comp.emploisSharedCollection).toContain(emploi);
      expect(comp.employesSharedCollection).toContain(employe);
      expect(comp.changementEmploi).toEqual(changementEmploi);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IChangementEmploi>>();
      const changementEmploi = { id: 123 };
      jest.spyOn(changementEmploiFormService, 'getChangementEmploi').mockReturnValue(changementEmploi);
      jest.spyOn(changementEmploiService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ changementEmploi });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: changementEmploi }));
      saveSubject.complete();

      // THEN
      expect(changementEmploiFormService.getChangementEmploi).toHaveBeenCalled();
      expect(comp.previousState).toHaveBeenCalled();
      expect(changementEmploiService.update).toHaveBeenCalledWith(expect.objectContaining(changementEmploi));
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IChangementEmploi>>();
      const changementEmploi = { id: 123 };
      jest.spyOn(changementEmploiFormService, 'getChangementEmploi').mockReturnValue({ id: null });
      jest.spyOn(changementEmploiService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ changementEmploi: null });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: changementEmploi }));
      saveSubject.complete();

      // THEN
      expect(changementEmploiFormService.getChangementEmploi).toHaveBeenCalled();
      expect(changementEmploiService.create).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IChangementEmploi>>();
      const changementEmploi = { id: 123 };
      jest.spyOn(changementEmploiService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ changementEmploi });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(changementEmploiService.update).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Compare relationships', () => {
    describe('compareEmploi', () => {
      it('Should forward to emploiService', () => {
        const entity = { id: 123 };
        const entity2 = { id: 456 };
        jest.spyOn(emploiService, 'compareEmploi');
        comp.compareEmploi(entity, entity2);
        expect(emploiService.compareEmploi).toHaveBeenCalledWith(entity, entity2);
      });
    });

    describe('compareEmploye', () => {
      it('Should forward to employeService', () => {
        const entity = { id: 123 };
        const entity2 = { id: 456 };
        jest.spyOn(employeService, 'compareEmploye');
        comp.compareEmploye(entity, entity2);
        expect(employeService.compareEmploye).toHaveBeenCalledWith(entity, entity2);
      });
    });
  });
});

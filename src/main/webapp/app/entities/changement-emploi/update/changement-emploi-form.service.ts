import { Injectable } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { IChangementEmploi, NewChangementEmploi } from '../changement-emploi.model';

/**
 * A partial Type with required key is used as form input.
 */
type PartialWithRequiredKeyOf<T extends { id: unknown }> = Partial<Omit<T, 'id'>> & { id: T['id'] };

/**
 * Type for createFormGroup and resetForm argument.
 * It accepts IChangementEmploi for edit and NewChangementEmploiFormGroupInput for create.
 */
type ChangementEmploiFormGroupInput = IChangementEmploi | PartialWithRequiredKeyOf<NewChangementEmploi>;

type ChangementEmploiFormDefaults = Pick<NewChangementEmploi, 'id'>;

type ChangementEmploiFormGroupContent = {
  id: FormControl<IChangementEmploi['id'] | NewChangementEmploi['id']>;
  dateChangement: FormControl<IChangementEmploi['dateChangement']>;
  emploi: FormControl<IChangementEmploi['emploi']>;
  employe: FormControl<IChangementEmploi['employe']>;
};

export type ChangementEmploiFormGroup = FormGroup<ChangementEmploiFormGroupContent>;

@Injectable({ providedIn: 'root' })
export class ChangementEmploiFormService {
  createChangementEmploiFormGroup(changementEmploi: ChangementEmploiFormGroupInput = { id: null }): ChangementEmploiFormGroup {
    const changementEmploiRawValue = {
      ...this.getFormDefaults(),
      ...changementEmploi,
    };
    return new FormGroup<ChangementEmploiFormGroupContent>({
      id: new FormControl(
        { value: changementEmploiRawValue.id, disabled: true },
        {
          nonNullable: true,
          validators: [Validators.required],
        }
      ),
      dateChangement: new FormControl(changementEmploiRawValue.dateChangement),
      emploi: new FormControl(changementEmploiRawValue.emploi),
      employe: new FormControl(changementEmploiRawValue.employe),
    });
  }

  getChangementEmploi(form: ChangementEmploiFormGroup): IChangementEmploi | NewChangementEmploi {
    return form.getRawValue() as IChangementEmploi | NewChangementEmploi;
  }

  resetForm(form: ChangementEmploiFormGroup, changementEmploi: ChangementEmploiFormGroupInput): void {
    const changementEmploiRawValue = { ...this.getFormDefaults(), ...changementEmploi };
    form.reset(
      {
        ...changementEmploiRawValue,
        id: { value: changementEmploiRawValue.id, disabled: true },
      } as any /* cast to workaround https://github.com/angular/angular/issues/46458 */
    );
  }

  private getFormDefaults(): ChangementEmploiFormDefaults {
    return {
      id: null,
    };
  }
}

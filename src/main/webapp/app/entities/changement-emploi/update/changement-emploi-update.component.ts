import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { ChangementEmploiFormService, ChangementEmploiFormGroup } from './changement-emploi-form.service';
import { IChangementEmploi } from '../changement-emploi.model';
import { ChangementEmploiService } from '../service/changement-emploi.service';
import { IEmploi } from 'app/entities/emploi/emploi.model';
import { EmploiService } from 'app/entities/emploi/service/emploi.service';
import { IEmploye } from 'app/entities/employe/employe.model';
import { EmployeService } from 'app/entities/employe/service/employe.service';

@Component({
  selector: 'jhi-changement-emploi-update',
  templateUrl: './changement-emploi-update.component.html',
})
export class ChangementEmploiUpdateComponent implements OnInit {
  isSaving = false;
  changementEmploi: IChangementEmploi | null = null;

  emploisSharedCollection: IEmploi[] = [];
  employesSharedCollection: IEmploye[] = [];

  editForm: ChangementEmploiFormGroup = this.changementEmploiFormService.createChangementEmploiFormGroup();

  constructor(
    protected changementEmploiService: ChangementEmploiService,
    protected changementEmploiFormService: ChangementEmploiFormService,
    protected emploiService: EmploiService,
    protected employeService: EmployeService,
    protected activatedRoute: ActivatedRoute
  ) {}

  compareEmploi = (o1: IEmploi | null, o2: IEmploi | null): boolean => this.emploiService.compareEmploi(o1, o2);

  compareEmploye = (o1: IEmploye | null, o2: IEmploye | null): boolean => this.employeService.compareEmploye(o1, o2);

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ changementEmploi }) => {
      this.changementEmploi = changementEmploi;
      if (changementEmploi) {
        this.updateForm(changementEmploi);
      }

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const changementEmploi = this.changementEmploiFormService.getChangementEmploi(this.editForm);
    if (changementEmploi.id !== null) {
      this.subscribeToSaveResponse(this.changementEmploiService.update(changementEmploi));
    } else {
      this.subscribeToSaveResponse(this.changementEmploiService.create(changementEmploi));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IChangementEmploi>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(changementEmploi: IChangementEmploi): void {
    this.changementEmploi = changementEmploi;
    this.changementEmploiFormService.resetForm(this.editForm, changementEmploi);

    this.emploisSharedCollection = this.emploiService.addEmploiToCollectionIfMissing<IEmploi>(
      this.emploisSharedCollection,
      changementEmploi.emploi
    );
    this.employesSharedCollection = this.employeService.addEmployeToCollectionIfMissing<IEmploye>(
      this.employesSharedCollection,
      changementEmploi.employe
    );
  }

  protected loadRelationshipsOptions(): void {
    this.emploiService
      .query()
      .pipe(map((res: HttpResponse<IEmploi[]>) => res.body ?? []))
      .pipe(map((emplois: IEmploi[]) => this.emploiService.addEmploiToCollectionIfMissing<IEmploi>(emplois, this.changementEmploi?.emploi)))
      .subscribe((emplois: IEmploi[]) => (this.emploisSharedCollection = emplois));

    this.employeService
      .query()
      .pipe(map((res: HttpResponse<IEmploye[]>) => res.body ?? []))
      .pipe(
        map((employes: IEmploye[]) =>
          this.employeService.addEmployeToCollectionIfMissing<IEmploye>(employes, this.changementEmploi?.employe)
        )
      )
      .subscribe((employes: IEmploye[]) => (this.employesSharedCollection = employes));
  }
}

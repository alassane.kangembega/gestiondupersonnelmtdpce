import { TestBed } from '@angular/core/testing';

import { sampleWithRequiredData, sampleWithNewData } from '../changement-emploi.test-samples';

import { ChangementEmploiFormService } from './changement-emploi-form.service';

describe('ChangementEmploi Form Service', () => {
  let service: ChangementEmploiFormService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ChangementEmploiFormService);
  });

  describe('Service methods', () => {
    describe('createChangementEmploiFormGroup', () => {
      it('should create a new form with FormControl', () => {
        const formGroup = service.createChangementEmploiFormGroup();

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            dateChangement: expect.any(Object),
            emploi: expect.any(Object),
            employe: expect.any(Object),
          })
        );
      });

      it('passing IChangementEmploi should create a new form with FormGroup', () => {
        const formGroup = service.createChangementEmploiFormGroup(sampleWithRequiredData);

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            dateChangement: expect.any(Object),
            emploi: expect.any(Object),
            employe: expect.any(Object),
          })
        );
      });
    });

    describe('getChangementEmploi', () => {
      it('should return NewChangementEmploi for default ChangementEmploi initial value', () => {
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        const formGroup = service.createChangementEmploiFormGroup(sampleWithNewData);

        const changementEmploi = service.getChangementEmploi(formGroup) as any;

        expect(changementEmploi).toMatchObject(sampleWithNewData);
      });

      it('should return NewChangementEmploi for empty ChangementEmploi initial value', () => {
        const formGroup = service.createChangementEmploiFormGroup();

        const changementEmploi = service.getChangementEmploi(formGroup) as any;

        expect(changementEmploi).toMatchObject({});
      });

      it('should return IChangementEmploi', () => {
        const formGroup = service.createChangementEmploiFormGroup(sampleWithRequiredData);

        const changementEmploi = service.getChangementEmploi(formGroup) as any;

        expect(changementEmploi).toMatchObject(sampleWithRequiredData);
      });
    });

    describe('resetForm', () => {
      it('passing IChangementEmploi should not enable id FormControl', () => {
        const formGroup = service.createChangementEmploiFormGroup();
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, sampleWithRequiredData);

        expect(formGroup.controls.id.disabled).toBe(true);
      });

      it('passing NewChangementEmploi should disable id FormControl', () => {
        const formGroup = service.createChangementEmploiFormGroup(sampleWithRequiredData);
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, { id: null });

        expect(formGroup.controls.id.disabled).toBe(true);
      });
    });
  });
});

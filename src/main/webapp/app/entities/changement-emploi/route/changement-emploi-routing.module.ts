import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { ChangementEmploiComponent } from '../list/changement-emploi.component';
import { ChangementEmploiDetailComponent } from '../detail/changement-emploi-detail.component';
import { ChangementEmploiUpdateComponent } from '../update/changement-emploi-update.component';
import { ChangementEmploiRoutingResolveService } from './changement-emploi-routing-resolve.service';
import { ASC } from 'app/config/navigation.constants';

const changementEmploiRoute: Routes = [
  {
    path: '',
    component: ChangementEmploiComponent,
    data: {
      defaultSort: 'id,' + ASC,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: ChangementEmploiDetailComponent,
    resolve: {
      changementEmploi: ChangementEmploiRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: ChangementEmploiUpdateComponent,
    resolve: {
      changementEmploi: ChangementEmploiRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: ChangementEmploiUpdateComponent,
    resolve: {
      changementEmploi: ChangementEmploiRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(changementEmploiRoute)],
  exports: [RouterModule],
})
export class ChangementEmploiRoutingModule {}

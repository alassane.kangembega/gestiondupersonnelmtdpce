import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IChangementEmploi } from '../changement-emploi.model';
import { ChangementEmploiService } from '../service/changement-emploi.service';

@Injectable({ providedIn: 'root' })
export class ChangementEmploiRoutingResolveService implements Resolve<IChangementEmploi | null> {
  constructor(protected service: ChangementEmploiService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IChangementEmploi | null | never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((changementEmploi: HttpResponse<IChangementEmploi>) => {
          if (changementEmploi.body) {
            return of(changementEmploi.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(null);
  }
}

export interface IRegion {
  id: number;
  libelle?: string | null;
}

export type NewRegion = Omit<IRegion, 'id'> & { id: null };

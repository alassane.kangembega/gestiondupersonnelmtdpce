import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';

import { NatureStageService } from '../service/nature-stage.service';

import { NatureStageComponent } from './nature-stage.component';

describe('NatureStage Management Component', () => {
  let comp: NatureStageComponent;
  let fixture: ComponentFixture<NatureStageComponent>;
  let service: NatureStageService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule.withRoutes([{ path: 'nature-stage', component: NatureStageComponent }]), HttpClientTestingModule],
      declarations: [NatureStageComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            data: of({
              defaultSort: 'id,asc',
            }),
            queryParamMap: of(
              jest.requireActual('@angular/router').convertToParamMap({
                page: '1',
                size: '1',
                sort: 'id,desc',
              })
            ),
            snapshot: { queryParams: {} },
          },
        },
      ],
    })
      .overrideTemplate(NatureStageComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(NatureStageComponent);
    comp = fixture.componentInstance;
    service = TestBed.inject(NatureStageService);

    const headers = new HttpHeaders();
    jest.spyOn(service, 'query').mockReturnValue(
      of(
        new HttpResponse({
          body: [{ id: 123 }],
          headers,
        })
      )
    );
  });

  it('Should call load all on init', () => {
    // WHEN
    comp.ngOnInit();

    // THEN
    expect(service.query).toHaveBeenCalled();
    expect(comp.natureStages?.[0]).toEqual(expect.objectContaining({ id: 123 }));
  });

  describe('trackId', () => {
    it('Should forward to natureStageService', () => {
      const entity = { id: 123 };
      jest.spyOn(service, 'getNatureStageIdentifier');
      const id = comp.trackId(0, entity);
      expect(service.getNatureStageIdentifier).toHaveBeenCalledWith(entity);
      expect(id).toBe(entity.id);
    });
  });
});

import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { INatureStage } from '../nature-stage.model';
import { NatureStageService } from '../service/nature-stage.service';
import { ITEM_DELETED_EVENT } from 'app/config/navigation.constants';

@Component({
  templateUrl: './nature-stage-delete-dialog.component.html',
})
export class NatureStageDeleteDialogComponent {
  natureStage?: INatureStage;

  constructor(protected natureStageService: NatureStageService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.natureStageService.delete(id).subscribe(() => {
      this.activeModal.close(ITEM_DELETED_EVENT);
    });
  }
}

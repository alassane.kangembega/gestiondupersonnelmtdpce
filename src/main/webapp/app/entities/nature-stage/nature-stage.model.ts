export interface INatureStage {
  id: number;
  natureStage?: string | null;
}

export type NewNatureStage = Omit<INatureStage, 'id'> & { id: null };

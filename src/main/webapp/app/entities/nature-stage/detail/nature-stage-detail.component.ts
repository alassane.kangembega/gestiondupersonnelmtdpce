import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { INatureStage } from '../nature-stage.model';

@Component({
  selector: 'jhi-nature-stage-detail',
  templateUrl: './nature-stage-detail.component.html',
})
export class NatureStageDetailComponent implements OnInit {
  natureStage: INatureStage | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ natureStage }) => {
      this.natureStage = natureStage;
    });
  }

  previousState(): void {
    window.history.back();
  }
}

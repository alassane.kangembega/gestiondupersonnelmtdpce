import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { NatureStageDetailComponent } from './nature-stage-detail.component';

describe('NatureStage Management Detail Component', () => {
  let comp: NatureStageDetailComponent;
  let fixture: ComponentFixture<NatureStageDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [NatureStageDetailComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: { data: of({ natureStage: { id: 123 } }) },
        },
      ],
    })
      .overrideTemplate(NatureStageDetailComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(NatureStageDetailComponent);
    comp = fixture.componentInstance;
  });

  describe('OnInit', () => {
    it('Should load natureStage on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(comp.natureStage).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});

import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { INatureStage } from '../nature-stage.model';
import { NatureStageService } from '../service/nature-stage.service';

@Injectable({ providedIn: 'root' })
export class NatureStageRoutingResolveService implements Resolve<INatureStage | null> {
  constructor(protected service: NatureStageService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<INatureStage | null | never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((natureStage: HttpResponse<INatureStage>) => {
          if (natureStage.body) {
            return of(natureStage.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(null);
  }
}

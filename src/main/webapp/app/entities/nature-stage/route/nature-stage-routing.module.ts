import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { NatureStageComponent } from '../list/nature-stage.component';
import { NatureStageDetailComponent } from '../detail/nature-stage-detail.component';
import { NatureStageUpdateComponent } from '../update/nature-stage-update.component';
import { NatureStageRoutingResolveService } from './nature-stage-routing-resolve.service';
import { ASC } from 'app/config/navigation.constants';

const natureStageRoute: Routes = [
  {
    path: '',
    component: NatureStageComponent,
    data: {
      defaultSort: 'id,' + ASC,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: NatureStageDetailComponent,
    resolve: {
      natureStage: NatureStageRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: NatureStageUpdateComponent,
    resolve: {
      natureStage: NatureStageRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: NatureStageUpdateComponent,
    resolve: {
      natureStage: NatureStageRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(natureStageRoute)],
  exports: [RouterModule],
})
export class NatureStageRoutingModule {}

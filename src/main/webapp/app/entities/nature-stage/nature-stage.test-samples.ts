import { INatureStage, NewNatureStage } from './nature-stage.model';

export const sampleWithRequiredData: INatureStage = {
  id: 23882,
};

export const sampleWithPartialData: INatureStage = {
  id: 12637,
  natureStage: 'Superviseur AI',
};

export const sampleWithFullData: INatureStage = {
  id: 90489,
  natureStage: 'Cambridgeshire',
};

export const sampleWithNewData: NewNatureStage = {
  id: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);

import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

import { NatureStageFormService, NatureStageFormGroup } from './nature-stage-form.service';
import { INatureStage } from '../nature-stage.model';
import { NatureStageService } from '../service/nature-stage.service';

@Component({
  selector: 'jhi-nature-stage-update',
  templateUrl: './nature-stage-update.component.html',
})
export class NatureStageUpdateComponent implements OnInit {
  isSaving = false;
  natureStage: INatureStage | null = null;

  editForm: NatureStageFormGroup = this.natureStageFormService.createNatureStageFormGroup();

  constructor(
    protected natureStageService: NatureStageService,
    protected natureStageFormService: NatureStageFormService,
    protected activatedRoute: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ natureStage }) => {
      this.natureStage = natureStage;
      if (natureStage) {
        this.updateForm(natureStage);
      }
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const natureStage = this.natureStageFormService.getNatureStage(this.editForm);
    if (natureStage.id !== null) {
      this.subscribeToSaveResponse(this.natureStageService.update(natureStage));
    } else {
      this.subscribeToSaveResponse(this.natureStageService.create(natureStage));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<INatureStage>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(natureStage: INatureStage): void {
    this.natureStage = natureStage;
    this.natureStageFormService.resetForm(this.editForm, natureStage);
  }
}

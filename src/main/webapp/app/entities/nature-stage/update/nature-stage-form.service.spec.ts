import { TestBed } from '@angular/core/testing';

import { sampleWithRequiredData, sampleWithNewData } from '../nature-stage.test-samples';

import { NatureStageFormService } from './nature-stage-form.service';

describe('NatureStage Form Service', () => {
  let service: NatureStageFormService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(NatureStageFormService);
  });

  describe('Service methods', () => {
    describe('createNatureStageFormGroup', () => {
      it('should create a new form with FormControl', () => {
        const formGroup = service.createNatureStageFormGroup();

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            natureStage: expect.any(Object),
          })
        );
      });

      it('passing INatureStage should create a new form with FormGroup', () => {
        const formGroup = service.createNatureStageFormGroup(sampleWithRequiredData);

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            natureStage: expect.any(Object),
          })
        );
      });
    });

    describe('getNatureStage', () => {
      it('should return NewNatureStage for default NatureStage initial value', () => {
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        const formGroup = service.createNatureStageFormGroup(sampleWithNewData);

        const natureStage = service.getNatureStage(formGroup) as any;

        expect(natureStage).toMatchObject(sampleWithNewData);
      });

      it('should return NewNatureStage for empty NatureStage initial value', () => {
        const formGroup = service.createNatureStageFormGroup();

        const natureStage = service.getNatureStage(formGroup) as any;

        expect(natureStage).toMatchObject({});
      });

      it('should return INatureStage', () => {
        const formGroup = service.createNatureStageFormGroup(sampleWithRequiredData);

        const natureStage = service.getNatureStage(formGroup) as any;

        expect(natureStage).toMatchObject(sampleWithRequiredData);
      });
    });

    describe('resetForm', () => {
      it('passing INatureStage should not enable id FormControl', () => {
        const formGroup = service.createNatureStageFormGroup();
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, sampleWithRequiredData);

        expect(formGroup.controls.id.disabled).toBe(true);
      });

      it('passing NewNatureStage should disable id FormControl', () => {
        const formGroup = service.createNatureStageFormGroup(sampleWithRequiredData);
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, { id: null });

        expect(formGroup.controls.id.disabled).toBe(true);
      });
    });
  });
});

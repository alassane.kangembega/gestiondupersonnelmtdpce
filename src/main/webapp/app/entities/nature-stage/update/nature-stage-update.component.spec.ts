import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { NatureStageFormService } from './nature-stage-form.service';
import { NatureStageService } from '../service/nature-stage.service';
import { INatureStage } from '../nature-stage.model';

import { NatureStageUpdateComponent } from './nature-stage-update.component';

describe('NatureStage Management Update Component', () => {
  let comp: NatureStageUpdateComponent;
  let fixture: ComponentFixture<NatureStageUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let natureStageFormService: NatureStageFormService;
  let natureStageService: NatureStageService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      declarations: [NatureStageUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(NatureStageUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(NatureStageUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    natureStageFormService = TestBed.inject(NatureStageFormService);
    natureStageService = TestBed.inject(NatureStageService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should update editForm', () => {
      const natureStage: INatureStage = { id: 456 };

      activatedRoute.data = of({ natureStage });
      comp.ngOnInit();

      expect(comp.natureStage).toEqual(natureStage);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<INatureStage>>();
      const natureStage = { id: 123 };
      jest.spyOn(natureStageFormService, 'getNatureStage').mockReturnValue(natureStage);
      jest.spyOn(natureStageService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ natureStage });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: natureStage }));
      saveSubject.complete();

      // THEN
      expect(natureStageFormService.getNatureStage).toHaveBeenCalled();
      expect(comp.previousState).toHaveBeenCalled();
      expect(natureStageService.update).toHaveBeenCalledWith(expect.objectContaining(natureStage));
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<INatureStage>>();
      const natureStage = { id: 123 };
      jest.spyOn(natureStageFormService, 'getNatureStage').mockReturnValue({ id: null });
      jest.spyOn(natureStageService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ natureStage: null });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: natureStage }));
      saveSubject.complete();

      // THEN
      expect(natureStageFormService.getNatureStage).toHaveBeenCalled();
      expect(natureStageService.create).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<INatureStage>>();
      const natureStage = { id: 123 };
      jest.spyOn(natureStageService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ natureStage });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(natureStageService.update).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });
});

import { Injectable } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { INatureStage, NewNatureStage } from '../nature-stage.model';

/**
 * A partial Type with required key is used as form input.
 */
type PartialWithRequiredKeyOf<T extends { id: unknown }> = Partial<Omit<T, 'id'>> & { id: T['id'] };

/**
 * Type for createFormGroup and resetForm argument.
 * It accepts INatureStage for edit and NewNatureStageFormGroupInput for create.
 */
type NatureStageFormGroupInput = INatureStage | PartialWithRequiredKeyOf<NewNatureStage>;

type NatureStageFormDefaults = Pick<NewNatureStage, 'id'>;

type NatureStageFormGroupContent = {
  id: FormControl<INatureStage['id'] | NewNatureStage['id']>;
  natureStage: FormControl<INatureStage['natureStage']>;
};

export type NatureStageFormGroup = FormGroup<NatureStageFormGroupContent>;

@Injectable({ providedIn: 'root' })
export class NatureStageFormService {
  createNatureStageFormGroup(natureStage: NatureStageFormGroupInput = { id: null }): NatureStageFormGroup {
    const natureStageRawValue = {
      ...this.getFormDefaults(),
      ...natureStage,
    };
    return new FormGroup<NatureStageFormGroupContent>({
      id: new FormControl(
        { value: natureStageRawValue.id, disabled: true },
        {
          nonNullable: true,
          validators: [Validators.required],
        }
      ),
      natureStage: new FormControl(natureStageRawValue.natureStage),
    });
  }

  getNatureStage(form: NatureStageFormGroup): INatureStage | NewNatureStage {
    return form.getRawValue() as INatureStage | NewNatureStage;
  }

  resetForm(form: NatureStageFormGroup, natureStage: NatureStageFormGroupInput): void {
    const natureStageRawValue = { ...this.getFormDefaults(), ...natureStage };
    form.reset(
      {
        ...natureStageRawValue,
        id: { value: natureStageRawValue.id, disabled: true },
      } as any /* cast to workaround https://github.com/angular/angular/issues/46458 */
    );
  }

  private getFormDefaults(): NatureStageFormDefaults {
    return {
      id: null,
    };
  }
}

import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { NatureStageComponent } from './list/nature-stage.component';
import { NatureStageDetailComponent } from './detail/nature-stage-detail.component';
import { NatureStageUpdateComponent } from './update/nature-stage-update.component';
import { NatureStageDeleteDialogComponent } from './delete/nature-stage-delete-dialog.component';
import { NatureStageRoutingModule } from './route/nature-stage-routing.module';

@NgModule({
  imports: [SharedModule, NatureStageRoutingModule],
  declarations: [NatureStageComponent, NatureStageDetailComponent, NatureStageUpdateComponent, NatureStageDeleteDialogComponent],
})
export class NatureStageModule {}

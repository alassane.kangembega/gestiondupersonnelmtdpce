import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { INatureStage, NewNatureStage } from '../nature-stage.model';

export type PartialUpdateNatureStage = Partial<INatureStage> & Pick<INatureStage, 'id'>;

export type EntityResponseType = HttpResponse<INatureStage>;
export type EntityArrayResponseType = HttpResponse<INatureStage[]>;

@Injectable({ providedIn: 'root' })
export class NatureStageService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/nature-stages');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(natureStage: NewNatureStage): Observable<EntityResponseType> {
    return this.http.post<INatureStage>(this.resourceUrl, natureStage, { observe: 'response' });
  }

  update(natureStage: INatureStage): Observable<EntityResponseType> {
    return this.http.put<INatureStage>(`${this.resourceUrl}/${this.getNatureStageIdentifier(natureStage)}`, natureStage, {
      observe: 'response',
    });
  }

  partialUpdate(natureStage: PartialUpdateNatureStage): Observable<EntityResponseType> {
    return this.http.patch<INatureStage>(`${this.resourceUrl}/${this.getNatureStageIdentifier(natureStage)}`, natureStage, {
      observe: 'response',
    });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<INatureStage>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<INatureStage[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  getNatureStageIdentifier(natureStage: Pick<INatureStage, 'id'>): number {
    return natureStage.id;
  }

  compareNatureStage(o1: Pick<INatureStage, 'id'> | null, o2: Pick<INatureStage, 'id'> | null): boolean {
    return o1 && o2 ? this.getNatureStageIdentifier(o1) === this.getNatureStageIdentifier(o2) : o1 === o2;
  }

  addNatureStageToCollectionIfMissing<Type extends Pick<INatureStage, 'id'>>(
    natureStageCollection: Type[],
    ...natureStagesToCheck: (Type | null | undefined)[]
  ): Type[] {
    const natureStages: Type[] = natureStagesToCheck.filter(isPresent);
    if (natureStages.length > 0) {
      const natureStageCollectionIdentifiers = natureStageCollection.map(
        natureStageItem => this.getNatureStageIdentifier(natureStageItem)!
      );
      const natureStagesToAdd = natureStages.filter(natureStageItem => {
        const natureStageIdentifier = this.getNatureStageIdentifier(natureStageItem);
        if (natureStageCollectionIdentifiers.includes(natureStageIdentifier)) {
          return false;
        }
        natureStageCollectionIdentifiers.push(natureStageIdentifier);
        return true;
      });
      return [...natureStagesToAdd, ...natureStageCollection];
    }
    return natureStageCollection;
  }
}

import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { INatureStage } from '../nature-stage.model';
import { sampleWithRequiredData, sampleWithNewData, sampleWithPartialData, sampleWithFullData } from '../nature-stage.test-samples';

import { NatureStageService } from './nature-stage.service';

const requireRestSample: INatureStage = {
  ...sampleWithRequiredData,
};

describe('NatureStage Service', () => {
  let service: NatureStageService;
  let httpMock: HttpTestingController;
  let expectedResult: INatureStage | INatureStage[] | boolean | null;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(NatureStageService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should create a NatureStage', () => {
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      const natureStage = { ...sampleWithNewData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.create(natureStage).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a NatureStage', () => {
      const natureStage = { ...sampleWithRequiredData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.update(natureStage).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a NatureStage', () => {
      const patchObject = { ...sampleWithPartialData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of NatureStage', () => {
      const returnedFromService = { ...requireRestSample };

      const expected = { ...sampleWithRequiredData };

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toMatchObject([expected]);
    });

    it('should delete a NatureStage', () => {
      const expected = true;

      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult).toBe(expected);
    });

    describe('addNatureStageToCollectionIfMissing', () => {
      it('should add a NatureStage to an empty array', () => {
        const natureStage: INatureStage = sampleWithRequiredData;
        expectedResult = service.addNatureStageToCollectionIfMissing([], natureStage);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(natureStage);
      });

      it('should not add a NatureStage to an array that contains it', () => {
        const natureStage: INatureStage = sampleWithRequiredData;
        const natureStageCollection: INatureStage[] = [
          {
            ...natureStage,
          },
          sampleWithPartialData,
        ];
        expectedResult = service.addNatureStageToCollectionIfMissing(natureStageCollection, natureStage);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a NatureStage to an array that doesn't contain it", () => {
        const natureStage: INatureStage = sampleWithRequiredData;
        const natureStageCollection: INatureStage[] = [sampleWithPartialData];
        expectedResult = service.addNatureStageToCollectionIfMissing(natureStageCollection, natureStage);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(natureStage);
      });

      it('should add only unique NatureStage to an array', () => {
        const natureStageArray: INatureStage[] = [sampleWithRequiredData, sampleWithPartialData, sampleWithFullData];
        const natureStageCollection: INatureStage[] = [sampleWithRequiredData];
        expectedResult = service.addNatureStageToCollectionIfMissing(natureStageCollection, ...natureStageArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const natureStage: INatureStage = sampleWithRequiredData;
        const natureStage2: INatureStage = sampleWithPartialData;
        expectedResult = service.addNatureStageToCollectionIfMissing([], natureStage, natureStage2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(natureStage);
        expect(expectedResult).toContain(natureStage2);
      });

      it('should accept null and undefined values', () => {
        const natureStage: INatureStage = sampleWithRequiredData;
        expectedResult = service.addNatureStageToCollectionIfMissing([], null, natureStage, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(natureStage);
      });

      it('should return initial array if no NatureStage is added', () => {
        const natureStageCollection: INatureStage[] = [sampleWithRequiredData];
        expectedResult = service.addNatureStageToCollectionIfMissing(natureStageCollection, undefined, null);
        expect(expectedResult).toEqual(natureStageCollection);
      });
    });

    describe('compareNatureStage', () => {
      it('Should return true if both entities are null', () => {
        const entity1 = null;
        const entity2 = null;

        const compareResult = service.compareNatureStage(entity1, entity2);

        expect(compareResult).toEqual(true);
      });

      it('Should return false if one entity is null', () => {
        const entity1 = { id: 123 };
        const entity2 = null;

        const compareResult1 = service.compareNatureStage(entity1, entity2);
        const compareResult2 = service.compareNatureStage(entity2, entity1);

        expect(compareResult1).toEqual(false);
        expect(compareResult2).toEqual(false);
      });

      it('Should return false if primaryKey differs', () => {
        const entity1 = { id: 123 };
        const entity2 = { id: 456 };

        const compareResult1 = service.compareNatureStage(entity1, entity2);
        const compareResult2 = service.compareNatureStage(entity2, entity1);

        expect(compareResult1).toEqual(false);
        expect(compareResult2).toEqual(false);
      });

      it('Should return false if primaryKey matches', () => {
        const entity1 = { id: 123 };
        const entity2 = { id: 123 };

        const compareResult1 = service.compareNatureStage(entity1, entity2);
        const compareResult2 = service.compareNatureStage(entity2, entity1);

        expect(compareResult1).toEqual(true);
        expect(compareResult2).toEqual(true);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});

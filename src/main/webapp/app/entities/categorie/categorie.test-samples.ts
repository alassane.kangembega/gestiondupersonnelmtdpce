import { ICategorie, NewCategorie } from './categorie.model';

export const sampleWithRequiredData: ICategorie = {
  id: 37918,
};

export const sampleWithPartialData: ICategorie = {
  id: 76982,
};

export const sampleWithFullData: ICategorie = {
  id: 48523,
  libelle: 'Dollar calculating Specialiste',
};

export const sampleWithNewData: NewCategorie = {
  id: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { EmployeFormService } from './employe-form.service';
import { EmployeService } from '../service/employe.service';
import { IEmploye } from '../employe.model';
import { INatureRecrutement } from 'app/entities/nature-recrutement/nature-recrutement.model';
import { NatureRecrutementService } from 'app/entities/nature-recrutement/service/nature-recrutement.service';
import { IStatut } from 'app/entities/statut/statut.model';
import { StatutService } from 'app/entities/statut/service/statut.service';

import { EmployeUpdateComponent } from './employe-update.component';

describe('Employe Management Update Component', () => {
  let comp: EmployeUpdateComponent;
  let fixture: ComponentFixture<EmployeUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let employeFormService: EmployeFormService;
  let employeService: EmployeService;
  let natureRecrutementService: NatureRecrutementService;
  let statutService: StatutService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      declarations: [EmployeUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(EmployeUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(EmployeUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    employeFormService = TestBed.inject(EmployeFormService);
    employeService = TestBed.inject(EmployeService);
    natureRecrutementService = TestBed.inject(NatureRecrutementService);
    statutService = TestBed.inject(StatutService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call NatureRecrutement query and add missing value', () => {
      const employe: IEmploye = { id: 456 };
      const natureRecrutement: INatureRecrutement = { id: 36956 };
      employe.natureRecrutement = natureRecrutement;

      const natureRecrutementCollection: INatureRecrutement[] = [{ id: 41971 }];
      jest.spyOn(natureRecrutementService, 'query').mockReturnValue(of(new HttpResponse({ body: natureRecrutementCollection })));
      const additionalNatureRecrutements = [natureRecrutement];
      const expectedCollection: INatureRecrutement[] = [...additionalNatureRecrutements, ...natureRecrutementCollection];
      jest.spyOn(natureRecrutementService, 'addNatureRecrutementToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ employe });
      comp.ngOnInit();

      expect(natureRecrutementService.query).toHaveBeenCalled();
      expect(natureRecrutementService.addNatureRecrutementToCollectionIfMissing).toHaveBeenCalledWith(
        natureRecrutementCollection,
        ...additionalNatureRecrutements.map(expect.objectContaining)
      );
      expect(comp.natureRecrutementsSharedCollection).toEqual(expectedCollection);
    });

    it('Should call Statut query and add missing value', () => {
      const employe: IEmploye = { id: 456 };
      const statut: IStatut = { id: 12230 };
      employe.statut = statut;

      const statutCollection: IStatut[] = [{ id: 52811 }];
      jest.spyOn(statutService, 'query').mockReturnValue(of(new HttpResponse({ body: statutCollection })));
      const additionalStatuts = [statut];
      const expectedCollection: IStatut[] = [...additionalStatuts, ...statutCollection];
      jest.spyOn(statutService, 'addStatutToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ employe });
      comp.ngOnInit();

      expect(statutService.query).toHaveBeenCalled();
      expect(statutService.addStatutToCollectionIfMissing).toHaveBeenCalledWith(
        statutCollection,
        ...additionalStatuts.map(expect.objectContaining)
      );
      expect(comp.statutsSharedCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const employe: IEmploye = { id: 456 };
      const natureRecrutement: INatureRecrutement = { id: 97738 };
      employe.natureRecrutement = natureRecrutement;
      const statut: IStatut = { id: 73557 };
      employe.statut = statut;

      activatedRoute.data = of({ employe });
      comp.ngOnInit();

      expect(comp.natureRecrutementsSharedCollection).toContain(natureRecrutement);
      expect(comp.statutsSharedCollection).toContain(statut);
      expect(comp.employe).toEqual(employe);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IEmploye>>();
      const employe = { id: 123 };
      jest.spyOn(employeFormService, 'getEmploye').mockReturnValue(employe);
      jest.spyOn(employeService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ employe });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: employe }));
      saveSubject.complete();

      // THEN
      expect(employeFormService.getEmploye).toHaveBeenCalled();
      expect(comp.previousState).toHaveBeenCalled();
      expect(employeService.update).toHaveBeenCalledWith(expect.objectContaining(employe));
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IEmploye>>();
      const employe = { id: 123 };
      jest.spyOn(employeFormService, 'getEmploye').mockReturnValue({ id: null });
      jest.spyOn(employeService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ employe: null });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: employe }));
      saveSubject.complete();

      // THEN
      expect(employeFormService.getEmploye).toHaveBeenCalled();
      expect(employeService.create).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IEmploye>>();
      const employe = { id: 123 };
      jest.spyOn(employeService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ employe });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(employeService.update).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Compare relationships', () => {
    describe('compareNatureRecrutement', () => {
      it('Should forward to natureRecrutementService', () => {
        const entity = { id: 123 };
        const entity2 = { id: 456 };
        jest.spyOn(natureRecrutementService, 'compareNatureRecrutement');
        comp.compareNatureRecrutement(entity, entity2);
        expect(natureRecrutementService.compareNatureRecrutement).toHaveBeenCalledWith(entity, entity2);
      });
    });

    describe('compareStatut', () => {
      it('Should forward to statutService', () => {
        const entity = { id: 123 };
        const entity2 = { id: 456 };
        jest.spyOn(statutService, 'compareStatut');
        comp.compareStatut(entity, entity2);
        expect(statutService.compareStatut).toHaveBeenCalledWith(entity, entity2);
      });
    });
  });
});

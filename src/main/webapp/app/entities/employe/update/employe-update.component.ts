import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { EmployeFormService, EmployeFormGroup } from './employe-form.service';
import { IEmploye } from '../employe.model';
import { EmployeService } from '../service/employe.service';
import { INatureRecrutement } from 'app/entities/nature-recrutement/nature-recrutement.model';
import { NatureRecrutementService } from 'app/entities/nature-recrutement/service/nature-recrutement.service';
import { IStatut } from 'app/entities/statut/statut.model';
import { StatutService } from 'app/entities/statut/service/statut.service';
import { Sexe } from 'app/entities/enumerations/sexe.model';
import { SituationMatrimoniale } from 'app/entities/enumerations/situation-matrimoniale.model';
import { StatutAgentPublic } from 'app/entities/enumerations/statut-agent-public.model';

@Component({
  selector: 'jhi-employe-update',
  templateUrl: './employe-update.component.html',
})
export class EmployeUpdateComponent implements OnInit {
  isSaving = false;
  employe: IEmploye | null = null;
  sexeValues = Object.keys(Sexe);
  situationMatrimonialeValues = Object.keys(SituationMatrimoniale);
  statutAgentPublicValues = Object.keys(StatutAgentPublic);

  natureRecrutementsSharedCollection: INatureRecrutement[] = [];
  statutsSharedCollection: IStatut[] = [];

  editForm: EmployeFormGroup = this.employeFormService.createEmployeFormGroup();

  constructor(
    protected employeService: EmployeService,
    protected employeFormService: EmployeFormService,
    protected natureRecrutementService: NatureRecrutementService,
    protected statutService: StatutService,
    protected activatedRoute: ActivatedRoute
  ) {}

  compareNatureRecrutement = (o1: INatureRecrutement | null, o2: INatureRecrutement | null): boolean =>
    this.natureRecrutementService.compareNatureRecrutement(o1, o2);

  compareStatut = (o1: IStatut | null, o2: IStatut | null): boolean => this.statutService.compareStatut(o1, o2);

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ employe }) => {
      this.employe = employe;
      if (employe) {
        this.updateForm(employe);
      }

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const employe = this.employeFormService.getEmploye(this.editForm);
    if (employe.id !== null) {
      this.subscribeToSaveResponse(this.employeService.update(employe));
    } else {
      this.subscribeToSaveResponse(this.employeService.create(employe));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IEmploye>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(employe: IEmploye): void {
    this.employe = employe;
    this.employeFormService.resetForm(this.editForm, employe);

    this.natureRecrutementsSharedCollection = this.natureRecrutementService.addNatureRecrutementToCollectionIfMissing<INatureRecrutement>(
      this.natureRecrutementsSharedCollection,
      employe.natureRecrutement
    );
    this.statutsSharedCollection = this.statutService.addStatutToCollectionIfMissing<IStatut>(this.statutsSharedCollection, employe.statut);
  }

  protected loadRelationshipsOptions(): void {
    this.natureRecrutementService
      .query()
      .pipe(map((res: HttpResponse<INatureRecrutement[]>) => res.body ?? []))
      .pipe(
        map((natureRecrutements: INatureRecrutement[]) =>
          this.natureRecrutementService.addNatureRecrutementToCollectionIfMissing<INatureRecrutement>(
            natureRecrutements,
            this.employe?.natureRecrutement
          )
        )
      )
      .subscribe((natureRecrutements: INatureRecrutement[]) => (this.natureRecrutementsSharedCollection = natureRecrutements));

    this.statutService
      .query()
      .pipe(map((res: HttpResponse<IStatut[]>) => res.body ?? []))
      .pipe(map((statuts: IStatut[]) => this.statutService.addStatutToCollectionIfMissing<IStatut>(statuts, this.employe?.statut)))
      .subscribe((statuts: IStatut[]) => (this.statutsSharedCollection = statuts));
  }
}

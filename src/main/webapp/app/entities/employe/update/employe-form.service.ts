import { Injectable } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { IEmploye, NewEmploye } from '../employe.model';

/**
 * A partial Type with required key is used as form input.
 */
type PartialWithRequiredKeyOf<T extends { id: unknown }> = Partial<Omit<T, 'id'>> & { id: T['id'] };

/**
 * Type for createFormGroup and resetForm argument.
 * It accepts IEmploye for edit and NewEmployeFormGroupInput for create.
 */
type EmployeFormGroupInput = IEmploye | PartialWithRequiredKeyOf<NewEmploye>;

type EmployeFormDefaults = Pick<NewEmploye, 'id'>;

type EmployeFormGroupContent = {
  id: FormControl<IEmploye['id'] | NewEmploye['id']>;
  matricule: FormControl<IEmploye['matricule']>;
  nom: FormControl<IEmploye['nom']>;
  prenom: FormControl<IEmploye['prenom']>;
  sexe: FormControl<IEmploye['sexe']>;
  dateNaissance: FormControl<IEmploye['dateNaissance']>;
  dateIntegration: FormControl<IEmploye['dateIntegration']>;
  situationMatrimoniale: FormControl<IEmploye['situationMatrimoniale']>;
  statutAgentPublic: FormControl<IEmploye['statutAgentPublic']>;
  indice: FormControl<IEmploye['indice']>;
  soldeIndiciaire: FormControl<IEmploye['soldeIndiciaire']>;
  salaireBase: FormControl<IEmploye['salaireBase']>;
  nombreCharge: FormControl<IEmploye['nombreCharge']>;
  priseServicePosteActuel: FormControl<IEmploye['priseServicePosteActuel']>;
  telephone: FormControl<IEmploye['telephone']>;
  email: FormControl<IEmploye['email']>;
  dateProbableRetraite: FormControl<IEmploye['dateProbableRetraite']>;
  dateAnniversaire: FormControl<IEmploye['dateAnniversaire']>;
  observation: FormControl<IEmploye['observation']>;
  dateRecrutement: FormControl<IEmploye['dateRecrutement']>;
  allocation: FormControl<IEmploye['allocation']>;
  natureRecrutement: FormControl<IEmploye['natureRecrutement']>;
  statut: FormControl<IEmploye['statut']>;
};

export type EmployeFormGroup = FormGroup<EmployeFormGroupContent>;

@Injectable({ providedIn: 'root' })
export class EmployeFormService {
  createEmployeFormGroup(employe: EmployeFormGroupInput = { id: null }): EmployeFormGroup {
    const employeRawValue = {
      ...this.getFormDefaults(),
      ...employe,
    };
    return new FormGroup<EmployeFormGroupContent>({
      id: new FormControl(
        { value: employeRawValue.id, disabled: true },
        {
          nonNullable: true,
          validators: [Validators.required],
        }
      ),
      matricule: new FormControl(employeRawValue.matricule),
      nom: new FormControl(employeRawValue.nom),
      prenom: new FormControl(employeRawValue.prenom),
      sexe: new FormControl(employeRawValue.sexe),
      dateNaissance: new FormControl(employeRawValue.dateNaissance),
      dateIntegration: new FormControl(employeRawValue.dateIntegration),
      situationMatrimoniale: new FormControl(employeRawValue.situationMatrimoniale),
      statutAgentPublic: new FormControl(employeRawValue.statutAgentPublic),
      indice: new FormControl(employeRawValue.indice),
      soldeIndiciaire: new FormControl(employeRawValue.soldeIndiciaire),
      salaireBase: new FormControl(employeRawValue.salaireBase),
      nombreCharge: new FormControl(employeRawValue.nombreCharge),
      priseServicePosteActuel: new FormControl(employeRawValue.priseServicePosteActuel),
      telephone: new FormControl(employeRawValue.telephone),
      email: new FormControl(employeRawValue.email),
      dateProbableRetraite: new FormControl(employeRawValue.dateProbableRetraite),
      dateAnniversaire: new FormControl(employeRawValue.dateAnniversaire),
      observation: new FormControl(employeRawValue.observation),
      dateRecrutement: new FormControl(employeRawValue.dateRecrutement),
      allocation: new FormControl(employeRawValue.allocation),
      natureRecrutement: new FormControl(employeRawValue.natureRecrutement),
      statut: new FormControl(employeRawValue.statut),
    });
  }

  getEmploye(form: EmployeFormGroup): IEmploye | NewEmploye {
    return form.getRawValue() as IEmploye | NewEmploye;
  }

  resetForm(form: EmployeFormGroup, employe: EmployeFormGroupInput): void {
    const employeRawValue = { ...this.getFormDefaults(), ...employe };
    form.reset(
      {
        ...employeRawValue,
        id: { value: employeRawValue.id, disabled: true },
      } as any /* cast to workaround https://github.com/angular/angular/issues/46458 */
    );
  }

  private getFormDefaults(): EmployeFormDefaults {
    return {
      id: null,
    };
  }
}

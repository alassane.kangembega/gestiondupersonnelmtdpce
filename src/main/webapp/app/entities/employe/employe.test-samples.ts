import dayjs from 'dayjs/esm';

import { Sexe } from 'app/entities/enumerations/sexe.model';
import { SituationMatrimoniale } from 'app/entities/enumerations/situation-matrimoniale.model';
import { StatutAgentPublic } from 'app/entities/enumerations/statut-agent-public.model';

import { IEmploye, NewEmploye } from './employe.model';

export const sampleWithRequiredData: IEmploye = {
  id: 50596,
};

export const sampleWithPartialData: IEmploye = {
  id: 81334,
  nom: 'Auvergne',
  prenom: 'b Gorgeous Bedfordshire',
  sexe: Sexe['Masculin'],
  dateIntegration: dayjs('2022-09-06'),
  statutAgentPublic: StatutAgentPublic['Temporaire'],
  soldeIndiciaire: 57055,
  salaireBase: 14969,
  priseServicePosteActuel: dayjs('2022-09-06'),
  telephone: '+33 792991653',
  dateProbableRetraite: dayjs('2022-09-06'),
};

export const sampleWithFullData: IEmploye = {
  id: 58376,
  matricule: 'Agent Granite maximize',
  nom: 'Fresh vertical',
  prenom: 'channels Mouse withdrawal',
  sexe: Sexe['Masculin'],
  dateNaissance: dayjs('2022-09-06'),
  dateIntegration: dayjs('2022-09-07'),
  situationMatrimoniale: SituationMatrimoniale['Celibataire'],
  statutAgentPublic: StatutAgentPublic['Temporaire'],
  indice: 85639,
  soldeIndiciaire: 45053,
  salaireBase: 53527,
  nombreCharge: 51100,
  priseServicePosteActuel: dayjs('2022-09-07'),
  telephone: '0223168407',
  email: 'Flavie40@hotmail.fr',
  dateProbableRetraite: dayjs('2022-09-07'),
  dateAnniversaire: dayjs('2022-09-07'),
  observation: 'Vanuatu',
  dateRecrutement: dayjs('2022-09-07'),
  allocation: 4664,
};

export const sampleWithNewData: NewEmploye = {
  id: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);

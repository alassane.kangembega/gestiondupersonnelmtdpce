import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';

import { EmployeService } from '../service/employe.service';

import { EmployeComponent } from './employe.component';

describe('Employe Management Component', () => {
  let comp: EmployeComponent;
  let fixture: ComponentFixture<EmployeComponent>;
  let service: EmployeService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule.withRoutes([{ path: 'employe', component: EmployeComponent }]), HttpClientTestingModule],
      declarations: [EmployeComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            data: of({
              defaultSort: 'id,asc',
            }),
            queryParamMap: of(
              jest.requireActual('@angular/router').convertToParamMap({
                page: '1',
                size: '1',
                sort: 'id,desc',
              })
            ),
            snapshot: { queryParams: {} },
          },
        },
      ],
    })
      .overrideTemplate(EmployeComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(EmployeComponent);
    comp = fixture.componentInstance;
    service = TestBed.inject(EmployeService);

    const headers = new HttpHeaders();
    jest.spyOn(service, 'query').mockReturnValue(
      of(
        new HttpResponse({
          body: [{ id: 123 }],
          headers,
        })
      )
    );
  });

  it('Should call load all on init', () => {
    // WHEN
    comp.ngOnInit();

    // THEN
    expect(service.query).toHaveBeenCalled();
    expect(comp.employes?.[0]).toEqual(expect.objectContaining({ id: 123 }));
  });

  describe('trackId', () => {
    it('Should forward to employeService', () => {
      const entity = { id: 123 };
      jest.spyOn(service, 'getEmployeIdentifier');
      const id = comp.trackId(0, entity);
      expect(service.getEmployeIdentifier).toHaveBeenCalledWith(entity);
      expect(id).toBe(entity.id);
    });
  });
});

import dayjs from 'dayjs/esm';
import { INatureRecrutement } from 'app/entities/nature-recrutement/nature-recrutement.model';
import { IStatut } from 'app/entities/statut/statut.model';
import { Sexe } from 'app/entities/enumerations/sexe.model';
import { SituationMatrimoniale } from 'app/entities/enumerations/situation-matrimoniale.model';
import { StatutAgentPublic } from 'app/entities/enumerations/statut-agent-public.model';

export interface IEmploye {
  id: number;
  matricule?: string | null;
  nom?: string | null;
  prenom?: string | null;
  sexe?: Sexe | null;
  dateNaissance?: dayjs.Dayjs | null;
  dateIntegration?: dayjs.Dayjs | null;
  situationMatrimoniale?: SituationMatrimoniale | null;
  statutAgentPublic?: StatutAgentPublic | null;
  indice?: number | null;
  soldeIndiciaire?: number | null;
  salaireBase?: number | null;
  nombreCharge?: number | null;
  priseServicePosteActuel?: dayjs.Dayjs | null;
  telephone?: string | null;
  email?: string | null;
  dateProbableRetraite?: dayjs.Dayjs | null;
  dateAnniversaire?: dayjs.Dayjs | null;
  observation?: string | null;
  dateRecrutement?: dayjs.Dayjs | null;
  allocation?: number | null;
  natureRecrutement?: Pick<INatureRecrutement, 'id'> | null;
  statut?: Pick<IStatut, 'id'> | null;
}

export type NewEmploye = Omit<IEmploye, 'id'> & { id: null };

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';

import { DecorationService } from '../service/decoration.service';

import { DecorationComponent } from './decoration.component';

describe('Decoration Management Component', () => {
  let comp: DecorationComponent;
  let fixture: ComponentFixture<DecorationComponent>;
  let service: DecorationService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule.withRoutes([{ path: 'decoration', component: DecorationComponent }]), HttpClientTestingModule],
      declarations: [DecorationComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            data: of({
              defaultSort: 'id,asc',
            }),
            queryParamMap: of(
              jest.requireActual('@angular/router').convertToParamMap({
                page: '1',
                size: '1',
                sort: 'id,desc',
              })
            ),
            snapshot: { queryParams: {} },
          },
        },
      ],
    })
      .overrideTemplate(DecorationComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(DecorationComponent);
    comp = fixture.componentInstance;
    service = TestBed.inject(DecorationService);

    const headers = new HttpHeaders();
    jest.spyOn(service, 'query').mockReturnValue(
      of(
        new HttpResponse({
          body: [{ id: 123 }],
          headers,
        })
      )
    );
  });

  it('Should call load all on init', () => {
    // WHEN
    comp.ngOnInit();

    // THEN
    expect(service.query).toHaveBeenCalled();
    expect(comp.decorations?.[0]).toEqual(expect.objectContaining({ id: 123 }));
  });

  describe('trackId', () => {
    it('Should forward to decorationService', () => {
      const entity = { id: 123 };
      jest.spyOn(service, 'getDecorationIdentifier');
      const id = comp.trackId(0, entity);
      expect(service.getDecorationIdentifier).toHaveBeenCalledWith(entity);
      expect(id).toBe(entity.id);
    });
  });
});

import dayjs from 'dayjs/esm';
import { ITypeDecoration } from 'app/entities/type-decoration/type-decoration.model';
import { IEmploye } from 'app/entities/employe/employe.model';

export interface IDecoration {
  id: number;
  dateDecoration?: dayjs.Dayjs | null;
  typeDecoration?: Pick<ITypeDecoration, 'id'> | null;
  employe?: Pick<IEmploye, 'id'> | null;
}

export type NewDecoration = Omit<IDecoration, 'id'> & { id: null };

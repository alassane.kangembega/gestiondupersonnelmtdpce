import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { DATE_FORMAT } from 'app/config/input.constants';
import { IDecoration } from '../decoration.model';
import { sampleWithRequiredData, sampleWithNewData, sampleWithPartialData, sampleWithFullData } from '../decoration.test-samples';

import { DecorationService, RestDecoration } from './decoration.service';

const requireRestSample: RestDecoration = {
  ...sampleWithRequiredData,
  dateDecoration: sampleWithRequiredData.dateDecoration?.format(DATE_FORMAT),
};

describe('Decoration Service', () => {
  let service: DecorationService;
  let httpMock: HttpTestingController;
  let expectedResult: IDecoration | IDecoration[] | boolean | null;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(DecorationService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should create a Decoration', () => {
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      const decoration = { ...sampleWithNewData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.create(decoration).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a Decoration', () => {
      const decoration = { ...sampleWithRequiredData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.update(decoration).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a Decoration', () => {
      const patchObject = { ...sampleWithPartialData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of Decoration', () => {
      const returnedFromService = { ...requireRestSample };

      const expected = { ...sampleWithRequiredData };

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toMatchObject([expected]);
    });

    it('should delete a Decoration', () => {
      const expected = true;

      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult).toBe(expected);
    });

    describe('addDecorationToCollectionIfMissing', () => {
      it('should add a Decoration to an empty array', () => {
        const decoration: IDecoration = sampleWithRequiredData;
        expectedResult = service.addDecorationToCollectionIfMissing([], decoration);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(decoration);
      });

      it('should not add a Decoration to an array that contains it', () => {
        const decoration: IDecoration = sampleWithRequiredData;
        const decorationCollection: IDecoration[] = [
          {
            ...decoration,
          },
          sampleWithPartialData,
        ];
        expectedResult = service.addDecorationToCollectionIfMissing(decorationCollection, decoration);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a Decoration to an array that doesn't contain it", () => {
        const decoration: IDecoration = sampleWithRequiredData;
        const decorationCollection: IDecoration[] = [sampleWithPartialData];
        expectedResult = service.addDecorationToCollectionIfMissing(decorationCollection, decoration);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(decoration);
      });

      it('should add only unique Decoration to an array', () => {
        const decorationArray: IDecoration[] = [sampleWithRequiredData, sampleWithPartialData, sampleWithFullData];
        const decorationCollection: IDecoration[] = [sampleWithRequiredData];
        expectedResult = service.addDecorationToCollectionIfMissing(decorationCollection, ...decorationArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const decoration: IDecoration = sampleWithRequiredData;
        const decoration2: IDecoration = sampleWithPartialData;
        expectedResult = service.addDecorationToCollectionIfMissing([], decoration, decoration2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(decoration);
        expect(expectedResult).toContain(decoration2);
      });

      it('should accept null and undefined values', () => {
        const decoration: IDecoration = sampleWithRequiredData;
        expectedResult = service.addDecorationToCollectionIfMissing([], null, decoration, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(decoration);
      });

      it('should return initial array if no Decoration is added', () => {
        const decorationCollection: IDecoration[] = [sampleWithRequiredData];
        expectedResult = service.addDecorationToCollectionIfMissing(decorationCollection, undefined, null);
        expect(expectedResult).toEqual(decorationCollection);
      });
    });

    describe('compareDecoration', () => {
      it('Should return true if both entities are null', () => {
        const entity1 = null;
        const entity2 = null;

        const compareResult = service.compareDecoration(entity1, entity2);

        expect(compareResult).toEqual(true);
      });

      it('Should return false if one entity is null', () => {
        const entity1 = { id: 123 };
        const entity2 = null;

        const compareResult1 = service.compareDecoration(entity1, entity2);
        const compareResult2 = service.compareDecoration(entity2, entity1);

        expect(compareResult1).toEqual(false);
        expect(compareResult2).toEqual(false);
      });

      it('Should return false if primaryKey differs', () => {
        const entity1 = { id: 123 };
        const entity2 = { id: 456 };

        const compareResult1 = service.compareDecoration(entity1, entity2);
        const compareResult2 = service.compareDecoration(entity2, entity1);

        expect(compareResult1).toEqual(false);
        expect(compareResult2).toEqual(false);
      });

      it('Should return false if primaryKey matches', () => {
        const entity1 = { id: 123 };
        const entity2 = { id: 123 };

        const compareResult1 = service.compareDecoration(entity1, entity2);
        const compareResult2 = service.compareDecoration(entity2, entity1);

        expect(compareResult1).toEqual(true);
        expect(compareResult2).toEqual(true);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});

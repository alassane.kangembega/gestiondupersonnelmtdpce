import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import dayjs from 'dayjs/esm';

import { isPresent } from 'app/core/util/operators';
import { DATE_FORMAT } from 'app/config/input.constants';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IDecoration, NewDecoration } from '../decoration.model';

export type PartialUpdateDecoration = Partial<IDecoration> & Pick<IDecoration, 'id'>;

type RestOf<T extends IDecoration | NewDecoration> = Omit<T, 'dateDecoration'> & {
  dateDecoration?: string | null;
};

export type RestDecoration = RestOf<IDecoration>;

export type NewRestDecoration = RestOf<NewDecoration>;

export type PartialUpdateRestDecoration = RestOf<PartialUpdateDecoration>;

export type EntityResponseType = HttpResponse<IDecoration>;
export type EntityArrayResponseType = HttpResponse<IDecoration[]>;

@Injectable({ providedIn: 'root' })
export class DecorationService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/decorations');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(decoration: NewDecoration): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(decoration);
    return this.http
      .post<RestDecoration>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  update(decoration: IDecoration): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(decoration);
    return this.http
      .put<RestDecoration>(`${this.resourceUrl}/${this.getDecorationIdentifier(decoration)}`, copy, { observe: 'response' })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  partialUpdate(decoration: PartialUpdateDecoration): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(decoration);
    return this.http
      .patch<RestDecoration>(`${this.resourceUrl}/${this.getDecorationIdentifier(decoration)}`, copy, { observe: 'response' })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<RestDecoration>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<RestDecoration[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map(res => this.convertResponseArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  getDecorationIdentifier(decoration: Pick<IDecoration, 'id'>): number {
    return decoration.id;
  }

  compareDecoration(o1: Pick<IDecoration, 'id'> | null, o2: Pick<IDecoration, 'id'> | null): boolean {
    return o1 && o2 ? this.getDecorationIdentifier(o1) === this.getDecorationIdentifier(o2) : o1 === o2;
  }

  addDecorationToCollectionIfMissing<Type extends Pick<IDecoration, 'id'>>(
    decorationCollection: Type[],
    ...decorationsToCheck: (Type | null | undefined)[]
  ): Type[] {
    const decorations: Type[] = decorationsToCheck.filter(isPresent);
    if (decorations.length > 0) {
      const decorationCollectionIdentifiers = decorationCollection.map(decorationItem => this.getDecorationIdentifier(decorationItem)!);
      const decorationsToAdd = decorations.filter(decorationItem => {
        const decorationIdentifier = this.getDecorationIdentifier(decorationItem);
        if (decorationCollectionIdentifiers.includes(decorationIdentifier)) {
          return false;
        }
        decorationCollectionIdentifiers.push(decorationIdentifier);
        return true;
      });
      return [...decorationsToAdd, ...decorationCollection];
    }
    return decorationCollection;
  }

  protected convertDateFromClient<T extends IDecoration | NewDecoration | PartialUpdateDecoration>(decoration: T): RestOf<T> {
    return {
      ...decoration,
      dateDecoration: decoration.dateDecoration?.format(DATE_FORMAT) ?? null,
    };
  }

  protected convertDateFromServer(restDecoration: RestDecoration): IDecoration {
    return {
      ...restDecoration,
      dateDecoration: restDecoration.dateDecoration ? dayjs(restDecoration.dateDecoration) : undefined,
    };
  }

  protected convertResponseFromServer(res: HttpResponse<RestDecoration>): HttpResponse<IDecoration> {
    return res.clone({
      body: res.body ? this.convertDateFromServer(res.body) : null,
    });
  }

  protected convertResponseArrayFromServer(res: HttpResponse<RestDecoration[]>): HttpResponse<IDecoration[]> {
    return res.clone({
      body: res.body ? res.body.map(item => this.convertDateFromServer(item)) : null,
    });
  }
}

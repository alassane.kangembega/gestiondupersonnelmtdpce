import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { DecorationComponent } from './list/decoration.component';
import { DecorationDetailComponent } from './detail/decoration-detail.component';
import { DecorationUpdateComponent } from './update/decoration-update.component';
import { DecorationDeleteDialogComponent } from './delete/decoration-delete-dialog.component';
import { DecorationRoutingModule } from './route/decoration-routing.module';

@NgModule({
  imports: [SharedModule, DecorationRoutingModule],
  declarations: [DecorationComponent, DecorationDetailComponent, DecorationUpdateComponent, DecorationDeleteDialogComponent],
})
export class DecorationModule {}

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { DecorationFormService } from './decoration-form.service';
import { DecorationService } from '../service/decoration.service';
import { IDecoration } from '../decoration.model';
import { ITypeDecoration } from 'app/entities/type-decoration/type-decoration.model';
import { TypeDecorationService } from 'app/entities/type-decoration/service/type-decoration.service';
import { IEmploye } from 'app/entities/employe/employe.model';
import { EmployeService } from 'app/entities/employe/service/employe.service';

import { DecorationUpdateComponent } from './decoration-update.component';

describe('Decoration Management Update Component', () => {
  let comp: DecorationUpdateComponent;
  let fixture: ComponentFixture<DecorationUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let decorationFormService: DecorationFormService;
  let decorationService: DecorationService;
  let typeDecorationService: TypeDecorationService;
  let employeService: EmployeService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      declarations: [DecorationUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(DecorationUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(DecorationUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    decorationFormService = TestBed.inject(DecorationFormService);
    decorationService = TestBed.inject(DecorationService);
    typeDecorationService = TestBed.inject(TypeDecorationService);
    employeService = TestBed.inject(EmployeService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call TypeDecoration query and add missing value', () => {
      const decoration: IDecoration = { id: 456 };
      const typeDecoration: ITypeDecoration = { id: 42472 };
      decoration.typeDecoration = typeDecoration;

      const typeDecorationCollection: ITypeDecoration[] = [{ id: 43603 }];
      jest.spyOn(typeDecorationService, 'query').mockReturnValue(of(new HttpResponse({ body: typeDecorationCollection })));
      const additionalTypeDecorations = [typeDecoration];
      const expectedCollection: ITypeDecoration[] = [...additionalTypeDecorations, ...typeDecorationCollection];
      jest.spyOn(typeDecorationService, 'addTypeDecorationToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ decoration });
      comp.ngOnInit();

      expect(typeDecorationService.query).toHaveBeenCalled();
      expect(typeDecorationService.addTypeDecorationToCollectionIfMissing).toHaveBeenCalledWith(
        typeDecorationCollection,
        ...additionalTypeDecorations.map(expect.objectContaining)
      );
      expect(comp.typeDecorationsSharedCollection).toEqual(expectedCollection);
    });

    it('Should call Employe query and add missing value', () => {
      const decoration: IDecoration = { id: 456 };
      const employe: IEmploye = { id: 15067 };
      decoration.employe = employe;

      const employeCollection: IEmploye[] = [{ id: 99945 }];
      jest.spyOn(employeService, 'query').mockReturnValue(of(new HttpResponse({ body: employeCollection })));
      const additionalEmployes = [employe];
      const expectedCollection: IEmploye[] = [...additionalEmployes, ...employeCollection];
      jest.spyOn(employeService, 'addEmployeToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ decoration });
      comp.ngOnInit();

      expect(employeService.query).toHaveBeenCalled();
      expect(employeService.addEmployeToCollectionIfMissing).toHaveBeenCalledWith(
        employeCollection,
        ...additionalEmployes.map(expect.objectContaining)
      );
      expect(comp.employesSharedCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const decoration: IDecoration = { id: 456 };
      const typeDecoration: ITypeDecoration = { id: 48107 };
      decoration.typeDecoration = typeDecoration;
      const employe: IEmploye = { id: 94745 };
      decoration.employe = employe;

      activatedRoute.data = of({ decoration });
      comp.ngOnInit();

      expect(comp.typeDecorationsSharedCollection).toContain(typeDecoration);
      expect(comp.employesSharedCollection).toContain(employe);
      expect(comp.decoration).toEqual(decoration);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IDecoration>>();
      const decoration = { id: 123 };
      jest.spyOn(decorationFormService, 'getDecoration').mockReturnValue(decoration);
      jest.spyOn(decorationService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ decoration });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: decoration }));
      saveSubject.complete();

      // THEN
      expect(decorationFormService.getDecoration).toHaveBeenCalled();
      expect(comp.previousState).toHaveBeenCalled();
      expect(decorationService.update).toHaveBeenCalledWith(expect.objectContaining(decoration));
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IDecoration>>();
      const decoration = { id: 123 };
      jest.spyOn(decorationFormService, 'getDecoration').mockReturnValue({ id: null });
      jest.spyOn(decorationService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ decoration: null });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: decoration }));
      saveSubject.complete();

      // THEN
      expect(decorationFormService.getDecoration).toHaveBeenCalled();
      expect(decorationService.create).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IDecoration>>();
      const decoration = { id: 123 };
      jest.spyOn(decorationService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ decoration });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(decorationService.update).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Compare relationships', () => {
    describe('compareTypeDecoration', () => {
      it('Should forward to typeDecorationService', () => {
        const entity = { id: 123 };
        const entity2 = { id: 456 };
        jest.spyOn(typeDecorationService, 'compareTypeDecoration');
        comp.compareTypeDecoration(entity, entity2);
        expect(typeDecorationService.compareTypeDecoration).toHaveBeenCalledWith(entity, entity2);
      });
    });

    describe('compareEmploye', () => {
      it('Should forward to employeService', () => {
        const entity = { id: 123 };
        const entity2 = { id: 456 };
        jest.spyOn(employeService, 'compareEmploye');
        comp.compareEmploye(entity, entity2);
        expect(employeService.compareEmploye).toHaveBeenCalledWith(entity, entity2);
      });
    });
  });
});

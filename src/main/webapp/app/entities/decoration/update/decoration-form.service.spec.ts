import { TestBed } from '@angular/core/testing';

import { sampleWithRequiredData, sampleWithNewData } from '../decoration.test-samples';

import { DecorationFormService } from './decoration-form.service';

describe('Decoration Form Service', () => {
  let service: DecorationFormService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DecorationFormService);
  });

  describe('Service methods', () => {
    describe('createDecorationFormGroup', () => {
      it('should create a new form with FormControl', () => {
        const formGroup = service.createDecorationFormGroup();

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            dateDecoration: expect.any(Object),
            typeDecoration: expect.any(Object),
            employe: expect.any(Object),
          })
        );
      });

      it('passing IDecoration should create a new form with FormGroup', () => {
        const formGroup = service.createDecorationFormGroup(sampleWithRequiredData);

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            dateDecoration: expect.any(Object),
            typeDecoration: expect.any(Object),
            employe: expect.any(Object),
          })
        );
      });
    });

    describe('getDecoration', () => {
      it('should return NewDecoration for default Decoration initial value', () => {
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        const formGroup = service.createDecorationFormGroup(sampleWithNewData);

        const decoration = service.getDecoration(formGroup) as any;

        expect(decoration).toMatchObject(sampleWithNewData);
      });

      it('should return NewDecoration for empty Decoration initial value', () => {
        const formGroup = service.createDecorationFormGroup();

        const decoration = service.getDecoration(formGroup) as any;

        expect(decoration).toMatchObject({});
      });

      it('should return IDecoration', () => {
        const formGroup = service.createDecorationFormGroup(sampleWithRequiredData);

        const decoration = service.getDecoration(formGroup) as any;

        expect(decoration).toMatchObject(sampleWithRequiredData);
      });
    });

    describe('resetForm', () => {
      it('passing IDecoration should not enable id FormControl', () => {
        const formGroup = service.createDecorationFormGroup();
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, sampleWithRequiredData);

        expect(formGroup.controls.id.disabled).toBe(true);
      });

      it('passing NewDecoration should disable id FormControl', () => {
        const formGroup = service.createDecorationFormGroup(sampleWithRequiredData);
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, { id: null });

        expect(formGroup.controls.id.disabled).toBe(true);
      });
    });
  });
});

import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { DecorationFormService, DecorationFormGroup } from './decoration-form.service';
import { IDecoration } from '../decoration.model';
import { DecorationService } from '../service/decoration.service';
import { ITypeDecoration } from 'app/entities/type-decoration/type-decoration.model';
import { TypeDecorationService } from 'app/entities/type-decoration/service/type-decoration.service';
import { IEmploye } from 'app/entities/employe/employe.model';
import { EmployeService } from 'app/entities/employe/service/employe.service';

@Component({
  selector: 'jhi-decoration-update',
  templateUrl: './decoration-update.component.html',
})
export class DecorationUpdateComponent implements OnInit {
  isSaving = false;
  decoration: IDecoration | null = null;

  typeDecorationsSharedCollection: ITypeDecoration[] = [];
  employesSharedCollection: IEmploye[] = [];

  editForm: DecorationFormGroup = this.decorationFormService.createDecorationFormGroup();

  constructor(
    protected decorationService: DecorationService,
    protected decorationFormService: DecorationFormService,
    protected typeDecorationService: TypeDecorationService,
    protected employeService: EmployeService,
    protected activatedRoute: ActivatedRoute
  ) {}

  compareTypeDecoration = (o1: ITypeDecoration | null, o2: ITypeDecoration | null): boolean =>
    this.typeDecorationService.compareTypeDecoration(o1, o2);

  compareEmploye = (o1: IEmploye | null, o2: IEmploye | null): boolean => this.employeService.compareEmploye(o1, o2);

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ decoration }) => {
      this.decoration = decoration;
      if (decoration) {
        this.updateForm(decoration);
      }

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const decoration = this.decorationFormService.getDecoration(this.editForm);
    if (decoration.id !== null) {
      this.subscribeToSaveResponse(this.decorationService.update(decoration));
    } else {
      this.subscribeToSaveResponse(this.decorationService.create(decoration));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IDecoration>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(decoration: IDecoration): void {
    this.decoration = decoration;
    this.decorationFormService.resetForm(this.editForm, decoration);

    this.typeDecorationsSharedCollection = this.typeDecorationService.addTypeDecorationToCollectionIfMissing<ITypeDecoration>(
      this.typeDecorationsSharedCollection,
      decoration.typeDecoration
    );
    this.employesSharedCollection = this.employeService.addEmployeToCollectionIfMissing<IEmploye>(
      this.employesSharedCollection,
      decoration.employe
    );
  }

  protected loadRelationshipsOptions(): void {
    this.typeDecorationService
      .query()
      .pipe(map((res: HttpResponse<ITypeDecoration[]>) => res.body ?? []))
      .pipe(
        map((typeDecorations: ITypeDecoration[]) =>
          this.typeDecorationService.addTypeDecorationToCollectionIfMissing<ITypeDecoration>(
            typeDecorations,
            this.decoration?.typeDecoration
          )
        )
      )
      .subscribe((typeDecorations: ITypeDecoration[]) => (this.typeDecorationsSharedCollection = typeDecorations));

    this.employeService
      .query()
      .pipe(map((res: HttpResponse<IEmploye[]>) => res.body ?? []))
      .pipe(
        map((employes: IEmploye[]) => this.employeService.addEmployeToCollectionIfMissing<IEmploye>(employes, this.decoration?.employe))
      )
      .subscribe((employes: IEmploye[]) => (this.employesSharedCollection = employes));
  }
}

import { Injectable } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { IDecoration, NewDecoration } from '../decoration.model';

/**
 * A partial Type with required key is used as form input.
 */
type PartialWithRequiredKeyOf<T extends { id: unknown }> = Partial<Omit<T, 'id'>> & { id: T['id'] };

/**
 * Type for createFormGroup and resetForm argument.
 * It accepts IDecoration for edit and NewDecorationFormGroupInput for create.
 */
type DecorationFormGroupInput = IDecoration | PartialWithRequiredKeyOf<NewDecoration>;

type DecorationFormDefaults = Pick<NewDecoration, 'id'>;

type DecorationFormGroupContent = {
  id: FormControl<IDecoration['id'] | NewDecoration['id']>;
  dateDecoration: FormControl<IDecoration['dateDecoration']>;
  typeDecoration: FormControl<IDecoration['typeDecoration']>;
  employe: FormControl<IDecoration['employe']>;
};

export type DecorationFormGroup = FormGroup<DecorationFormGroupContent>;

@Injectable({ providedIn: 'root' })
export class DecorationFormService {
  createDecorationFormGroup(decoration: DecorationFormGroupInput = { id: null }): DecorationFormGroup {
    const decorationRawValue = {
      ...this.getFormDefaults(),
      ...decoration,
    };
    return new FormGroup<DecorationFormGroupContent>({
      id: new FormControl(
        { value: decorationRawValue.id, disabled: true },
        {
          nonNullable: true,
          validators: [Validators.required],
        }
      ),
      dateDecoration: new FormControl(decorationRawValue.dateDecoration),
      typeDecoration: new FormControl(decorationRawValue.typeDecoration),
      employe: new FormControl(decorationRawValue.employe),
    });
  }

  getDecoration(form: DecorationFormGroup): IDecoration | NewDecoration {
    return form.getRawValue() as IDecoration | NewDecoration;
  }

  resetForm(form: DecorationFormGroup, decoration: DecorationFormGroupInput): void {
    const decorationRawValue = { ...this.getFormDefaults(), ...decoration };
    form.reset(
      {
        ...decorationRawValue,
        id: { value: decorationRawValue.id, disabled: true },
      } as any /* cast to workaround https://github.com/angular/angular/issues/46458 */
    );
  }

  private getFormDefaults(): DecorationFormDefaults {
    return {
      id: null,
    };
  }
}

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { DecorationDetailComponent } from './decoration-detail.component';

describe('Decoration Management Detail Component', () => {
  let comp: DecorationDetailComponent;
  let fixture: ComponentFixture<DecorationDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [DecorationDetailComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: { data: of({ decoration: { id: 123 } }) },
        },
      ],
    })
      .overrideTemplate(DecorationDetailComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(DecorationDetailComponent);
    comp = fixture.componentInstance;
  });

  describe('OnInit', () => {
    it('Should load decoration on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(comp.decoration).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IDecoration } from '../decoration.model';

@Component({
  selector: 'jhi-decoration-detail',
  templateUrl: './decoration-detail.component.html',
})
export class DecorationDetailComponent implements OnInit {
  decoration: IDecoration | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ decoration }) => {
      this.decoration = decoration;
    });
  }

  previousState(): void {
    window.history.back();
  }
}

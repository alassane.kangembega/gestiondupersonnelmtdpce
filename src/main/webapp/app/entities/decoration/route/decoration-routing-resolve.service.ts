import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IDecoration } from '../decoration.model';
import { DecorationService } from '../service/decoration.service';

@Injectable({ providedIn: 'root' })
export class DecorationRoutingResolveService implements Resolve<IDecoration | null> {
  constructor(protected service: DecorationService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IDecoration | null | never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((decoration: HttpResponse<IDecoration>) => {
          if (decoration.body) {
            return of(decoration.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(null);
  }
}

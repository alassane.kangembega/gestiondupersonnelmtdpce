import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { DecorationComponent } from '../list/decoration.component';
import { DecorationDetailComponent } from '../detail/decoration-detail.component';
import { DecorationUpdateComponent } from '../update/decoration-update.component';
import { DecorationRoutingResolveService } from './decoration-routing-resolve.service';
import { ASC } from 'app/config/navigation.constants';

const decorationRoute: Routes = [
  {
    path: '',
    component: DecorationComponent,
    data: {
      defaultSort: 'id,' + ASC,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: DecorationDetailComponent,
    resolve: {
      decoration: DecorationRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: DecorationUpdateComponent,
    resolve: {
      decoration: DecorationRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: DecorationUpdateComponent,
    resolve: {
      decoration: DecorationRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(decorationRoute)],
  exports: [RouterModule],
})
export class DecorationRoutingModule {}

import dayjs from 'dayjs/esm';

import { IDecoration, NewDecoration } from './decoration.model';

export const sampleWithRequiredData: IDecoration = {
  id: 25745,
};

export const sampleWithPartialData: IDecoration = {
  id: 77133,
  dateDecoration: dayjs('2022-09-07'),
};

export const sampleWithFullData: IDecoration = {
  id: 2699,
  dateDecoration: dayjs('2022-09-06'),
};

export const sampleWithNewData: NewDecoration = {
  id: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);

import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { IIndemnite } from '../indemnite.model';
import { sampleWithRequiredData, sampleWithNewData, sampleWithPartialData, sampleWithFullData } from '../indemnite.test-samples';

import { IndemniteService } from './indemnite.service';

const requireRestSample: IIndemnite = {
  ...sampleWithRequiredData,
};

describe('Indemnite Service', () => {
  let service: IndemniteService;
  let httpMock: HttpTestingController;
  let expectedResult: IIndemnite | IIndemnite[] | boolean | null;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(IndemniteService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should create a Indemnite', () => {
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      const indemnite = { ...sampleWithNewData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.create(indemnite).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a Indemnite', () => {
      const indemnite = { ...sampleWithRequiredData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.update(indemnite).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a Indemnite', () => {
      const patchObject = { ...sampleWithPartialData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of Indemnite', () => {
      const returnedFromService = { ...requireRestSample };

      const expected = { ...sampleWithRequiredData };

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toMatchObject([expected]);
    });

    it('should delete a Indemnite', () => {
      const expected = true;

      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult).toBe(expected);
    });

    describe('addIndemniteToCollectionIfMissing', () => {
      it('should add a Indemnite to an empty array', () => {
        const indemnite: IIndemnite = sampleWithRequiredData;
        expectedResult = service.addIndemniteToCollectionIfMissing([], indemnite);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(indemnite);
      });

      it('should not add a Indemnite to an array that contains it', () => {
        const indemnite: IIndemnite = sampleWithRequiredData;
        const indemniteCollection: IIndemnite[] = [
          {
            ...indemnite,
          },
          sampleWithPartialData,
        ];
        expectedResult = service.addIndemniteToCollectionIfMissing(indemniteCollection, indemnite);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a Indemnite to an array that doesn't contain it", () => {
        const indemnite: IIndemnite = sampleWithRequiredData;
        const indemniteCollection: IIndemnite[] = [sampleWithPartialData];
        expectedResult = service.addIndemniteToCollectionIfMissing(indemniteCollection, indemnite);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(indemnite);
      });

      it('should add only unique Indemnite to an array', () => {
        const indemniteArray: IIndemnite[] = [sampleWithRequiredData, sampleWithPartialData, sampleWithFullData];
        const indemniteCollection: IIndemnite[] = [sampleWithRequiredData];
        expectedResult = service.addIndemniteToCollectionIfMissing(indemniteCollection, ...indemniteArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const indemnite: IIndemnite = sampleWithRequiredData;
        const indemnite2: IIndemnite = sampleWithPartialData;
        expectedResult = service.addIndemniteToCollectionIfMissing([], indemnite, indemnite2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(indemnite);
        expect(expectedResult).toContain(indemnite2);
      });

      it('should accept null and undefined values', () => {
        const indemnite: IIndemnite = sampleWithRequiredData;
        expectedResult = service.addIndemniteToCollectionIfMissing([], null, indemnite, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(indemnite);
      });

      it('should return initial array if no Indemnite is added', () => {
        const indemniteCollection: IIndemnite[] = [sampleWithRequiredData];
        expectedResult = service.addIndemniteToCollectionIfMissing(indemniteCollection, undefined, null);
        expect(expectedResult).toEqual(indemniteCollection);
      });
    });

    describe('compareIndemnite', () => {
      it('Should return true if both entities are null', () => {
        const entity1 = null;
        const entity2 = null;

        const compareResult = service.compareIndemnite(entity1, entity2);

        expect(compareResult).toEqual(true);
      });

      it('Should return false if one entity is null', () => {
        const entity1 = { id: 123 };
        const entity2 = null;

        const compareResult1 = service.compareIndemnite(entity1, entity2);
        const compareResult2 = service.compareIndemnite(entity2, entity1);

        expect(compareResult1).toEqual(false);
        expect(compareResult2).toEqual(false);
      });

      it('Should return false if primaryKey differs', () => {
        const entity1 = { id: 123 };
        const entity2 = { id: 456 };

        const compareResult1 = service.compareIndemnite(entity1, entity2);
        const compareResult2 = service.compareIndemnite(entity2, entity1);

        expect(compareResult1).toEqual(false);
        expect(compareResult2).toEqual(false);
      });

      it('Should return false if primaryKey matches', () => {
        const entity1 = { id: 123 };
        const entity2 = { id: 123 };

        const compareResult1 = service.compareIndemnite(entity1, entity2);
        const compareResult2 = service.compareIndemnite(entity2, entity1);

        expect(compareResult1).toEqual(true);
        expect(compareResult2).toEqual(true);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});

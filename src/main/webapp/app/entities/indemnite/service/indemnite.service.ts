import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IIndemnite, NewIndemnite } from '../indemnite.model';

export type PartialUpdateIndemnite = Partial<IIndemnite> & Pick<IIndemnite, 'id'>;

export type EntityResponseType = HttpResponse<IIndemnite>;
export type EntityArrayResponseType = HttpResponse<IIndemnite[]>;

@Injectable({ providedIn: 'root' })
export class IndemniteService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/indemnites');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(indemnite: NewIndemnite): Observable<EntityResponseType> {
    return this.http.post<IIndemnite>(this.resourceUrl, indemnite, { observe: 'response' });
  }

  update(indemnite: IIndemnite): Observable<EntityResponseType> {
    return this.http.put<IIndemnite>(`${this.resourceUrl}/${this.getIndemniteIdentifier(indemnite)}`, indemnite, { observe: 'response' });
  }

  partialUpdate(indemnite: PartialUpdateIndemnite): Observable<EntityResponseType> {
    return this.http.patch<IIndemnite>(`${this.resourceUrl}/${this.getIndemniteIdentifier(indemnite)}`, indemnite, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IIndemnite>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IIndemnite[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  getIndemniteIdentifier(indemnite: Pick<IIndemnite, 'id'>): number {
    return indemnite.id;
  }

  compareIndemnite(o1: Pick<IIndemnite, 'id'> | null, o2: Pick<IIndemnite, 'id'> | null): boolean {
    return o1 && o2 ? this.getIndemniteIdentifier(o1) === this.getIndemniteIdentifier(o2) : o1 === o2;
  }

  addIndemniteToCollectionIfMissing<Type extends Pick<IIndemnite, 'id'>>(
    indemniteCollection: Type[],
    ...indemnitesToCheck: (Type | null | undefined)[]
  ): Type[] {
    const indemnites: Type[] = indemnitesToCheck.filter(isPresent);
    if (indemnites.length > 0) {
      const indemniteCollectionIdentifiers = indemniteCollection.map(indemniteItem => this.getIndemniteIdentifier(indemniteItem)!);
      const indemnitesToAdd = indemnites.filter(indemniteItem => {
        const indemniteIdentifier = this.getIndemniteIdentifier(indemniteItem);
        if (indemniteCollectionIdentifiers.includes(indemniteIdentifier)) {
          return false;
        }
        indemniteCollectionIdentifiers.push(indemniteIdentifier);
        return true;
      });
      return [...indemnitesToAdd, ...indemniteCollection];
    }
    return indemniteCollection;
  }
}

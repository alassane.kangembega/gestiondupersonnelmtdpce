import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IIndemnite } from '../indemnite.model';
import { IndemniteService } from '../service/indemnite.service';

@Injectable({ providedIn: 'root' })
export class IndemniteRoutingResolveService implements Resolve<IIndemnite | null> {
  constructor(protected service: IndemniteService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IIndemnite | null | never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((indemnite: HttpResponse<IIndemnite>) => {
          if (indemnite.body) {
            return of(indemnite.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(null);
  }
}

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { IndemniteComponent } from '../list/indemnite.component';
import { IndemniteDetailComponent } from '../detail/indemnite-detail.component';
import { IndemniteUpdateComponent } from '../update/indemnite-update.component';
import { IndemniteRoutingResolveService } from './indemnite-routing-resolve.service';
import { ASC } from 'app/config/navigation.constants';

const indemniteRoute: Routes = [
  {
    path: '',
    component: IndemniteComponent,
    data: {
      defaultSort: 'id,' + ASC,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: IndemniteDetailComponent,
    resolve: {
      indemnite: IndemniteRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: IndemniteUpdateComponent,
    resolve: {
      indemnite: IndemniteRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: IndemniteUpdateComponent,
    resolve: {
      indemnite: IndemniteRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(indemniteRoute)],
  exports: [RouterModule],
})
export class IndemniteRoutingModule {}

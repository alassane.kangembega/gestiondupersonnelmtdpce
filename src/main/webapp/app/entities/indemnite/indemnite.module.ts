import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { IndemniteComponent } from './list/indemnite.component';
import { IndemniteDetailComponent } from './detail/indemnite-detail.component';
import { IndemniteUpdateComponent } from './update/indemnite-update.component';
import { IndemniteDeleteDialogComponent } from './delete/indemnite-delete-dialog.component';
import { IndemniteRoutingModule } from './route/indemnite-routing.module';

@NgModule({
  imports: [SharedModule, IndemniteRoutingModule],
  declarations: [IndemniteComponent, IndemniteDetailComponent, IndemniteUpdateComponent, IndemniteDeleteDialogComponent],
})
export class IndemniteModule {}

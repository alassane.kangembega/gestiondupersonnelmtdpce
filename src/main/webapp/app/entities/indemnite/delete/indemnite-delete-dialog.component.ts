import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IIndemnite } from '../indemnite.model';
import { IndemniteService } from '../service/indemnite.service';
import { ITEM_DELETED_EVENT } from 'app/config/navigation.constants';

@Component({
  templateUrl: './indemnite-delete-dialog.component.html',
})
export class IndemniteDeleteDialogComponent {
  indemnite?: IIndemnite;

  constructor(protected indemniteService: IndemniteService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.indemniteService.delete(id).subscribe(() => {
      this.activeModal.close(ITEM_DELETED_EVENT);
    });
  }
}

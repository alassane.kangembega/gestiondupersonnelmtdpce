import { TestBed } from '@angular/core/testing';

import { sampleWithRequiredData, sampleWithNewData } from '../indemnite.test-samples';

import { IndemniteFormService } from './indemnite-form.service';

describe('Indemnite Form Service', () => {
  let service: IndemniteFormService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(IndemniteFormService);
  });

  describe('Service methods', () => {
    describe('createIndemniteFormGroup', () => {
      it('should create a new form with FormControl', () => {
        const formGroup = service.createIndemniteFormGroup();

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            typeIndemnite: expect.any(Object),
            montantIndemnite: expect.any(Object),
            employe: expect.any(Object),
          })
        );
      });

      it('passing IIndemnite should create a new form with FormGroup', () => {
        const formGroup = service.createIndemniteFormGroup(sampleWithRequiredData);

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            typeIndemnite: expect.any(Object),
            montantIndemnite: expect.any(Object),
            employe: expect.any(Object),
          })
        );
      });
    });

    describe('getIndemnite', () => {
      it('should return NewIndemnite for default Indemnite initial value', () => {
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        const formGroup = service.createIndemniteFormGroup(sampleWithNewData);

        const indemnite = service.getIndemnite(formGroup) as any;

        expect(indemnite).toMatchObject(sampleWithNewData);
      });

      it('should return NewIndemnite for empty Indemnite initial value', () => {
        const formGroup = service.createIndemniteFormGroup();

        const indemnite = service.getIndemnite(formGroup) as any;

        expect(indemnite).toMatchObject({});
      });

      it('should return IIndemnite', () => {
        const formGroup = service.createIndemniteFormGroup(sampleWithRequiredData);

        const indemnite = service.getIndemnite(formGroup) as any;

        expect(indemnite).toMatchObject(sampleWithRequiredData);
      });
    });

    describe('resetForm', () => {
      it('passing IIndemnite should not enable id FormControl', () => {
        const formGroup = service.createIndemniteFormGroup();
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, sampleWithRequiredData);

        expect(formGroup.controls.id.disabled).toBe(true);
      });

      it('passing NewIndemnite should disable id FormControl', () => {
        const formGroup = service.createIndemniteFormGroup(sampleWithRequiredData);
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, { id: null });

        expect(formGroup.controls.id.disabled).toBe(true);
      });
    });
  });
});

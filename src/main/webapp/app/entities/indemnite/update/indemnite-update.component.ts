import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { IndemniteFormService, IndemniteFormGroup } from './indemnite-form.service';
import { IIndemnite } from '../indemnite.model';
import { IndemniteService } from '../service/indemnite.service';
import { IEmploye } from 'app/entities/employe/employe.model';
import { EmployeService } from 'app/entities/employe/service/employe.service';

@Component({
  selector: 'jhi-indemnite-update',
  templateUrl: './indemnite-update.component.html',
})
export class IndemniteUpdateComponent implements OnInit {
  isSaving = false;
  indemnite: IIndemnite | null = null;

  employesSharedCollection: IEmploye[] = [];

  editForm: IndemniteFormGroup = this.indemniteFormService.createIndemniteFormGroup();

  constructor(
    protected indemniteService: IndemniteService,
    protected indemniteFormService: IndemniteFormService,
    protected employeService: EmployeService,
    protected activatedRoute: ActivatedRoute
  ) {}

  compareEmploye = (o1: IEmploye | null, o2: IEmploye | null): boolean => this.employeService.compareEmploye(o1, o2);

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ indemnite }) => {
      this.indemnite = indemnite;
      if (indemnite) {
        this.updateForm(indemnite);
      }

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const indemnite = this.indemniteFormService.getIndemnite(this.editForm);
    if (indemnite.id !== null) {
      this.subscribeToSaveResponse(this.indemniteService.update(indemnite));
    } else {
      this.subscribeToSaveResponse(this.indemniteService.create(indemnite));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IIndemnite>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(indemnite: IIndemnite): void {
    this.indemnite = indemnite;
    this.indemniteFormService.resetForm(this.editForm, indemnite);

    this.employesSharedCollection = this.employeService.addEmployeToCollectionIfMissing<IEmploye>(
      this.employesSharedCollection,
      indemnite.employe
    );
  }

  protected loadRelationshipsOptions(): void {
    this.employeService
      .query()
      .pipe(map((res: HttpResponse<IEmploye[]>) => res.body ?? []))
      .pipe(map((employes: IEmploye[]) => this.employeService.addEmployeToCollectionIfMissing<IEmploye>(employes, this.indemnite?.employe)))
      .subscribe((employes: IEmploye[]) => (this.employesSharedCollection = employes));
  }
}

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { IndemniteFormService } from './indemnite-form.service';
import { IndemniteService } from '../service/indemnite.service';
import { IIndemnite } from '../indemnite.model';
import { IEmploye } from 'app/entities/employe/employe.model';
import { EmployeService } from 'app/entities/employe/service/employe.service';

import { IndemniteUpdateComponent } from './indemnite-update.component';

describe('Indemnite Management Update Component', () => {
  let comp: IndemniteUpdateComponent;
  let fixture: ComponentFixture<IndemniteUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let indemniteFormService: IndemniteFormService;
  let indemniteService: IndemniteService;
  let employeService: EmployeService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      declarations: [IndemniteUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(IndemniteUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(IndemniteUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    indemniteFormService = TestBed.inject(IndemniteFormService);
    indemniteService = TestBed.inject(IndemniteService);
    employeService = TestBed.inject(EmployeService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call Employe query and add missing value', () => {
      const indemnite: IIndemnite = { id: 456 };
      const employe: IEmploye = { id: 12284 };
      indemnite.employe = employe;

      const employeCollection: IEmploye[] = [{ id: 5004 }];
      jest.spyOn(employeService, 'query').mockReturnValue(of(new HttpResponse({ body: employeCollection })));
      const additionalEmployes = [employe];
      const expectedCollection: IEmploye[] = [...additionalEmployes, ...employeCollection];
      jest.spyOn(employeService, 'addEmployeToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ indemnite });
      comp.ngOnInit();

      expect(employeService.query).toHaveBeenCalled();
      expect(employeService.addEmployeToCollectionIfMissing).toHaveBeenCalledWith(
        employeCollection,
        ...additionalEmployes.map(expect.objectContaining)
      );
      expect(comp.employesSharedCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const indemnite: IIndemnite = { id: 456 };
      const employe: IEmploye = { id: 79418 };
      indemnite.employe = employe;

      activatedRoute.data = of({ indemnite });
      comp.ngOnInit();

      expect(comp.employesSharedCollection).toContain(employe);
      expect(comp.indemnite).toEqual(indemnite);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IIndemnite>>();
      const indemnite = { id: 123 };
      jest.spyOn(indemniteFormService, 'getIndemnite').mockReturnValue(indemnite);
      jest.spyOn(indemniteService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ indemnite });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: indemnite }));
      saveSubject.complete();

      // THEN
      expect(indemniteFormService.getIndemnite).toHaveBeenCalled();
      expect(comp.previousState).toHaveBeenCalled();
      expect(indemniteService.update).toHaveBeenCalledWith(expect.objectContaining(indemnite));
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IIndemnite>>();
      const indemnite = { id: 123 };
      jest.spyOn(indemniteFormService, 'getIndemnite').mockReturnValue({ id: null });
      jest.spyOn(indemniteService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ indemnite: null });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: indemnite }));
      saveSubject.complete();

      // THEN
      expect(indemniteFormService.getIndemnite).toHaveBeenCalled();
      expect(indemniteService.create).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IIndemnite>>();
      const indemnite = { id: 123 };
      jest.spyOn(indemniteService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ indemnite });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(indemniteService.update).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Compare relationships', () => {
    describe('compareEmploye', () => {
      it('Should forward to employeService', () => {
        const entity = { id: 123 };
        const entity2 = { id: 456 };
        jest.spyOn(employeService, 'compareEmploye');
        comp.compareEmploye(entity, entity2);
        expect(employeService.compareEmploye).toHaveBeenCalledWith(entity, entity2);
      });
    });
  });
});

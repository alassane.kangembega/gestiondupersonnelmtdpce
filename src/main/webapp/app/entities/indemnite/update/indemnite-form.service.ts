import { Injectable } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { IIndemnite, NewIndemnite } from '../indemnite.model';

/**
 * A partial Type with required key is used as form input.
 */
type PartialWithRequiredKeyOf<T extends { id: unknown }> = Partial<Omit<T, 'id'>> & { id: T['id'] };

/**
 * Type for createFormGroup and resetForm argument.
 * It accepts IIndemnite for edit and NewIndemniteFormGroupInput for create.
 */
type IndemniteFormGroupInput = IIndemnite | PartialWithRequiredKeyOf<NewIndemnite>;

type IndemniteFormDefaults = Pick<NewIndemnite, 'id'>;

type IndemniteFormGroupContent = {
  id: FormControl<IIndemnite['id'] | NewIndemnite['id']>;
  typeIndemnite: FormControl<IIndemnite['typeIndemnite']>;
  montantIndemnite: FormControl<IIndemnite['montantIndemnite']>;
  employe: FormControl<IIndemnite['employe']>;
};

export type IndemniteFormGroup = FormGroup<IndemniteFormGroupContent>;

@Injectable({ providedIn: 'root' })
export class IndemniteFormService {
  createIndemniteFormGroup(indemnite: IndemniteFormGroupInput = { id: null }): IndemniteFormGroup {
    const indemniteRawValue = {
      ...this.getFormDefaults(),
      ...indemnite,
    };
    return new FormGroup<IndemniteFormGroupContent>({
      id: new FormControl(
        { value: indemniteRawValue.id, disabled: true },
        {
          nonNullable: true,
          validators: [Validators.required],
        }
      ),
      typeIndemnite: new FormControl(indemniteRawValue.typeIndemnite),
      montantIndemnite: new FormControl(indemniteRawValue.montantIndemnite),
      employe: new FormControl(indemniteRawValue.employe),
    });
  }

  getIndemnite(form: IndemniteFormGroup): IIndemnite | NewIndemnite {
    return form.getRawValue() as IIndemnite | NewIndemnite;
  }

  resetForm(form: IndemniteFormGroup, indemnite: IndemniteFormGroupInput): void {
    const indemniteRawValue = { ...this.getFormDefaults(), ...indemnite };
    form.reset(
      {
        ...indemniteRawValue,
        id: { value: indemniteRawValue.id, disabled: true },
      } as any /* cast to workaround https://github.com/angular/angular/issues/46458 */
    );
  }

  private getFormDefaults(): IndemniteFormDefaults {
    return {
      id: null,
    };
  }
}

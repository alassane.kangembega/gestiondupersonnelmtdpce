import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';

import { IndemniteService } from '../service/indemnite.service';

import { IndemniteComponent } from './indemnite.component';

describe('Indemnite Management Component', () => {
  let comp: IndemniteComponent;
  let fixture: ComponentFixture<IndemniteComponent>;
  let service: IndemniteService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule.withRoutes([{ path: 'indemnite', component: IndemniteComponent }]), HttpClientTestingModule],
      declarations: [IndemniteComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            data: of({
              defaultSort: 'id,asc',
            }),
            queryParamMap: of(
              jest.requireActual('@angular/router').convertToParamMap({
                page: '1',
                size: '1',
                sort: 'id,desc',
              })
            ),
            snapshot: { queryParams: {} },
          },
        },
      ],
    })
      .overrideTemplate(IndemniteComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(IndemniteComponent);
    comp = fixture.componentInstance;
    service = TestBed.inject(IndemniteService);

    const headers = new HttpHeaders();
    jest.spyOn(service, 'query').mockReturnValue(
      of(
        new HttpResponse({
          body: [{ id: 123 }],
          headers,
        })
      )
    );
  });

  it('Should call load all on init', () => {
    // WHEN
    comp.ngOnInit();

    // THEN
    expect(service.query).toHaveBeenCalled();
    expect(comp.indemnites?.[0]).toEqual(expect.objectContaining({ id: 123 }));
  });

  describe('trackId', () => {
    it('Should forward to indemniteService', () => {
      const entity = { id: 123 };
      jest.spyOn(service, 'getIndemniteIdentifier');
      const id = comp.trackId(0, entity);
      expect(service.getIndemniteIdentifier).toHaveBeenCalledWith(entity);
      expect(id).toBe(entity.id);
    });
  });
});

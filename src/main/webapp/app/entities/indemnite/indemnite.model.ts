import { IEmploye } from 'app/entities/employe/employe.model';

export interface IIndemnite {
  id: number;
  typeIndemnite?: string | null;
  montantIndemnite?: number | null;
  employe?: Pick<IEmploye, 'id'> | null;
}

export type NewIndemnite = Omit<IIndemnite, 'id'> & { id: null };

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { IndemniteDetailComponent } from './indemnite-detail.component';

describe('Indemnite Management Detail Component', () => {
  let comp: IndemniteDetailComponent;
  let fixture: ComponentFixture<IndemniteDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [IndemniteDetailComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: { data: of({ indemnite: { id: 123 } }) },
        },
      ],
    })
      .overrideTemplate(IndemniteDetailComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(IndemniteDetailComponent);
    comp = fixture.componentInstance;
  });

  describe('OnInit', () => {
    it('Should load indemnite on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(comp.indemnite).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});

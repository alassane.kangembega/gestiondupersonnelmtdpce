import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IIndemnite } from '../indemnite.model';

@Component({
  selector: 'jhi-indemnite-detail',
  templateUrl: './indemnite-detail.component.html',
})
export class IndemniteDetailComponent implements OnInit {
  indemnite: IIndemnite | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ indemnite }) => {
      this.indemnite = indemnite;
    });
  }

  previousState(): void {
    window.history.back();
  }
}

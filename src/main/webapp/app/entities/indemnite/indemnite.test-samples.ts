import { IIndemnite, NewIndemnite } from './indemnite.model';

export const sampleWithRequiredData: IIndemnite = {
  id: 65203,
};

export const sampleWithPartialData: IIndemnite = {
  id: 74444,
  montantIndemnite: 18355,
};

export const sampleWithFullData: IIndemnite = {
  id: 72171,
  typeIndemnite: 'Jewelery communities',
  montantIndemnite: 33557,
};

export const sampleWithNewData: NewIndemnite = {
  id: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);

import dayjs from 'dayjs/esm';
import { ICategorie } from 'app/entities/categorie/categorie.model';
import { IEmploye } from 'app/entities/employe/employe.model';

export interface IAvancementCategorie {
  id: number;
  dateAvancement?: dayjs.Dayjs | null;
  categorie?: Pick<ICategorie, 'id'> | null;
  employe?: Pick<IEmploye, 'id'> | null;
}

export type NewAvancementCategorie = Omit<IAvancementCategorie, 'id'> & { id: null };

import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IAvancementCategorie } from '../avancement-categorie.model';
import { AvancementCategorieService } from '../service/avancement-categorie.service';
import { ITEM_DELETED_EVENT } from 'app/config/navigation.constants';

@Component({
  templateUrl: './avancement-categorie-delete-dialog.component.html',
})
export class AvancementCategorieDeleteDialogComponent {
  avancementCategorie?: IAvancementCategorie;

  constructor(protected avancementCategorieService: AvancementCategorieService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.avancementCategorieService.delete(id).subscribe(() => {
      this.activeModal.close(ITEM_DELETED_EVENT);
    });
  }
}

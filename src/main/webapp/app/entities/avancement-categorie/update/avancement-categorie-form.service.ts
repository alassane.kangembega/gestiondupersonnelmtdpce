import { Injectable } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { IAvancementCategorie, NewAvancementCategorie } from '../avancement-categorie.model';

/**
 * A partial Type with required key is used as form input.
 */
type PartialWithRequiredKeyOf<T extends { id: unknown }> = Partial<Omit<T, 'id'>> & { id: T['id'] };

/**
 * Type for createFormGroup and resetForm argument.
 * It accepts IAvancementCategorie for edit and NewAvancementCategorieFormGroupInput for create.
 */
type AvancementCategorieFormGroupInput = IAvancementCategorie | PartialWithRequiredKeyOf<NewAvancementCategorie>;

type AvancementCategorieFormDefaults = Pick<NewAvancementCategorie, 'id'>;

type AvancementCategorieFormGroupContent = {
  id: FormControl<IAvancementCategorie['id'] | NewAvancementCategorie['id']>;
  dateAvancement: FormControl<IAvancementCategorie['dateAvancement']>;
  categorie: FormControl<IAvancementCategorie['categorie']>;
  employe: FormControl<IAvancementCategorie['employe']>;
};

export type AvancementCategorieFormGroup = FormGroup<AvancementCategorieFormGroupContent>;

@Injectable({ providedIn: 'root' })
export class AvancementCategorieFormService {
  createAvancementCategorieFormGroup(avancementCategorie: AvancementCategorieFormGroupInput = { id: null }): AvancementCategorieFormGroup {
    const avancementCategorieRawValue = {
      ...this.getFormDefaults(),
      ...avancementCategorie,
    };
    return new FormGroup<AvancementCategorieFormGroupContent>({
      id: new FormControl(
        { value: avancementCategorieRawValue.id, disabled: true },
        {
          nonNullable: true,
          validators: [Validators.required],
        }
      ),
      dateAvancement: new FormControl(avancementCategorieRawValue.dateAvancement),
      categorie: new FormControl(avancementCategorieRawValue.categorie),
      employe: new FormControl(avancementCategorieRawValue.employe),
    });
  }

  getAvancementCategorie(form: AvancementCategorieFormGroup): IAvancementCategorie | NewAvancementCategorie {
    return form.getRawValue() as IAvancementCategorie | NewAvancementCategorie;
  }

  resetForm(form: AvancementCategorieFormGroup, avancementCategorie: AvancementCategorieFormGroupInput): void {
    const avancementCategorieRawValue = { ...this.getFormDefaults(), ...avancementCategorie };
    form.reset(
      {
        ...avancementCategorieRawValue,
        id: { value: avancementCategorieRawValue.id, disabled: true },
      } as any /* cast to workaround https://github.com/angular/angular/issues/46458 */
    );
  }

  private getFormDefaults(): AvancementCategorieFormDefaults {
    return {
      id: null,
    };
  }
}

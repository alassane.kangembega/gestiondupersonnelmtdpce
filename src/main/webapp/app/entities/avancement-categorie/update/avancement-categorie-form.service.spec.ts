import { TestBed } from '@angular/core/testing';

import { sampleWithRequiredData, sampleWithNewData } from '../avancement-categorie.test-samples';

import { AvancementCategorieFormService } from './avancement-categorie-form.service';

describe('AvancementCategorie Form Service', () => {
  let service: AvancementCategorieFormService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AvancementCategorieFormService);
  });

  describe('Service methods', () => {
    describe('createAvancementCategorieFormGroup', () => {
      it('should create a new form with FormControl', () => {
        const formGroup = service.createAvancementCategorieFormGroup();

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            dateAvancement: expect.any(Object),
            categorie: expect.any(Object),
            employe: expect.any(Object),
          })
        );
      });

      it('passing IAvancementCategorie should create a new form with FormGroup', () => {
        const formGroup = service.createAvancementCategorieFormGroup(sampleWithRequiredData);

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            dateAvancement: expect.any(Object),
            categorie: expect.any(Object),
            employe: expect.any(Object),
          })
        );
      });
    });

    describe('getAvancementCategorie', () => {
      it('should return NewAvancementCategorie for default AvancementCategorie initial value', () => {
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        const formGroup = service.createAvancementCategorieFormGroup(sampleWithNewData);

        const avancementCategorie = service.getAvancementCategorie(formGroup) as any;

        expect(avancementCategorie).toMatchObject(sampleWithNewData);
      });

      it('should return NewAvancementCategorie for empty AvancementCategorie initial value', () => {
        const formGroup = service.createAvancementCategorieFormGroup();

        const avancementCategorie = service.getAvancementCategorie(formGroup) as any;

        expect(avancementCategorie).toMatchObject({});
      });

      it('should return IAvancementCategorie', () => {
        const formGroup = service.createAvancementCategorieFormGroup(sampleWithRequiredData);

        const avancementCategorie = service.getAvancementCategorie(formGroup) as any;

        expect(avancementCategorie).toMatchObject(sampleWithRequiredData);
      });
    });

    describe('resetForm', () => {
      it('passing IAvancementCategorie should not enable id FormControl', () => {
        const formGroup = service.createAvancementCategorieFormGroup();
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, sampleWithRequiredData);

        expect(formGroup.controls.id.disabled).toBe(true);
      });

      it('passing NewAvancementCategorie should disable id FormControl', () => {
        const formGroup = service.createAvancementCategorieFormGroup(sampleWithRequiredData);
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, { id: null });

        expect(formGroup.controls.id.disabled).toBe(true);
      });
    });
  });
});

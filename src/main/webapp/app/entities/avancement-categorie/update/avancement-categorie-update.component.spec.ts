import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { AvancementCategorieFormService } from './avancement-categorie-form.service';
import { AvancementCategorieService } from '../service/avancement-categorie.service';
import { IAvancementCategorie } from '../avancement-categorie.model';
import { ICategorie } from 'app/entities/categorie/categorie.model';
import { CategorieService } from 'app/entities/categorie/service/categorie.service';
import { IEmploye } from 'app/entities/employe/employe.model';
import { EmployeService } from 'app/entities/employe/service/employe.service';

import { AvancementCategorieUpdateComponent } from './avancement-categorie-update.component';

describe('AvancementCategorie Management Update Component', () => {
  let comp: AvancementCategorieUpdateComponent;
  let fixture: ComponentFixture<AvancementCategorieUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let avancementCategorieFormService: AvancementCategorieFormService;
  let avancementCategorieService: AvancementCategorieService;
  let categorieService: CategorieService;
  let employeService: EmployeService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      declarations: [AvancementCategorieUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(AvancementCategorieUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(AvancementCategorieUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    avancementCategorieFormService = TestBed.inject(AvancementCategorieFormService);
    avancementCategorieService = TestBed.inject(AvancementCategorieService);
    categorieService = TestBed.inject(CategorieService);
    employeService = TestBed.inject(EmployeService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call Categorie query and add missing value', () => {
      const avancementCategorie: IAvancementCategorie = { id: 456 };
      const categorie: ICategorie = { id: 18716 };
      avancementCategorie.categorie = categorie;

      const categorieCollection: ICategorie[] = [{ id: 18644 }];
      jest.spyOn(categorieService, 'query').mockReturnValue(of(new HttpResponse({ body: categorieCollection })));
      const additionalCategories = [categorie];
      const expectedCollection: ICategorie[] = [...additionalCategories, ...categorieCollection];
      jest.spyOn(categorieService, 'addCategorieToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ avancementCategorie });
      comp.ngOnInit();

      expect(categorieService.query).toHaveBeenCalled();
      expect(categorieService.addCategorieToCollectionIfMissing).toHaveBeenCalledWith(
        categorieCollection,
        ...additionalCategories.map(expect.objectContaining)
      );
      expect(comp.categoriesSharedCollection).toEqual(expectedCollection);
    });

    it('Should call Employe query and add missing value', () => {
      const avancementCategorie: IAvancementCategorie = { id: 456 };
      const employe: IEmploye = { id: 7172 };
      avancementCategorie.employe = employe;

      const employeCollection: IEmploye[] = [{ id: 40101 }];
      jest.spyOn(employeService, 'query').mockReturnValue(of(new HttpResponse({ body: employeCollection })));
      const additionalEmployes = [employe];
      const expectedCollection: IEmploye[] = [...additionalEmployes, ...employeCollection];
      jest.spyOn(employeService, 'addEmployeToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ avancementCategorie });
      comp.ngOnInit();

      expect(employeService.query).toHaveBeenCalled();
      expect(employeService.addEmployeToCollectionIfMissing).toHaveBeenCalledWith(
        employeCollection,
        ...additionalEmployes.map(expect.objectContaining)
      );
      expect(comp.employesSharedCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const avancementCategorie: IAvancementCategorie = { id: 456 };
      const categorie: ICategorie = { id: 31017 };
      avancementCategorie.categorie = categorie;
      const employe: IEmploye = { id: 41891 };
      avancementCategorie.employe = employe;

      activatedRoute.data = of({ avancementCategorie });
      comp.ngOnInit();

      expect(comp.categoriesSharedCollection).toContain(categorie);
      expect(comp.employesSharedCollection).toContain(employe);
      expect(comp.avancementCategorie).toEqual(avancementCategorie);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IAvancementCategorie>>();
      const avancementCategorie = { id: 123 };
      jest.spyOn(avancementCategorieFormService, 'getAvancementCategorie').mockReturnValue(avancementCategorie);
      jest.spyOn(avancementCategorieService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ avancementCategorie });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: avancementCategorie }));
      saveSubject.complete();

      // THEN
      expect(avancementCategorieFormService.getAvancementCategorie).toHaveBeenCalled();
      expect(comp.previousState).toHaveBeenCalled();
      expect(avancementCategorieService.update).toHaveBeenCalledWith(expect.objectContaining(avancementCategorie));
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IAvancementCategorie>>();
      const avancementCategorie = { id: 123 };
      jest.spyOn(avancementCategorieFormService, 'getAvancementCategorie').mockReturnValue({ id: null });
      jest.spyOn(avancementCategorieService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ avancementCategorie: null });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: avancementCategorie }));
      saveSubject.complete();

      // THEN
      expect(avancementCategorieFormService.getAvancementCategorie).toHaveBeenCalled();
      expect(avancementCategorieService.create).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IAvancementCategorie>>();
      const avancementCategorie = { id: 123 };
      jest.spyOn(avancementCategorieService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ avancementCategorie });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(avancementCategorieService.update).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Compare relationships', () => {
    describe('compareCategorie', () => {
      it('Should forward to categorieService', () => {
        const entity = { id: 123 };
        const entity2 = { id: 456 };
        jest.spyOn(categorieService, 'compareCategorie');
        comp.compareCategorie(entity, entity2);
        expect(categorieService.compareCategorie).toHaveBeenCalledWith(entity, entity2);
      });
    });

    describe('compareEmploye', () => {
      it('Should forward to employeService', () => {
        const entity = { id: 123 };
        const entity2 = { id: 456 };
        jest.spyOn(employeService, 'compareEmploye');
        comp.compareEmploye(entity, entity2);
        expect(employeService.compareEmploye).toHaveBeenCalledWith(entity, entity2);
      });
    });
  });
});

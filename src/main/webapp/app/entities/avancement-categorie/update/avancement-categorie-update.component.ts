import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { AvancementCategorieFormService, AvancementCategorieFormGroup } from './avancement-categorie-form.service';
import { IAvancementCategorie } from '../avancement-categorie.model';
import { AvancementCategorieService } from '../service/avancement-categorie.service';
import { ICategorie } from 'app/entities/categorie/categorie.model';
import { CategorieService } from 'app/entities/categorie/service/categorie.service';
import { IEmploye } from 'app/entities/employe/employe.model';
import { EmployeService } from 'app/entities/employe/service/employe.service';

@Component({
  selector: 'jhi-avancement-categorie-update',
  templateUrl: './avancement-categorie-update.component.html',
})
export class AvancementCategorieUpdateComponent implements OnInit {
  isSaving = false;
  avancementCategorie: IAvancementCategorie | null = null;

  categoriesSharedCollection: ICategorie[] = [];
  employesSharedCollection: IEmploye[] = [];

  editForm: AvancementCategorieFormGroup = this.avancementCategorieFormService.createAvancementCategorieFormGroup();

  constructor(
    protected avancementCategorieService: AvancementCategorieService,
    protected avancementCategorieFormService: AvancementCategorieFormService,
    protected categorieService: CategorieService,
    protected employeService: EmployeService,
    protected activatedRoute: ActivatedRoute
  ) {}

  compareCategorie = (o1: ICategorie | null, o2: ICategorie | null): boolean => this.categorieService.compareCategorie(o1, o2);

  compareEmploye = (o1: IEmploye | null, o2: IEmploye | null): boolean => this.employeService.compareEmploye(o1, o2);

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ avancementCategorie }) => {
      this.avancementCategorie = avancementCategorie;
      if (avancementCategorie) {
        this.updateForm(avancementCategorie);
      }

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const avancementCategorie = this.avancementCategorieFormService.getAvancementCategorie(this.editForm);
    if (avancementCategorie.id !== null) {
      this.subscribeToSaveResponse(this.avancementCategorieService.update(avancementCategorie));
    } else {
      this.subscribeToSaveResponse(this.avancementCategorieService.create(avancementCategorie));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IAvancementCategorie>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(avancementCategorie: IAvancementCategorie): void {
    this.avancementCategorie = avancementCategorie;
    this.avancementCategorieFormService.resetForm(this.editForm, avancementCategorie);

    this.categoriesSharedCollection = this.categorieService.addCategorieToCollectionIfMissing<ICategorie>(
      this.categoriesSharedCollection,
      avancementCategorie.categorie
    );
    this.employesSharedCollection = this.employeService.addEmployeToCollectionIfMissing<IEmploye>(
      this.employesSharedCollection,
      avancementCategorie.employe
    );
  }

  protected loadRelationshipsOptions(): void {
    this.categorieService
      .query()
      .pipe(map((res: HttpResponse<ICategorie[]>) => res.body ?? []))
      .pipe(
        map((categories: ICategorie[]) =>
          this.categorieService.addCategorieToCollectionIfMissing<ICategorie>(categories, this.avancementCategorie?.categorie)
        )
      )
      .subscribe((categories: ICategorie[]) => (this.categoriesSharedCollection = categories));

    this.employeService
      .query()
      .pipe(map((res: HttpResponse<IEmploye[]>) => res.body ?? []))
      .pipe(
        map((employes: IEmploye[]) =>
          this.employeService.addEmployeToCollectionIfMissing<IEmploye>(employes, this.avancementCategorie?.employe)
        )
      )
      .subscribe((employes: IEmploye[]) => (this.employesSharedCollection = employes));
  }
}

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { AvancementCategorieDetailComponent } from './avancement-categorie-detail.component';

describe('AvancementCategorie Management Detail Component', () => {
  let comp: AvancementCategorieDetailComponent;
  let fixture: ComponentFixture<AvancementCategorieDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [AvancementCategorieDetailComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: { data: of({ avancementCategorie: { id: 123 } }) },
        },
      ],
    })
      .overrideTemplate(AvancementCategorieDetailComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(AvancementCategorieDetailComponent);
    comp = fixture.componentInstance;
  });

  describe('OnInit', () => {
    it('Should load avancementCategorie on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(comp.avancementCategorie).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});

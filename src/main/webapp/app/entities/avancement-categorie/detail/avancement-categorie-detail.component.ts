import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IAvancementCategorie } from '../avancement-categorie.model';

@Component({
  selector: 'jhi-avancement-categorie-detail',
  templateUrl: './avancement-categorie-detail.component.html',
})
export class AvancementCategorieDetailComponent implements OnInit {
  avancementCategorie: IAvancementCategorie | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ avancementCategorie }) => {
      this.avancementCategorie = avancementCategorie;
    });
  }

  previousState(): void {
    window.history.back();
  }
}

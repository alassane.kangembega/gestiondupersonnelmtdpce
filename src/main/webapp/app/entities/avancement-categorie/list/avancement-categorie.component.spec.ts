import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';

import { AvancementCategorieService } from '../service/avancement-categorie.service';

import { AvancementCategorieComponent } from './avancement-categorie.component';

describe('AvancementCategorie Management Component', () => {
  let comp: AvancementCategorieComponent;
  let fixture: ComponentFixture<AvancementCategorieComponent>;
  let service: AvancementCategorieService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule.withRoutes([{ path: 'avancement-categorie', component: AvancementCategorieComponent }]),
        HttpClientTestingModule,
      ],
      declarations: [AvancementCategorieComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            data: of({
              defaultSort: 'id,asc',
            }),
            queryParamMap: of(
              jest.requireActual('@angular/router').convertToParamMap({
                page: '1',
                size: '1',
                sort: 'id,desc',
              })
            ),
            snapshot: { queryParams: {} },
          },
        },
      ],
    })
      .overrideTemplate(AvancementCategorieComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(AvancementCategorieComponent);
    comp = fixture.componentInstance;
    service = TestBed.inject(AvancementCategorieService);

    const headers = new HttpHeaders();
    jest.spyOn(service, 'query').mockReturnValue(
      of(
        new HttpResponse({
          body: [{ id: 123 }],
          headers,
        })
      )
    );
  });

  it('Should call load all on init', () => {
    // WHEN
    comp.ngOnInit();

    // THEN
    expect(service.query).toHaveBeenCalled();
    expect(comp.avancementCategories?.[0]).toEqual(expect.objectContaining({ id: 123 }));
  });

  describe('trackId', () => {
    it('Should forward to avancementCategorieService', () => {
      const entity = { id: 123 };
      jest.spyOn(service, 'getAvancementCategorieIdentifier');
      const id = comp.trackId(0, entity);
      expect(service.getAvancementCategorieIdentifier).toHaveBeenCalledWith(entity);
      expect(id).toBe(entity.id);
    });
  });
});

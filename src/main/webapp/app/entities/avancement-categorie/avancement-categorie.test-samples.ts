import dayjs from 'dayjs/esm';

import { IAvancementCategorie, NewAvancementCategorie } from './avancement-categorie.model';

export const sampleWithRequiredData: IAvancementCategorie = {
  id: 70861,
};

export const sampleWithPartialData: IAvancementCategorie = {
  id: 50967,
  dateAvancement: dayjs('2022-09-06'),
};

export const sampleWithFullData: IAvancementCategorie = {
  id: 74366,
  dateAvancement: dayjs('2022-09-06'),
};

export const sampleWithNewData: NewAvancementCategorie = {
  id: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);

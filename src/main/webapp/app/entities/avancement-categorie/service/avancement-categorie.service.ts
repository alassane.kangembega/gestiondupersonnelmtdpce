import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import dayjs from 'dayjs/esm';

import { isPresent } from 'app/core/util/operators';
import { DATE_FORMAT } from 'app/config/input.constants';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IAvancementCategorie, NewAvancementCategorie } from '../avancement-categorie.model';

export type PartialUpdateAvancementCategorie = Partial<IAvancementCategorie> & Pick<IAvancementCategorie, 'id'>;

type RestOf<T extends IAvancementCategorie | NewAvancementCategorie> = Omit<T, 'dateAvancement'> & {
  dateAvancement?: string | null;
};

export type RestAvancementCategorie = RestOf<IAvancementCategorie>;

export type NewRestAvancementCategorie = RestOf<NewAvancementCategorie>;

export type PartialUpdateRestAvancementCategorie = RestOf<PartialUpdateAvancementCategorie>;

export type EntityResponseType = HttpResponse<IAvancementCategorie>;
export type EntityArrayResponseType = HttpResponse<IAvancementCategorie[]>;

@Injectable({ providedIn: 'root' })
export class AvancementCategorieService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/avancement-categories');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(avancementCategorie: NewAvancementCategorie): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(avancementCategorie);
    return this.http
      .post<RestAvancementCategorie>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  update(avancementCategorie: IAvancementCategorie): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(avancementCategorie);
    return this.http
      .put<RestAvancementCategorie>(`${this.resourceUrl}/${this.getAvancementCategorieIdentifier(avancementCategorie)}`, copy, {
        observe: 'response',
      })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  partialUpdate(avancementCategorie: PartialUpdateAvancementCategorie): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(avancementCategorie);
    return this.http
      .patch<RestAvancementCategorie>(`${this.resourceUrl}/${this.getAvancementCategorieIdentifier(avancementCategorie)}`, copy, {
        observe: 'response',
      })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<RestAvancementCategorie>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<RestAvancementCategorie[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map(res => this.convertResponseArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  getAvancementCategorieIdentifier(avancementCategorie: Pick<IAvancementCategorie, 'id'>): number {
    return avancementCategorie.id;
  }

  compareAvancementCategorie(o1: Pick<IAvancementCategorie, 'id'> | null, o2: Pick<IAvancementCategorie, 'id'> | null): boolean {
    return o1 && o2 ? this.getAvancementCategorieIdentifier(o1) === this.getAvancementCategorieIdentifier(o2) : o1 === o2;
  }

  addAvancementCategorieToCollectionIfMissing<Type extends Pick<IAvancementCategorie, 'id'>>(
    avancementCategorieCollection: Type[],
    ...avancementCategoriesToCheck: (Type | null | undefined)[]
  ): Type[] {
    const avancementCategories: Type[] = avancementCategoriesToCheck.filter(isPresent);
    if (avancementCategories.length > 0) {
      const avancementCategorieCollectionIdentifiers = avancementCategorieCollection.map(
        avancementCategorieItem => this.getAvancementCategorieIdentifier(avancementCategorieItem)!
      );
      const avancementCategoriesToAdd = avancementCategories.filter(avancementCategorieItem => {
        const avancementCategorieIdentifier = this.getAvancementCategorieIdentifier(avancementCategorieItem);
        if (avancementCategorieCollectionIdentifiers.includes(avancementCategorieIdentifier)) {
          return false;
        }
        avancementCategorieCollectionIdentifiers.push(avancementCategorieIdentifier);
        return true;
      });
      return [...avancementCategoriesToAdd, ...avancementCategorieCollection];
    }
    return avancementCategorieCollection;
  }

  protected convertDateFromClient<T extends IAvancementCategorie | NewAvancementCategorie | PartialUpdateAvancementCategorie>(
    avancementCategorie: T
  ): RestOf<T> {
    return {
      ...avancementCategorie,
      dateAvancement: avancementCategorie.dateAvancement?.format(DATE_FORMAT) ?? null,
    };
  }

  protected convertDateFromServer(restAvancementCategorie: RestAvancementCategorie): IAvancementCategorie {
    return {
      ...restAvancementCategorie,
      dateAvancement: restAvancementCategorie.dateAvancement ? dayjs(restAvancementCategorie.dateAvancement) : undefined,
    };
  }

  protected convertResponseFromServer(res: HttpResponse<RestAvancementCategorie>): HttpResponse<IAvancementCategorie> {
    return res.clone({
      body: res.body ? this.convertDateFromServer(res.body) : null,
    });
  }

  protected convertResponseArrayFromServer(res: HttpResponse<RestAvancementCategorie[]>): HttpResponse<IAvancementCategorie[]> {
    return res.clone({
      body: res.body ? res.body.map(item => this.convertDateFromServer(item)) : null,
    });
  }
}

import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { DATE_FORMAT } from 'app/config/input.constants';
import { IAvancementCategorie } from '../avancement-categorie.model';
import { sampleWithRequiredData, sampleWithNewData, sampleWithPartialData, sampleWithFullData } from '../avancement-categorie.test-samples';

import { AvancementCategorieService, RestAvancementCategorie } from './avancement-categorie.service';

const requireRestSample: RestAvancementCategorie = {
  ...sampleWithRequiredData,
  dateAvancement: sampleWithRequiredData.dateAvancement?.format(DATE_FORMAT),
};

describe('AvancementCategorie Service', () => {
  let service: AvancementCategorieService;
  let httpMock: HttpTestingController;
  let expectedResult: IAvancementCategorie | IAvancementCategorie[] | boolean | null;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(AvancementCategorieService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should create a AvancementCategorie', () => {
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      const avancementCategorie = { ...sampleWithNewData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.create(avancementCategorie).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a AvancementCategorie', () => {
      const avancementCategorie = { ...sampleWithRequiredData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.update(avancementCategorie).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a AvancementCategorie', () => {
      const patchObject = { ...sampleWithPartialData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of AvancementCategorie', () => {
      const returnedFromService = { ...requireRestSample };

      const expected = { ...sampleWithRequiredData };

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toMatchObject([expected]);
    });

    it('should delete a AvancementCategorie', () => {
      const expected = true;

      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult).toBe(expected);
    });

    describe('addAvancementCategorieToCollectionIfMissing', () => {
      it('should add a AvancementCategorie to an empty array', () => {
        const avancementCategorie: IAvancementCategorie = sampleWithRequiredData;
        expectedResult = service.addAvancementCategorieToCollectionIfMissing([], avancementCategorie);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(avancementCategorie);
      });

      it('should not add a AvancementCategorie to an array that contains it', () => {
        const avancementCategorie: IAvancementCategorie = sampleWithRequiredData;
        const avancementCategorieCollection: IAvancementCategorie[] = [
          {
            ...avancementCategorie,
          },
          sampleWithPartialData,
        ];
        expectedResult = service.addAvancementCategorieToCollectionIfMissing(avancementCategorieCollection, avancementCategorie);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a AvancementCategorie to an array that doesn't contain it", () => {
        const avancementCategorie: IAvancementCategorie = sampleWithRequiredData;
        const avancementCategorieCollection: IAvancementCategorie[] = [sampleWithPartialData];
        expectedResult = service.addAvancementCategorieToCollectionIfMissing(avancementCategorieCollection, avancementCategorie);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(avancementCategorie);
      });

      it('should add only unique AvancementCategorie to an array', () => {
        const avancementCategorieArray: IAvancementCategorie[] = [sampleWithRequiredData, sampleWithPartialData, sampleWithFullData];
        const avancementCategorieCollection: IAvancementCategorie[] = [sampleWithRequiredData];
        expectedResult = service.addAvancementCategorieToCollectionIfMissing(avancementCategorieCollection, ...avancementCategorieArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const avancementCategorie: IAvancementCategorie = sampleWithRequiredData;
        const avancementCategorie2: IAvancementCategorie = sampleWithPartialData;
        expectedResult = service.addAvancementCategorieToCollectionIfMissing([], avancementCategorie, avancementCategorie2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(avancementCategorie);
        expect(expectedResult).toContain(avancementCategorie2);
      });

      it('should accept null and undefined values', () => {
        const avancementCategorie: IAvancementCategorie = sampleWithRequiredData;
        expectedResult = service.addAvancementCategorieToCollectionIfMissing([], null, avancementCategorie, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(avancementCategorie);
      });

      it('should return initial array if no AvancementCategorie is added', () => {
        const avancementCategorieCollection: IAvancementCategorie[] = [sampleWithRequiredData];
        expectedResult = service.addAvancementCategorieToCollectionIfMissing(avancementCategorieCollection, undefined, null);
        expect(expectedResult).toEqual(avancementCategorieCollection);
      });
    });

    describe('compareAvancementCategorie', () => {
      it('Should return true if both entities are null', () => {
        const entity1 = null;
        const entity2 = null;

        const compareResult = service.compareAvancementCategorie(entity1, entity2);

        expect(compareResult).toEqual(true);
      });

      it('Should return false if one entity is null', () => {
        const entity1 = { id: 123 };
        const entity2 = null;

        const compareResult1 = service.compareAvancementCategorie(entity1, entity2);
        const compareResult2 = service.compareAvancementCategorie(entity2, entity1);

        expect(compareResult1).toEqual(false);
        expect(compareResult2).toEqual(false);
      });

      it('Should return false if primaryKey differs', () => {
        const entity1 = { id: 123 };
        const entity2 = { id: 456 };

        const compareResult1 = service.compareAvancementCategorie(entity1, entity2);
        const compareResult2 = service.compareAvancementCategorie(entity2, entity1);

        expect(compareResult1).toEqual(false);
        expect(compareResult2).toEqual(false);
      });

      it('Should return false if primaryKey matches', () => {
        const entity1 = { id: 123 };
        const entity2 = { id: 123 };

        const compareResult1 = service.compareAvancementCategorie(entity1, entity2);
        const compareResult2 = service.compareAvancementCategorie(entity2, entity1);

        expect(compareResult1).toEqual(true);
        expect(compareResult2).toEqual(true);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});

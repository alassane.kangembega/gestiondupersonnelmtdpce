import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { AvancementCategorieComponent } from './list/avancement-categorie.component';
import { AvancementCategorieDetailComponent } from './detail/avancement-categorie-detail.component';
import { AvancementCategorieUpdateComponent } from './update/avancement-categorie-update.component';
import { AvancementCategorieDeleteDialogComponent } from './delete/avancement-categorie-delete-dialog.component';
import { AvancementCategorieRoutingModule } from './route/avancement-categorie-routing.module';

@NgModule({
  imports: [SharedModule, AvancementCategorieRoutingModule],
  declarations: [
    AvancementCategorieComponent,
    AvancementCategorieDetailComponent,
    AvancementCategorieUpdateComponent,
    AvancementCategorieDeleteDialogComponent,
  ],
})
export class AvancementCategorieModule {}

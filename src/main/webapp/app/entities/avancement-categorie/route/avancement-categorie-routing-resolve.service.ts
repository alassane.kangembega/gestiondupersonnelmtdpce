import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IAvancementCategorie } from '../avancement-categorie.model';
import { AvancementCategorieService } from '../service/avancement-categorie.service';

@Injectable({ providedIn: 'root' })
export class AvancementCategorieRoutingResolveService implements Resolve<IAvancementCategorie | null> {
  constructor(protected service: AvancementCategorieService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IAvancementCategorie | null | never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((avancementCategorie: HttpResponse<IAvancementCategorie>) => {
          if (avancementCategorie.body) {
            return of(avancementCategorie.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(null);
  }
}

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { AvancementCategorieComponent } from '../list/avancement-categorie.component';
import { AvancementCategorieDetailComponent } from '../detail/avancement-categorie-detail.component';
import { AvancementCategorieUpdateComponent } from '../update/avancement-categorie-update.component';
import { AvancementCategorieRoutingResolveService } from './avancement-categorie-routing-resolve.service';
import { ASC } from 'app/config/navigation.constants';

const avancementCategorieRoute: Routes = [
  {
    path: '',
    component: AvancementCategorieComponent,
    data: {
      defaultSort: 'id,' + ASC,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: AvancementCategorieDetailComponent,
    resolve: {
      avancementCategorie: AvancementCategorieRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: AvancementCategorieUpdateComponent,
    resolve: {
      avancementCategorie: AvancementCategorieRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: AvancementCategorieUpdateComponent,
    resolve: {
      avancementCategorie: AvancementCategorieRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(avancementCategorieRoute)],
  exports: [RouterModule],
})
export class AvancementCategorieRoutingModule {}

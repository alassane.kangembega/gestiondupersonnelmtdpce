export enum TypeStructure {
  DirectionGenerale = 'DirectionGenerale',

  DirectionCentrale = 'DirectionCentrale',

  DirectionTechnique = 'DirectionTechnique',

  Service = 'Service',
}

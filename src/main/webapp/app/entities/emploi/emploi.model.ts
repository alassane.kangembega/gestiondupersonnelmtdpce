export interface IEmploi {
  id: number;
  libelle?: string | null;
}

export type NewEmploi = Omit<IEmploi, 'id'> & { id: null };

import { IEmploi, NewEmploi } from './emploi.model';

export const sampleWithRequiredData: IEmploi = {
  id: 64700,
};

export const sampleWithPartialData: IEmploi = {
  id: 57880,
  libelle: 'definition calculating cyan',
};

export const sampleWithFullData: IEmploi = {
  id: 93523,
  libelle: 'Profit-focused',
};

export const sampleWithNewData: NewEmploi = {
  id: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);

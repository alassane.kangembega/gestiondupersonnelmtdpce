import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';

import { EmploiService } from '../service/emploi.service';

import { EmploiComponent } from './emploi.component';

describe('Emploi Management Component', () => {
  let comp: EmploiComponent;
  let fixture: ComponentFixture<EmploiComponent>;
  let service: EmploiService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule.withRoutes([{ path: 'emploi', component: EmploiComponent }]), HttpClientTestingModule],
      declarations: [EmploiComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            data: of({
              defaultSort: 'id,asc',
            }),
            queryParamMap: of(
              jest.requireActual('@angular/router').convertToParamMap({
                page: '1',
                size: '1',
                sort: 'id,desc',
              })
            ),
            snapshot: { queryParams: {} },
          },
        },
      ],
    })
      .overrideTemplate(EmploiComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(EmploiComponent);
    comp = fixture.componentInstance;
    service = TestBed.inject(EmploiService);

    const headers = new HttpHeaders();
    jest.spyOn(service, 'query').mockReturnValue(
      of(
        new HttpResponse({
          body: [{ id: 123 }],
          headers,
        })
      )
    );
  });

  it('Should call load all on init', () => {
    // WHEN
    comp.ngOnInit();

    // THEN
    expect(service.query).toHaveBeenCalled();
    expect(comp.emplois?.[0]).toEqual(expect.objectContaining({ id: 123 }));
  });

  describe('trackId', () => {
    it('Should forward to emploiService', () => {
      const entity = { id: 123 };
      jest.spyOn(service, 'getEmploiIdentifier');
      const id = comp.trackId(0, entity);
      expect(service.getEmploiIdentifier).toHaveBeenCalledWith(entity);
      expect(id).toBe(entity.id);
    });
  });
});

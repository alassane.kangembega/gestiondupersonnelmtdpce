import { TestBed } from '@angular/core/testing';

import { sampleWithRequiredData, sampleWithNewData } from '../emploi.test-samples';

import { EmploiFormService } from './emploi-form.service';

describe('Emploi Form Service', () => {
  let service: EmploiFormService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EmploiFormService);
  });

  describe('Service methods', () => {
    describe('createEmploiFormGroup', () => {
      it('should create a new form with FormControl', () => {
        const formGroup = service.createEmploiFormGroup();

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            libelle: expect.any(Object),
          })
        );
      });

      it('passing IEmploi should create a new form with FormGroup', () => {
        const formGroup = service.createEmploiFormGroup(sampleWithRequiredData);

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            libelle: expect.any(Object),
          })
        );
      });
    });

    describe('getEmploi', () => {
      it('should return NewEmploi for default Emploi initial value', () => {
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        const formGroup = service.createEmploiFormGroup(sampleWithNewData);

        const emploi = service.getEmploi(formGroup) as any;

        expect(emploi).toMatchObject(sampleWithNewData);
      });

      it('should return NewEmploi for empty Emploi initial value', () => {
        const formGroup = service.createEmploiFormGroup();

        const emploi = service.getEmploi(formGroup) as any;

        expect(emploi).toMatchObject({});
      });

      it('should return IEmploi', () => {
        const formGroup = service.createEmploiFormGroup(sampleWithRequiredData);

        const emploi = service.getEmploi(formGroup) as any;

        expect(emploi).toMatchObject(sampleWithRequiredData);
      });
    });

    describe('resetForm', () => {
      it('passing IEmploi should not enable id FormControl', () => {
        const formGroup = service.createEmploiFormGroup();
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, sampleWithRequiredData);

        expect(formGroup.controls.id.disabled).toBe(true);
      });

      it('passing NewEmploi should disable id FormControl', () => {
        const formGroup = service.createEmploiFormGroup(sampleWithRequiredData);
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, { id: null });

        expect(formGroup.controls.id.disabled).toBe(true);
      });
    });
  });
});

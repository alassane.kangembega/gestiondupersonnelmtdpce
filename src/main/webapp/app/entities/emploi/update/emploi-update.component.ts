import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

import { EmploiFormService, EmploiFormGroup } from './emploi-form.service';
import { IEmploi } from '../emploi.model';
import { EmploiService } from '../service/emploi.service';

@Component({
  selector: 'jhi-emploi-update',
  templateUrl: './emploi-update.component.html',
})
export class EmploiUpdateComponent implements OnInit {
  isSaving = false;
  emploi: IEmploi | null = null;

  editForm: EmploiFormGroup = this.emploiFormService.createEmploiFormGroup();

  constructor(
    protected emploiService: EmploiService,
    protected emploiFormService: EmploiFormService,
    protected activatedRoute: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ emploi }) => {
      this.emploi = emploi;
      if (emploi) {
        this.updateForm(emploi);
      }
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const emploi = this.emploiFormService.getEmploi(this.editForm);
    if (emploi.id !== null) {
      this.subscribeToSaveResponse(this.emploiService.update(emploi));
    } else {
      this.subscribeToSaveResponse(this.emploiService.create(emploi));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IEmploi>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(emploi: IEmploi): void {
    this.emploi = emploi;
    this.emploiFormService.resetForm(this.editForm, emploi);
  }
}

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { EmploiFormService } from './emploi-form.service';
import { EmploiService } from '../service/emploi.service';
import { IEmploi } from '../emploi.model';

import { EmploiUpdateComponent } from './emploi-update.component';

describe('Emploi Management Update Component', () => {
  let comp: EmploiUpdateComponent;
  let fixture: ComponentFixture<EmploiUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let emploiFormService: EmploiFormService;
  let emploiService: EmploiService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      declarations: [EmploiUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(EmploiUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(EmploiUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    emploiFormService = TestBed.inject(EmploiFormService);
    emploiService = TestBed.inject(EmploiService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should update editForm', () => {
      const emploi: IEmploi = { id: 456 };

      activatedRoute.data = of({ emploi });
      comp.ngOnInit();

      expect(comp.emploi).toEqual(emploi);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IEmploi>>();
      const emploi = { id: 123 };
      jest.spyOn(emploiFormService, 'getEmploi').mockReturnValue(emploi);
      jest.spyOn(emploiService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ emploi });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: emploi }));
      saveSubject.complete();

      // THEN
      expect(emploiFormService.getEmploi).toHaveBeenCalled();
      expect(comp.previousState).toHaveBeenCalled();
      expect(emploiService.update).toHaveBeenCalledWith(expect.objectContaining(emploi));
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IEmploi>>();
      const emploi = { id: 123 };
      jest.spyOn(emploiFormService, 'getEmploi').mockReturnValue({ id: null });
      jest.spyOn(emploiService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ emploi: null });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: emploi }));
      saveSubject.complete();

      // THEN
      expect(emploiFormService.getEmploi).toHaveBeenCalled();
      expect(emploiService.create).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IEmploi>>();
      const emploi = { id: 123 };
      jest.spyOn(emploiService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ emploi });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(emploiService.update).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });
});

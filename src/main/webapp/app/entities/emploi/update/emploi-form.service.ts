import { Injectable } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { IEmploi, NewEmploi } from '../emploi.model';

/**
 * A partial Type with required key is used as form input.
 */
type PartialWithRequiredKeyOf<T extends { id: unknown }> = Partial<Omit<T, 'id'>> & { id: T['id'] };

/**
 * Type for createFormGroup and resetForm argument.
 * It accepts IEmploi for edit and NewEmploiFormGroupInput for create.
 */
type EmploiFormGroupInput = IEmploi | PartialWithRequiredKeyOf<NewEmploi>;

type EmploiFormDefaults = Pick<NewEmploi, 'id'>;

type EmploiFormGroupContent = {
  id: FormControl<IEmploi['id'] | NewEmploi['id']>;
  libelle: FormControl<IEmploi['libelle']>;
};

export type EmploiFormGroup = FormGroup<EmploiFormGroupContent>;

@Injectable({ providedIn: 'root' })
export class EmploiFormService {
  createEmploiFormGroup(emploi: EmploiFormGroupInput = { id: null }): EmploiFormGroup {
    const emploiRawValue = {
      ...this.getFormDefaults(),
      ...emploi,
    };
    return new FormGroup<EmploiFormGroupContent>({
      id: new FormControl(
        { value: emploiRawValue.id, disabled: true },
        {
          nonNullable: true,
          validators: [Validators.required],
        }
      ),
      libelle: new FormControl(emploiRawValue.libelle),
    });
  }

  getEmploi(form: EmploiFormGroup): IEmploi | NewEmploi {
    return form.getRawValue() as IEmploi | NewEmploi;
  }

  resetForm(form: EmploiFormGroup, emploi: EmploiFormGroupInput): void {
    const emploiRawValue = { ...this.getFormDefaults(), ...emploi };
    form.reset(
      {
        ...emploiRawValue,
        id: { value: emploiRawValue.id, disabled: true },
      } as any /* cast to workaround https://github.com/angular/angular/issues/46458 */
    );
  }

  private getFormDefaults(): EmploiFormDefaults {
    return {
      id: null,
    };
  }
}

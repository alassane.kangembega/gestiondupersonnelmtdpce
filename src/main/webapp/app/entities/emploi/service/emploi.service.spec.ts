import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { IEmploi } from '../emploi.model';
import { sampleWithRequiredData, sampleWithNewData, sampleWithPartialData, sampleWithFullData } from '../emploi.test-samples';

import { EmploiService } from './emploi.service';

const requireRestSample: IEmploi = {
  ...sampleWithRequiredData,
};

describe('Emploi Service', () => {
  let service: EmploiService;
  let httpMock: HttpTestingController;
  let expectedResult: IEmploi | IEmploi[] | boolean | null;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(EmploiService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should create a Emploi', () => {
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      const emploi = { ...sampleWithNewData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.create(emploi).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a Emploi', () => {
      const emploi = { ...sampleWithRequiredData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.update(emploi).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a Emploi', () => {
      const patchObject = { ...sampleWithPartialData };
      const returnedFromService = { ...requireRestSample };
      const expected = { ...sampleWithRequiredData };

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of Emploi', () => {
      const returnedFromService = { ...requireRestSample };

      const expected = { ...sampleWithRequiredData };

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toMatchObject([expected]);
    });

    it('should delete a Emploi', () => {
      const expected = true;

      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult).toBe(expected);
    });

    describe('addEmploiToCollectionIfMissing', () => {
      it('should add a Emploi to an empty array', () => {
        const emploi: IEmploi = sampleWithRequiredData;
        expectedResult = service.addEmploiToCollectionIfMissing([], emploi);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(emploi);
      });

      it('should not add a Emploi to an array that contains it', () => {
        const emploi: IEmploi = sampleWithRequiredData;
        const emploiCollection: IEmploi[] = [
          {
            ...emploi,
          },
          sampleWithPartialData,
        ];
        expectedResult = service.addEmploiToCollectionIfMissing(emploiCollection, emploi);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a Emploi to an array that doesn't contain it", () => {
        const emploi: IEmploi = sampleWithRequiredData;
        const emploiCollection: IEmploi[] = [sampleWithPartialData];
        expectedResult = service.addEmploiToCollectionIfMissing(emploiCollection, emploi);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(emploi);
      });

      it('should add only unique Emploi to an array', () => {
        const emploiArray: IEmploi[] = [sampleWithRequiredData, sampleWithPartialData, sampleWithFullData];
        const emploiCollection: IEmploi[] = [sampleWithRequiredData];
        expectedResult = service.addEmploiToCollectionIfMissing(emploiCollection, ...emploiArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const emploi: IEmploi = sampleWithRequiredData;
        const emploi2: IEmploi = sampleWithPartialData;
        expectedResult = service.addEmploiToCollectionIfMissing([], emploi, emploi2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(emploi);
        expect(expectedResult).toContain(emploi2);
      });

      it('should accept null and undefined values', () => {
        const emploi: IEmploi = sampleWithRequiredData;
        expectedResult = service.addEmploiToCollectionIfMissing([], null, emploi, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(emploi);
      });

      it('should return initial array if no Emploi is added', () => {
        const emploiCollection: IEmploi[] = [sampleWithRequiredData];
        expectedResult = service.addEmploiToCollectionIfMissing(emploiCollection, undefined, null);
        expect(expectedResult).toEqual(emploiCollection);
      });
    });

    describe('compareEmploi', () => {
      it('Should return true if both entities are null', () => {
        const entity1 = null;
        const entity2 = null;

        const compareResult = service.compareEmploi(entity1, entity2);

        expect(compareResult).toEqual(true);
      });

      it('Should return false if one entity is null', () => {
        const entity1 = { id: 123 };
        const entity2 = null;

        const compareResult1 = service.compareEmploi(entity1, entity2);
        const compareResult2 = service.compareEmploi(entity2, entity1);

        expect(compareResult1).toEqual(false);
        expect(compareResult2).toEqual(false);
      });

      it('Should return false if primaryKey differs', () => {
        const entity1 = { id: 123 };
        const entity2 = { id: 456 };

        const compareResult1 = service.compareEmploi(entity1, entity2);
        const compareResult2 = service.compareEmploi(entity2, entity1);

        expect(compareResult1).toEqual(false);
        expect(compareResult2).toEqual(false);
      });

      it('Should return false if primaryKey matches', () => {
        const entity1 = { id: 123 };
        const entity2 = { id: 123 };

        const compareResult1 = service.compareEmploi(entity1, entity2);
        const compareResult2 = service.compareEmploi(entity2, entity1);

        expect(compareResult1).toEqual(true);
        expect(compareResult2).toEqual(true);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});

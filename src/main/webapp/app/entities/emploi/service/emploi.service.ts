import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IEmploi, NewEmploi } from '../emploi.model';

export type PartialUpdateEmploi = Partial<IEmploi> & Pick<IEmploi, 'id'>;

export type EntityResponseType = HttpResponse<IEmploi>;
export type EntityArrayResponseType = HttpResponse<IEmploi[]>;

@Injectable({ providedIn: 'root' })
export class EmploiService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/emplois');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(emploi: NewEmploi): Observable<EntityResponseType> {
    return this.http.post<IEmploi>(this.resourceUrl, emploi, { observe: 'response' });
  }

  update(emploi: IEmploi): Observable<EntityResponseType> {
    return this.http.put<IEmploi>(`${this.resourceUrl}/${this.getEmploiIdentifier(emploi)}`, emploi, { observe: 'response' });
  }

  partialUpdate(emploi: PartialUpdateEmploi): Observable<EntityResponseType> {
    return this.http.patch<IEmploi>(`${this.resourceUrl}/${this.getEmploiIdentifier(emploi)}`, emploi, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IEmploi>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IEmploi[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  getEmploiIdentifier(emploi: Pick<IEmploi, 'id'>): number {
    return emploi.id;
  }

  compareEmploi(o1: Pick<IEmploi, 'id'> | null, o2: Pick<IEmploi, 'id'> | null): boolean {
    return o1 && o2 ? this.getEmploiIdentifier(o1) === this.getEmploiIdentifier(o2) : o1 === o2;
  }

  addEmploiToCollectionIfMissing<Type extends Pick<IEmploi, 'id'>>(
    emploiCollection: Type[],
    ...emploisToCheck: (Type | null | undefined)[]
  ): Type[] {
    const emplois: Type[] = emploisToCheck.filter(isPresent);
    if (emplois.length > 0) {
      const emploiCollectionIdentifiers = emploiCollection.map(emploiItem => this.getEmploiIdentifier(emploiItem)!);
      const emploisToAdd = emplois.filter(emploiItem => {
        const emploiIdentifier = this.getEmploiIdentifier(emploiItem);
        if (emploiCollectionIdentifiers.includes(emploiIdentifier)) {
          return false;
        }
        emploiCollectionIdentifiers.push(emploiIdentifier);
        return true;
      });
      return [...emploisToAdd, ...emploiCollection];
    }
    return emploiCollection;
  }
}

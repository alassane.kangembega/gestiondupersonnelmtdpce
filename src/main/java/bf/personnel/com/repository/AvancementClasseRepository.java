package bf.personnel.com.repository;

import bf.personnel.com.domain.AvancementClasse;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the AvancementClasse entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AvancementClasseRepository extends JpaRepository<AvancementClasse, Long> {}

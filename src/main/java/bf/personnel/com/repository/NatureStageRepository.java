package bf.personnel.com.repository;

import bf.personnel.com.domain.NatureStage;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the NatureStage entity.
 */
@SuppressWarnings("unused")
@Repository
public interface NatureStageRepository extends JpaRepository<NatureStage, Long> {}

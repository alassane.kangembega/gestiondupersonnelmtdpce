package bf.personnel.com.repository;

import bf.personnel.com.domain.AvancementEchelon;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the AvancementEchelon entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AvancementEchelonRepository extends JpaRepository<AvancementEchelon, Long> {}

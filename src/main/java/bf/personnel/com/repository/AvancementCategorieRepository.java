package bf.personnel.com.repository;

import bf.personnel.com.domain.AvancementCategorie;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the AvancementCategorie entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AvancementCategorieRepository extends JpaRepository<AvancementCategorie, Long> {}

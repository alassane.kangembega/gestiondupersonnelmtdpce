package bf.personnel.com.repository;

import bf.personnel.com.domain.ChangementEmploi;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the ChangementEmploi entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ChangementEmploiRepository extends JpaRepository<ChangementEmploi, Long> {}

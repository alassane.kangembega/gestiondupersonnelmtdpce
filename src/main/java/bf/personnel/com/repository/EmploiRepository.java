package bf.personnel.com.repository;

import bf.personnel.com.domain.Emploi;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the Emploi entity.
 */
@SuppressWarnings("unused")
@Repository
public interface EmploiRepository extends JpaRepository<Emploi, Long> {}

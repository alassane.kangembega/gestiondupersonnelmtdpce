package bf.personnel.com.repository;

import bf.personnel.com.domain.NaturePositionIrreguliere;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the NaturePositionIrreguliere entity.
 */
@SuppressWarnings("unused")
@Repository
public interface NaturePositionIrreguliereRepository extends JpaRepository<NaturePositionIrreguliere, Long> {}

package bf.personnel.com.repository;

import bf.personnel.com.domain.Decoration;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the Decoration entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DecorationRepository extends JpaRepository<Decoration, Long> {}

package bf.personnel.com.repository;

import bf.personnel.com.domain.Entree;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the Entree entity.
 */
@SuppressWarnings("unused")
@Repository
public interface EntreeRepository extends JpaRepository<Entree, Long> {}

package bf.personnel.com.repository;

import bf.personnel.com.domain.Indemnite;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the Indemnite entity.
 */
@SuppressWarnings("unused")
@Repository
public interface IndemniteRepository extends JpaRepository<Indemnite, Long> {}

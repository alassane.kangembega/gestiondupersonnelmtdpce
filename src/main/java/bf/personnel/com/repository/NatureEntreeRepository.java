package bf.personnel.com.repository;

import bf.personnel.com.domain.NatureEntree;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the NatureEntree entity.
 */
@SuppressWarnings("unused")
@Repository
public interface NatureEntreeRepository extends JpaRepository<NatureEntree, Long> {}

package bf.personnel.com.repository;

import bf.personnel.com.domain.Echelon;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the Echelon entity.
 */
@SuppressWarnings("unused")
@Repository
public interface EchelonRepository extends JpaRepository<Echelon, Long> {}

package bf.personnel.com.repository;

import bf.personnel.com.domain.TypeDecoration;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the TypeDecoration entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TypeDecorationRepository extends JpaRepository<TypeDecoration, Long> {}

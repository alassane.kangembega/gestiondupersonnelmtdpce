package bf.personnel.com.repository;

import bf.personnel.com.domain.NatureRecrutement;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the NatureRecrutement entity.
 */
@SuppressWarnings("unused")
@Repository
public interface NatureRecrutementRepository extends JpaRepository<NatureRecrutement, Long> {}

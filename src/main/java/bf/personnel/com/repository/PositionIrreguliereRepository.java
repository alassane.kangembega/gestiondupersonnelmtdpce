package bf.personnel.com.repository;

import bf.personnel.com.domain.PositionIrreguliere;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the PositionIrreguliere entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PositionIrreguliereRepository extends JpaRepository<PositionIrreguliere, Long> {}

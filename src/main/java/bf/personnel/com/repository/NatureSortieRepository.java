package bf.personnel.com.repository;

import bf.personnel.com.domain.NatureSortie;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the NatureSortie entity.
 */
@SuppressWarnings("unused")
@Repository
public interface NatureSortieRepository extends JpaRepository<NatureSortie, Long> {}

package bf.personnel.com.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;

/**
 * A TypeDecoration.
 */
@Entity
@Table(name = "type_decoration")
@SuppressWarnings("common-java:DuplicatedBlocks")
public class TypeDecoration implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @Column(name = "libelle")
    private String libelle;

    @OneToMany(mappedBy = "typeDecoration")
    @JsonIgnoreProperties(value = { "typeDecoration", "employe" }, allowSetters = true)
    private Set<Decoration> decorations = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public TypeDecoration id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLibelle() {
        return this.libelle;
    }

    public TypeDecoration libelle(String libelle) {
        this.setLibelle(libelle);
        return this;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public Set<Decoration> getDecorations() {
        return this.decorations;
    }

    public void setDecorations(Set<Decoration> decorations) {
        if (this.decorations != null) {
            this.decorations.forEach(i -> i.setTypeDecoration(null));
        }
        if (decorations != null) {
            decorations.forEach(i -> i.setTypeDecoration(this));
        }
        this.decorations = decorations;
    }

    public TypeDecoration decorations(Set<Decoration> decorations) {
        this.setDecorations(decorations);
        return this;
    }

    public TypeDecoration addDecoration(Decoration decoration) {
        this.decorations.add(decoration);
        decoration.setTypeDecoration(this);
        return this;
    }

    public TypeDecoration removeDecoration(Decoration decoration) {
        this.decorations.remove(decoration);
        decoration.setTypeDecoration(null);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof TypeDecoration)) {
            return false;
        }
        return id != null && id.equals(((TypeDecoration) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "TypeDecoration{" +
            "id=" + getId() +
            ", libelle='" + getLibelle() + "'" +
            "}";
    }
}

package bf.personnel.com.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;

/**
 * A NatureStage.
 */
@Entity
@Table(name = "nature_stage")
@SuppressWarnings("common-java:DuplicatedBlocks")
public class NatureStage implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @Column(name = "nature_stage")
    private String natureStage;

    @OneToMany(mappedBy = "natureStage")
    @JsonIgnoreProperties(value = { "natureStage", "employe" }, allowSetters = true)
    private Set<Stage> stages = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public NatureStage id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNatureStage() {
        return this.natureStage;
    }

    public NatureStage natureStage(String natureStage) {
        this.setNatureStage(natureStage);
        return this;
    }

    public void setNatureStage(String natureStage) {
        this.natureStage = natureStage;
    }

    public Set<Stage> getStages() {
        return this.stages;
    }

    public void setStages(Set<Stage> stages) {
        if (this.stages != null) {
            this.stages.forEach(i -> i.setNatureStage(null));
        }
        if (stages != null) {
            stages.forEach(i -> i.setNatureStage(this));
        }
        this.stages = stages;
    }

    public NatureStage stages(Set<Stage> stages) {
        this.setStages(stages);
        return this;
    }

    public NatureStage addStage(Stage stage) {
        this.stages.add(stage);
        stage.setNatureStage(this);
        return this;
    }

    public NatureStage removeStage(Stage stage) {
        this.stages.remove(stage);
        stage.setNatureStage(null);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof NatureStage)) {
            return false;
        }
        return id != null && id.equals(((NatureStage) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "NatureStage{" +
            "id=" + getId() +
            ", natureStage='" + getNatureStage() + "'" +
            "}";
    }
}

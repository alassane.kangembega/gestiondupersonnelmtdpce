package bf.personnel.com.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;

/**
 * A Emploi.
 */
@Entity
@Table(name = "emploi")
@SuppressWarnings("common-java:DuplicatedBlocks")
public class Emploi implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @Column(name = "libelle")
    private String libelle;

    @OneToMany(mappedBy = "emploi")
    @JsonIgnoreProperties(value = { "emploi", "employe" }, allowSetters = true)
    private Set<ChangementEmploi> changementEmplois = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Emploi id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLibelle() {
        return this.libelle;
    }

    public Emploi libelle(String libelle) {
        this.setLibelle(libelle);
        return this;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public Set<ChangementEmploi> getChangementEmplois() {
        return this.changementEmplois;
    }

    public void setChangementEmplois(Set<ChangementEmploi> changementEmplois) {
        if (this.changementEmplois != null) {
            this.changementEmplois.forEach(i -> i.setEmploi(null));
        }
        if (changementEmplois != null) {
            changementEmplois.forEach(i -> i.setEmploi(this));
        }
        this.changementEmplois = changementEmplois;
    }

    public Emploi changementEmplois(Set<ChangementEmploi> changementEmplois) {
        this.setChangementEmplois(changementEmplois);
        return this;
    }

    public Emploi addChangementEmploi(ChangementEmploi changementEmploi) {
        this.changementEmplois.add(changementEmploi);
        changementEmploi.setEmploi(this);
        return this;
    }

    public Emploi removeChangementEmploi(ChangementEmploi changementEmploi) {
        this.changementEmplois.remove(changementEmploi);
        changementEmploi.setEmploi(null);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Emploi)) {
            return false;
        }
        return id != null && id.equals(((Emploi) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Emploi{" +
            "id=" + getId() +
            ", libelle='" + getLibelle() + "'" +
            "}";
    }
}

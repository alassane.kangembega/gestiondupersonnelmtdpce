package bf.personnel.com.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;

/**
 * A Echelon.
 */
@Entity
@Table(name = "echelon")
@SuppressWarnings("common-java:DuplicatedBlocks")
public class Echelon implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @Column(name = "numero")
    private Integer numero;

    @OneToMany(mappedBy = "echelon")
    @JsonIgnoreProperties(value = { "echelon", "employe" }, allowSetters = true)
    private Set<AvancementEchelon> avancementEchelons = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Echelon id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getNumero() {
        return this.numero;
    }

    public Echelon numero(Integer numero) {
        this.setNumero(numero);
        return this;
    }

    public void setNumero(Integer numero) {
        this.numero = numero;
    }

    public Set<AvancementEchelon> getAvancementEchelons() {
        return this.avancementEchelons;
    }

    public void setAvancementEchelons(Set<AvancementEchelon> avancementEchelons) {
        if (this.avancementEchelons != null) {
            this.avancementEchelons.forEach(i -> i.setEchelon(null));
        }
        if (avancementEchelons != null) {
            avancementEchelons.forEach(i -> i.setEchelon(this));
        }
        this.avancementEchelons = avancementEchelons;
    }

    public Echelon avancementEchelons(Set<AvancementEchelon> avancementEchelons) {
        this.setAvancementEchelons(avancementEchelons);
        return this;
    }

    public Echelon addAvancementEchelon(AvancementEchelon avancementEchelon) {
        this.avancementEchelons.add(avancementEchelon);
        avancementEchelon.setEchelon(this);
        return this;
    }

    public Echelon removeAvancementEchelon(AvancementEchelon avancementEchelon) {
        this.avancementEchelons.remove(avancementEchelon);
        avancementEchelon.setEchelon(null);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Echelon)) {
            return false;
        }
        return id != null && id.equals(((Echelon) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Echelon{" +
            "id=" + getId() +
            ", numero=" + getNumero() +
            "}";
    }
}

package bf.personnel.com.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.time.LocalDate;
import javax.persistence.*;

/**
 * A ChangementEmploi.
 */
@Entity
@Table(name = "changement_emploi")
@SuppressWarnings("common-java:DuplicatedBlocks")
public class ChangementEmploi implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @Column(name = "date_changement")
    private LocalDate dateChangement;

    @ManyToOne
    @JsonIgnoreProperties(value = { "changementEmplois" }, allowSetters = true)
    private Emploi emploi;

    @ManyToOne
    @JsonIgnoreProperties(
        value = {
            "positionIrregulieres",
            "stages",
            "entrees",
            "sorties",
            "changementEmplois",
            "affectations",
            "avancementCategories",
            "avancementClasses",
            "avancementEchelons",
            "decorations",
            "natureRecrutement",
            "statut",
            "indemnites",
        },
        allowSetters = true
    )
    private Employe employe;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public ChangementEmploi id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getDateChangement() {
        return this.dateChangement;
    }

    public ChangementEmploi dateChangement(LocalDate dateChangement) {
        this.setDateChangement(dateChangement);
        return this;
    }

    public void setDateChangement(LocalDate dateChangement) {
        this.dateChangement = dateChangement;
    }

    public Emploi getEmploi() {
        return this.emploi;
    }

    public void setEmploi(Emploi emploi) {
        this.emploi = emploi;
    }

    public ChangementEmploi emploi(Emploi emploi) {
        this.setEmploi(emploi);
        return this;
    }

    public Employe getEmploye() {
        return this.employe;
    }

    public void setEmploye(Employe employe) {
        this.employe = employe;
    }

    public ChangementEmploi employe(Employe employe) {
        this.setEmploye(employe);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ChangementEmploi)) {
            return false;
        }
        return id != null && id.equals(((ChangementEmploi) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ChangementEmploi{" +
            "id=" + getId() +
            ", dateChangement='" + getDateChangement() + "'" +
            "}";
    }
}

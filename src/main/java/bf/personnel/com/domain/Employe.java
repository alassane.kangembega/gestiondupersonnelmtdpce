package bf.personnel.com.domain;

import bf.personnel.com.domain.enumeration.Sexe;
import bf.personnel.com.domain.enumeration.SituationMatrimoniale;
import bf.personnel.com.domain.enumeration.StatutAgentPublic;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;

/**
 * A Employe.
 */
@Entity
@Table(name = "employe")
@SuppressWarnings("common-java:DuplicatedBlocks")
public class Employe implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @Column(name = "matricule")
    private String matricule;

    @Column(name = "nom")
    private String nom;

    @Column(name = "prenom")
    private String prenom;

    @Enumerated(EnumType.STRING)
    @Column(name = "sexe")
    private Sexe sexe;

    @Column(name = "date_naissance")
    private LocalDate dateNaissance;

    @Column(name = "date_integration")
    private LocalDate dateIntegration;

    @Enumerated(EnumType.STRING)
    @Column(name = "situation_matrimoniale")
    private SituationMatrimoniale situationMatrimoniale;

    @Enumerated(EnumType.STRING)
    @Column(name = "statut_agent_public")
    private StatutAgentPublic statutAgentPublic;

    @Column(name = "indice")
    private Integer indice;

    @Column(name = "solde_indiciaire")
    private Float soldeIndiciaire;

    @Column(name = "salaire_base")
    private Float salaireBase;

    @Column(name = "nombre_charge")
    private Integer nombreCharge;

    @Column(name = "prise_service_poste_actuel")
    private LocalDate priseServicePosteActuel;

    @Column(name = "telephone")
    private String telephone;

    @Column(name = "email")
    private String email;

    @Column(name = "date_probable_retraite")
    private LocalDate dateProbableRetraite;

    @Column(name = "date_anniversaire")
    private LocalDate dateAnniversaire;

    @Column(name = "observation")
    private String observation;

    @Column(name = "date_recrutement")
    private LocalDate dateRecrutement;

    @Column(name = "allocation")
    private Float allocation;

    @OneToMany(mappedBy = "employe")
    @JsonIgnoreProperties(value = { "naturePositionIrreguliere", "employe" }, allowSetters = true)
    private Set<PositionIrreguliere> positionIrregulieres = new HashSet<>();

    @OneToMany(mappedBy = "employe")
    @JsonIgnoreProperties(value = { "natureStage", "employe" }, allowSetters = true)
    private Set<Stage> stages = new HashSet<>();

    @OneToMany(mappedBy = "employe")
    @JsonIgnoreProperties(value = { "employe", "natureEntree" }, allowSetters = true)
    private Set<Entree> entrees = new HashSet<>();

    @OneToMany(mappedBy = "employe")
    @JsonIgnoreProperties(value = { "natureSortie", "employe" }, allowSetters = true)
    private Set<Sortie> sorties = new HashSet<>();

    @OneToMany(mappedBy = "employe")
    @JsonIgnoreProperties(value = { "emploi", "employe" }, allowSetters = true)
    private Set<ChangementEmploi> changementEmplois = new HashSet<>();

    @OneToMany(mappedBy = "employe")
    @JsonIgnoreProperties(value = { "region", "fonction", "employe", "structure" }, allowSetters = true)
    private Set<Affectation> affectations = new HashSet<>();

    @OneToMany(mappedBy = "employe")
    @JsonIgnoreProperties(value = { "categorie", "employe" }, allowSetters = true)
    private Set<AvancementCategorie> avancementCategories = new HashSet<>();

    @OneToMany(mappedBy = "employe")
    @JsonIgnoreProperties(value = { "classe", "employe" }, allowSetters = true)
    private Set<AvancementClasse> avancementClasses = new HashSet<>();

    @OneToMany(mappedBy = "employe")
    @JsonIgnoreProperties(value = { "echelon", "employe" }, allowSetters = true)
    private Set<AvancementEchelon> avancementEchelons = new HashSet<>();

    @OneToMany(mappedBy = "employe")
    @JsonIgnoreProperties(value = { "typeDecoration", "employe" }, allowSetters = true)
    private Set<Decoration> decorations = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties(value = { "employes" }, allowSetters = true)
    private NatureRecrutement natureRecrutement;

    @ManyToOne
    @JsonIgnoreProperties(value = { "employes" }, allowSetters = true)
    private Statut statut;

    @OneToMany(mappedBy = "employe")
    @JsonIgnoreProperties(value = { "employe" }, allowSetters = true)
    private Set<Indemnite> indemnites = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Employe id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMatricule() {
        return this.matricule;
    }

    public Employe matricule(String matricule) {
        this.setMatricule(matricule);
        return this;
    }

    public void setMatricule(String matricule) {
        this.matricule = matricule;
    }

    public String getNom() {
        return this.nom;
    }

    public Employe nom(String nom) {
        this.setNom(nom);
        return this;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return this.prenom;
    }

    public Employe prenom(String prenom) {
        this.setPrenom(prenom);
        return this;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public Sexe getSexe() {
        return this.sexe;
    }

    public Employe sexe(Sexe sexe) {
        this.setSexe(sexe);
        return this;
    }

    public void setSexe(Sexe sexe) {
        this.sexe = sexe;
    }

    public LocalDate getDateNaissance() {
        return this.dateNaissance;
    }

    public Employe dateNaissance(LocalDate dateNaissance) {
        this.setDateNaissance(dateNaissance);
        return this;
    }

    public void setDateNaissance(LocalDate dateNaissance) {
        this.dateNaissance = dateNaissance;
    }

    public LocalDate getDateIntegration() {
        return this.dateIntegration;
    }

    public Employe dateIntegration(LocalDate dateIntegration) {
        this.setDateIntegration(dateIntegration);
        return this;
    }

    public void setDateIntegration(LocalDate dateIntegration) {
        this.dateIntegration = dateIntegration;
    }

    public SituationMatrimoniale getSituationMatrimoniale() {
        return this.situationMatrimoniale;
    }

    public Employe situationMatrimoniale(SituationMatrimoniale situationMatrimoniale) {
        this.setSituationMatrimoniale(situationMatrimoniale);
        return this;
    }

    public void setSituationMatrimoniale(SituationMatrimoniale situationMatrimoniale) {
        this.situationMatrimoniale = situationMatrimoniale;
    }

    public StatutAgentPublic getStatutAgentPublic() {
        return this.statutAgentPublic;
    }

    public Employe statutAgentPublic(StatutAgentPublic statutAgentPublic) {
        this.setStatutAgentPublic(statutAgentPublic);
        return this;
    }

    public void setStatutAgentPublic(StatutAgentPublic statutAgentPublic) {
        this.statutAgentPublic = statutAgentPublic;
    }

    public Integer getIndice() {
        return this.indice;
    }

    public Employe indice(Integer indice) {
        this.setIndice(indice);
        return this;
    }

    public void setIndice(Integer indice) {
        this.indice = indice;
    }

    public Float getSoldeIndiciaire() {
        return this.soldeIndiciaire;
    }

    public Employe soldeIndiciaire(Float soldeIndiciaire) {
        this.setSoldeIndiciaire(soldeIndiciaire);
        return this;
    }

    public void setSoldeIndiciaire(Float soldeIndiciaire) {
        this.soldeIndiciaire = soldeIndiciaire;
    }

    public Float getSalaireBase() {
        return this.salaireBase;
    }

    public Employe salaireBase(Float salaireBase) {
        this.setSalaireBase(salaireBase);
        return this;
    }

    public void setSalaireBase(Float salaireBase) {
        this.salaireBase = salaireBase;
    }

    public Integer getNombreCharge() {
        return this.nombreCharge;
    }

    public Employe nombreCharge(Integer nombreCharge) {
        this.setNombreCharge(nombreCharge);
        return this;
    }

    public void setNombreCharge(Integer nombreCharge) {
        this.nombreCharge = nombreCharge;
    }

    public LocalDate getPriseServicePosteActuel() {
        return this.priseServicePosteActuel;
    }

    public Employe priseServicePosteActuel(LocalDate priseServicePosteActuel) {
        this.setPriseServicePosteActuel(priseServicePosteActuel);
        return this;
    }

    public void setPriseServicePosteActuel(LocalDate priseServicePosteActuel) {
        this.priseServicePosteActuel = priseServicePosteActuel;
    }

    public String getTelephone() {
        return this.telephone;
    }

    public Employe telephone(String telephone) {
        this.setTelephone(telephone);
        return this;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getEmail() {
        return this.email;
    }

    public Employe email(String email) {
        this.setEmail(email);
        return this;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public LocalDate getDateProbableRetraite() {
        return this.dateProbableRetraite;
    }

    public Employe dateProbableRetraite(LocalDate dateProbableRetraite) {
        this.setDateProbableRetraite(dateProbableRetraite);
        return this;
    }

    public void setDateProbableRetraite(LocalDate dateProbableRetraite) {
        this.dateProbableRetraite = dateProbableRetraite;
    }

    public LocalDate getDateAnniversaire() {
        return this.dateAnniversaire;
    }

    public Employe dateAnniversaire(LocalDate dateAnniversaire) {
        this.setDateAnniversaire(dateAnniversaire);
        return this;
    }

    public void setDateAnniversaire(LocalDate dateAnniversaire) {
        this.dateAnniversaire = dateAnniversaire;
    }

    public String getObservation() {
        return this.observation;
    }

    public Employe observation(String observation) {
        this.setObservation(observation);
        return this;
    }

    public void setObservation(String observation) {
        this.observation = observation;
    }

    public LocalDate getDateRecrutement() {
        return this.dateRecrutement;
    }

    public Employe dateRecrutement(LocalDate dateRecrutement) {
        this.setDateRecrutement(dateRecrutement);
        return this;
    }

    public void setDateRecrutement(LocalDate dateRecrutement) {
        this.dateRecrutement = dateRecrutement;
    }

    public Float getAllocation() {
        return this.allocation;
    }

    public Employe allocation(Float allocation) {
        this.setAllocation(allocation);
        return this;
    }

    public void setAllocation(Float allocation) {
        this.allocation = allocation;
    }

    public Set<PositionIrreguliere> getPositionIrregulieres() {
        return this.positionIrregulieres;
    }

    public void setPositionIrregulieres(Set<PositionIrreguliere> positionIrregulieres) {
        if (this.positionIrregulieres != null) {
            this.positionIrregulieres.forEach(i -> i.setEmploye(null));
        }
        if (positionIrregulieres != null) {
            positionIrregulieres.forEach(i -> i.setEmploye(this));
        }
        this.positionIrregulieres = positionIrregulieres;
    }

    public Employe positionIrregulieres(Set<PositionIrreguliere> positionIrregulieres) {
        this.setPositionIrregulieres(positionIrregulieres);
        return this;
    }

    public Employe addPositionIrreguliere(PositionIrreguliere positionIrreguliere) {
        this.positionIrregulieres.add(positionIrreguliere);
        positionIrreguliere.setEmploye(this);
        return this;
    }

    public Employe removePositionIrreguliere(PositionIrreguliere positionIrreguliere) {
        this.positionIrregulieres.remove(positionIrreguliere);
        positionIrreguliere.setEmploye(null);
        return this;
    }

    public Set<Stage> getStages() {
        return this.stages;
    }

    public void setStages(Set<Stage> stages) {
        if (this.stages != null) {
            this.stages.forEach(i -> i.setEmploye(null));
        }
        if (stages != null) {
            stages.forEach(i -> i.setEmploye(this));
        }
        this.stages = stages;
    }

    public Employe stages(Set<Stage> stages) {
        this.setStages(stages);
        return this;
    }

    public Employe addStage(Stage stage) {
        this.stages.add(stage);
        stage.setEmploye(this);
        return this;
    }

    public Employe removeStage(Stage stage) {
        this.stages.remove(stage);
        stage.setEmploye(null);
        return this;
    }

    public Set<Entree> getEntrees() {
        return this.entrees;
    }

    public void setEntrees(Set<Entree> entrees) {
        if (this.entrees != null) {
            this.entrees.forEach(i -> i.setEmploye(null));
        }
        if (entrees != null) {
            entrees.forEach(i -> i.setEmploye(this));
        }
        this.entrees = entrees;
    }

    public Employe entrees(Set<Entree> entrees) {
        this.setEntrees(entrees);
        return this;
    }

    public Employe addEntree(Entree entree) {
        this.entrees.add(entree);
        entree.setEmploye(this);
        return this;
    }

    public Employe removeEntree(Entree entree) {
        this.entrees.remove(entree);
        entree.setEmploye(null);
        return this;
    }

    public Set<Sortie> getSorties() {
        return this.sorties;
    }

    public void setSorties(Set<Sortie> sorties) {
        if (this.sorties != null) {
            this.sorties.forEach(i -> i.setEmploye(null));
        }
        if (sorties != null) {
            sorties.forEach(i -> i.setEmploye(this));
        }
        this.sorties = sorties;
    }

    public Employe sorties(Set<Sortie> sorties) {
        this.setSorties(sorties);
        return this;
    }

    public Employe addSortie(Sortie sortie) {
        this.sorties.add(sortie);
        sortie.setEmploye(this);
        return this;
    }

    public Employe removeSortie(Sortie sortie) {
        this.sorties.remove(sortie);
        sortie.setEmploye(null);
        return this;
    }

    public Set<ChangementEmploi> getChangementEmplois() {
        return this.changementEmplois;
    }

    public void setChangementEmplois(Set<ChangementEmploi> changementEmplois) {
        if (this.changementEmplois != null) {
            this.changementEmplois.forEach(i -> i.setEmploye(null));
        }
        if (changementEmplois != null) {
            changementEmplois.forEach(i -> i.setEmploye(this));
        }
        this.changementEmplois = changementEmplois;
    }

    public Employe changementEmplois(Set<ChangementEmploi> changementEmplois) {
        this.setChangementEmplois(changementEmplois);
        return this;
    }

    public Employe addChangementEmploi(ChangementEmploi changementEmploi) {
        this.changementEmplois.add(changementEmploi);
        changementEmploi.setEmploye(this);
        return this;
    }

    public Employe removeChangementEmploi(ChangementEmploi changementEmploi) {
        this.changementEmplois.remove(changementEmploi);
        changementEmploi.setEmploye(null);
        return this;
    }

    public Set<Affectation> getAffectations() {
        return this.affectations;
    }

    public void setAffectations(Set<Affectation> affectations) {
        if (this.affectations != null) {
            this.affectations.forEach(i -> i.setEmploye(null));
        }
        if (affectations != null) {
            affectations.forEach(i -> i.setEmploye(this));
        }
        this.affectations = affectations;
    }

    public Employe affectations(Set<Affectation> affectations) {
        this.setAffectations(affectations);
        return this;
    }

    public Employe addAffectation(Affectation affectation) {
        this.affectations.add(affectation);
        affectation.setEmploye(this);
        return this;
    }

    public Employe removeAffectation(Affectation affectation) {
        this.affectations.remove(affectation);
        affectation.setEmploye(null);
        return this;
    }

    public Set<AvancementCategorie> getAvancementCategories() {
        return this.avancementCategories;
    }

    public void setAvancementCategories(Set<AvancementCategorie> avancementCategories) {
        if (this.avancementCategories != null) {
            this.avancementCategories.forEach(i -> i.setEmploye(null));
        }
        if (avancementCategories != null) {
            avancementCategories.forEach(i -> i.setEmploye(this));
        }
        this.avancementCategories = avancementCategories;
    }

    public Employe avancementCategories(Set<AvancementCategorie> avancementCategories) {
        this.setAvancementCategories(avancementCategories);
        return this;
    }

    public Employe addAvancementCategorie(AvancementCategorie avancementCategorie) {
        this.avancementCategories.add(avancementCategorie);
        avancementCategorie.setEmploye(this);
        return this;
    }

    public Employe removeAvancementCategorie(AvancementCategorie avancementCategorie) {
        this.avancementCategories.remove(avancementCategorie);
        avancementCategorie.setEmploye(null);
        return this;
    }

    public Set<AvancementClasse> getAvancementClasses() {
        return this.avancementClasses;
    }

    public void setAvancementClasses(Set<AvancementClasse> avancementClasses) {
        if (this.avancementClasses != null) {
            this.avancementClasses.forEach(i -> i.setEmploye(null));
        }
        if (avancementClasses != null) {
            avancementClasses.forEach(i -> i.setEmploye(this));
        }
        this.avancementClasses = avancementClasses;
    }

    public Employe avancementClasses(Set<AvancementClasse> avancementClasses) {
        this.setAvancementClasses(avancementClasses);
        return this;
    }

    public Employe addAvancementClasse(AvancementClasse avancementClasse) {
        this.avancementClasses.add(avancementClasse);
        avancementClasse.setEmploye(this);
        return this;
    }

    public Employe removeAvancementClasse(AvancementClasse avancementClasse) {
        this.avancementClasses.remove(avancementClasse);
        avancementClasse.setEmploye(null);
        return this;
    }

    public Set<AvancementEchelon> getAvancementEchelons() {
        return this.avancementEchelons;
    }

    public void setAvancementEchelons(Set<AvancementEchelon> avancementEchelons) {
        if (this.avancementEchelons != null) {
            this.avancementEchelons.forEach(i -> i.setEmploye(null));
        }
        if (avancementEchelons != null) {
            avancementEchelons.forEach(i -> i.setEmploye(this));
        }
        this.avancementEchelons = avancementEchelons;
    }

    public Employe avancementEchelons(Set<AvancementEchelon> avancementEchelons) {
        this.setAvancementEchelons(avancementEchelons);
        return this;
    }

    public Employe addAvancementEchelon(AvancementEchelon avancementEchelon) {
        this.avancementEchelons.add(avancementEchelon);
        avancementEchelon.setEmploye(this);
        return this;
    }

    public Employe removeAvancementEchelon(AvancementEchelon avancementEchelon) {
        this.avancementEchelons.remove(avancementEchelon);
        avancementEchelon.setEmploye(null);
        return this;
    }

    public Set<Decoration> getDecorations() {
        return this.decorations;
    }

    public void setDecorations(Set<Decoration> decorations) {
        if (this.decorations != null) {
            this.decorations.forEach(i -> i.setEmploye(null));
        }
        if (decorations != null) {
            decorations.forEach(i -> i.setEmploye(this));
        }
        this.decorations = decorations;
    }

    public Employe decorations(Set<Decoration> decorations) {
        this.setDecorations(decorations);
        return this;
    }

    public Employe addDecoration(Decoration decoration) {
        this.decorations.add(decoration);
        decoration.setEmploye(this);
        return this;
    }

    public Employe removeDecoration(Decoration decoration) {
        this.decorations.remove(decoration);
        decoration.setEmploye(null);
        return this;
    }

    public NatureRecrutement getNatureRecrutement() {
        return this.natureRecrutement;
    }

    public void setNatureRecrutement(NatureRecrutement natureRecrutement) {
        this.natureRecrutement = natureRecrutement;
    }

    public Employe natureRecrutement(NatureRecrutement natureRecrutement) {
        this.setNatureRecrutement(natureRecrutement);
        return this;
    }

    public Statut getStatut() {
        return this.statut;
    }

    public void setStatut(Statut statut) {
        this.statut = statut;
    }

    public Employe statut(Statut statut) {
        this.setStatut(statut);
        return this;
    }

    public Set<Indemnite> getIndemnites() {
        return this.indemnites;
    }

    public void setIndemnites(Set<Indemnite> indemnites) {
        if (this.indemnites != null) {
            this.indemnites.forEach(i -> i.setEmploye(null));
        }
        if (indemnites != null) {
            indemnites.forEach(i -> i.setEmploye(this));
        }
        this.indemnites = indemnites;
    }

    public Employe indemnites(Set<Indemnite> indemnites) {
        this.setIndemnites(indemnites);
        return this;
    }

    public Employe addIndemnite(Indemnite indemnite) {
        this.indemnites.add(indemnite);
        indemnite.setEmploye(this);
        return this;
    }

    public Employe removeIndemnite(Indemnite indemnite) {
        this.indemnites.remove(indemnite);
        indemnite.setEmploye(null);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Employe)) {
            return false;
        }
        return id != null && id.equals(((Employe) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Employe{" +
            "id=" + getId() +
            ", matricule='" + getMatricule() + "'" +
            ", nom='" + getNom() + "'" +
            ", prenom='" + getPrenom() + "'" +
            ", sexe='" + getSexe() + "'" +
            ", dateNaissance='" + getDateNaissance() + "'" +
            ", dateIntegration='" + getDateIntegration() + "'" +
            ", situationMatrimoniale='" + getSituationMatrimoniale() + "'" +
            ", statutAgentPublic='" + getStatutAgentPublic() + "'" +
            ", indice=" + getIndice() +
            ", soldeIndiciaire=" + getSoldeIndiciaire() +
            ", salaireBase=" + getSalaireBase() +
            ", nombreCharge=" + getNombreCharge() +
            ", priseServicePosteActuel='" + getPriseServicePosteActuel() + "'" +
            ", telephone='" + getTelephone() + "'" +
            ", email='" + getEmail() + "'" +
            ", dateProbableRetraite='" + getDateProbableRetraite() + "'" +
            ", dateAnniversaire='" + getDateAnniversaire() + "'" +
            ", observation='" + getObservation() + "'" +
            ", dateRecrutement='" + getDateRecrutement() + "'" +
            ", allocation=" + getAllocation() +
            "}";
    }
}

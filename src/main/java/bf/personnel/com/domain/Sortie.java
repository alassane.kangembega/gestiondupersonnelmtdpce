package bf.personnel.com.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.time.LocalDate;
import javax.persistence.*;

/**
 * A Sortie.
 */
@Entity
@Table(name = "sortie")
@SuppressWarnings("common-java:DuplicatedBlocks")
public class Sortie implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @Column(name = "date_sortie")
    private LocalDate dateSortie;

    @ManyToOne
    @JsonIgnoreProperties(value = { "sorties" }, allowSetters = true)
    private NatureSortie natureSortie;

    @ManyToOne
    @JsonIgnoreProperties(
        value = {
            "positionIrregulieres",
            "stages",
            "entrees",
            "sorties",
            "changementEmplois",
            "affectations",
            "avancementCategories",
            "avancementClasses",
            "avancementEchelons",
            "decorations",
            "natureRecrutement",
            "statut",
            "indemnites",
        },
        allowSetters = true
    )
    private Employe employe;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Sortie id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getDateSortie() {
        return this.dateSortie;
    }

    public Sortie dateSortie(LocalDate dateSortie) {
        this.setDateSortie(dateSortie);
        return this;
    }

    public void setDateSortie(LocalDate dateSortie) {
        this.dateSortie = dateSortie;
    }

    public NatureSortie getNatureSortie() {
        return this.natureSortie;
    }

    public void setNatureSortie(NatureSortie natureSortie) {
        this.natureSortie = natureSortie;
    }

    public Sortie natureSortie(NatureSortie natureSortie) {
        this.setNatureSortie(natureSortie);
        return this;
    }

    public Employe getEmploye() {
        return this.employe;
    }

    public void setEmploye(Employe employe) {
        this.employe = employe;
    }

    public Sortie employe(Employe employe) {
        this.setEmploye(employe);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Sortie)) {
            return false;
        }
        return id != null && id.equals(((Sortie) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Sortie{" +
            "id=" + getId() +
            ", dateSortie='" + getDateSortie() + "'" +
            "}";
    }
}

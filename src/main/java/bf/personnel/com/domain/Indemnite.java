package bf.personnel.com.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import javax.persistence.*;

/**
 * A Indemnite.
 */
@Entity
@Table(name = "indemnite")
@SuppressWarnings("common-java:DuplicatedBlocks")
public class Indemnite implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @Column(name = "type_indemnite")
    private String typeIndemnite;

    @Column(name = "montant_indemnite")
    private Float montantIndemnite;

    @ManyToOne
    @JsonIgnoreProperties(
        value = {
            "positionIrregulieres",
            "stages",
            "entrees",
            "sorties",
            "changementEmplois",
            "affectations",
            "avancementCategories",
            "avancementClasses",
            "avancementEchelons",
            "decorations",
            "natureRecrutement",
            "statut",
            "indemnites",
        },
        allowSetters = true
    )
    private Employe employe;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Indemnite id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTypeIndemnite() {
        return this.typeIndemnite;
    }

    public Indemnite typeIndemnite(String typeIndemnite) {
        this.setTypeIndemnite(typeIndemnite);
        return this;
    }

    public void setTypeIndemnite(String typeIndemnite) {
        this.typeIndemnite = typeIndemnite;
    }

    public Float getMontantIndemnite() {
        return this.montantIndemnite;
    }

    public Indemnite montantIndemnite(Float montantIndemnite) {
        this.setMontantIndemnite(montantIndemnite);
        return this;
    }

    public void setMontantIndemnite(Float montantIndemnite) {
        this.montantIndemnite = montantIndemnite;
    }

    public Employe getEmploye() {
        return this.employe;
    }

    public void setEmploye(Employe employe) {
        this.employe = employe;
    }

    public Indemnite employe(Employe employe) {
        this.setEmploye(employe);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Indemnite)) {
            return false;
        }
        return id != null && id.equals(((Indemnite) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Indemnite{" +
            "id=" + getId() +
            ", typeIndemnite='" + getTypeIndemnite() + "'" +
            ", montantIndemnite=" + getMontantIndemnite() +
            "}";
    }
}

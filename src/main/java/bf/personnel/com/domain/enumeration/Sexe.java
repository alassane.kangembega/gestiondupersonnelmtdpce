package bf.personnel.com.domain.enumeration;

/**
 * The Sexe enumeration.
 */
public enum Sexe {
    Masculin,
    Feminin,
}

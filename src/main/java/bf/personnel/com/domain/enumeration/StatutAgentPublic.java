package bf.personnel.com.domain.enumeration;

/**
 * The StatutAgentPublic enumeration.
 */
public enum StatutAgentPublic {
    Fonctionnaire,
    Temporaire,
}

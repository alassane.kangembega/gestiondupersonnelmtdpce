package bf.personnel.com.domain.enumeration;

/**
 * The SituationMatrimoniale enumeration.
 */
public enum SituationMatrimoniale {
    Celibataire,
    Marie,
}

package bf.personnel.com.domain.enumeration;

/**
 * The TypeSortie enumeration.
 */
public enum TypeSortie {
    Temporaire,
    Definitif,
}

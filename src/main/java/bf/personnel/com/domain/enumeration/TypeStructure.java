package bf.personnel.com.domain.enumeration;

/**
 * The TypeStructure enumeration.
 */
public enum TypeStructure {
    DirectionGenerale,
    DirectionCentrale,
    DirectionTechnique,
    Service,
}

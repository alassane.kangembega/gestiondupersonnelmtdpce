package bf.personnel.com.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;

/**
 * A NatureRecrutement.
 */
@Entity
@Table(name = "nature_recrutement")
@SuppressWarnings("common-java:DuplicatedBlocks")
public class NatureRecrutement implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @Column(name = "nature_recrutement")
    private String natureRecrutement;

    @OneToMany(mappedBy = "natureRecrutement")
    @JsonIgnoreProperties(
        value = {
            "positionIrregulieres",
            "stages",
            "entrees",
            "sorties",
            "changementEmplois",
            "affectations",
            "avancementCategories",
            "avancementClasses",
            "avancementEchelons",
            "decorations",
            "natureRecrutement",
            "statut",
            "indemnites",
        },
        allowSetters = true
    )
    private Set<Employe> employes = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public NatureRecrutement id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNatureRecrutement() {
        return this.natureRecrutement;
    }

    public NatureRecrutement natureRecrutement(String natureRecrutement) {
        this.setNatureRecrutement(natureRecrutement);
        return this;
    }

    public void setNatureRecrutement(String natureRecrutement) {
        this.natureRecrutement = natureRecrutement;
    }

    public Set<Employe> getEmployes() {
        return this.employes;
    }

    public void setEmployes(Set<Employe> employes) {
        if (this.employes != null) {
            this.employes.forEach(i -> i.setNatureRecrutement(null));
        }
        if (employes != null) {
            employes.forEach(i -> i.setNatureRecrutement(this));
        }
        this.employes = employes;
    }

    public NatureRecrutement employes(Set<Employe> employes) {
        this.setEmployes(employes);
        return this;
    }

    public NatureRecrutement addEmploye(Employe employe) {
        this.employes.add(employe);
        employe.setNatureRecrutement(this);
        return this;
    }

    public NatureRecrutement removeEmploye(Employe employe) {
        this.employes.remove(employe);
        employe.setNatureRecrutement(null);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof NatureRecrutement)) {
            return false;
        }
        return id != null && id.equals(((NatureRecrutement) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "NatureRecrutement{" +
            "id=" + getId() +
            ", natureRecrutement='" + getNatureRecrutement() + "'" +
            "}";
    }
}

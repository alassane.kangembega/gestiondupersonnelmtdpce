package bf.personnel.com.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.time.LocalDate;
import javax.persistence.*;

/**
 * A Entree.
 */
@Entity
@Table(name = "entree")
@SuppressWarnings("common-java:DuplicatedBlocks")
public class Entree implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @Column(name = "date_entree")
    private LocalDate dateEntree;

    @ManyToOne
    @JsonIgnoreProperties(
        value = {
            "positionIrregulieres",
            "stages",
            "entrees",
            "sorties",
            "changementEmplois",
            "affectations",
            "avancementCategories",
            "avancementClasses",
            "avancementEchelons",
            "decorations",
            "natureRecrutement",
            "statut",
            "indemnites",
        },
        allowSetters = true
    )
    private Employe employe;

    @ManyToOne
    @JsonIgnoreProperties(value = { "entrees" }, allowSetters = true)
    private NatureEntree natureEntree;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Entree id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getDateEntree() {
        return this.dateEntree;
    }

    public Entree dateEntree(LocalDate dateEntree) {
        this.setDateEntree(dateEntree);
        return this;
    }

    public void setDateEntree(LocalDate dateEntree) {
        this.dateEntree = dateEntree;
    }

    public Employe getEmploye() {
        return this.employe;
    }

    public void setEmploye(Employe employe) {
        this.employe = employe;
    }

    public Entree employe(Employe employe) {
        this.setEmploye(employe);
        return this;
    }

    public NatureEntree getNatureEntree() {
        return this.natureEntree;
    }

    public void setNatureEntree(NatureEntree natureEntree) {
        this.natureEntree = natureEntree;
    }

    public Entree natureEntree(NatureEntree natureEntree) {
        this.setNatureEntree(natureEntree);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Entree)) {
            return false;
        }
        return id != null && id.equals(((Entree) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Entree{" +
            "id=" + getId() +
            ", dateEntree='" + getDateEntree() + "'" +
            "}";
    }
}

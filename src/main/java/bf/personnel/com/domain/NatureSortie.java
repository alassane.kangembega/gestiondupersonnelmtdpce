package bf.personnel.com.domain;

import bf.personnel.com.domain.enumeration.TypeSortie;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;

/**
 * A NatureSortie.
 */
@Entity
@Table(name = "nature_sortie")
@SuppressWarnings("common-java:DuplicatedBlocks")
public class NatureSortie implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @Enumerated(EnumType.STRING)
    @Column(name = "type_sortie")
    private TypeSortie typeSortie;

    @Column(name = "nature_sortie")
    private String natureSortie;

    @OneToMany(mappedBy = "natureSortie")
    @JsonIgnoreProperties(value = { "natureSortie", "employe" }, allowSetters = true)
    private Set<Sortie> sorties = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public NatureSortie id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public TypeSortie getTypeSortie() {
        return this.typeSortie;
    }

    public NatureSortie typeSortie(TypeSortie typeSortie) {
        this.setTypeSortie(typeSortie);
        return this;
    }

    public void setTypeSortie(TypeSortie typeSortie) {
        this.typeSortie = typeSortie;
    }

    public String getNatureSortie() {
        return this.natureSortie;
    }

    public NatureSortie natureSortie(String natureSortie) {
        this.setNatureSortie(natureSortie);
        return this;
    }

    public void setNatureSortie(String natureSortie) {
        this.natureSortie = natureSortie;
    }

    public Set<Sortie> getSorties() {
        return this.sorties;
    }

    public void setSorties(Set<Sortie> sorties) {
        if (this.sorties != null) {
            this.sorties.forEach(i -> i.setNatureSortie(null));
        }
        if (sorties != null) {
            sorties.forEach(i -> i.setNatureSortie(this));
        }
        this.sorties = sorties;
    }

    public NatureSortie sorties(Set<Sortie> sorties) {
        this.setSorties(sorties);
        return this;
    }

    public NatureSortie addSortie(Sortie sortie) {
        this.sorties.add(sortie);
        sortie.setNatureSortie(this);
        return this;
    }

    public NatureSortie removeSortie(Sortie sortie) {
        this.sorties.remove(sortie);
        sortie.setNatureSortie(null);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof NatureSortie)) {
            return false;
        }
        return id != null && id.equals(((NatureSortie) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "NatureSortie{" +
            "id=" + getId() +
            ", typeSortie='" + getTypeSortie() + "'" +
            ", natureSortie='" + getNatureSortie() + "'" +
            "}";
    }
}

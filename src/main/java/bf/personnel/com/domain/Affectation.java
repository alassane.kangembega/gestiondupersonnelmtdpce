package bf.personnel.com.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.time.LocalDate;
import javax.persistence.*;

/**
 * A Affectation.
 */
@Entity
@Table(name = "affectation")
@SuppressWarnings("common-java:DuplicatedBlocks")
public class Affectation implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @Column(name = "date_affectation")
    private LocalDate dateAffectation;

    @ManyToOne
    @JsonIgnoreProperties(value = { "affectations" }, allowSetters = true)
    private Region region;

    @ManyToOne
    @JsonIgnoreProperties(value = { "affectations" }, allowSetters = true)
    private Fonction fonction;

    @ManyToOne
    @JsonIgnoreProperties(
        value = {
            "positionIrregulieres",
            "stages",
            "entrees",
            "sorties",
            "changementEmplois",
            "affectations",
            "avancementCategories",
            "avancementClasses",
            "avancementEchelons",
            "decorations",
            "natureRecrutement",
            "statut",
            "indemnites",
        },
        allowSetters = true
    )
    private Employe employe;

    @ManyToOne
    @JsonIgnoreProperties(value = { "childStructures", "affectations", "parentStructure" }, allowSetters = true)
    private Structure structure;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Affectation id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getDateAffectation() {
        return this.dateAffectation;
    }

    public Affectation dateAffectation(LocalDate dateAffectation) {
        this.setDateAffectation(dateAffectation);
        return this;
    }

    public void setDateAffectation(LocalDate dateAffectation) {
        this.dateAffectation = dateAffectation;
    }

    public Region getRegion() {
        return this.region;
    }

    public void setRegion(Region region) {
        this.region = region;
    }

    public Affectation region(Region region) {
        this.setRegion(region);
        return this;
    }

    public Fonction getFonction() {
        return this.fonction;
    }

    public void setFonction(Fonction fonction) {
        this.fonction = fonction;
    }

    public Affectation fonction(Fonction fonction) {
        this.setFonction(fonction);
        return this;
    }

    public Employe getEmploye() {
        return this.employe;
    }

    public void setEmploye(Employe employe) {
        this.employe = employe;
    }

    public Affectation employe(Employe employe) {
        this.setEmploye(employe);
        return this;
    }

    public Structure getStructure() {
        return this.structure;
    }

    public void setStructure(Structure structure) {
        this.structure = structure;
    }

    public Affectation structure(Structure structure) {
        this.setStructure(structure);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Affectation)) {
            return false;
        }
        return id != null && id.equals(((Affectation) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Affectation{" +
            "id=" + getId() +
            ", dateAffectation='" + getDateAffectation() + "'" +
            "}";
    }
}

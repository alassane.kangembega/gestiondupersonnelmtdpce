package bf.personnel.com.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;

/**
 * A Classe.
 */
@Entity
@Table(name = "classe")
@SuppressWarnings("common-java:DuplicatedBlocks")
public class Classe implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @Column(name = "numero")
    private Integer numero;

    @OneToMany(mappedBy = "classe")
    @JsonIgnoreProperties(value = { "classe", "employe" }, allowSetters = true)
    private Set<AvancementClasse> avancementClasses = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Classe id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getNumero() {
        return this.numero;
    }

    public Classe numero(Integer numero) {
        this.setNumero(numero);
        return this;
    }

    public void setNumero(Integer numero) {
        this.numero = numero;
    }

    public Set<AvancementClasse> getAvancementClasses() {
        return this.avancementClasses;
    }

    public void setAvancementClasses(Set<AvancementClasse> avancementClasses) {
        if (this.avancementClasses != null) {
            this.avancementClasses.forEach(i -> i.setClasse(null));
        }
        if (avancementClasses != null) {
            avancementClasses.forEach(i -> i.setClasse(this));
        }
        this.avancementClasses = avancementClasses;
    }

    public Classe avancementClasses(Set<AvancementClasse> avancementClasses) {
        this.setAvancementClasses(avancementClasses);
        return this;
    }

    public Classe addAvancementClasse(AvancementClasse avancementClasse) {
        this.avancementClasses.add(avancementClasse);
        avancementClasse.setClasse(this);
        return this;
    }

    public Classe removeAvancementClasse(AvancementClasse avancementClasse) {
        this.avancementClasses.remove(avancementClasse);
        avancementClasse.setClasse(null);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Classe)) {
            return false;
        }
        return id != null && id.equals(((Classe) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Classe{" +
            "id=" + getId() +
            ", numero=" + getNumero() +
            "}";
    }
}

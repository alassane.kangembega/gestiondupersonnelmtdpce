package bf.personnel.com.domain;

import bf.personnel.com.domain.enumeration.TypeStructure;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;

/**
 * A Structure.
 */
@Entity
@Table(name = "structure")
@SuppressWarnings("common-java:DuplicatedBlocks")
public class Structure implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @Enumerated(EnumType.STRING)
    @Column(name = "type")
    private TypeStructure type;

    @Column(name = "sigle")
    private String sigle;

    @Column(name = "libelle")
    private String libelle;

    @OneToMany(mappedBy = "parentStructure")
    @JsonIgnoreProperties(value = { "childStructures", "affectations", "parentStructure" }, allowSetters = true)
    private Set<Structure> childStructures = new HashSet<>();

    @OneToMany(mappedBy = "structure")
    @JsonIgnoreProperties(value = { "region", "fonction", "employe", "structure" }, allowSetters = true)
    private Set<Affectation> affectations = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties(value = { "childStructures", "affectations", "parentStructure" }, allowSetters = true)
    private Structure parentStructure;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Structure id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public TypeStructure getType() {
        return this.type;
    }

    public Structure type(TypeStructure type) {
        this.setType(type);
        return this;
    }

    public void setType(TypeStructure type) {
        this.type = type;
    }

    public String getSigle() {
        return this.sigle;
    }

    public Structure sigle(String sigle) {
        this.setSigle(sigle);
        return this;
    }

    public void setSigle(String sigle) {
        this.sigle = sigle;
    }

    public String getLibelle() {
        return this.libelle;
    }

    public Structure libelle(String libelle) {
        this.setLibelle(libelle);
        return this;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public Set<Structure> getChildStructures() {
        return this.childStructures;
    }

    public void setChildStructures(Set<Structure> structures) {
        if (this.childStructures != null) {
            this.childStructures.forEach(i -> i.setParentStructure(null));
        }
        if (structures != null) {
            structures.forEach(i -> i.setParentStructure(this));
        }
        this.childStructures = structures;
    }

    public Structure childStructures(Set<Structure> structures) {
        this.setChildStructures(structures);
        return this;
    }

    public Structure addChildStructures(Structure structure) {
        this.childStructures.add(structure);
        structure.setParentStructure(this);
        return this;
    }

    public Structure removeChildStructures(Structure structure) {
        this.childStructures.remove(structure);
        structure.setParentStructure(null);
        return this;
    }

    public Set<Affectation> getAffectations() {
        return this.affectations;
    }

    public void setAffectations(Set<Affectation> affectations) {
        if (this.affectations != null) {
            this.affectations.forEach(i -> i.setStructure(null));
        }
        if (affectations != null) {
            affectations.forEach(i -> i.setStructure(this));
        }
        this.affectations = affectations;
    }

    public Structure affectations(Set<Affectation> affectations) {
        this.setAffectations(affectations);
        return this;
    }

    public Structure addAffectation(Affectation affectation) {
        this.affectations.add(affectation);
        affectation.setStructure(this);
        return this;
    }

    public Structure removeAffectation(Affectation affectation) {
        this.affectations.remove(affectation);
        affectation.setStructure(null);
        return this;
    }

    public Structure getParentStructure() {
        return this.parentStructure;
    }

    public void setParentStructure(Structure structure) {
        this.parentStructure = structure;
    }

    public Structure parentStructure(Structure structure) {
        this.setParentStructure(structure);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Structure)) {
            return false;
        }
        return id != null && id.equals(((Structure) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Structure{" +
            "id=" + getId() +
            ", type='" + getType() + "'" +
            ", sigle='" + getSigle() + "'" +
            ", libelle='" + getLibelle() + "'" +
            "}";
    }
}

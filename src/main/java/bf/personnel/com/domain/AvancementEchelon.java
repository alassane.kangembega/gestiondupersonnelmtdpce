package bf.personnel.com.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.time.LocalDate;
import javax.persistence.*;

/**
 * A AvancementEchelon.
 */
@Entity
@Table(name = "avancement_echelon")
@SuppressWarnings("common-java:DuplicatedBlocks")
public class AvancementEchelon implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @Column(name = "date_avancement")
    private LocalDate dateAvancement;

    @ManyToOne
    @JsonIgnoreProperties(value = { "avancementEchelons" }, allowSetters = true)
    private Echelon echelon;

    @ManyToOne
    @JsonIgnoreProperties(
        value = {
            "positionIrregulieres",
            "stages",
            "entrees",
            "sorties",
            "changementEmplois",
            "affectations",
            "avancementCategories",
            "avancementClasses",
            "avancementEchelons",
            "decorations",
            "natureRecrutement",
            "statut",
            "indemnites",
        },
        allowSetters = true
    )
    private Employe employe;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public AvancementEchelon id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getDateAvancement() {
        return this.dateAvancement;
    }

    public AvancementEchelon dateAvancement(LocalDate dateAvancement) {
        this.setDateAvancement(dateAvancement);
        return this;
    }

    public void setDateAvancement(LocalDate dateAvancement) {
        this.dateAvancement = dateAvancement;
    }

    public Echelon getEchelon() {
        return this.echelon;
    }

    public void setEchelon(Echelon echelon) {
        this.echelon = echelon;
    }

    public AvancementEchelon echelon(Echelon echelon) {
        this.setEchelon(echelon);
        return this;
    }

    public Employe getEmploye() {
        return this.employe;
    }

    public void setEmploye(Employe employe) {
        this.employe = employe;
    }

    public AvancementEchelon employe(Employe employe) {
        this.setEmploye(employe);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof AvancementEchelon)) {
            return false;
        }
        return id != null && id.equals(((AvancementEchelon) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "AvancementEchelon{" +
            "id=" + getId() +
            ", dateAvancement='" + getDateAvancement() + "'" +
            "}";
    }
}

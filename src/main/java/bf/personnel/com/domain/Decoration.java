package bf.personnel.com.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.time.LocalDate;
import javax.persistence.*;

/**
 * A Decoration.
 */
@Entity
@Table(name = "decoration")
@SuppressWarnings("common-java:DuplicatedBlocks")
public class Decoration implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @Column(name = "date_decoration")
    private LocalDate dateDecoration;

    @ManyToOne
    @JsonIgnoreProperties(value = { "decorations" }, allowSetters = true)
    private TypeDecoration typeDecoration;

    @ManyToOne
    @JsonIgnoreProperties(
        value = {
            "positionIrregulieres",
            "stages",
            "entrees",
            "sorties",
            "changementEmplois",
            "affectations",
            "avancementCategories",
            "avancementClasses",
            "avancementEchelons",
            "decorations",
            "natureRecrutement",
            "statut",
            "indemnites",
        },
        allowSetters = true
    )
    private Employe employe;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Decoration id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getDateDecoration() {
        return this.dateDecoration;
    }

    public Decoration dateDecoration(LocalDate dateDecoration) {
        this.setDateDecoration(dateDecoration);
        return this;
    }

    public void setDateDecoration(LocalDate dateDecoration) {
        this.dateDecoration = dateDecoration;
    }

    public TypeDecoration getTypeDecoration() {
        return this.typeDecoration;
    }

    public void setTypeDecoration(TypeDecoration typeDecoration) {
        this.typeDecoration = typeDecoration;
    }

    public Decoration typeDecoration(TypeDecoration typeDecoration) {
        this.setTypeDecoration(typeDecoration);
        return this;
    }

    public Employe getEmploye() {
        return this.employe;
    }

    public void setEmploye(Employe employe) {
        this.employe = employe;
    }

    public Decoration employe(Employe employe) {
        this.setEmploye(employe);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Decoration)) {
            return false;
        }
        return id != null && id.equals(((Decoration) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Decoration{" +
            "id=" + getId() +
            ", dateDecoration='" + getDateDecoration() + "'" +
            "}";
    }
}

package bf.personnel.com.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;

/**
 * A NatureEntree.
 */
@Entity
@Table(name = "nature_entree")
@SuppressWarnings("common-java:DuplicatedBlocks")
public class NatureEntree implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @Column(name = "nature_entree")
    private String natureEntree;

    @OneToMany(mappedBy = "natureEntree")
    @JsonIgnoreProperties(value = { "employe", "natureEntree" }, allowSetters = true)
    private Set<Entree> entrees = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public NatureEntree id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNatureEntree() {
        return this.natureEntree;
    }

    public NatureEntree natureEntree(String natureEntree) {
        this.setNatureEntree(natureEntree);
        return this;
    }

    public void setNatureEntree(String natureEntree) {
        this.natureEntree = natureEntree;
    }

    public Set<Entree> getEntrees() {
        return this.entrees;
    }

    public void setEntrees(Set<Entree> entrees) {
        if (this.entrees != null) {
            this.entrees.forEach(i -> i.setNatureEntree(null));
        }
        if (entrees != null) {
            entrees.forEach(i -> i.setNatureEntree(this));
        }
        this.entrees = entrees;
    }

    public NatureEntree entrees(Set<Entree> entrees) {
        this.setEntrees(entrees);
        return this;
    }

    public NatureEntree addEntree(Entree entree) {
        this.entrees.add(entree);
        entree.setNatureEntree(this);
        return this;
    }

    public NatureEntree removeEntree(Entree entree) {
        this.entrees.remove(entree);
        entree.setNatureEntree(null);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof NatureEntree)) {
            return false;
        }
        return id != null && id.equals(((NatureEntree) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "NatureEntree{" +
            "id=" + getId() +
            ", natureEntree='" + getNatureEntree() + "'" +
            "}";
    }
}

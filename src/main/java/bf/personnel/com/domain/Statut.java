package bf.personnel.com.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;

/**
 * A Statut.
 */
@Entity
@Table(name = "statut")
@SuppressWarnings("common-java:DuplicatedBlocks")
public class Statut implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @Column(name = "statut_agent")
    private String statutAgent;

    @OneToMany(mappedBy = "statut")
    @JsonIgnoreProperties(
        value = {
            "positionIrregulieres",
            "stages",
            "entrees",
            "sorties",
            "changementEmplois",
            "affectations",
            "avancementCategories",
            "avancementClasses",
            "avancementEchelons",
            "decorations",
            "natureRecrutement",
            "statut",
            "indemnites",
        },
        allowSetters = true
    )
    private Set<Employe> employes = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Statut id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getStatutAgent() {
        return this.statutAgent;
    }

    public Statut statutAgent(String statutAgent) {
        this.setStatutAgent(statutAgent);
        return this;
    }

    public void setStatutAgent(String statutAgent) {
        this.statutAgent = statutAgent;
    }

    public Set<Employe> getEmployes() {
        return this.employes;
    }

    public void setEmployes(Set<Employe> employes) {
        if (this.employes != null) {
            this.employes.forEach(i -> i.setStatut(null));
        }
        if (employes != null) {
            employes.forEach(i -> i.setStatut(this));
        }
        this.employes = employes;
    }

    public Statut employes(Set<Employe> employes) {
        this.setEmployes(employes);
        return this;
    }

    public Statut addEmploye(Employe employe) {
        this.employes.add(employe);
        employe.setStatut(this);
        return this;
    }

    public Statut removeEmploye(Employe employe) {
        this.employes.remove(employe);
        employe.setStatut(null);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Statut)) {
            return false;
        }
        return id != null && id.equals(((Statut) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Statut{" +
            "id=" + getId() +
            ", statutAgent='" + getStatutAgent() + "'" +
            "}";
    }
}

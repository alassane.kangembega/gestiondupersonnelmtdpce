package bf.personnel.com.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;

/**
 * A Categorie.
 */
@Entity
@Table(name = "categorie")
@SuppressWarnings("common-java:DuplicatedBlocks")
public class Categorie implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @Column(name = "libelle")
    private String libelle;

    @OneToMany(mappedBy = "categorie")
    @JsonIgnoreProperties(value = { "categorie", "employe" }, allowSetters = true)
    private Set<AvancementCategorie> avancementCategories = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Categorie id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLibelle() {
        return this.libelle;
    }

    public Categorie libelle(String libelle) {
        this.setLibelle(libelle);
        return this;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public Set<AvancementCategorie> getAvancementCategories() {
        return this.avancementCategories;
    }

    public void setAvancementCategories(Set<AvancementCategorie> avancementCategories) {
        if (this.avancementCategories != null) {
            this.avancementCategories.forEach(i -> i.setCategorie(null));
        }
        if (avancementCategories != null) {
            avancementCategories.forEach(i -> i.setCategorie(this));
        }
        this.avancementCategories = avancementCategories;
    }

    public Categorie avancementCategories(Set<AvancementCategorie> avancementCategories) {
        this.setAvancementCategories(avancementCategories);
        return this;
    }

    public Categorie addAvancementCategorie(AvancementCategorie avancementCategorie) {
        this.avancementCategories.add(avancementCategorie);
        avancementCategorie.setCategorie(this);
        return this;
    }

    public Categorie removeAvancementCategorie(AvancementCategorie avancementCategorie) {
        this.avancementCategories.remove(avancementCategorie);
        avancementCategorie.setCategorie(null);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Categorie)) {
            return false;
        }
        return id != null && id.equals(((Categorie) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Categorie{" +
            "id=" + getId() +
            ", libelle='" + getLibelle() + "'" +
            "}";
    }
}

package bf.personnel.com.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.time.LocalDate;
import javax.persistence.*;

/**
 * A AvancementClasse.
 */
@Entity
@Table(name = "avancement_classe")
@SuppressWarnings("common-java:DuplicatedBlocks")
public class AvancementClasse implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @Column(name = "date_avancement")
    private LocalDate dateAvancement;

    @ManyToOne
    @JsonIgnoreProperties(value = { "avancementClasses" }, allowSetters = true)
    private Classe classe;

    @ManyToOne
    @JsonIgnoreProperties(
        value = {
            "positionIrregulieres",
            "stages",
            "entrees",
            "sorties",
            "changementEmplois",
            "affectations",
            "avancementCategories",
            "avancementClasses",
            "avancementEchelons",
            "decorations",
            "natureRecrutement",
            "statut",
            "indemnites",
        },
        allowSetters = true
    )
    private Employe employe;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public AvancementClasse id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getDateAvancement() {
        return this.dateAvancement;
    }

    public AvancementClasse dateAvancement(LocalDate dateAvancement) {
        this.setDateAvancement(dateAvancement);
        return this;
    }

    public void setDateAvancement(LocalDate dateAvancement) {
        this.dateAvancement = dateAvancement;
    }

    public Classe getClasse() {
        return this.classe;
    }

    public void setClasse(Classe classe) {
        this.classe = classe;
    }

    public AvancementClasse classe(Classe classe) {
        this.setClasse(classe);
        return this;
    }

    public Employe getEmploye() {
        return this.employe;
    }

    public void setEmploye(Employe employe) {
        this.employe = employe;
    }

    public AvancementClasse employe(Employe employe) {
        this.setEmploye(employe);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof AvancementClasse)) {
            return false;
        }
        return id != null && id.equals(((AvancementClasse) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "AvancementClasse{" +
            "id=" + getId() +
            ", dateAvancement='" + getDateAvancement() + "'" +
            "}";
    }
}

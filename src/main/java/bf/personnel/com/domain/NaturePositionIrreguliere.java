package bf.personnel.com.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;

/**
 * A NaturePositionIrreguliere.
 */
@Entity
@Table(name = "nature_position_irreguliere")
@SuppressWarnings("common-java:DuplicatedBlocks")
public class NaturePositionIrreguliere implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @Column(name = "nature_position_irreguliere")
    private String naturePositionIrreguliere;

    @OneToMany(mappedBy = "naturePositionIrreguliere")
    @JsonIgnoreProperties(value = { "naturePositionIrreguliere", "employe" }, allowSetters = true)
    private Set<PositionIrreguliere> positionIrregulieres = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public NaturePositionIrreguliere id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNaturePositionIrreguliere() {
        return this.naturePositionIrreguliere;
    }

    public NaturePositionIrreguliere naturePositionIrreguliere(String naturePositionIrreguliere) {
        this.setNaturePositionIrreguliere(naturePositionIrreguliere);
        return this;
    }

    public void setNaturePositionIrreguliere(String naturePositionIrreguliere) {
        this.naturePositionIrreguliere = naturePositionIrreguliere;
    }

    public Set<PositionIrreguliere> getPositionIrregulieres() {
        return this.positionIrregulieres;
    }

    public void setPositionIrregulieres(Set<PositionIrreguliere> positionIrregulieres) {
        if (this.positionIrregulieres != null) {
            this.positionIrregulieres.forEach(i -> i.setNaturePositionIrreguliere(null));
        }
        if (positionIrregulieres != null) {
            positionIrregulieres.forEach(i -> i.setNaturePositionIrreguliere(this));
        }
        this.positionIrregulieres = positionIrregulieres;
    }

    public NaturePositionIrreguliere positionIrregulieres(Set<PositionIrreguliere> positionIrregulieres) {
        this.setPositionIrregulieres(positionIrregulieres);
        return this;
    }

    public NaturePositionIrreguliere addPositionIrreguliere(PositionIrreguliere positionIrreguliere) {
        this.positionIrregulieres.add(positionIrreguliere);
        positionIrreguliere.setNaturePositionIrreguliere(this);
        return this;
    }

    public NaturePositionIrreguliere removePositionIrreguliere(PositionIrreguliere positionIrreguliere) {
        this.positionIrregulieres.remove(positionIrreguliere);
        positionIrreguliere.setNaturePositionIrreguliere(null);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof NaturePositionIrreguliere)) {
            return false;
        }
        return id != null && id.equals(((NaturePositionIrreguliere) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "NaturePositionIrreguliere{" +
            "id=" + getId() +
            ", naturePositionIrreguliere='" + getNaturePositionIrreguliere() + "'" +
            "}";
    }
}

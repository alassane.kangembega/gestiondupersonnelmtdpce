package bf.personnel.com.service;

import bf.personnel.com.domain.NatureEntree;
import bf.personnel.com.repository.NatureEntreeRepository;
import bf.personnel.com.service.dto.NatureEntreeDTO;
import bf.personnel.com.service.mapper.NatureEntreeMapper;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link NatureEntree}.
 */
@Service
@Transactional
public class NatureEntreeService {

    private final Logger log = LoggerFactory.getLogger(NatureEntreeService.class);

    private final NatureEntreeRepository natureEntreeRepository;

    private final NatureEntreeMapper natureEntreeMapper;

    public NatureEntreeService(NatureEntreeRepository natureEntreeRepository, NatureEntreeMapper natureEntreeMapper) {
        this.natureEntreeRepository = natureEntreeRepository;
        this.natureEntreeMapper = natureEntreeMapper;
    }

    /**
     * Save a natureEntree.
     *
     * @param natureEntreeDTO the entity to save.
     * @return the persisted entity.
     */
    public NatureEntreeDTO save(NatureEntreeDTO natureEntreeDTO) {
        log.debug("Request to save NatureEntree : {}", natureEntreeDTO);
        NatureEntree natureEntree = natureEntreeMapper.toEntity(natureEntreeDTO);
        natureEntree = natureEntreeRepository.save(natureEntree);
        return natureEntreeMapper.toDto(natureEntree);
    }

    /**
     * Update a natureEntree.
     *
     * @param natureEntreeDTO the entity to save.
     * @return the persisted entity.
     */
    public NatureEntreeDTO update(NatureEntreeDTO natureEntreeDTO) {
        log.debug("Request to update NatureEntree : {}", natureEntreeDTO);
        NatureEntree natureEntree = natureEntreeMapper.toEntity(natureEntreeDTO);
        natureEntree = natureEntreeRepository.save(natureEntree);
        return natureEntreeMapper.toDto(natureEntree);
    }

    /**
     * Partially update a natureEntree.
     *
     * @param natureEntreeDTO the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<NatureEntreeDTO> partialUpdate(NatureEntreeDTO natureEntreeDTO) {
        log.debug("Request to partially update NatureEntree : {}", natureEntreeDTO);

        return natureEntreeRepository
            .findById(natureEntreeDTO.getId())
            .map(existingNatureEntree -> {
                natureEntreeMapper.partialUpdate(existingNatureEntree, natureEntreeDTO);

                return existingNatureEntree;
            })
            .map(natureEntreeRepository::save)
            .map(natureEntreeMapper::toDto);
    }

    /**
     * Get all the natureEntrees.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<NatureEntreeDTO> findAll() {
        log.debug("Request to get all NatureEntrees");
        return natureEntreeRepository.findAll().stream().map(natureEntreeMapper::toDto).collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * Get one natureEntree by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<NatureEntreeDTO> findOne(Long id) {
        log.debug("Request to get NatureEntree : {}", id);
        return natureEntreeRepository.findById(id).map(natureEntreeMapper::toDto);
    }

    /**
     * Delete the natureEntree by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete NatureEntree : {}", id);
        natureEntreeRepository.deleteById(id);
    }
}

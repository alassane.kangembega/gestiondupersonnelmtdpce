package bf.personnel.com.service;

import bf.personnel.com.domain.PositionIrreguliere;
import bf.personnel.com.repository.PositionIrreguliereRepository;
import bf.personnel.com.service.dto.PositionIrreguliereDTO;
import bf.personnel.com.service.mapper.PositionIrreguliereMapper;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link PositionIrreguliere}.
 */
@Service
@Transactional
public class PositionIrreguliereService {

    private final Logger log = LoggerFactory.getLogger(PositionIrreguliereService.class);

    private final PositionIrreguliereRepository positionIrreguliereRepository;

    private final PositionIrreguliereMapper positionIrreguliereMapper;

    public PositionIrreguliereService(
        PositionIrreguliereRepository positionIrreguliereRepository,
        PositionIrreguliereMapper positionIrreguliereMapper
    ) {
        this.positionIrreguliereRepository = positionIrreguliereRepository;
        this.positionIrreguliereMapper = positionIrreguliereMapper;
    }

    /**
     * Save a positionIrreguliere.
     *
     * @param positionIrreguliereDTO the entity to save.
     * @return the persisted entity.
     */
    public PositionIrreguliereDTO save(PositionIrreguliereDTO positionIrreguliereDTO) {
        log.debug("Request to save PositionIrreguliere : {}", positionIrreguliereDTO);
        PositionIrreguliere positionIrreguliere = positionIrreguliereMapper.toEntity(positionIrreguliereDTO);
        positionIrreguliere = positionIrreguliereRepository.save(positionIrreguliere);
        return positionIrreguliereMapper.toDto(positionIrreguliere);
    }

    /**
     * Update a positionIrreguliere.
     *
     * @param positionIrreguliereDTO the entity to save.
     * @return the persisted entity.
     */
    public PositionIrreguliereDTO update(PositionIrreguliereDTO positionIrreguliereDTO) {
        log.debug("Request to update PositionIrreguliere : {}", positionIrreguliereDTO);
        PositionIrreguliere positionIrreguliere = positionIrreguliereMapper.toEntity(positionIrreguliereDTO);
        positionIrreguliere = positionIrreguliereRepository.save(positionIrreguliere);
        return positionIrreguliereMapper.toDto(positionIrreguliere);
    }

    /**
     * Partially update a positionIrreguliere.
     *
     * @param positionIrreguliereDTO the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<PositionIrreguliereDTO> partialUpdate(PositionIrreguliereDTO positionIrreguliereDTO) {
        log.debug("Request to partially update PositionIrreguliere : {}", positionIrreguliereDTO);

        return positionIrreguliereRepository
            .findById(positionIrreguliereDTO.getId())
            .map(existingPositionIrreguliere -> {
                positionIrreguliereMapper.partialUpdate(existingPositionIrreguliere, positionIrreguliereDTO);

                return existingPositionIrreguliere;
            })
            .map(positionIrreguliereRepository::save)
            .map(positionIrreguliereMapper::toDto);
    }

    /**
     * Get all the positionIrregulieres.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<PositionIrreguliereDTO> findAll() {
        log.debug("Request to get all PositionIrregulieres");
        return positionIrreguliereRepository
            .findAll()
            .stream()
            .map(positionIrreguliereMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * Get one positionIrreguliere by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<PositionIrreguliereDTO> findOne(Long id) {
        log.debug("Request to get PositionIrreguliere : {}", id);
        return positionIrreguliereRepository.findById(id).map(positionIrreguliereMapper::toDto);
    }

    /**
     * Delete the positionIrreguliere by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete PositionIrreguliere : {}", id);
        positionIrreguliereRepository.deleteById(id);
    }
}

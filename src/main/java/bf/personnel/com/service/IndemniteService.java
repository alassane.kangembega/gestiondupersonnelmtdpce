package bf.personnel.com.service;

import bf.personnel.com.domain.Indemnite;
import bf.personnel.com.repository.IndemniteRepository;
import bf.personnel.com.service.dto.IndemniteDTO;
import bf.personnel.com.service.mapper.IndemniteMapper;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link Indemnite}.
 */
@Service
@Transactional
public class IndemniteService {

    private final Logger log = LoggerFactory.getLogger(IndemniteService.class);

    private final IndemniteRepository indemniteRepository;

    private final IndemniteMapper indemniteMapper;

    public IndemniteService(IndemniteRepository indemniteRepository, IndemniteMapper indemniteMapper) {
        this.indemniteRepository = indemniteRepository;
        this.indemniteMapper = indemniteMapper;
    }

    /**
     * Save a indemnite.
     *
     * @param indemniteDTO the entity to save.
     * @return the persisted entity.
     */
    public IndemniteDTO save(IndemniteDTO indemniteDTO) {
        log.debug("Request to save Indemnite : {}", indemniteDTO);
        Indemnite indemnite = indemniteMapper.toEntity(indemniteDTO);
        indemnite = indemniteRepository.save(indemnite);
        return indemniteMapper.toDto(indemnite);
    }

    /**
     * Update a indemnite.
     *
     * @param indemniteDTO the entity to save.
     * @return the persisted entity.
     */
    public IndemniteDTO update(IndemniteDTO indemniteDTO) {
        log.debug("Request to update Indemnite : {}", indemniteDTO);
        Indemnite indemnite = indemniteMapper.toEntity(indemniteDTO);
        indemnite = indemniteRepository.save(indemnite);
        return indemniteMapper.toDto(indemnite);
    }

    /**
     * Partially update a indemnite.
     *
     * @param indemniteDTO the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<IndemniteDTO> partialUpdate(IndemniteDTO indemniteDTO) {
        log.debug("Request to partially update Indemnite : {}", indemniteDTO);

        return indemniteRepository
            .findById(indemniteDTO.getId())
            .map(existingIndemnite -> {
                indemniteMapper.partialUpdate(existingIndemnite, indemniteDTO);

                return existingIndemnite;
            })
            .map(indemniteRepository::save)
            .map(indemniteMapper::toDto);
    }

    /**
     * Get all the indemnites.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<IndemniteDTO> findAll() {
        log.debug("Request to get all Indemnites");
        return indemniteRepository.findAll().stream().map(indemniteMapper::toDto).collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * Get one indemnite by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<IndemniteDTO> findOne(Long id) {
        log.debug("Request to get Indemnite : {}", id);
        return indemniteRepository.findById(id).map(indemniteMapper::toDto);
    }

    /**
     * Delete the indemnite by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Indemnite : {}", id);
        indemniteRepository.deleteById(id);
    }
}

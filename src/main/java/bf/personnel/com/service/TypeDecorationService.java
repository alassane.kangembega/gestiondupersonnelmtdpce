package bf.personnel.com.service;

import bf.personnel.com.domain.TypeDecoration;
import bf.personnel.com.repository.TypeDecorationRepository;
import bf.personnel.com.service.dto.TypeDecorationDTO;
import bf.personnel.com.service.mapper.TypeDecorationMapper;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link TypeDecoration}.
 */
@Service
@Transactional
public class TypeDecorationService {

    private final Logger log = LoggerFactory.getLogger(TypeDecorationService.class);

    private final TypeDecorationRepository typeDecorationRepository;

    private final TypeDecorationMapper typeDecorationMapper;

    public TypeDecorationService(TypeDecorationRepository typeDecorationRepository, TypeDecorationMapper typeDecorationMapper) {
        this.typeDecorationRepository = typeDecorationRepository;
        this.typeDecorationMapper = typeDecorationMapper;
    }

    /**
     * Save a typeDecoration.
     *
     * @param typeDecorationDTO the entity to save.
     * @return the persisted entity.
     */
    public TypeDecorationDTO save(TypeDecorationDTO typeDecorationDTO) {
        log.debug("Request to save TypeDecoration : {}", typeDecorationDTO);
        TypeDecoration typeDecoration = typeDecorationMapper.toEntity(typeDecorationDTO);
        typeDecoration = typeDecorationRepository.save(typeDecoration);
        return typeDecorationMapper.toDto(typeDecoration);
    }

    /**
     * Update a typeDecoration.
     *
     * @param typeDecorationDTO the entity to save.
     * @return the persisted entity.
     */
    public TypeDecorationDTO update(TypeDecorationDTO typeDecorationDTO) {
        log.debug("Request to update TypeDecoration : {}", typeDecorationDTO);
        TypeDecoration typeDecoration = typeDecorationMapper.toEntity(typeDecorationDTO);
        typeDecoration = typeDecorationRepository.save(typeDecoration);
        return typeDecorationMapper.toDto(typeDecoration);
    }

    /**
     * Partially update a typeDecoration.
     *
     * @param typeDecorationDTO the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<TypeDecorationDTO> partialUpdate(TypeDecorationDTO typeDecorationDTO) {
        log.debug("Request to partially update TypeDecoration : {}", typeDecorationDTO);

        return typeDecorationRepository
            .findById(typeDecorationDTO.getId())
            .map(existingTypeDecoration -> {
                typeDecorationMapper.partialUpdate(existingTypeDecoration, typeDecorationDTO);

                return existingTypeDecoration;
            })
            .map(typeDecorationRepository::save)
            .map(typeDecorationMapper::toDto);
    }

    /**
     * Get all the typeDecorations.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<TypeDecorationDTO> findAll() {
        log.debug("Request to get all TypeDecorations");
        return typeDecorationRepository
            .findAll()
            .stream()
            .map(typeDecorationMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * Get one typeDecoration by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<TypeDecorationDTO> findOne(Long id) {
        log.debug("Request to get TypeDecoration : {}", id);
        return typeDecorationRepository.findById(id).map(typeDecorationMapper::toDto);
    }

    /**
     * Delete the typeDecoration by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete TypeDecoration : {}", id);
        typeDecorationRepository.deleteById(id);
    }
}

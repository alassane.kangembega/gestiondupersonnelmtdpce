package bf.personnel.com.service;

import bf.personnel.com.domain.Echelon;
import bf.personnel.com.repository.EchelonRepository;
import bf.personnel.com.service.dto.EchelonDTO;
import bf.personnel.com.service.mapper.EchelonMapper;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link Echelon}.
 */
@Service
@Transactional
public class EchelonService {

    private final Logger log = LoggerFactory.getLogger(EchelonService.class);

    private final EchelonRepository echelonRepository;

    private final EchelonMapper echelonMapper;

    public EchelonService(EchelonRepository echelonRepository, EchelonMapper echelonMapper) {
        this.echelonRepository = echelonRepository;
        this.echelonMapper = echelonMapper;
    }

    /**
     * Save a echelon.
     *
     * @param echelonDTO the entity to save.
     * @return the persisted entity.
     */
    public EchelonDTO save(EchelonDTO echelonDTO) {
        log.debug("Request to save Echelon : {}", echelonDTO);
        Echelon echelon = echelonMapper.toEntity(echelonDTO);
        echelon = echelonRepository.save(echelon);
        return echelonMapper.toDto(echelon);
    }

    /**
     * Update a echelon.
     *
     * @param echelonDTO the entity to save.
     * @return the persisted entity.
     */
    public EchelonDTO update(EchelonDTO echelonDTO) {
        log.debug("Request to update Echelon : {}", echelonDTO);
        Echelon echelon = echelonMapper.toEntity(echelonDTO);
        echelon = echelonRepository.save(echelon);
        return echelonMapper.toDto(echelon);
    }

    /**
     * Partially update a echelon.
     *
     * @param echelonDTO the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<EchelonDTO> partialUpdate(EchelonDTO echelonDTO) {
        log.debug("Request to partially update Echelon : {}", echelonDTO);

        return echelonRepository
            .findById(echelonDTO.getId())
            .map(existingEchelon -> {
                echelonMapper.partialUpdate(existingEchelon, echelonDTO);

                return existingEchelon;
            })
            .map(echelonRepository::save)
            .map(echelonMapper::toDto);
    }

    /**
     * Get all the echelons.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<EchelonDTO> findAll() {
        log.debug("Request to get all Echelons");
        return echelonRepository.findAll().stream().map(echelonMapper::toDto).collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * Get one echelon by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<EchelonDTO> findOne(Long id) {
        log.debug("Request to get Echelon : {}", id);
        return echelonRepository.findById(id).map(echelonMapper::toDto);
    }

    /**
     * Delete the echelon by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Echelon : {}", id);
        echelonRepository.deleteById(id);
    }
}

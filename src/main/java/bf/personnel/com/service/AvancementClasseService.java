package bf.personnel.com.service;

import bf.personnel.com.domain.AvancementClasse;
import bf.personnel.com.repository.AvancementClasseRepository;
import bf.personnel.com.service.dto.AvancementClasseDTO;
import bf.personnel.com.service.mapper.AvancementClasseMapper;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link AvancementClasse}.
 */
@Service
@Transactional
public class AvancementClasseService {

    private final Logger log = LoggerFactory.getLogger(AvancementClasseService.class);

    private final AvancementClasseRepository avancementClasseRepository;

    private final AvancementClasseMapper avancementClasseMapper;

    public AvancementClasseService(AvancementClasseRepository avancementClasseRepository, AvancementClasseMapper avancementClasseMapper) {
        this.avancementClasseRepository = avancementClasseRepository;
        this.avancementClasseMapper = avancementClasseMapper;
    }

    /**
     * Save a avancementClasse.
     *
     * @param avancementClasseDTO the entity to save.
     * @return the persisted entity.
     */
    public AvancementClasseDTO save(AvancementClasseDTO avancementClasseDTO) {
        log.debug("Request to save AvancementClasse : {}", avancementClasseDTO);
        AvancementClasse avancementClasse = avancementClasseMapper.toEntity(avancementClasseDTO);
        avancementClasse = avancementClasseRepository.save(avancementClasse);
        return avancementClasseMapper.toDto(avancementClasse);
    }

    /**
     * Update a avancementClasse.
     *
     * @param avancementClasseDTO the entity to save.
     * @return the persisted entity.
     */
    public AvancementClasseDTO update(AvancementClasseDTO avancementClasseDTO) {
        log.debug("Request to update AvancementClasse : {}", avancementClasseDTO);
        AvancementClasse avancementClasse = avancementClasseMapper.toEntity(avancementClasseDTO);
        avancementClasse = avancementClasseRepository.save(avancementClasse);
        return avancementClasseMapper.toDto(avancementClasse);
    }

    /**
     * Partially update a avancementClasse.
     *
     * @param avancementClasseDTO the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<AvancementClasseDTO> partialUpdate(AvancementClasseDTO avancementClasseDTO) {
        log.debug("Request to partially update AvancementClasse : {}", avancementClasseDTO);

        return avancementClasseRepository
            .findById(avancementClasseDTO.getId())
            .map(existingAvancementClasse -> {
                avancementClasseMapper.partialUpdate(existingAvancementClasse, avancementClasseDTO);

                return existingAvancementClasse;
            })
            .map(avancementClasseRepository::save)
            .map(avancementClasseMapper::toDto);
    }

    /**
     * Get all the avancementClasses.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<AvancementClasseDTO> findAll() {
        log.debug("Request to get all AvancementClasses");
        return avancementClasseRepository
            .findAll()
            .stream()
            .map(avancementClasseMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * Get one avancementClasse by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<AvancementClasseDTO> findOne(Long id) {
        log.debug("Request to get AvancementClasse : {}", id);
        return avancementClasseRepository.findById(id).map(avancementClasseMapper::toDto);
    }

    /**
     * Delete the avancementClasse by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete AvancementClasse : {}", id);
        avancementClasseRepository.deleteById(id);
    }
}

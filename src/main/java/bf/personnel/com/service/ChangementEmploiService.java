package bf.personnel.com.service;

import bf.personnel.com.domain.ChangementEmploi;
import bf.personnel.com.repository.ChangementEmploiRepository;
import bf.personnel.com.service.dto.ChangementEmploiDTO;
import bf.personnel.com.service.mapper.ChangementEmploiMapper;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link ChangementEmploi}.
 */
@Service
@Transactional
public class ChangementEmploiService {

    private final Logger log = LoggerFactory.getLogger(ChangementEmploiService.class);

    private final ChangementEmploiRepository changementEmploiRepository;

    private final ChangementEmploiMapper changementEmploiMapper;

    public ChangementEmploiService(ChangementEmploiRepository changementEmploiRepository, ChangementEmploiMapper changementEmploiMapper) {
        this.changementEmploiRepository = changementEmploiRepository;
        this.changementEmploiMapper = changementEmploiMapper;
    }

    /**
     * Save a changementEmploi.
     *
     * @param changementEmploiDTO the entity to save.
     * @return the persisted entity.
     */
    public ChangementEmploiDTO save(ChangementEmploiDTO changementEmploiDTO) {
        log.debug("Request to save ChangementEmploi : {}", changementEmploiDTO);
        ChangementEmploi changementEmploi = changementEmploiMapper.toEntity(changementEmploiDTO);
        changementEmploi = changementEmploiRepository.save(changementEmploi);
        return changementEmploiMapper.toDto(changementEmploi);
    }

    /**
     * Update a changementEmploi.
     *
     * @param changementEmploiDTO the entity to save.
     * @return the persisted entity.
     */
    public ChangementEmploiDTO update(ChangementEmploiDTO changementEmploiDTO) {
        log.debug("Request to update ChangementEmploi : {}", changementEmploiDTO);
        ChangementEmploi changementEmploi = changementEmploiMapper.toEntity(changementEmploiDTO);
        changementEmploi = changementEmploiRepository.save(changementEmploi);
        return changementEmploiMapper.toDto(changementEmploi);
    }

    /**
     * Partially update a changementEmploi.
     *
     * @param changementEmploiDTO the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<ChangementEmploiDTO> partialUpdate(ChangementEmploiDTO changementEmploiDTO) {
        log.debug("Request to partially update ChangementEmploi : {}", changementEmploiDTO);

        return changementEmploiRepository
            .findById(changementEmploiDTO.getId())
            .map(existingChangementEmploi -> {
                changementEmploiMapper.partialUpdate(existingChangementEmploi, changementEmploiDTO);

                return existingChangementEmploi;
            })
            .map(changementEmploiRepository::save)
            .map(changementEmploiMapper::toDto);
    }

    /**
     * Get all the changementEmplois.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<ChangementEmploiDTO> findAll() {
        log.debug("Request to get all ChangementEmplois");
        return changementEmploiRepository
            .findAll()
            .stream()
            .map(changementEmploiMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * Get one changementEmploi by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<ChangementEmploiDTO> findOne(Long id) {
        log.debug("Request to get ChangementEmploi : {}", id);
        return changementEmploiRepository.findById(id).map(changementEmploiMapper::toDto);
    }

    /**
     * Delete the changementEmploi by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete ChangementEmploi : {}", id);
        changementEmploiRepository.deleteById(id);
    }
}

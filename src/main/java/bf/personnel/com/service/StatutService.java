package bf.personnel.com.service;

import bf.personnel.com.domain.Statut;
import bf.personnel.com.repository.StatutRepository;
import bf.personnel.com.service.dto.StatutDTO;
import bf.personnel.com.service.mapper.StatutMapper;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link Statut}.
 */
@Service
@Transactional
public class StatutService {

    private final Logger log = LoggerFactory.getLogger(StatutService.class);

    private final StatutRepository statutRepository;

    private final StatutMapper statutMapper;

    public StatutService(StatutRepository statutRepository, StatutMapper statutMapper) {
        this.statutRepository = statutRepository;
        this.statutMapper = statutMapper;
    }

    /**
     * Save a statut.
     *
     * @param statutDTO the entity to save.
     * @return the persisted entity.
     */
    public StatutDTO save(StatutDTO statutDTO) {
        log.debug("Request to save Statut : {}", statutDTO);
        Statut statut = statutMapper.toEntity(statutDTO);
        statut = statutRepository.save(statut);
        return statutMapper.toDto(statut);
    }

    /**
     * Update a statut.
     *
     * @param statutDTO the entity to save.
     * @return the persisted entity.
     */
    public StatutDTO update(StatutDTO statutDTO) {
        log.debug("Request to update Statut : {}", statutDTO);
        Statut statut = statutMapper.toEntity(statutDTO);
        statut = statutRepository.save(statut);
        return statutMapper.toDto(statut);
    }

    /**
     * Partially update a statut.
     *
     * @param statutDTO the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<StatutDTO> partialUpdate(StatutDTO statutDTO) {
        log.debug("Request to partially update Statut : {}", statutDTO);

        return statutRepository
            .findById(statutDTO.getId())
            .map(existingStatut -> {
                statutMapper.partialUpdate(existingStatut, statutDTO);

                return existingStatut;
            })
            .map(statutRepository::save)
            .map(statutMapper::toDto);
    }

    /**
     * Get all the statuts.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<StatutDTO> findAll() {
        log.debug("Request to get all Statuts");
        return statutRepository.findAll().stream().map(statutMapper::toDto).collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * Get one statut by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<StatutDTO> findOne(Long id) {
        log.debug("Request to get Statut : {}", id);
        return statutRepository.findById(id).map(statutMapper::toDto);
    }

    /**
     * Delete the statut by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Statut : {}", id);
        statutRepository.deleteById(id);
    }
}

package bf.personnel.com.service.dto;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A DTO for the {@link bf.personnel.com.domain.AvancementClasse} entity.
 */
@SuppressWarnings("common-java:DuplicatedBlocks")
public class AvancementClasseDTO implements Serializable {

    private Long id;

    private LocalDate dateAvancement;

    private ClasseDTO classe;

    private EmployeDTO employe;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getDateAvancement() {
        return dateAvancement;
    }

    public void setDateAvancement(LocalDate dateAvancement) {
        this.dateAvancement = dateAvancement;
    }

    public ClasseDTO getClasse() {
        return classe;
    }

    public void setClasse(ClasseDTO classe) {
        this.classe = classe;
    }

    public EmployeDTO getEmploye() {
        return employe;
    }

    public void setEmploye(EmployeDTO employe) {
        this.employe = employe;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof AvancementClasseDTO)) {
            return false;
        }

        AvancementClasseDTO avancementClasseDTO = (AvancementClasseDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, avancementClasseDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "AvancementClasseDTO{" +
            "id=" + getId() +
            ", dateAvancement='" + getDateAvancement() + "'" +
            ", classe=" + getClasse() +
            ", employe=" + getEmploye() +
            "}";
    }
}

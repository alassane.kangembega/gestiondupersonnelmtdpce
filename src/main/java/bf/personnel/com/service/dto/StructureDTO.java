package bf.personnel.com.service.dto;

import bf.personnel.com.domain.enumeration.TypeStructure;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link bf.personnel.com.domain.Structure} entity.
 */
@SuppressWarnings("common-java:DuplicatedBlocks")
public class StructureDTO implements Serializable {

    private Long id;

    private TypeStructure type;

    private String sigle;

    private String libelle;

    private StructureDTO parentStructure;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public TypeStructure getType() {
        return type;
    }

    public void setType(TypeStructure type) {
        this.type = type;
    }

    public String getSigle() {
        return sigle;
    }

    public void setSigle(String sigle) {
        this.sigle = sigle;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public StructureDTO getParentStructure() {
        return parentStructure;
    }

    public void setParentStructure(StructureDTO parentStructure) {
        this.parentStructure = parentStructure;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof StructureDTO)) {
            return false;
        }

        StructureDTO structureDTO = (StructureDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, structureDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "StructureDTO{" +
            "id=" + getId() +
            ", type='" + getType() + "'" +
            ", sigle='" + getSigle() + "'" +
            ", libelle='" + getLibelle() + "'" +
            ", parentStructure=" + getParentStructure() +
            "}";
    }
}

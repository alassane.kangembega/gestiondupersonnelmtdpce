package bf.personnel.com.service.dto;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A DTO for the {@link bf.personnel.com.domain.AvancementEchelon} entity.
 */
@SuppressWarnings("common-java:DuplicatedBlocks")
public class AvancementEchelonDTO implements Serializable {

    private Long id;

    private LocalDate dateAvancement;

    private EchelonDTO echelon;

    private EmployeDTO employe;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getDateAvancement() {
        return dateAvancement;
    }

    public void setDateAvancement(LocalDate dateAvancement) {
        this.dateAvancement = dateAvancement;
    }

    public EchelonDTO getEchelon() {
        return echelon;
    }

    public void setEchelon(EchelonDTO echelon) {
        this.echelon = echelon;
    }

    public EmployeDTO getEmploye() {
        return employe;
    }

    public void setEmploye(EmployeDTO employe) {
        this.employe = employe;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof AvancementEchelonDTO)) {
            return false;
        }

        AvancementEchelonDTO avancementEchelonDTO = (AvancementEchelonDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, avancementEchelonDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "AvancementEchelonDTO{" +
            "id=" + getId() +
            ", dateAvancement='" + getDateAvancement() + "'" +
            ", echelon=" + getEchelon() +
            ", employe=" + getEmploye() +
            "}";
    }
}

package bf.personnel.com.service.dto;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link bf.personnel.com.domain.Echelon} entity.
 */
@SuppressWarnings("common-java:DuplicatedBlocks")
public class EchelonDTO implements Serializable {

    private Long id;

    private Integer numero;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getNumero() {
        return numero;
    }

    public void setNumero(Integer numero) {
        this.numero = numero;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof EchelonDTO)) {
            return false;
        }

        EchelonDTO echelonDTO = (EchelonDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, echelonDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "EchelonDTO{" +
            "id=" + getId() +
            ", numero=" + getNumero() +
            "}";
    }
}

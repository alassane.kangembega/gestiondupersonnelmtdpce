package bf.personnel.com.service.dto;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link bf.personnel.com.domain.NatureEntree} entity.
 */
@SuppressWarnings("common-java:DuplicatedBlocks")
public class NatureEntreeDTO implements Serializable {

    private Long id;

    private String natureEntree;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNatureEntree() {
        return natureEntree;
    }

    public void setNatureEntree(String natureEntree) {
        this.natureEntree = natureEntree;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof NatureEntreeDTO)) {
            return false;
        }

        NatureEntreeDTO natureEntreeDTO = (NatureEntreeDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, natureEntreeDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "NatureEntreeDTO{" +
            "id=" + getId() +
            ", natureEntree='" + getNatureEntree() + "'" +
            "}";
    }
}

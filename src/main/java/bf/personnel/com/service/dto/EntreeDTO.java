package bf.personnel.com.service.dto;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A DTO for the {@link bf.personnel.com.domain.Entree} entity.
 */
@SuppressWarnings("common-java:DuplicatedBlocks")
public class EntreeDTO implements Serializable {

    private Long id;

    private LocalDate dateEntree;

    private EmployeDTO employe;

    private NatureEntreeDTO natureEntree;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getDateEntree() {
        return dateEntree;
    }

    public void setDateEntree(LocalDate dateEntree) {
        this.dateEntree = dateEntree;
    }

    public EmployeDTO getEmploye() {
        return employe;
    }

    public void setEmploye(EmployeDTO employe) {
        this.employe = employe;
    }

    public NatureEntreeDTO getNatureEntree() {
        return natureEntree;
    }

    public void setNatureEntree(NatureEntreeDTO natureEntree) {
        this.natureEntree = natureEntree;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof EntreeDTO)) {
            return false;
        }

        EntreeDTO entreeDTO = (EntreeDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, entreeDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "EntreeDTO{" +
            "id=" + getId() +
            ", dateEntree='" + getDateEntree() + "'" +
            ", employe=" + getEmploye() +
            ", natureEntree=" + getNatureEntree() +
            "}";
    }
}

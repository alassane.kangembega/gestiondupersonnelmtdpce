package bf.personnel.com.service.dto;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A DTO for the {@link bf.personnel.com.domain.PositionIrreguliere} entity.
 */
@SuppressWarnings("common-java:DuplicatedBlocks")
public class PositionIrreguliereDTO implements Serializable {

    private Long id;

    private LocalDate dateDebut;

    private LocalDate dateFin;

    private NaturePositionIrreguliereDTO naturePositionIrreguliere;

    private EmployeDTO employe;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getDateDebut() {
        return dateDebut;
    }

    public void setDateDebut(LocalDate dateDebut) {
        this.dateDebut = dateDebut;
    }

    public LocalDate getDateFin() {
        return dateFin;
    }

    public void setDateFin(LocalDate dateFin) {
        this.dateFin = dateFin;
    }

    public NaturePositionIrreguliereDTO getNaturePositionIrreguliere() {
        return naturePositionIrreguliere;
    }

    public void setNaturePositionIrreguliere(NaturePositionIrreguliereDTO naturePositionIrreguliere) {
        this.naturePositionIrreguliere = naturePositionIrreguliere;
    }

    public EmployeDTO getEmploye() {
        return employe;
    }

    public void setEmploye(EmployeDTO employe) {
        this.employe = employe;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof PositionIrreguliereDTO)) {
            return false;
        }

        PositionIrreguliereDTO positionIrreguliereDTO = (PositionIrreguliereDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, positionIrreguliereDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "PositionIrreguliereDTO{" +
            "id=" + getId() +
            ", dateDebut='" + getDateDebut() + "'" +
            ", dateFin='" + getDateFin() + "'" +
            ", naturePositionIrreguliere=" + getNaturePositionIrreguliere() +
            ", employe=" + getEmploye() +
            "}";
    }
}

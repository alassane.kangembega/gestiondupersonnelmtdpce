package bf.personnel.com.service.dto;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link bf.personnel.com.domain.NaturePositionIrreguliere} entity.
 */
@SuppressWarnings("common-java:DuplicatedBlocks")
public class NaturePositionIrreguliereDTO implements Serializable {

    private Long id;

    private String naturePositionIrreguliere;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNaturePositionIrreguliere() {
        return naturePositionIrreguliere;
    }

    public void setNaturePositionIrreguliere(String naturePositionIrreguliere) {
        this.naturePositionIrreguliere = naturePositionIrreguliere;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof NaturePositionIrreguliereDTO)) {
            return false;
        }

        NaturePositionIrreguliereDTO naturePositionIrreguliereDTO = (NaturePositionIrreguliereDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, naturePositionIrreguliereDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "NaturePositionIrreguliereDTO{" +
            "id=" + getId() +
            ", naturePositionIrreguliere='" + getNaturePositionIrreguliere() + "'" +
            "}";
    }
}

package bf.personnel.com.service.dto;

import bf.personnel.com.domain.enumeration.Sexe;
import bf.personnel.com.domain.enumeration.SituationMatrimoniale;
import bf.personnel.com.domain.enumeration.StatutAgentPublic;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A DTO for the {@link bf.personnel.com.domain.Employe} entity.
 */
@SuppressWarnings("common-java:DuplicatedBlocks")
public class EmployeDTO implements Serializable {

    private Long id;

    private String matricule;

    private String nom;

    private String prenom;

    private Sexe sexe;

    private LocalDate dateNaissance;

    private LocalDate dateIntegration;

    private SituationMatrimoniale situationMatrimoniale;

    private StatutAgentPublic statutAgentPublic;

    private Integer indice;

    private Float soldeIndiciaire;

    private Float salaireBase;

    private Integer nombreCharge;

    private LocalDate priseServicePosteActuel;

    private String telephone;

    private String email;

    private LocalDate dateProbableRetraite;

    private LocalDate dateAnniversaire;

    private String observation;

    private LocalDate dateRecrutement;

    private Float allocation;

    private NatureRecrutementDTO natureRecrutement;

    private StatutDTO statut;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMatricule() {
        return matricule;
    }

    public void setMatricule(String matricule) {
        this.matricule = matricule;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public Sexe getSexe() {
        return sexe;
    }

    public void setSexe(Sexe sexe) {
        this.sexe = sexe;
    }

    public LocalDate getDateNaissance() {
        return dateNaissance;
    }

    public void setDateNaissance(LocalDate dateNaissance) {
        this.dateNaissance = dateNaissance;
    }

    public LocalDate getDateIntegration() {
        return dateIntegration;
    }

    public void setDateIntegration(LocalDate dateIntegration) {
        this.dateIntegration = dateIntegration;
    }

    public SituationMatrimoniale getSituationMatrimoniale() {
        return situationMatrimoniale;
    }

    public void setSituationMatrimoniale(SituationMatrimoniale situationMatrimoniale) {
        this.situationMatrimoniale = situationMatrimoniale;
    }

    public StatutAgentPublic getStatutAgentPublic() {
        return statutAgentPublic;
    }

    public void setStatutAgentPublic(StatutAgentPublic statutAgentPublic) {
        this.statutAgentPublic = statutAgentPublic;
    }

    public Integer getIndice() {
        return indice;
    }

    public void setIndice(Integer indice) {
        this.indice = indice;
    }

    public Float getSoldeIndiciaire() {
        return soldeIndiciaire;
    }

    public void setSoldeIndiciaire(Float soldeIndiciaire) {
        this.soldeIndiciaire = soldeIndiciaire;
    }

    public Float getSalaireBase() {
        return salaireBase;
    }

    public void setSalaireBase(Float salaireBase) {
        this.salaireBase = salaireBase;
    }

    public Integer getNombreCharge() {
        return nombreCharge;
    }

    public void setNombreCharge(Integer nombreCharge) {
        this.nombreCharge = nombreCharge;
    }

    public LocalDate getPriseServicePosteActuel() {
        return priseServicePosteActuel;
    }

    public void setPriseServicePosteActuel(LocalDate priseServicePosteActuel) {
        this.priseServicePosteActuel = priseServicePosteActuel;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public LocalDate getDateProbableRetraite() {
        return dateProbableRetraite;
    }

    public void setDateProbableRetraite(LocalDate dateProbableRetraite) {
        this.dateProbableRetraite = dateProbableRetraite;
    }

    public LocalDate getDateAnniversaire() {
        return dateAnniversaire;
    }

    public void setDateAnniversaire(LocalDate dateAnniversaire) {
        this.dateAnniversaire = dateAnniversaire;
    }

    public String getObservation() {
        return observation;
    }

    public void setObservation(String observation) {
        this.observation = observation;
    }

    public LocalDate getDateRecrutement() {
        return dateRecrutement;
    }

    public void setDateRecrutement(LocalDate dateRecrutement) {
        this.dateRecrutement = dateRecrutement;
    }

    public Float getAllocation() {
        return allocation;
    }

    public void setAllocation(Float allocation) {
        this.allocation = allocation;
    }

    public NatureRecrutementDTO getNatureRecrutement() {
        return natureRecrutement;
    }

    public void setNatureRecrutement(NatureRecrutementDTO natureRecrutement) {
        this.natureRecrutement = natureRecrutement;
    }

    public StatutDTO getStatut() {
        return statut;
    }

    public void setStatut(StatutDTO statut) {
        this.statut = statut;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof EmployeDTO)) {
            return false;
        }

        EmployeDTO employeDTO = (EmployeDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, employeDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "EmployeDTO{" +
            "id=" + getId() +
            ", matricule='" + getMatricule() + "'" +
            ", nom='" + getNom() + "'" +
            ", prenom='" + getPrenom() + "'" +
            ", sexe='" + getSexe() + "'" +
            ", dateNaissance='" + getDateNaissance() + "'" +
            ", dateIntegration='" + getDateIntegration() + "'" +
            ", situationMatrimoniale='" + getSituationMatrimoniale() + "'" +
            ", statutAgentPublic='" + getStatutAgentPublic() + "'" +
            ", indice=" + getIndice() +
            ", soldeIndiciaire=" + getSoldeIndiciaire() +
            ", salaireBase=" + getSalaireBase() +
            ", nombreCharge=" + getNombreCharge() +
            ", priseServicePosteActuel='" + getPriseServicePosteActuel() + "'" +
            ", telephone='" + getTelephone() + "'" +
            ", email='" + getEmail() + "'" +
            ", dateProbableRetraite='" + getDateProbableRetraite() + "'" +
            ", dateAnniversaire='" + getDateAnniversaire() + "'" +
            ", observation='" + getObservation() + "'" +
            ", dateRecrutement='" + getDateRecrutement() + "'" +
            ", allocation=" + getAllocation() +
            ", natureRecrutement=" + getNatureRecrutement() +
            ", statut=" + getStatut() +
            "}";
    }
}

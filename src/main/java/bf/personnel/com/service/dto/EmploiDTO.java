package bf.personnel.com.service.dto;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link bf.personnel.com.domain.Emploi} entity.
 */
@SuppressWarnings("common-java:DuplicatedBlocks")
public class EmploiDTO implements Serializable {

    private Long id;

    private String libelle;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof EmploiDTO)) {
            return false;
        }

        EmploiDTO emploiDTO = (EmploiDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, emploiDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "EmploiDTO{" +
            "id=" + getId() +
            ", libelle='" + getLibelle() + "'" +
            "}";
    }
}

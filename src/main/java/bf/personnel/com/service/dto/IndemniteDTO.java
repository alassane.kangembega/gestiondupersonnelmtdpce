package bf.personnel.com.service.dto;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link bf.personnel.com.domain.Indemnite} entity.
 */
@SuppressWarnings("common-java:DuplicatedBlocks")
public class IndemniteDTO implements Serializable {

    private Long id;

    private String typeIndemnite;

    private Float montantIndemnite;

    private EmployeDTO employe;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTypeIndemnite() {
        return typeIndemnite;
    }

    public void setTypeIndemnite(String typeIndemnite) {
        this.typeIndemnite = typeIndemnite;
    }

    public Float getMontantIndemnite() {
        return montantIndemnite;
    }

    public void setMontantIndemnite(Float montantIndemnite) {
        this.montantIndemnite = montantIndemnite;
    }

    public EmployeDTO getEmploye() {
        return employe;
    }

    public void setEmploye(EmployeDTO employe) {
        this.employe = employe;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof IndemniteDTO)) {
            return false;
        }

        IndemniteDTO indemniteDTO = (IndemniteDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, indemniteDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "IndemniteDTO{" +
            "id=" + getId() +
            ", typeIndemnite='" + getTypeIndemnite() + "'" +
            ", montantIndemnite=" + getMontantIndemnite() +
            ", employe=" + getEmploye() +
            "}";
    }
}

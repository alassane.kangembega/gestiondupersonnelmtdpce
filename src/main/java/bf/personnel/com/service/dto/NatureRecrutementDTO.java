package bf.personnel.com.service.dto;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link bf.personnel.com.domain.NatureRecrutement} entity.
 */
@SuppressWarnings("common-java:DuplicatedBlocks")
public class NatureRecrutementDTO implements Serializable {

    private Long id;

    private String natureRecrutement;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNatureRecrutement() {
        return natureRecrutement;
    }

    public void setNatureRecrutement(String natureRecrutement) {
        this.natureRecrutement = natureRecrutement;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof NatureRecrutementDTO)) {
            return false;
        }

        NatureRecrutementDTO natureRecrutementDTO = (NatureRecrutementDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, natureRecrutementDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "NatureRecrutementDTO{" +
            "id=" + getId() +
            ", natureRecrutement='" + getNatureRecrutement() + "'" +
            "}";
    }
}

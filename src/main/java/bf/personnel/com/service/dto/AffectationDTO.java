package bf.personnel.com.service.dto;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A DTO for the {@link bf.personnel.com.domain.Affectation} entity.
 */
@SuppressWarnings("common-java:DuplicatedBlocks")
public class AffectationDTO implements Serializable {

    private Long id;

    private LocalDate dateAffectation;

    private RegionDTO region;

    private FonctionDTO fonction;

    private EmployeDTO employe;

    private StructureDTO structure;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getDateAffectation() {
        return dateAffectation;
    }

    public void setDateAffectation(LocalDate dateAffectation) {
        this.dateAffectation = dateAffectation;
    }

    public RegionDTO getRegion() {
        return region;
    }

    public void setRegion(RegionDTO region) {
        this.region = region;
    }

    public FonctionDTO getFonction() {
        return fonction;
    }

    public void setFonction(FonctionDTO fonction) {
        this.fonction = fonction;
    }

    public EmployeDTO getEmploye() {
        return employe;
    }

    public void setEmploye(EmployeDTO employe) {
        this.employe = employe;
    }

    public StructureDTO getStructure() {
        return structure;
    }

    public void setStructure(StructureDTO structure) {
        this.structure = structure;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof AffectationDTO)) {
            return false;
        }

        AffectationDTO affectationDTO = (AffectationDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, affectationDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "AffectationDTO{" +
            "id=" + getId() +
            ", dateAffectation='" + getDateAffectation() + "'" +
            ", region=" + getRegion() +
            ", fonction=" + getFonction() +
            ", employe=" + getEmploye() +
            ", structure=" + getStructure() +
            "}";
    }
}

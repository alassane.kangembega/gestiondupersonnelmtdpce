package bf.personnel.com.service.dto;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A DTO for the {@link bf.personnel.com.domain.ChangementEmploi} entity.
 */
@SuppressWarnings("common-java:DuplicatedBlocks")
public class ChangementEmploiDTO implements Serializable {

    private Long id;

    private LocalDate dateChangement;

    private EmploiDTO emploi;

    private EmployeDTO employe;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getDateChangement() {
        return dateChangement;
    }

    public void setDateChangement(LocalDate dateChangement) {
        this.dateChangement = dateChangement;
    }

    public EmploiDTO getEmploi() {
        return emploi;
    }

    public void setEmploi(EmploiDTO emploi) {
        this.emploi = emploi;
    }

    public EmployeDTO getEmploye() {
        return employe;
    }

    public void setEmploye(EmployeDTO employe) {
        this.employe = employe;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ChangementEmploiDTO)) {
            return false;
        }

        ChangementEmploiDTO changementEmploiDTO = (ChangementEmploiDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, changementEmploiDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ChangementEmploiDTO{" +
            "id=" + getId() +
            ", dateChangement='" + getDateChangement() + "'" +
            ", emploi=" + getEmploi() +
            ", employe=" + getEmploye() +
            "}";
    }
}

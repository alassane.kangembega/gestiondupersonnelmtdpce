package bf.personnel.com.service.dto;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A DTO for the {@link bf.personnel.com.domain.Sortie} entity.
 */
@SuppressWarnings("common-java:DuplicatedBlocks")
public class SortieDTO implements Serializable {

    private Long id;

    private LocalDate dateSortie;

    private NatureSortieDTO natureSortie;

    private EmployeDTO employe;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getDateSortie() {
        return dateSortie;
    }

    public void setDateSortie(LocalDate dateSortie) {
        this.dateSortie = dateSortie;
    }

    public NatureSortieDTO getNatureSortie() {
        return natureSortie;
    }

    public void setNatureSortie(NatureSortieDTO natureSortie) {
        this.natureSortie = natureSortie;
    }

    public EmployeDTO getEmploye() {
        return employe;
    }

    public void setEmploye(EmployeDTO employe) {
        this.employe = employe;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof SortieDTO)) {
            return false;
        }

        SortieDTO sortieDTO = (SortieDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, sortieDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "SortieDTO{" +
            "id=" + getId() +
            ", dateSortie='" + getDateSortie() + "'" +
            ", natureSortie=" + getNatureSortie() +
            ", employe=" + getEmploye() +
            "}";
    }
}

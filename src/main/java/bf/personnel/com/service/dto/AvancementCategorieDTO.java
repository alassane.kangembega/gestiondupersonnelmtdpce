package bf.personnel.com.service.dto;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A DTO for the {@link bf.personnel.com.domain.AvancementCategorie} entity.
 */
@SuppressWarnings("common-java:DuplicatedBlocks")
public class AvancementCategorieDTO implements Serializable {

    private Long id;

    private LocalDate dateAvancement;

    private CategorieDTO categorie;

    private EmployeDTO employe;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getDateAvancement() {
        return dateAvancement;
    }

    public void setDateAvancement(LocalDate dateAvancement) {
        this.dateAvancement = dateAvancement;
    }

    public CategorieDTO getCategorie() {
        return categorie;
    }

    public void setCategorie(CategorieDTO categorie) {
        this.categorie = categorie;
    }

    public EmployeDTO getEmploye() {
        return employe;
    }

    public void setEmploye(EmployeDTO employe) {
        this.employe = employe;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof AvancementCategorieDTO)) {
            return false;
        }

        AvancementCategorieDTO avancementCategorieDTO = (AvancementCategorieDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, avancementCategorieDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "AvancementCategorieDTO{" +
            "id=" + getId() +
            ", dateAvancement='" + getDateAvancement() + "'" +
            ", categorie=" + getCategorie() +
            ", employe=" + getEmploye() +
            "}";
    }
}

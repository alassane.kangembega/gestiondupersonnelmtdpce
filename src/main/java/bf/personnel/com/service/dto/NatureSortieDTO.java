package bf.personnel.com.service.dto;

import bf.personnel.com.domain.enumeration.TypeSortie;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link bf.personnel.com.domain.NatureSortie} entity.
 */
@SuppressWarnings("common-java:DuplicatedBlocks")
public class NatureSortieDTO implements Serializable {

    private Long id;

    private TypeSortie typeSortie;

    private String natureSortie;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public TypeSortie getTypeSortie() {
        return typeSortie;
    }

    public void setTypeSortie(TypeSortie typeSortie) {
        this.typeSortie = typeSortie;
    }

    public String getNatureSortie() {
        return natureSortie;
    }

    public void setNatureSortie(String natureSortie) {
        this.natureSortie = natureSortie;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof NatureSortieDTO)) {
            return false;
        }

        NatureSortieDTO natureSortieDTO = (NatureSortieDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, natureSortieDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "NatureSortieDTO{" +
            "id=" + getId() +
            ", typeSortie='" + getTypeSortie() + "'" +
            ", natureSortie='" + getNatureSortie() + "'" +
            "}";
    }
}

package bf.personnel.com.service.dto;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A DTO for the {@link bf.personnel.com.domain.Decoration} entity.
 */
@SuppressWarnings("common-java:DuplicatedBlocks")
public class DecorationDTO implements Serializable {

    private Long id;

    private LocalDate dateDecoration;

    private TypeDecorationDTO typeDecoration;

    private EmployeDTO employe;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getDateDecoration() {
        return dateDecoration;
    }

    public void setDateDecoration(LocalDate dateDecoration) {
        this.dateDecoration = dateDecoration;
    }

    public TypeDecorationDTO getTypeDecoration() {
        return typeDecoration;
    }

    public void setTypeDecoration(TypeDecorationDTO typeDecoration) {
        this.typeDecoration = typeDecoration;
    }

    public EmployeDTO getEmploye() {
        return employe;
    }

    public void setEmploye(EmployeDTO employe) {
        this.employe = employe;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof DecorationDTO)) {
            return false;
        }

        DecorationDTO decorationDTO = (DecorationDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, decorationDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "DecorationDTO{" +
            "id=" + getId() +
            ", dateDecoration='" + getDateDecoration() + "'" +
            ", typeDecoration=" + getTypeDecoration() +
            ", employe=" + getEmploye() +
            "}";
    }
}

package bf.personnel.com.service.dto;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link bf.personnel.com.domain.NatureStage} entity.
 */
@SuppressWarnings("common-java:DuplicatedBlocks")
public class NatureStageDTO implements Serializable {

    private Long id;

    private String natureStage;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNatureStage() {
        return natureStage;
    }

    public void setNatureStage(String natureStage) {
        this.natureStage = natureStage;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof NatureStageDTO)) {
            return false;
        }

        NatureStageDTO natureStageDTO = (NatureStageDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, natureStageDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "NatureStageDTO{" +
            "id=" + getId() +
            ", natureStage='" + getNatureStage() + "'" +
            "}";
    }
}

package bf.personnel.com.service;

import bf.personnel.com.domain.NatureSortie;
import bf.personnel.com.repository.NatureSortieRepository;
import bf.personnel.com.service.dto.NatureSortieDTO;
import bf.personnel.com.service.mapper.NatureSortieMapper;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link NatureSortie}.
 */
@Service
@Transactional
public class NatureSortieService {

    private final Logger log = LoggerFactory.getLogger(NatureSortieService.class);

    private final NatureSortieRepository natureSortieRepository;

    private final NatureSortieMapper natureSortieMapper;

    public NatureSortieService(NatureSortieRepository natureSortieRepository, NatureSortieMapper natureSortieMapper) {
        this.natureSortieRepository = natureSortieRepository;
        this.natureSortieMapper = natureSortieMapper;
    }

    /**
     * Save a natureSortie.
     *
     * @param natureSortieDTO the entity to save.
     * @return the persisted entity.
     */
    public NatureSortieDTO save(NatureSortieDTO natureSortieDTO) {
        log.debug("Request to save NatureSortie : {}", natureSortieDTO);
        NatureSortie natureSortie = natureSortieMapper.toEntity(natureSortieDTO);
        natureSortie = natureSortieRepository.save(natureSortie);
        return natureSortieMapper.toDto(natureSortie);
    }

    /**
     * Update a natureSortie.
     *
     * @param natureSortieDTO the entity to save.
     * @return the persisted entity.
     */
    public NatureSortieDTO update(NatureSortieDTO natureSortieDTO) {
        log.debug("Request to update NatureSortie : {}", natureSortieDTO);
        NatureSortie natureSortie = natureSortieMapper.toEntity(natureSortieDTO);
        natureSortie = natureSortieRepository.save(natureSortie);
        return natureSortieMapper.toDto(natureSortie);
    }

    /**
     * Partially update a natureSortie.
     *
     * @param natureSortieDTO the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<NatureSortieDTO> partialUpdate(NatureSortieDTO natureSortieDTO) {
        log.debug("Request to partially update NatureSortie : {}", natureSortieDTO);

        return natureSortieRepository
            .findById(natureSortieDTO.getId())
            .map(existingNatureSortie -> {
                natureSortieMapper.partialUpdate(existingNatureSortie, natureSortieDTO);

                return existingNatureSortie;
            })
            .map(natureSortieRepository::save)
            .map(natureSortieMapper::toDto);
    }

    /**
     * Get all the natureSorties.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<NatureSortieDTO> findAll() {
        log.debug("Request to get all NatureSorties");
        return natureSortieRepository.findAll().stream().map(natureSortieMapper::toDto).collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * Get one natureSortie by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<NatureSortieDTO> findOne(Long id) {
        log.debug("Request to get NatureSortie : {}", id);
        return natureSortieRepository.findById(id).map(natureSortieMapper::toDto);
    }

    /**
     * Delete the natureSortie by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete NatureSortie : {}", id);
        natureSortieRepository.deleteById(id);
    }
}

package bf.personnel.com.service;

import bf.personnel.com.domain.AvancementEchelon;
import bf.personnel.com.repository.AvancementEchelonRepository;
import bf.personnel.com.service.dto.AvancementEchelonDTO;
import bf.personnel.com.service.mapper.AvancementEchelonMapper;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link AvancementEchelon}.
 */
@Service
@Transactional
public class AvancementEchelonService {

    private final Logger log = LoggerFactory.getLogger(AvancementEchelonService.class);

    private final AvancementEchelonRepository avancementEchelonRepository;

    private final AvancementEchelonMapper avancementEchelonMapper;

    public AvancementEchelonService(
        AvancementEchelonRepository avancementEchelonRepository,
        AvancementEchelonMapper avancementEchelonMapper
    ) {
        this.avancementEchelonRepository = avancementEchelonRepository;
        this.avancementEchelonMapper = avancementEchelonMapper;
    }

    /**
     * Save a avancementEchelon.
     *
     * @param avancementEchelonDTO the entity to save.
     * @return the persisted entity.
     */
    public AvancementEchelonDTO save(AvancementEchelonDTO avancementEchelonDTO) {
        log.debug("Request to save AvancementEchelon : {}", avancementEchelonDTO);
        AvancementEchelon avancementEchelon = avancementEchelonMapper.toEntity(avancementEchelonDTO);
        avancementEchelon = avancementEchelonRepository.save(avancementEchelon);
        return avancementEchelonMapper.toDto(avancementEchelon);
    }

    /**
     * Update a avancementEchelon.
     *
     * @param avancementEchelonDTO the entity to save.
     * @return the persisted entity.
     */
    public AvancementEchelonDTO update(AvancementEchelonDTO avancementEchelonDTO) {
        log.debug("Request to update AvancementEchelon : {}", avancementEchelonDTO);
        AvancementEchelon avancementEchelon = avancementEchelonMapper.toEntity(avancementEchelonDTO);
        avancementEchelon = avancementEchelonRepository.save(avancementEchelon);
        return avancementEchelonMapper.toDto(avancementEchelon);
    }

    /**
     * Partially update a avancementEchelon.
     *
     * @param avancementEchelonDTO the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<AvancementEchelonDTO> partialUpdate(AvancementEchelonDTO avancementEchelonDTO) {
        log.debug("Request to partially update AvancementEchelon : {}", avancementEchelonDTO);

        return avancementEchelonRepository
            .findById(avancementEchelonDTO.getId())
            .map(existingAvancementEchelon -> {
                avancementEchelonMapper.partialUpdate(existingAvancementEchelon, avancementEchelonDTO);

                return existingAvancementEchelon;
            })
            .map(avancementEchelonRepository::save)
            .map(avancementEchelonMapper::toDto);
    }

    /**
     * Get all the avancementEchelons.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<AvancementEchelonDTO> findAll() {
        log.debug("Request to get all AvancementEchelons");
        return avancementEchelonRepository
            .findAll()
            .stream()
            .map(avancementEchelonMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * Get one avancementEchelon by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<AvancementEchelonDTO> findOne(Long id) {
        log.debug("Request to get AvancementEchelon : {}", id);
        return avancementEchelonRepository.findById(id).map(avancementEchelonMapper::toDto);
    }

    /**
     * Delete the avancementEchelon by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete AvancementEchelon : {}", id);
        avancementEchelonRepository.deleteById(id);
    }
}

package bf.personnel.com.service;

import bf.personnel.com.domain.NatureRecrutement;
import bf.personnel.com.repository.NatureRecrutementRepository;
import bf.personnel.com.service.dto.NatureRecrutementDTO;
import bf.personnel.com.service.mapper.NatureRecrutementMapper;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link NatureRecrutement}.
 */
@Service
@Transactional
public class NatureRecrutementService {

    private final Logger log = LoggerFactory.getLogger(NatureRecrutementService.class);

    private final NatureRecrutementRepository natureRecrutementRepository;

    private final NatureRecrutementMapper natureRecrutementMapper;

    public NatureRecrutementService(
        NatureRecrutementRepository natureRecrutementRepository,
        NatureRecrutementMapper natureRecrutementMapper
    ) {
        this.natureRecrutementRepository = natureRecrutementRepository;
        this.natureRecrutementMapper = natureRecrutementMapper;
    }

    /**
     * Save a natureRecrutement.
     *
     * @param natureRecrutementDTO the entity to save.
     * @return the persisted entity.
     */
    public NatureRecrutementDTO save(NatureRecrutementDTO natureRecrutementDTO) {
        log.debug("Request to save NatureRecrutement : {}", natureRecrutementDTO);
        NatureRecrutement natureRecrutement = natureRecrutementMapper.toEntity(natureRecrutementDTO);
        natureRecrutement = natureRecrutementRepository.save(natureRecrutement);
        return natureRecrutementMapper.toDto(natureRecrutement);
    }

    /**
     * Update a natureRecrutement.
     *
     * @param natureRecrutementDTO the entity to save.
     * @return the persisted entity.
     */
    public NatureRecrutementDTO update(NatureRecrutementDTO natureRecrutementDTO) {
        log.debug("Request to update NatureRecrutement : {}", natureRecrutementDTO);
        NatureRecrutement natureRecrutement = natureRecrutementMapper.toEntity(natureRecrutementDTO);
        natureRecrutement = natureRecrutementRepository.save(natureRecrutement);
        return natureRecrutementMapper.toDto(natureRecrutement);
    }

    /**
     * Partially update a natureRecrutement.
     *
     * @param natureRecrutementDTO the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<NatureRecrutementDTO> partialUpdate(NatureRecrutementDTO natureRecrutementDTO) {
        log.debug("Request to partially update NatureRecrutement : {}", natureRecrutementDTO);

        return natureRecrutementRepository
            .findById(natureRecrutementDTO.getId())
            .map(existingNatureRecrutement -> {
                natureRecrutementMapper.partialUpdate(existingNatureRecrutement, natureRecrutementDTO);

                return existingNatureRecrutement;
            })
            .map(natureRecrutementRepository::save)
            .map(natureRecrutementMapper::toDto);
    }

    /**
     * Get all the natureRecrutements.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<NatureRecrutementDTO> findAll() {
        log.debug("Request to get all NatureRecrutements");
        return natureRecrutementRepository
            .findAll()
            .stream()
            .map(natureRecrutementMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * Get one natureRecrutement by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<NatureRecrutementDTO> findOne(Long id) {
        log.debug("Request to get NatureRecrutement : {}", id);
        return natureRecrutementRepository.findById(id).map(natureRecrutementMapper::toDto);
    }

    /**
     * Delete the natureRecrutement by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete NatureRecrutement : {}", id);
        natureRecrutementRepository.deleteById(id);
    }
}

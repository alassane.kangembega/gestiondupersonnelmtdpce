package bf.personnel.com.service;

import bf.personnel.com.domain.NatureStage;
import bf.personnel.com.repository.NatureStageRepository;
import bf.personnel.com.service.dto.NatureStageDTO;
import bf.personnel.com.service.mapper.NatureStageMapper;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link NatureStage}.
 */
@Service
@Transactional
public class NatureStageService {

    private final Logger log = LoggerFactory.getLogger(NatureStageService.class);

    private final NatureStageRepository natureStageRepository;

    private final NatureStageMapper natureStageMapper;

    public NatureStageService(NatureStageRepository natureStageRepository, NatureStageMapper natureStageMapper) {
        this.natureStageRepository = natureStageRepository;
        this.natureStageMapper = natureStageMapper;
    }

    /**
     * Save a natureStage.
     *
     * @param natureStageDTO the entity to save.
     * @return the persisted entity.
     */
    public NatureStageDTO save(NatureStageDTO natureStageDTO) {
        log.debug("Request to save NatureStage : {}", natureStageDTO);
        NatureStage natureStage = natureStageMapper.toEntity(natureStageDTO);
        natureStage = natureStageRepository.save(natureStage);
        return natureStageMapper.toDto(natureStage);
    }

    /**
     * Update a natureStage.
     *
     * @param natureStageDTO the entity to save.
     * @return the persisted entity.
     */
    public NatureStageDTO update(NatureStageDTO natureStageDTO) {
        log.debug("Request to update NatureStage : {}", natureStageDTO);
        NatureStage natureStage = natureStageMapper.toEntity(natureStageDTO);
        natureStage = natureStageRepository.save(natureStage);
        return natureStageMapper.toDto(natureStage);
    }

    /**
     * Partially update a natureStage.
     *
     * @param natureStageDTO the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<NatureStageDTO> partialUpdate(NatureStageDTO natureStageDTO) {
        log.debug("Request to partially update NatureStage : {}", natureStageDTO);

        return natureStageRepository
            .findById(natureStageDTO.getId())
            .map(existingNatureStage -> {
                natureStageMapper.partialUpdate(existingNatureStage, natureStageDTO);

                return existingNatureStage;
            })
            .map(natureStageRepository::save)
            .map(natureStageMapper::toDto);
    }

    /**
     * Get all the natureStages.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<NatureStageDTO> findAll() {
        log.debug("Request to get all NatureStages");
        return natureStageRepository.findAll().stream().map(natureStageMapper::toDto).collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * Get one natureStage by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<NatureStageDTO> findOne(Long id) {
        log.debug("Request to get NatureStage : {}", id);
        return natureStageRepository.findById(id).map(natureStageMapper::toDto);
    }

    /**
     * Delete the natureStage by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete NatureStage : {}", id);
        natureStageRepository.deleteById(id);
    }
}

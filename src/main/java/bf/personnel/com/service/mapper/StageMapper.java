package bf.personnel.com.service.mapper;

import bf.personnel.com.domain.Employe;
import bf.personnel.com.domain.NatureStage;
import bf.personnel.com.domain.Stage;
import bf.personnel.com.service.dto.EmployeDTO;
import bf.personnel.com.service.dto.NatureStageDTO;
import bf.personnel.com.service.dto.StageDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Stage} and its DTO {@link StageDTO}.
 */
@Mapper(componentModel = "spring")
public interface StageMapper extends EntityMapper<StageDTO, Stage> {
    @Mapping(target = "natureStage", source = "natureStage", qualifiedByName = "natureStageId")
    @Mapping(target = "employe", source = "employe", qualifiedByName = "employeId")
    StageDTO toDto(Stage s);

    @Named("natureStageId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    NatureStageDTO toDtoNatureStageId(NatureStage natureStage);

    @Named("employeId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    EmployeDTO toDtoEmployeId(Employe employe);
}

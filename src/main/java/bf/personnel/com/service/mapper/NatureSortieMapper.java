package bf.personnel.com.service.mapper;

import bf.personnel.com.domain.NatureSortie;
import bf.personnel.com.service.dto.NatureSortieDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link NatureSortie} and its DTO {@link NatureSortieDTO}.
 */
@Mapper(componentModel = "spring")
public interface NatureSortieMapper extends EntityMapper<NatureSortieDTO, NatureSortie> {}

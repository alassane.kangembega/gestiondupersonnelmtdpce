package bf.personnel.com.service.mapper;

import bf.personnel.com.domain.ChangementEmploi;
import bf.personnel.com.domain.Emploi;
import bf.personnel.com.domain.Employe;
import bf.personnel.com.service.dto.ChangementEmploiDTO;
import bf.personnel.com.service.dto.EmploiDTO;
import bf.personnel.com.service.dto.EmployeDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link ChangementEmploi} and its DTO {@link ChangementEmploiDTO}.
 */
@Mapper(componentModel = "spring")
public interface ChangementEmploiMapper extends EntityMapper<ChangementEmploiDTO, ChangementEmploi> {
    @Mapping(target = "emploi", source = "emploi", qualifiedByName = "emploiId")
    @Mapping(target = "employe", source = "employe", qualifiedByName = "employeId")
    ChangementEmploiDTO toDto(ChangementEmploi s);

    @Named("emploiId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    EmploiDTO toDtoEmploiId(Emploi emploi);

    @Named("employeId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    EmployeDTO toDtoEmployeId(Employe employe);
}

package bf.personnel.com.service.mapper;

import bf.personnel.com.domain.Emploi;
import bf.personnel.com.service.dto.EmploiDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Emploi} and its DTO {@link EmploiDTO}.
 */
@Mapper(componentModel = "spring")
public interface EmploiMapper extends EntityMapper<EmploiDTO, Emploi> {}

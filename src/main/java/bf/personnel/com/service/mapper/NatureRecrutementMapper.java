package bf.personnel.com.service.mapper;

import bf.personnel.com.domain.NatureRecrutement;
import bf.personnel.com.service.dto.NatureRecrutementDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link NatureRecrutement} and its DTO {@link NatureRecrutementDTO}.
 */
@Mapper(componentModel = "spring")
public interface NatureRecrutementMapper extends EntityMapper<NatureRecrutementDTO, NatureRecrutement> {}

package bf.personnel.com.service.mapper;

import bf.personnel.com.domain.Structure;
import bf.personnel.com.service.dto.StructureDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Structure} and its DTO {@link StructureDTO}.
 */
@Mapper(componentModel = "spring")
public interface StructureMapper extends EntityMapper<StructureDTO, Structure> {
    @Mapping(target = "parentStructure", source = "parentStructure", qualifiedByName = "structureId")
    StructureDTO toDto(Structure s);

    @Named("structureId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    StructureDTO toDtoStructureId(Structure structure);
}

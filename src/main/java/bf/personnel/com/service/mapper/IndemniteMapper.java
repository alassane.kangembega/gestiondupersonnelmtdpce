package bf.personnel.com.service.mapper;

import bf.personnel.com.domain.Employe;
import bf.personnel.com.domain.Indemnite;
import bf.personnel.com.service.dto.EmployeDTO;
import bf.personnel.com.service.dto.IndemniteDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Indemnite} and its DTO {@link IndemniteDTO}.
 */
@Mapper(componentModel = "spring")
public interface IndemniteMapper extends EntityMapper<IndemniteDTO, Indemnite> {
    @Mapping(target = "employe", source = "employe", qualifiedByName = "employeId")
    IndemniteDTO toDto(Indemnite s);

    @Named("employeId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    EmployeDTO toDtoEmployeId(Employe employe);
}

package bf.personnel.com.service.mapper;

import bf.personnel.com.domain.Statut;
import bf.personnel.com.service.dto.StatutDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Statut} and its DTO {@link StatutDTO}.
 */
@Mapper(componentModel = "spring")
public interface StatutMapper extends EntityMapper<StatutDTO, Statut> {}

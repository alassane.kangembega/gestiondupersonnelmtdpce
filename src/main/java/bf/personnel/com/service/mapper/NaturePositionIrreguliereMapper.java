package bf.personnel.com.service.mapper;

import bf.personnel.com.domain.NaturePositionIrreguliere;
import bf.personnel.com.service.dto.NaturePositionIrreguliereDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link NaturePositionIrreguliere} and its DTO {@link NaturePositionIrreguliereDTO}.
 */
@Mapper(componentModel = "spring")
public interface NaturePositionIrreguliereMapper extends EntityMapper<NaturePositionIrreguliereDTO, NaturePositionIrreguliere> {}

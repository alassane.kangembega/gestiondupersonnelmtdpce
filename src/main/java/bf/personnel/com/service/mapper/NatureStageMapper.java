package bf.personnel.com.service.mapper;

import bf.personnel.com.domain.NatureStage;
import bf.personnel.com.service.dto.NatureStageDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link NatureStage} and its DTO {@link NatureStageDTO}.
 */
@Mapper(componentModel = "spring")
public interface NatureStageMapper extends EntityMapper<NatureStageDTO, NatureStage> {}

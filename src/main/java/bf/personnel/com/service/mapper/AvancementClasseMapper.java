package bf.personnel.com.service.mapper;

import bf.personnel.com.domain.AvancementClasse;
import bf.personnel.com.domain.Classe;
import bf.personnel.com.domain.Employe;
import bf.personnel.com.service.dto.AvancementClasseDTO;
import bf.personnel.com.service.dto.ClasseDTO;
import bf.personnel.com.service.dto.EmployeDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link AvancementClasse} and its DTO {@link AvancementClasseDTO}.
 */
@Mapper(componentModel = "spring")
public interface AvancementClasseMapper extends EntityMapper<AvancementClasseDTO, AvancementClasse> {
    @Mapping(target = "classe", source = "classe", qualifiedByName = "classeId")
    @Mapping(target = "employe", source = "employe", qualifiedByName = "employeId")
    AvancementClasseDTO toDto(AvancementClasse s);

    @Named("classeId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    ClasseDTO toDtoClasseId(Classe classe);

    @Named("employeId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    EmployeDTO toDtoEmployeId(Employe employe);
}

package bf.personnel.com.service.mapper;

import bf.personnel.com.domain.Employe;
import bf.personnel.com.domain.NaturePositionIrreguliere;
import bf.personnel.com.domain.PositionIrreguliere;
import bf.personnel.com.service.dto.EmployeDTO;
import bf.personnel.com.service.dto.NaturePositionIrreguliereDTO;
import bf.personnel.com.service.dto.PositionIrreguliereDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link PositionIrreguliere} and its DTO {@link PositionIrreguliereDTO}.
 */
@Mapper(componentModel = "spring")
public interface PositionIrreguliereMapper extends EntityMapper<PositionIrreguliereDTO, PositionIrreguliere> {
    @Mapping(target = "naturePositionIrreguliere", source = "naturePositionIrreguliere", qualifiedByName = "naturePositionIrreguliereId")
    @Mapping(target = "employe", source = "employe", qualifiedByName = "employeId")
    PositionIrreguliereDTO toDto(PositionIrreguliere s);

    @Named("naturePositionIrreguliereId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    NaturePositionIrreguliereDTO toDtoNaturePositionIrreguliereId(NaturePositionIrreguliere naturePositionIrreguliere);

    @Named("employeId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    EmployeDTO toDtoEmployeId(Employe employe);
}

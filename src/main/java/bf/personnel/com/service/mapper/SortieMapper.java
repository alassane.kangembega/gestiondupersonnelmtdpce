package bf.personnel.com.service.mapper;

import bf.personnel.com.domain.Employe;
import bf.personnel.com.domain.NatureSortie;
import bf.personnel.com.domain.Sortie;
import bf.personnel.com.service.dto.EmployeDTO;
import bf.personnel.com.service.dto.NatureSortieDTO;
import bf.personnel.com.service.dto.SortieDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Sortie} and its DTO {@link SortieDTO}.
 */
@Mapper(componentModel = "spring")
public interface SortieMapper extends EntityMapper<SortieDTO, Sortie> {
    @Mapping(target = "natureSortie", source = "natureSortie", qualifiedByName = "natureSortieId")
    @Mapping(target = "employe", source = "employe", qualifiedByName = "employeId")
    SortieDTO toDto(Sortie s);

    @Named("natureSortieId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    NatureSortieDTO toDtoNatureSortieId(NatureSortie natureSortie);

    @Named("employeId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    EmployeDTO toDtoEmployeId(Employe employe);
}

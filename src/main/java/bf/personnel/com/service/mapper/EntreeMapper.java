package bf.personnel.com.service.mapper;

import bf.personnel.com.domain.Employe;
import bf.personnel.com.domain.Entree;
import bf.personnel.com.domain.NatureEntree;
import bf.personnel.com.service.dto.EmployeDTO;
import bf.personnel.com.service.dto.EntreeDTO;
import bf.personnel.com.service.dto.NatureEntreeDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Entree} and its DTO {@link EntreeDTO}.
 */
@Mapper(componentModel = "spring")
public interface EntreeMapper extends EntityMapper<EntreeDTO, Entree> {
    @Mapping(target = "employe", source = "employe", qualifiedByName = "employeId")
    @Mapping(target = "natureEntree", source = "natureEntree", qualifiedByName = "natureEntreeId")
    EntreeDTO toDto(Entree s);

    @Named("employeId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    EmployeDTO toDtoEmployeId(Employe employe);

    @Named("natureEntreeId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    NatureEntreeDTO toDtoNatureEntreeId(NatureEntree natureEntree);
}

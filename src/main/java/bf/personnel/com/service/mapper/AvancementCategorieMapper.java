package bf.personnel.com.service.mapper;

import bf.personnel.com.domain.AvancementCategorie;
import bf.personnel.com.domain.Categorie;
import bf.personnel.com.domain.Employe;
import bf.personnel.com.service.dto.AvancementCategorieDTO;
import bf.personnel.com.service.dto.CategorieDTO;
import bf.personnel.com.service.dto.EmployeDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link AvancementCategorie} and its DTO {@link AvancementCategorieDTO}.
 */
@Mapper(componentModel = "spring")
public interface AvancementCategorieMapper extends EntityMapper<AvancementCategorieDTO, AvancementCategorie> {
    @Mapping(target = "categorie", source = "categorie", qualifiedByName = "categorieId")
    @Mapping(target = "employe", source = "employe", qualifiedByName = "employeId")
    AvancementCategorieDTO toDto(AvancementCategorie s);

    @Named("categorieId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    CategorieDTO toDtoCategorieId(Categorie categorie);

    @Named("employeId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    EmployeDTO toDtoEmployeId(Employe employe);
}

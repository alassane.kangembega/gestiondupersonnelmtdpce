package bf.personnel.com.service.mapper;

import bf.personnel.com.domain.Echelon;
import bf.personnel.com.service.dto.EchelonDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Echelon} and its DTO {@link EchelonDTO}.
 */
@Mapper(componentModel = "spring")
public interface EchelonMapper extends EntityMapper<EchelonDTO, Echelon> {}

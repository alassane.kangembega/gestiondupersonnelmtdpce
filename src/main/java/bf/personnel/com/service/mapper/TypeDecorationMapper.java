package bf.personnel.com.service.mapper;

import bf.personnel.com.domain.TypeDecoration;
import bf.personnel.com.service.dto.TypeDecorationDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link TypeDecoration} and its DTO {@link TypeDecorationDTO}.
 */
@Mapper(componentModel = "spring")
public interface TypeDecorationMapper extends EntityMapper<TypeDecorationDTO, TypeDecoration> {}

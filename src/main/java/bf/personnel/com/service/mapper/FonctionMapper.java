package bf.personnel.com.service.mapper;

import bf.personnel.com.domain.Fonction;
import bf.personnel.com.service.dto.FonctionDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Fonction} and its DTO {@link FonctionDTO}.
 */
@Mapper(componentModel = "spring")
public interface FonctionMapper extends EntityMapper<FonctionDTO, Fonction> {}

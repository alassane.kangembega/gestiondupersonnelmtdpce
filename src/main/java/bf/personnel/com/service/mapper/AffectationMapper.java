package bf.personnel.com.service.mapper;

import bf.personnel.com.domain.Affectation;
import bf.personnel.com.domain.Employe;
import bf.personnel.com.domain.Fonction;
import bf.personnel.com.domain.Region;
import bf.personnel.com.domain.Structure;
import bf.personnel.com.service.dto.AffectationDTO;
import bf.personnel.com.service.dto.EmployeDTO;
import bf.personnel.com.service.dto.FonctionDTO;
import bf.personnel.com.service.dto.RegionDTO;
import bf.personnel.com.service.dto.StructureDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Affectation} and its DTO {@link AffectationDTO}.
 */
@Mapper(componentModel = "spring")
public interface AffectationMapper extends EntityMapper<AffectationDTO, Affectation> {
    @Mapping(target = "region", source = "region", qualifiedByName = "regionId")
    @Mapping(target = "fonction", source = "fonction", qualifiedByName = "fonctionId")
    @Mapping(target = "employe", source = "employe", qualifiedByName = "employeId")
    @Mapping(target = "structure", source = "structure", qualifiedByName = "structureId")
    AffectationDTO toDto(Affectation s);

    @Named("regionId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    RegionDTO toDtoRegionId(Region region);

    @Named("fonctionId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    FonctionDTO toDtoFonctionId(Fonction fonction);

    @Named("employeId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    EmployeDTO toDtoEmployeId(Employe employe);

    @Named("structureId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    StructureDTO toDtoStructureId(Structure structure);
}

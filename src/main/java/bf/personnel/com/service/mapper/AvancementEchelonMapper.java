package bf.personnel.com.service.mapper;

import bf.personnel.com.domain.AvancementEchelon;
import bf.personnel.com.domain.Echelon;
import bf.personnel.com.domain.Employe;
import bf.personnel.com.service.dto.AvancementEchelonDTO;
import bf.personnel.com.service.dto.EchelonDTO;
import bf.personnel.com.service.dto.EmployeDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link AvancementEchelon} and its DTO {@link AvancementEchelonDTO}.
 */
@Mapper(componentModel = "spring")
public interface AvancementEchelonMapper extends EntityMapper<AvancementEchelonDTO, AvancementEchelon> {
    @Mapping(target = "echelon", source = "echelon", qualifiedByName = "echelonId")
    @Mapping(target = "employe", source = "employe", qualifiedByName = "employeId")
    AvancementEchelonDTO toDto(AvancementEchelon s);

    @Named("echelonId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    EchelonDTO toDtoEchelonId(Echelon echelon);

    @Named("employeId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    EmployeDTO toDtoEmployeId(Employe employe);
}

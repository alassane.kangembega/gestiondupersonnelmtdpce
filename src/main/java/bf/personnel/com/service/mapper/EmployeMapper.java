package bf.personnel.com.service.mapper;

import bf.personnel.com.domain.Employe;
import bf.personnel.com.domain.NatureRecrutement;
import bf.personnel.com.domain.Statut;
import bf.personnel.com.service.dto.EmployeDTO;
import bf.personnel.com.service.dto.NatureRecrutementDTO;
import bf.personnel.com.service.dto.StatutDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Employe} and its DTO {@link EmployeDTO}.
 */
@Mapper(componentModel = "spring")
public interface EmployeMapper extends EntityMapper<EmployeDTO, Employe> {
    @Mapping(target = "natureRecrutement", source = "natureRecrutement", qualifiedByName = "natureRecrutementId")
    @Mapping(target = "statut", source = "statut", qualifiedByName = "statutId")
    EmployeDTO toDto(Employe s);

    @Named("natureRecrutementId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    NatureRecrutementDTO toDtoNatureRecrutementId(NatureRecrutement natureRecrutement);

    @Named("statutId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    StatutDTO toDtoStatutId(Statut statut);
}

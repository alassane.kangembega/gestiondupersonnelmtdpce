package bf.personnel.com.service.mapper;

import bf.personnel.com.domain.NatureEntree;
import bf.personnel.com.service.dto.NatureEntreeDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link NatureEntree} and its DTO {@link NatureEntreeDTO}.
 */
@Mapper(componentModel = "spring")
public interface NatureEntreeMapper extends EntityMapper<NatureEntreeDTO, NatureEntree> {}

package bf.personnel.com.service.mapper;

import bf.personnel.com.domain.Decoration;
import bf.personnel.com.domain.Employe;
import bf.personnel.com.domain.TypeDecoration;
import bf.personnel.com.service.dto.DecorationDTO;
import bf.personnel.com.service.dto.EmployeDTO;
import bf.personnel.com.service.dto.TypeDecorationDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Decoration} and its DTO {@link DecorationDTO}.
 */
@Mapper(componentModel = "spring")
public interface DecorationMapper extends EntityMapper<DecorationDTO, Decoration> {
    @Mapping(target = "typeDecoration", source = "typeDecoration", qualifiedByName = "typeDecorationId")
    @Mapping(target = "employe", source = "employe", qualifiedByName = "employeId")
    DecorationDTO toDto(Decoration s);

    @Named("typeDecorationId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    TypeDecorationDTO toDtoTypeDecorationId(TypeDecoration typeDecoration);

    @Named("employeId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    EmployeDTO toDtoEmployeId(Employe employe);
}

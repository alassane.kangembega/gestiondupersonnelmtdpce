package bf.personnel.com.service;

import bf.personnel.com.domain.NaturePositionIrreguliere;
import bf.personnel.com.repository.NaturePositionIrreguliereRepository;
import bf.personnel.com.service.dto.NaturePositionIrreguliereDTO;
import bf.personnel.com.service.mapper.NaturePositionIrreguliereMapper;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link NaturePositionIrreguliere}.
 */
@Service
@Transactional
public class NaturePositionIrreguliereService {

    private final Logger log = LoggerFactory.getLogger(NaturePositionIrreguliereService.class);

    private final NaturePositionIrreguliereRepository naturePositionIrreguliereRepository;

    private final NaturePositionIrreguliereMapper naturePositionIrreguliereMapper;

    public NaturePositionIrreguliereService(
        NaturePositionIrreguliereRepository naturePositionIrreguliereRepository,
        NaturePositionIrreguliereMapper naturePositionIrreguliereMapper
    ) {
        this.naturePositionIrreguliereRepository = naturePositionIrreguliereRepository;
        this.naturePositionIrreguliereMapper = naturePositionIrreguliereMapper;
    }

    /**
     * Save a naturePositionIrreguliere.
     *
     * @param naturePositionIrreguliereDTO the entity to save.
     * @return the persisted entity.
     */
    public NaturePositionIrreguliereDTO save(NaturePositionIrreguliereDTO naturePositionIrreguliereDTO) {
        log.debug("Request to save NaturePositionIrreguliere : {}", naturePositionIrreguliereDTO);
        NaturePositionIrreguliere naturePositionIrreguliere = naturePositionIrreguliereMapper.toEntity(naturePositionIrreguliereDTO);
        naturePositionIrreguliere = naturePositionIrreguliereRepository.save(naturePositionIrreguliere);
        return naturePositionIrreguliereMapper.toDto(naturePositionIrreguliere);
    }

    /**
     * Update a naturePositionIrreguliere.
     *
     * @param naturePositionIrreguliereDTO the entity to save.
     * @return the persisted entity.
     */
    public NaturePositionIrreguliereDTO update(NaturePositionIrreguliereDTO naturePositionIrreguliereDTO) {
        log.debug("Request to update NaturePositionIrreguliere : {}", naturePositionIrreguliereDTO);
        NaturePositionIrreguliere naturePositionIrreguliere = naturePositionIrreguliereMapper.toEntity(naturePositionIrreguliereDTO);
        naturePositionIrreguliere = naturePositionIrreguliereRepository.save(naturePositionIrreguliere);
        return naturePositionIrreguliereMapper.toDto(naturePositionIrreguliere);
    }

    /**
     * Partially update a naturePositionIrreguliere.
     *
     * @param naturePositionIrreguliereDTO the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<NaturePositionIrreguliereDTO> partialUpdate(NaturePositionIrreguliereDTO naturePositionIrreguliereDTO) {
        log.debug("Request to partially update NaturePositionIrreguliere : {}", naturePositionIrreguliereDTO);

        return naturePositionIrreguliereRepository
            .findById(naturePositionIrreguliereDTO.getId())
            .map(existingNaturePositionIrreguliere -> {
                naturePositionIrreguliereMapper.partialUpdate(existingNaturePositionIrreguliere, naturePositionIrreguliereDTO);

                return existingNaturePositionIrreguliere;
            })
            .map(naturePositionIrreguliereRepository::save)
            .map(naturePositionIrreguliereMapper::toDto);
    }

    /**
     * Get all the naturePositionIrregulieres.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<NaturePositionIrreguliereDTO> findAll() {
        log.debug("Request to get all NaturePositionIrregulieres");
        return naturePositionIrreguliereRepository
            .findAll()
            .stream()
            .map(naturePositionIrreguliereMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * Get one naturePositionIrreguliere by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<NaturePositionIrreguliereDTO> findOne(Long id) {
        log.debug("Request to get NaturePositionIrreguliere : {}", id);
        return naturePositionIrreguliereRepository.findById(id).map(naturePositionIrreguliereMapper::toDto);
    }

    /**
     * Delete the naturePositionIrreguliere by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete NaturePositionIrreguliere : {}", id);
        naturePositionIrreguliereRepository.deleteById(id);
    }
}

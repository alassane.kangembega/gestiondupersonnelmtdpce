package bf.personnel.com.service;

import bf.personnel.com.domain.Emploi;
import bf.personnel.com.repository.EmploiRepository;
import bf.personnel.com.service.dto.EmploiDTO;
import bf.personnel.com.service.mapper.EmploiMapper;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link Emploi}.
 */
@Service
@Transactional
public class EmploiService {

    private final Logger log = LoggerFactory.getLogger(EmploiService.class);

    private final EmploiRepository emploiRepository;

    private final EmploiMapper emploiMapper;

    public EmploiService(EmploiRepository emploiRepository, EmploiMapper emploiMapper) {
        this.emploiRepository = emploiRepository;
        this.emploiMapper = emploiMapper;
    }

    /**
     * Save a emploi.
     *
     * @param emploiDTO the entity to save.
     * @return the persisted entity.
     */
    public EmploiDTO save(EmploiDTO emploiDTO) {
        log.debug("Request to save Emploi : {}", emploiDTO);
        Emploi emploi = emploiMapper.toEntity(emploiDTO);
        emploi = emploiRepository.save(emploi);
        return emploiMapper.toDto(emploi);
    }

    /**
     * Update a emploi.
     *
     * @param emploiDTO the entity to save.
     * @return the persisted entity.
     */
    public EmploiDTO update(EmploiDTO emploiDTO) {
        log.debug("Request to update Emploi : {}", emploiDTO);
        Emploi emploi = emploiMapper.toEntity(emploiDTO);
        emploi = emploiRepository.save(emploi);
        return emploiMapper.toDto(emploi);
    }

    /**
     * Partially update a emploi.
     *
     * @param emploiDTO the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<EmploiDTO> partialUpdate(EmploiDTO emploiDTO) {
        log.debug("Request to partially update Emploi : {}", emploiDTO);

        return emploiRepository
            .findById(emploiDTO.getId())
            .map(existingEmploi -> {
                emploiMapper.partialUpdate(existingEmploi, emploiDTO);

                return existingEmploi;
            })
            .map(emploiRepository::save)
            .map(emploiMapper::toDto);
    }

    /**
     * Get all the emplois.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<EmploiDTO> findAll() {
        log.debug("Request to get all Emplois");
        return emploiRepository.findAll().stream().map(emploiMapper::toDto).collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * Get one emploi by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<EmploiDTO> findOne(Long id) {
        log.debug("Request to get Emploi : {}", id);
        return emploiRepository.findById(id).map(emploiMapper::toDto);
    }

    /**
     * Delete the emploi by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Emploi : {}", id);
        emploiRepository.deleteById(id);
    }
}

package bf.personnel.com.service;

import bf.personnel.com.domain.Decoration;
import bf.personnel.com.repository.DecorationRepository;
import bf.personnel.com.service.dto.DecorationDTO;
import bf.personnel.com.service.mapper.DecorationMapper;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link Decoration}.
 */
@Service
@Transactional
public class DecorationService {

    private final Logger log = LoggerFactory.getLogger(DecorationService.class);

    private final DecorationRepository decorationRepository;

    private final DecorationMapper decorationMapper;

    public DecorationService(DecorationRepository decorationRepository, DecorationMapper decorationMapper) {
        this.decorationRepository = decorationRepository;
        this.decorationMapper = decorationMapper;
    }

    /**
     * Save a decoration.
     *
     * @param decorationDTO the entity to save.
     * @return the persisted entity.
     */
    public DecorationDTO save(DecorationDTO decorationDTO) {
        log.debug("Request to save Decoration : {}", decorationDTO);
        Decoration decoration = decorationMapper.toEntity(decorationDTO);
        decoration = decorationRepository.save(decoration);
        return decorationMapper.toDto(decoration);
    }

    /**
     * Update a decoration.
     *
     * @param decorationDTO the entity to save.
     * @return the persisted entity.
     */
    public DecorationDTO update(DecorationDTO decorationDTO) {
        log.debug("Request to update Decoration : {}", decorationDTO);
        Decoration decoration = decorationMapper.toEntity(decorationDTO);
        decoration = decorationRepository.save(decoration);
        return decorationMapper.toDto(decoration);
    }

    /**
     * Partially update a decoration.
     *
     * @param decorationDTO the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<DecorationDTO> partialUpdate(DecorationDTO decorationDTO) {
        log.debug("Request to partially update Decoration : {}", decorationDTO);

        return decorationRepository
            .findById(decorationDTO.getId())
            .map(existingDecoration -> {
                decorationMapper.partialUpdate(existingDecoration, decorationDTO);

                return existingDecoration;
            })
            .map(decorationRepository::save)
            .map(decorationMapper::toDto);
    }

    /**
     * Get all the decorations.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<DecorationDTO> findAll() {
        log.debug("Request to get all Decorations");
        List<Decoration> decorationList = decorationRepository.findAll();
        List<DecorationDTO> decorationDTOS = decorationRepository.findAll().stream().map(decorationMapper::toDto).collect(Collectors.toCollection(LinkedList::new));
        log.info("****************************************");
        for (Decoration decoration: decorationList) {
            if (decoration.getEmploye() != null){
                log.info(decoration.getEmploye().toString());
            }
        }
        log.info("****************************************");
        return decorationDTOS;
    }

    /**
     * Get one decoration by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<DecorationDTO> findOne(Long id) {
        log.debug("Request to get Decoration : {}", id);
        return decorationRepository.findById(id).map(decorationMapper::toDto);
    }

    /**
     * Delete the decoration by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Decoration : {}", id);
        decorationRepository.deleteById(id);
    }
}

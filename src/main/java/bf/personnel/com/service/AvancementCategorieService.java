package bf.personnel.com.service;

import bf.personnel.com.domain.AvancementCategorie;
import bf.personnel.com.repository.AvancementCategorieRepository;
import bf.personnel.com.service.dto.AvancementCategorieDTO;
import bf.personnel.com.service.mapper.AvancementCategorieMapper;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link AvancementCategorie}.
 */
@Service
@Transactional
public class AvancementCategorieService {

    private final Logger log = LoggerFactory.getLogger(AvancementCategorieService.class);

    private final AvancementCategorieRepository avancementCategorieRepository;

    private final AvancementCategorieMapper avancementCategorieMapper;

    public AvancementCategorieService(
        AvancementCategorieRepository avancementCategorieRepository,
        AvancementCategorieMapper avancementCategorieMapper
    ) {
        this.avancementCategorieRepository = avancementCategorieRepository;
        this.avancementCategorieMapper = avancementCategorieMapper;
    }

    /**
     * Save a avancementCategorie.
     *
     * @param avancementCategorieDTO the entity to save.
     * @return the persisted entity.
     */
    public AvancementCategorieDTO save(AvancementCategorieDTO avancementCategorieDTO) {
        log.debug("Request to save AvancementCategorie : {}", avancementCategorieDTO);
        AvancementCategorie avancementCategorie = avancementCategorieMapper.toEntity(avancementCategorieDTO);
        avancementCategorie = avancementCategorieRepository.save(avancementCategorie);
        return avancementCategorieMapper.toDto(avancementCategorie);
    }

    /**
     * Update a avancementCategorie.
     *
     * @param avancementCategorieDTO the entity to save.
     * @return the persisted entity.
     */
    public AvancementCategorieDTO update(AvancementCategorieDTO avancementCategorieDTO) {
        log.debug("Request to update AvancementCategorie : {}", avancementCategorieDTO);
        AvancementCategorie avancementCategorie = avancementCategorieMapper.toEntity(avancementCategorieDTO);
        avancementCategorie = avancementCategorieRepository.save(avancementCategorie);
        return avancementCategorieMapper.toDto(avancementCategorie);
    }

    /**
     * Partially update a avancementCategorie.
     *
     * @param avancementCategorieDTO the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<AvancementCategorieDTO> partialUpdate(AvancementCategorieDTO avancementCategorieDTO) {
        log.debug("Request to partially update AvancementCategorie : {}", avancementCategorieDTO);

        return avancementCategorieRepository
            .findById(avancementCategorieDTO.getId())
            .map(existingAvancementCategorie -> {
                avancementCategorieMapper.partialUpdate(existingAvancementCategorie, avancementCategorieDTO);

                return existingAvancementCategorie;
            })
            .map(avancementCategorieRepository::save)
            .map(avancementCategorieMapper::toDto);
    }

    /**
     * Get all the avancementCategories.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<AvancementCategorieDTO> findAll() {
        log.debug("Request to get all AvancementCategories");
        return avancementCategorieRepository
            .findAll()
            .stream()
            .map(avancementCategorieMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * Get one avancementCategorie by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<AvancementCategorieDTO> findOne(Long id) {
        log.debug("Request to get AvancementCategorie : {}", id);
        return avancementCategorieRepository.findById(id).map(avancementCategorieMapper::toDto);
    }

    /**
     * Delete the avancementCategorie by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete AvancementCategorie : {}", id);
        avancementCategorieRepository.deleteById(id);
    }
}

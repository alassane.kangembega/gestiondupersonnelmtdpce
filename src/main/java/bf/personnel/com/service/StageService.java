package bf.personnel.com.service;

import bf.personnel.com.domain.Stage;
import bf.personnel.com.repository.StageRepository;
import bf.personnel.com.service.dto.StageDTO;
import bf.personnel.com.service.mapper.StageMapper;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link Stage}.
 */
@Service
@Transactional
public class StageService {

    private final Logger log = LoggerFactory.getLogger(StageService.class);

    private final StageRepository stageRepository;

    private final StageMapper stageMapper;

    public StageService(StageRepository stageRepository, StageMapper stageMapper) {
        this.stageRepository = stageRepository;
        this.stageMapper = stageMapper;
    }

    /**
     * Save a stage.
     *
     * @param stageDTO the entity to save.
     * @return the persisted entity.
     */
    public StageDTO save(StageDTO stageDTO) {
        log.debug("Request to save Stage : {}", stageDTO);
        Stage stage = stageMapper.toEntity(stageDTO);
        stage = stageRepository.save(stage);
        return stageMapper.toDto(stage);
    }

    /**
     * Update a stage.
     *
     * @param stageDTO the entity to save.
     * @return the persisted entity.
     */
    public StageDTO update(StageDTO stageDTO) {
        log.debug("Request to update Stage : {}", stageDTO);
        Stage stage = stageMapper.toEntity(stageDTO);
        stage = stageRepository.save(stage);
        return stageMapper.toDto(stage);
    }

    /**
     * Partially update a stage.
     *
     * @param stageDTO the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<StageDTO> partialUpdate(StageDTO stageDTO) {
        log.debug("Request to partially update Stage : {}", stageDTO);

        return stageRepository
            .findById(stageDTO.getId())
            .map(existingStage -> {
                stageMapper.partialUpdate(existingStage, stageDTO);

                return existingStage;
            })
            .map(stageRepository::save)
            .map(stageMapper::toDto);
    }

    /**
     * Get all the stages.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<StageDTO> findAll() {
        log.debug("Request to get all Stages");
        return stageRepository.findAll().stream().map(stageMapper::toDto).collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * Get one stage by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<StageDTO> findOne(Long id) {
        log.debug("Request to get Stage : {}", id);
        return stageRepository.findById(id).map(stageMapper::toDto);
    }

    /**
     * Delete the stage by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Stage : {}", id);
        stageRepository.deleteById(id);
    }
}

package bf.personnel.com.service;

import bf.personnel.com.domain.Sortie;
import bf.personnel.com.repository.SortieRepository;
import bf.personnel.com.service.dto.SortieDTO;
import bf.personnel.com.service.mapper.SortieMapper;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link Sortie}.
 */
@Service
@Transactional
public class SortieService {

    private final Logger log = LoggerFactory.getLogger(SortieService.class);

    private final SortieRepository sortieRepository;

    private final SortieMapper sortieMapper;

    public SortieService(SortieRepository sortieRepository, SortieMapper sortieMapper) {
        this.sortieRepository = sortieRepository;
        this.sortieMapper = sortieMapper;
    }

    /**
     * Save a sortie.
     *
     * @param sortieDTO the entity to save.
     * @return the persisted entity.
     */
    public SortieDTO save(SortieDTO sortieDTO) {
        log.debug("Request to save Sortie : {}", sortieDTO);
        Sortie sortie = sortieMapper.toEntity(sortieDTO);
        sortie = sortieRepository.save(sortie);
        return sortieMapper.toDto(sortie);
    }

    /**
     * Update a sortie.
     *
     * @param sortieDTO the entity to save.
     * @return the persisted entity.
     */
    public SortieDTO update(SortieDTO sortieDTO) {
        log.debug("Request to update Sortie : {}", sortieDTO);
        Sortie sortie = sortieMapper.toEntity(sortieDTO);
        sortie = sortieRepository.save(sortie);
        return sortieMapper.toDto(sortie);
    }

    /**
     * Partially update a sortie.
     *
     * @param sortieDTO the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<SortieDTO> partialUpdate(SortieDTO sortieDTO) {
        log.debug("Request to partially update Sortie : {}", sortieDTO);

        return sortieRepository
            .findById(sortieDTO.getId())
            .map(existingSortie -> {
                sortieMapper.partialUpdate(existingSortie, sortieDTO);

                return existingSortie;
            })
            .map(sortieRepository::save)
            .map(sortieMapper::toDto);
    }

    /**
     * Get all the sorties.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<SortieDTO> findAll() {
        log.debug("Request to get all Sorties");
        return sortieRepository.findAll().stream().map(sortieMapper::toDto).collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * Get one sortie by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<SortieDTO> findOne(Long id) {
        log.debug("Request to get Sortie : {}", id);
        return sortieRepository.findById(id).map(sortieMapper::toDto);
    }

    /**
     * Delete the sortie by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Sortie : {}", id);
        sortieRepository.deleteById(id);
    }
}

package bf.personnel.com.web.rest;

import bf.personnel.com.repository.NatureRecrutementRepository;
import bf.personnel.com.service.NatureRecrutementService;
import bf.personnel.com.service.dto.NatureRecrutementDTO;
import bf.personnel.com.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link bf.personnel.com.domain.NatureRecrutement}.
 */
@RestController
@RequestMapping("/api")
public class NatureRecrutementResource {

    private final Logger log = LoggerFactory.getLogger(NatureRecrutementResource.class);

    private static final String ENTITY_NAME = "natureRecrutement";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final NatureRecrutementService natureRecrutementService;

    private final NatureRecrutementRepository natureRecrutementRepository;

    public NatureRecrutementResource(
        NatureRecrutementService natureRecrutementService,
        NatureRecrutementRepository natureRecrutementRepository
    ) {
        this.natureRecrutementService = natureRecrutementService;
        this.natureRecrutementRepository = natureRecrutementRepository;
    }

    /**
     * {@code POST  /nature-recrutements} : Create a new natureRecrutement.
     *
     * @param natureRecrutementDTO the natureRecrutementDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new natureRecrutementDTO, or with status {@code 400 (Bad Request)} if the natureRecrutement has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/nature-recrutements")
    public ResponseEntity<NatureRecrutementDTO> createNatureRecrutement(@RequestBody NatureRecrutementDTO natureRecrutementDTO)
        throws URISyntaxException {
        log.debug("REST request to save NatureRecrutement : {}", natureRecrutementDTO);
        if (natureRecrutementDTO.getId() != null) {
            throw new BadRequestAlertException("A new natureRecrutement cannot already have an ID", ENTITY_NAME, "idexists");
        }
        NatureRecrutementDTO result = natureRecrutementService.save(natureRecrutementDTO);
        return ResponseEntity
            .created(new URI("/api/nature-recrutements/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /nature-recrutements/:id} : Updates an existing natureRecrutement.
     *
     * @param id the id of the natureRecrutementDTO to save.
     * @param natureRecrutementDTO the natureRecrutementDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated natureRecrutementDTO,
     * or with status {@code 400 (Bad Request)} if the natureRecrutementDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the natureRecrutementDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/nature-recrutements/{id}")
    public ResponseEntity<NatureRecrutementDTO> updateNatureRecrutement(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody NatureRecrutementDTO natureRecrutementDTO
    ) throws URISyntaxException {
        log.debug("REST request to update NatureRecrutement : {}, {}", id, natureRecrutementDTO);
        if (natureRecrutementDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, natureRecrutementDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!natureRecrutementRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        NatureRecrutementDTO result = natureRecrutementService.update(natureRecrutementDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, natureRecrutementDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /nature-recrutements/:id} : Partial updates given fields of an existing natureRecrutement, field will ignore if it is null
     *
     * @param id the id of the natureRecrutementDTO to save.
     * @param natureRecrutementDTO the natureRecrutementDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated natureRecrutementDTO,
     * or with status {@code 400 (Bad Request)} if the natureRecrutementDTO is not valid,
     * or with status {@code 404 (Not Found)} if the natureRecrutementDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the natureRecrutementDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/nature-recrutements/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<NatureRecrutementDTO> partialUpdateNatureRecrutement(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody NatureRecrutementDTO natureRecrutementDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update NatureRecrutement partially : {}, {}", id, natureRecrutementDTO);
        if (natureRecrutementDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, natureRecrutementDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!natureRecrutementRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<NatureRecrutementDTO> result = natureRecrutementService.partialUpdate(natureRecrutementDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, natureRecrutementDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /nature-recrutements} : get all the natureRecrutements.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of natureRecrutements in body.
     */
    @GetMapping("/nature-recrutements")
    public List<NatureRecrutementDTO> getAllNatureRecrutements() {
        log.debug("REST request to get all NatureRecrutements");
        return natureRecrutementService.findAll();
    }

    /**
     * {@code GET  /nature-recrutements/:id} : get the "id" natureRecrutement.
     *
     * @param id the id of the natureRecrutementDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the natureRecrutementDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/nature-recrutements/{id}")
    public ResponseEntity<NatureRecrutementDTO> getNatureRecrutement(@PathVariable Long id) {
        log.debug("REST request to get NatureRecrutement : {}", id);
        Optional<NatureRecrutementDTO> natureRecrutementDTO = natureRecrutementService.findOne(id);
        return ResponseUtil.wrapOrNotFound(natureRecrutementDTO);
    }

    /**
     * {@code DELETE  /nature-recrutements/:id} : delete the "id" natureRecrutement.
     *
     * @param id the id of the natureRecrutementDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/nature-recrutements/{id}")
    public ResponseEntity<Void> deleteNatureRecrutement(@PathVariable Long id) {
        log.debug("REST request to delete NatureRecrutement : {}", id);
        natureRecrutementService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}

package bf.personnel.com.web.rest;

import bf.personnel.com.repository.DecorationRepository;
import bf.personnel.com.service.DecorationService;
import bf.personnel.com.service.dto.DecorationDTO;
import bf.personnel.com.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link bf.personnel.com.domain.Decoration}.
 */
@RestController
@RequestMapping("/api")
public class DecorationResource {

    private final Logger log = LoggerFactory.getLogger(DecorationResource.class);

    private static final String ENTITY_NAME = "decoration";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final DecorationService decorationService;

    private final DecorationRepository decorationRepository;

    public DecorationResource(DecorationService decorationService, DecorationRepository decorationRepository) {
        this.decorationService = decorationService;
        this.decorationRepository = decorationRepository;
    }

    /**
     * {@code POST  /decorations} : Create a new decoration.
     *
     * @param decorationDTO the decorationDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new decorationDTO, or with status {@code 400 (Bad Request)} if the decoration has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/decorations")
    public ResponseEntity<DecorationDTO> createDecoration(@RequestBody DecorationDTO decorationDTO) throws URISyntaxException {
        log.debug("REST request to save Decoration : {}", decorationDTO);
        if (decorationDTO.getId() != null) {
            throw new BadRequestAlertException("A new decoration cannot already have an ID", ENTITY_NAME, "idexists");
        }
        DecorationDTO result = decorationService.save(decorationDTO);
        return ResponseEntity
            .created(new URI("/api/decorations/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /decorations/:id} : Updates an existing decoration.
     *
     * @param id the id of the decorationDTO to save.
     * @param decorationDTO the decorationDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated decorationDTO,
     * or with status {@code 400 (Bad Request)} if the decorationDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the decorationDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/decorations/{id}")
    public ResponseEntity<DecorationDTO> updateDecoration(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody DecorationDTO decorationDTO
    ) throws URISyntaxException {
        log.debug("REST request to update Decoration : {}, {}", id, decorationDTO);
        if (decorationDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, decorationDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!decorationRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        DecorationDTO result = decorationService.update(decorationDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, decorationDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /decorations/:id} : Partial updates given fields of an existing decoration, field will ignore if it is null
     *
     * @param id the id of the decorationDTO to save.
     * @param decorationDTO the decorationDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated decorationDTO,
     * or with status {@code 400 (Bad Request)} if the decorationDTO is not valid,
     * or with status {@code 404 (Not Found)} if the decorationDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the decorationDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/decorations/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<DecorationDTO> partialUpdateDecoration(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody DecorationDTO decorationDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update Decoration partially : {}, {}", id, decorationDTO);
        if (decorationDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, decorationDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!decorationRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<DecorationDTO> result = decorationService.partialUpdate(decorationDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, decorationDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /decorations} : get all the decorations.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of decorations in body.
     */
    @GetMapping("/decorations")
    public List<DecorationDTO> getAllDecorations() {
        log.debug("REST request to get all Decorations");
        return decorationService.findAll();
    }

    /**
     * {@code GET  /decorations/:id} : get the "id" decoration.
     *
     * @param id the id of the decorationDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the decorationDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/decorations/{id}")
    public ResponseEntity<DecorationDTO> getDecoration(@PathVariable Long id) {
        log.debug("REST request to get Decoration : {}", id);
        Optional<DecorationDTO> decorationDTO = decorationService.findOne(id);
        return ResponseUtil.wrapOrNotFound(decorationDTO);
    }

    /**
     * {@code DELETE  /decorations/:id} : delete the "id" decoration.
     *
     * @param id the id of the decorationDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/decorations/{id}")
    public ResponseEntity<Void> deleteDecoration(@PathVariable Long id) {
        log.debug("REST request to delete Decoration : {}", id);
        decorationService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}

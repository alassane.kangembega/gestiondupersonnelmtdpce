package bf.personnel.com.web.rest;

import bf.personnel.com.repository.ChangementEmploiRepository;
import bf.personnel.com.service.ChangementEmploiService;
import bf.personnel.com.service.dto.ChangementEmploiDTO;
import bf.personnel.com.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link bf.personnel.com.domain.ChangementEmploi}.
 */
@RestController
@RequestMapping("/api")
public class ChangementEmploiResource {

    private final Logger log = LoggerFactory.getLogger(ChangementEmploiResource.class);

    private static final String ENTITY_NAME = "changementEmploi";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ChangementEmploiService changementEmploiService;

    private final ChangementEmploiRepository changementEmploiRepository;

    public ChangementEmploiResource(
        ChangementEmploiService changementEmploiService,
        ChangementEmploiRepository changementEmploiRepository
    ) {
        this.changementEmploiService = changementEmploiService;
        this.changementEmploiRepository = changementEmploiRepository;
    }

    /**
     * {@code POST  /changement-emplois} : Create a new changementEmploi.
     *
     * @param changementEmploiDTO the changementEmploiDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new changementEmploiDTO, or with status {@code 400 (Bad Request)} if the changementEmploi has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/changement-emplois")
    public ResponseEntity<ChangementEmploiDTO> createChangementEmploi(@RequestBody ChangementEmploiDTO changementEmploiDTO)
        throws URISyntaxException {
        log.debug("REST request to save ChangementEmploi : {}", changementEmploiDTO);
        if (changementEmploiDTO.getId() != null) {
            throw new BadRequestAlertException("A new changementEmploi cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ChangementEmploiDTO result = changementEmploiService.save(changementEmploiDTO);
        return ResponseEntity
            .created(new URI("/api/changement-emplois/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /changement-emplois/:id} : Updates an existing changementEmploi.
     *
     * @param id the id of the changementEmploiDTO to save.
     * @param changementEmploiDTO the changementEmploiDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated changementEmploiDTO,
     * or with status {@code 400 (Bad Request)} if the changementEmploiDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the changementEmploiDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/changement-emplois/{id}")
    public ResponseEntity<ChangementEmploiDTO> updateChangementEmploi(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody ChangementEmploiDTO changementEmploiDTO
    ) throws URISyntaxException {
        log.debug("REST request to update ChangementEmploi : {}, {}", id, changementEmploiDTO);
        if (changementEmploiDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, changementEmploiDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!changementEmploiRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        ChangementEmploiDTO result = changementEmploiService.update(changementEmploiDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, changementEmploiDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /changement-emplois/:id} : Partial updates given fields of an existing changementEmploi, field will ignore if it is null
     *
     * @param id the id of the changementEmploiDTO to save.
     * @param changementEmploiDTO the changementEmploiDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated changementEmploiDTO,
     * or with status {@code 400 (Bad Request)} if the changementEmploiDTO is not valid,
     * or with status {@code 404 (Not Found)} if the changementEmploiDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the changementEmploiDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/changement-emplois/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<ChangementEmploiDTO> partialUpdateChangementEmploi(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody ChangementEmploiDTO changementEmploiDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update ChangementEmploi partially : {}, {}", id, changementEmploiDTO);
        if (changementEmploiDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, changementEmploiDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!changementEmploiRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<ChangementEmploiDTO> result = changementEmploiService.partialUpdate(changementEmploiDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, changementEmploiDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /changement-emplois} : get all the changementEmplois.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of changementEmplois in body.
     */
    @GetMapping("/changement-emplois")
    public List<ChangementEmploiDTO> getAllChangementEmplois() {
        log.debug("REST request to get all ChangementEmplois");
        return changementEmploiService.findAll();
    }

    /**
     * {@code GET  /changement-emplois/:id} : get the "id" changementEmploi.
     *
     * @param id the id of the changementEmploiDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the changementEmploiDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/changement-emplois/{id}")
    public ResponseEntity<ChangementEmploiDTO> getChangementEmploi(@PathVariable Long id) {
        log.debug("REST request to get ChangementEmploi : {}", id);
        Optional<ChangementEmploiDTO> changementEmploiDTO = changementEmploiService.findOne(id);
        return ResponseUtil.wrapOrNotFound(changementEmploiDTO);
    }

    /**
     * {@code DELETE  /changement-emplois/:id} : delete the "id" changementEmploi.
     *
     * @param id the id of the changementEmploiDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/changement-emplois/{id}")
    public ResponseEntity<Void> deleteChangementEmploi(@PathVariable Long id) {
        log.debug("REST request to delete ChangementEmploi : {}", id);
        changementEmploiService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}

package bf.personnel.com.web.rest;

import bf.personnel.com.repository.NatureStageRepository;
import bf.personnel.com.service.NatureStageService;
import bf.personnel.com.service.dto.NatureStageDTO;
import bf.personnel.com.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link bf.personnel.com.domain.NatureStage}.
 */
@RestController
@RequestMapping("/api")
public class NatureStageResource {

    private final Logger log = LoggerFactory.getLogger(NatureStageResource.class);

    private static final String ENTITY_NAME = "natureStage";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final NatureStageService natureStageService;

    private final NatureStageRepository natureStageRepository;

    public NatureStageResource(NatureStageService natureStageService, NatureStageRepository natureStageRepository) {
        this.natureStageService = natureStageService;
        this.natureStageRepository = natureStageRepository;
    }

    /**
     * {@code POST  /nature-stages} : Create a new natureStage.
     *
     * @param natureStageDTO the natureStageDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new natureStageDTO, or with status {@code 400 (Bad Request)} if the natureStage has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/nature-stages")
    public ResponseEntity<NatureStageDTO> createNatureStage(@RequestBody NatureStageDTO natureStageDTO) throws URISyntaxException {
        log.debug("REST request to save NatureStage : {}", natureStageDTO);
        if (natureStageDTO.getId() != null) {
            throw new BadRequestAlertException("A new natureStage cannot already have an ID", ENTITY_NAME, "idexists");
        }
        NatureStageDTO result = natureStageService.save(natureStageDTO);
        return ResponseEntity
            .created(new URI("/api/nature-stages/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /nature-stages/:id} : Updates an existing natureStage.
     *
     * @param id the id of the natureStageDTO to save.
     * @param natureStageDTO the natureStageDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated natureStageDTO,
     * or with status {@code 400 (Bad Request)} if the natureStageDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the natureStageDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/nature-stages/{id}")
    public ResponseEntity<NatureStageDTO> updateNatureStage(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody NatureStageDTO natureStageDTO
    ) throws URISyntaxException {
        log.debug("REST request to update NatureStage : {}, {}", id, natureStageDTO);
        if (natureStageDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, natureStageDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!natureStageRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        NatureStageDTO result = natureStageService.update(natureStageDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, natureStageDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /nature-stages/:id} : Partial updates given fields of an existing natureStage, field will ignore if it is null
     *
     * @param id the id of the natureStageDTO to save.
     * @param natureStageDTO the natureStageDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated natureStageDTO,
     * or with status {@code 400 (Bad Request)} if the natureStageDTO is not valid,
     * or with status {@code 404 (Not Found)} if the natureStageDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the natureStageDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/nature-stages/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<NatureStageDTO> partialUpdateNatureStage(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody NatureStageDTO natureStageDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update NatureStage partially : {}, {}", id, natureStageDTO);
        if (natureStageDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, natureStageDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!natureStageRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<NatureStageDTO> result = natureStageService.partialUpdate(natureStageDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, natureStageDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /nature-stages} : get all the natureStages.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of natureStages in body.
     */
    @GetMapping("/nature-stages")
    public List<NatureStageDTO> getAllNatureStages() {
        log.debug("REST request to get all NatureStages");
        return natureStageService.findAll();
    }

    /**
     * {@code GET  /nature-stages/:id} : get the "id" natureStage.
     *
     * @param id the id of the natureStageDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the natureStageDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/nature-stages/{id}")
    public ResponseEntity<NatureStageDTO> getNatureStage(@PathVariable Long id) {
        log.debug("REST request to get NatureStage : {}", id);
        Optional<NatureStageDTO> natureStageDTO = natureStageService.findOne(id);
        return ResponseUtil.wrapOrNotFound(natureStageDTO);
    }

    /**
     * {@code DELETE  /nature-stages/:id} : delete the "id" natureStage.
     *
     * @param id the id of the natureStageDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/nature-stages/{id}")
    public ResponseEntity<Void> deleteNatureStage(@PathVariable Long id) {
        log.debug("REST request to delete NatureStage : {}", id);
        natureStageService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}

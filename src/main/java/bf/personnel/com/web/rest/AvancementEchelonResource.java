package bf.personnel.com.web.rest;

import bf.personnel.com.repository.AvancementEchelonRepository;
import bf.personnel.com.service.AvancementEchelonService;
import bf.personnel.com.service.dto.AvancementEchelonDTO;
import bf.personnel.com.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link bf.personnel.com.domain.AvancementEchelon}.
 */
@RestController
@RequestMapping("/api")
public class AvancementEchelonResource {

    private final Logger log = LoggerFactory.getLogger(AvancementEchelonResource.class);

    private static final String ENTITY_NAME = "avancementEchelon";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final AvancementEchelonService avancementEchelonService;

    private final AvancementEchelonRepository avancementEchelonRepository;

    public AvancementEchelonResource(
        AvancementEchelonService avancementEchelonService,
        AvancementEchelonRepository avancementEchelonRepository
    ) {
        this.avancementEchelonService = avancementEchelonService;
        this.avancementEchelonRepository = avancementEchelonRepository;
    }

    /**
     * {@code POST  /avancement-echelons} : Create a new avancementEchelon.
     *
     * @param avancementEchelonDTO the avancementEchelonDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new avancementEchelonDTO, or with status {@code 400 (Bad Request)} if the avancementEchelon has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/avancement-echelons")
    public ResponseEntity<AvancementEchelonDTO> createAvancementEchelon(@RequestBody AvancementEchelonDTO avancementEchelonDTO)
        throws URISyntaxException {
        log.debug("REST request to save AvancementEchelon : {}", avancementEchelonDTO);
        if (avancementEchelonDTO.getId() != null) {
            throw new BadRequestAlertException("A new avancementEchelon cannot already have an ID", ENTITY_NAME, "idexists");
        }
        AvancementEchelonDTO result = avancementEchelonService.save(avancementEchelonDTO);
        return ResponseEntity
            .created(new URI("/api/avancement-echelons/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /avancement-echelons/:id} : Updates an existing avancementEchelon.
     *
     * @param id the id of the avancementEchelonDTO to save.
     * @param avancementEchelonDTO the avancementEchelonDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated avancementEchelonDTO,
     * or with status {@code 400 (Bad Request)} if the avancementEchelonDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the avancementEchelonDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/avancement-echelons/{id}")
    public ResponseEntity<AvancementEchelonDTO> updateAvancementEchelon(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody AvancementEchelonDTO avancementEchelonDTO
    ) throws URISyntaxException {
        log.debug("REST request to update AvancementEchelon : {}, {}", id, avancementEchelonDTO);
        if (avancementEchelonDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, avancementEchelonDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!avancementEchelonRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        AvancementEchelonDTO result = avancementEchelonService.update(avancementEchelonDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, avancementEchelonDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /avancement-echelons/:id} : Partial updates given fields of an existing avancementEchelon, field will ignore if it is null
     *
     * @param id the id of the avancementEchelonDTO to save.
     * @param avancementEchelonDTO the avancementEchelonDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated avancementEchelonDTO,
     * or with status {@code 400 (Bad Request)} if the avancementEchelonDTO is not valid,
     * or with status {@code 404 (Not Found)} if the avancementEchelonDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the avancementEchelonDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/avancement-echelons/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<AvancementEchelonDTO> partialUpdateAvancementEchelon(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody AvancementEchelonDTO avancementEchelonDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update AvancementEchelon partially : {}, {}", id, avancementEchelonDTO);
        if (avancementEchelonDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, avancementEchelonDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!avancementEchelonRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<AvancementEchelonDTO> result = avancementEchelonService.partialUpdate(avancementEchelonDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, avancementEchelonDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /avancement-echelons} : get all the avancementEchelons.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of avancementEchelons in body.
     */
    @GetMapping("/avancement-echelons")
    public List<AvancementEchelonDTO> getAllAvancementEchelons() {
        log.debug("REST request to get all AvancementEchelons");
        return avancementEchelonService.findAll();
    }

    /**
     * {@code GET  /avancement-echelons/:id} : get the "id" avancementEchelon.
     *
     * @param id the id of the avancementEchelonDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the avancementEchelonDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/avancement-echelons/{id}")
    public ResponseEntity<AvancementEchelonDTO> getAvancementEchelon(@PathVariable Long id) {
        log.debug("REST request to get AvancementEchelon : {}", id);
        Optional<AvancementEchelonDTO> avancementEchelonDTO = avancementEchelonService.findOne(id);
        return ResponseUtil.wrapOrNotFound(avancementEchelonDTO);
    }

    /**
     * {@code DELETE  /avancement-echelons/:id} : delete the "id" avancementEchelon.
     *
     * @param id the id of the avancementEchelonDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/avancement-echelons/{id}")
    public ResponseEntity<Void> deleteAvancementEchelon(@PathVariable Long id) {
        log.debug("REST request to delete AvancementEchelon : {}", id);
        avancementEchelonService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}

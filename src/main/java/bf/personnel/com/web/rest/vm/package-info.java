/**
 * View Models used by Spring MVC REST controllers.
 */
package bf.personnel.com.web.rest.vm;

package bf.personnel.com.web.rest;

import bf.personnel.com.repository.PositionIrreguliereRepository;
import bf.personnel.com.service.PositionIrreguliereService;
import bf.personnel.com.service.dto.PositionIrreguliereDTO;
import bf.personnel.com.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link bf.personnel.com.domain.PositionIrreguliere}.
 */
@RestController
@RequestMapping("/api")
public class PositionIrreguliereResource {

    private final Logger log = LoggerFactory.getLogger(PositionIrreguliereResource.class);

    private static final String ENTITY_NAME = "positionIrreguliere";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final PositionIrreguliereService positionIrreguliereService;

    private final PositionIrreguliereRepository positionIrreguliereRepository;

    public PositionIrreguliereResource(
        PositionIrreguliereService positionIrreguliereService,
        PositionIrreguliereRepository positionIrreguliereRepository
    ) {
        this.positionIrreguliereService = positionIrreguliereService;
        this.positionIrreguliereRepository = positionIrreguliereRepository;
    }

    /**
     * {@code POST  /position-irregulieres} : Create a new positionIrreguliere.
     *
     * @param positionIrreguliereDTO the positionIrreguliereDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new positionIrreguliereDTO, or with status {@code 400 (Bad Request)} if the positionIrreguliere has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/position-irregulieres")
    public ResponseEntity<PositionIrreguliereDTO> createPositionIrreguliere(@RequestBody PositionIrreguliereDTO positionIrreguliereDTO)
        throws URISyntaxException {
        log.debug("REST request to save PositionIrreguliere : {}", positionIrreguliereDTO);
        if (positionIrreguliereDTO.getId() != null) {
            throw new BadRequestAlertException("A new positionIrreguliere cannot already have an ID", ENTITY_NAME, "idexists");
        }
        PositionIrreguliereDTO result = positionIrreguliereService.save(positionIrreguliereDTO);
        return ResponseEntity
            .created(new URI("/api/position-irregulieres/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /position-irregulieres/:id} : Updates an existing positionIrreguliere.
     *
     * @param id the id of the positionIrreguliereDTO to save.
     * @param positionIrreguliereDTO the positionIrreguliereDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated positionIrreguliereDTO,
     * or with status {@code 400 (Bad Request)} if the positionIrreguliereDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the positionIrreguliereDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/position-irregulieres/{id}")
    public ResponseEntity<PositionIrreguliereDTO> updatePositionIrreguliere(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody PositionIrreguliereDTO positionIrreguliereDTO
    ) throws URISyntaxException {
        log.debug("REST request to update PositionIrreguliere : {}, {}", id, positionIrreguliereDTO);
        if (positionIrreguliereDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, positionIrreguliereDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!positionIrreguliereRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        PositionIrreguliereDTO result = positionIrreguliereService.update(positionIrreguliereDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, positionIrreguliereDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /position-irregulieres/:id} : Partial updates given fields of an existing positionIrreguliere, field will ignore if it is null
     *
     * @param id the id of the positionIrreguliereDTO to save.
     * @param positionIrreguliereDTO the positionIrreguliereDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated positionIrreguliereDTO,
     * or with status {@code 400 (Bad Request)} if the positionIrreguliereDTO is not valid,
     * or with status {@code 404 (Not Found)} if the positionIrreguliereDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the positionIrreguliereDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/position-irregulieres/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<PositionIrreguliereDTO> partialUpdatePositionIrreguliere(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody PositionIrreguliereDTO positionIrreguliereDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update PositionIrreguliere partially : {}, {}", id, positionIrreguliereDTO);
        if (positionIrreguliereDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, positionIrreguliereDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!positionIrreguliereRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<PositionIrreguliereDTO> result = positionIrreguliereService.partialUpdate(positionIrreguliereDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, positionIrreguliereDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /position-irregulieres} : get all the positionIrregulieres.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of positionIrregulieres in body.
     */
    @GetMapping("/position-irregulieres")
    public List<PositionIrreguliereDTO> getAllPositionIrregulieres() {
        log.debug("REST request to get all PositionIrregulieres");
        return positionIrreguliereService.findAll();
    }

    /**
     * {@code GET  /position-irregulieres/:id} : get the "id" positionIrreguliere.
     *
     * @param id the id of the positionIrreguliereDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the positionIrreguliereDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/position-irregulieres/{id}")
    public ResponseEntity<PositionIrreguliereDTO> getPositionIrreguliere(@PathVariable Long id) {
        log.debug("REST request to get PositionIrreguliere : {}", id);
        Optional<PositionIrreguliereDTO> positionIrreguliereDTO = positionIrreguliereService.findOne(id);
        return ResponseUtil.wrapOrNotFound(positionIrreguliereDTO);
    }

    /**
     * {@code DELETE  /position-irregulieres/:id} : delete the "id" positionIrreguliere.
     *
     * @param id the id of the positionIrreguliereDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/position-irregulieres/{id}")
    public ResponseEntity<Void> deletePositionIrreguliere(@PathVariable Long id) {
        log.debug("REST request to delete PositionIrreguliere : {}", id);
        positionIrreguliereService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}

package bf.personnel.com.web.rest;

import bf.personnel.com.repository.NatureEntreeRepository;
import bf.personnel.com.service.NatureEntreeService;
import bf.personnel.com.service.dto.NatureEntreeDTO;
import bf.personnel.com.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link bf.personnel.com.domain.NatureEntree}.
 */
@RestController
@RequestMapping("/api")
public class NatureEntreeResource {

    private final Logger log = LoggerFactory.getLogger(NatureEntreeResource.class);

    private static final String ENTITY_NAME = "natureEntree";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final NatureEntreeService natureEntreeService;

    private final NatureEntreeRepository natureEntreeRepository;

    public NatureEntreeResource(NatureEntreeService natureEntreeService, NatureEntreeRepository natureEntreeRepository) {
        this.natureEntreeService = natureEntreeService;
        this.natureEntreeRepository = natureEntreeRepository;
    }

    /**
     * {@code POST  /nature-entrees} : Create a new natureEntree.
     *
     * @param natureEntreeDTO the natureEntreeDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new natureEntreeDTO, or with status {@code 400 (Bad Request)} if the natureEntree has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/nature-entrees")
    public ResponseEntity<NatureEntreeDTO> createNatureEntree(@RequestBody NatureEntreeDTO natureEntreeDTO) throws URISyntaxException {
        log.debug("REST request to save NatureEntree : {}", natureEntreeDTO);
        if (natureEntreeDTO.getId() != null) {
            throw new BadRequestAlertException("A new natureEntree cannot already have an ID", ENTITY_NAME, "idexists");
        }
        NatureEntreeDTO result = natureEntreeService.save(natureEntreeDTO);
        return ResponseEntity
            .created(new URI("/api/nature-entrees/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /nature-entrees/:id} : Updates an existing natureEntree.
     *
     * @param id the id of the natureEntreeDTO to save.
     * @param natureEntreeDTO the natureEntreeDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated natureEntreeDTO,
     * or with status {@code 400 (Bad Request)} if the natureEntreeDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the natureEntreeDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/nature-entrees/{id}")
    public ResponseEntity<NatureEntreeDTO> updateNatureEntree(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody NatureEntreeDTO natureEntreeDTO
    ) throws URISyntaxException {
        log.debug("REST request to update NatureEntree : {}, {}", id, natureEntreeDTO);
        if (natureEntreeDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, natureEntreeDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!natureEntreeRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        NatureEntreeDTO result = natureEntreeService.update(natureEntreeDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, natureEntreeDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /nature-entrees/:id} : Partial updates given fields of an existing natureEntree, field will ignore if it is null
     *
     * @param id the id of the natureEntreeDTO to save.
     * @param natureEntreeDTO the natureEntreeDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated natureEntreeDTO,
     * or with status {@code 400 (Bad Request)} if the natureEntreeDTO is not valid,
     * or with status {@code 404 (Not Found)} if the natureEntreeDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the natureEntreeDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/nature-entrees/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<NatureEntreeDTO> partialUpdateNatureEntree(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody NatureEntreeDTO natureEntreeDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update NatureEntree partially : {}, {}", id, natureEntreeDTO);
        if (natureEntreeDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, natureEntreeDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!natureEntreeRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<NatureEntreeDTO> result = natureEntreeService.partialUpdate(natureEntreeDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, natureEntreeDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /nature-entrees} : get all the natureEntrees.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of natureEntrees in body.
     */
    @GetMapping("/nature-entrees")
    public List<NatureEntreeDTO> getAllNatureEntrees() {
        log.debug("REST request to get all NatureEntrees");
        return natureEntreeService.findAll();
    }

    /**
     * {@code GET  /nature-entrees/:id} : get the "id" natureEntree.
     *
     * @param id the id of the natureEntreeDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the natureEntreeDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/nature-entrees/{id}")
    public ResponseEntity<NatureEntreeDTO> getNatureEntree(@PathVariable Long id) {
        log.debug("REST request to get NatureEntree : {}", id);
        Optional<NatureEntreeDTO> natureEntreeDTO = natureEntreeService.findOne(id);
        return ResponseUtil.wrapOrNotFound(natureEntreeDTO);
    }

    /**
     * {@code DELETE  /nature-entrees/:id} : delete the "id" natureEntree.
     *
     * @param id the id of the natureEntreeDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/nature-entrees/{id}")
    public ResponseEntity<Void> deleteNatureEntree(@PathVariable Long id) {
        log.debug("REST request to delete NatureEntree : {}", id);
        natureEntreeService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}

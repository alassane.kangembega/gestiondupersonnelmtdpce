package bf.personnel.com.web.rest;

import bf.personnel.com.repository.NatureSortieRepository;
import bf.personnel.com.service.NatureSortieService;
import bf.personnel.com.service.dto.NatureSortieDTO;
import bf.personnel.com.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link bf.personnel.com.domain.NatureSortie}.
 */
@RestController
@RequestMapping("/api")
public class NatureSortieResource {

    private final Logger log = LoggerFactory.getLogger(NatureSortieResource.class);

    private static final String ENTITY_NAME = "natureSortie";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final NatureSortieService natureSortieService;

    private final NatureSortieRepository natureSortieRepository;

    public NatureSortieResource(NatureSortieService natureSortieService, NatureSortieRepository natureSortieRepository) {
        this.natureSortieService = natureSortieService;
        this.natureSortieRepository = natureSortieRepository;
    }

    /**
     * {@code POST  /nature-sorties} : Create a new natureSortie.
     *
     * @param natureSortieDTO the natureSortieDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new natureSortieDTO, or with status {@code 400 (Bad Request)} if the natureSortie has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/nature-sorties")
    public ResponseEntity<NatureSortieDTO> createNatureSortie(@RequestBody NatureSortieDTO natureSortieDTO) throws URISyntaxException {
        log.debug("REST request to save NatureSortie : {}", natureSortieDTO);
        if (natureSortieDTO.getId() != null) {
            throw new BadRequestAlertException("A new natureSortie cannot already have an ID", ENTITY_NAME, "idexists");
        }
        NatureSortieDTO result = natureSortieService.save(natureSortieDTO);
        return ResponseEntity
            .created(new URI("/api/nature-sorties/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /nature-sorties/:id} : Updates an existing natureSortie.
     *
     * @param id the id of the natureSortieDTO to save.
     * @param natureSortieDTO the natureSortieDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated natureSortieDTO,
     * or with status {@code 400 (Bad Request)} if the natureSortieDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the natureSortieDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/nature-sorties/{id}")
    public ResponseEntity<NatureSortieDTO> updateNatureSortie(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody NatureSortieDTO natureSortieDTO
    ) throws URISyntaxException {
        log.debug("REST request to update NatureSortie : {}, {}", id, natureSortieDTO);
        if (natureSortieDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, natureSortieDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!natureSortieRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        NatureSortieDTO result = natureSortieService.update(natureSortieDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, natureSortieDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /nature-sorties/:id} : Partial updates given fields of an existing natureSortie, field will ignore if it is null
     *
     * @param id the id of the natureSortieDTO to save.
     * @param natureSortieDTO the natureSortieDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated natureSortieDTO,
     * or with status {@code 400 (Bad Request)} if the natureSortieDTO is not valid,
     * or with status {@code 404 (Not Found)} if the natureSortieDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the natureSortieDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/nature-sorties/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<NatureSortieDTO> partialUpdateNatureSortie(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody NatureSortieDTO natureSortieDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update NatureSortie partially : {}, {}", id, natureSortieDTO);
        if (natureSortieDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, natureSortieDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!natureSortieRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<NatureSortieDTO> result = natureSortieService.partialUpdate(natureSortieDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, natureSortieDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /nature-sorties} : get all the natureSorties.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of natureSorties in body.
     */
    @GetMapping("/nature-sorties")
    public List<NatureSortieDTO> getAllNatureSorties() {
        log.debug("REST request to get all NatureSorties");
        return natureSortieService.findAll();
    }

    /**
     * {@code GET  /nature-sorties/:id} : get the "id" natureSortie.
     *
     * @param id the id of the natureSortieDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the natureSortieDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/nature-sorties/{id}")
    public ResponseEntity<NatureSortieDTO> getNatureSortie(@PathVariable Long id) {
        log.debug("REST request to get NatureSortie : {}", id);
        Optional<NatureSortieDTO> natureSortieDTO = natureSortieService.findOne(id);
        return ResponseUtil.wrapOrNotFound(natureSortieDTO);
    }

    /**
     * {@code DELETE  /nature-sorties/:id} : delete the "id" natureSortie.
     *
     * @param id the id of the natureSortieDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/nature-sorties/{id}")
    public ResponseEntity<Void> deleteNatureSortie(@PathVariable Long id) {
        log.debug("REST request to delete NatureSortie : {}", id);
        natureSortieService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}

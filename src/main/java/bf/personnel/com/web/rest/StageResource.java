package bf.personnel.com.web.rest;

import bf.personnel.com.repository.StageRepository;
import bf.personnel.com.service.StageService;
import bf.personnel.com.service.dto.StageDTO;
import bf.personnel.com.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link bf.personnel.com.domain.Stage}.
 */
@RestController
@RequestMapping("/api")
public class StageResource {

    private final Logger log = LoggerFactory.getLogger(StageResource.class);

    private static final String ENTITY_NAME = "stage";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final StageService stageService;

    private final StageRepository stageRepository;

    public StageResource(StageService stageService, StageRepository stageRepository) {
        this.stageService = stageService;
        this.stageRepository = stageRepository;
    }

    /**
     * {@code POST  /stages} : Create a new stage.
     *
     * @param stageDTO the stageDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new stageDTO, or with status {@code 400 (Bad Request)} if the stage has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/stages")
    public ResponseEntity<StageDTO> createStage(@RequestBody StageDTO stageDTO) throws URISyntaxException {
        log.debug("REST request to save Stage : {}", stageDTO);
        if (stageDTO.getId() != null) {
            throw new BadRequestAlertException("A new stage cannot already have an ID", ENTITY_NAME, "idexists");
        }
        StageDTO result = stageService.save(stageDTO);
        return ResponseEntity
            .created(new URI("/api/stages/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /stages/:id} : Updates an existing stage.
     *
     * @param id the id of the stageDTO to save.
     * @param stageDTO the stageDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated stageDTO,
     * or with status {@code 400 (Bad Request)} if the stageDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the stageDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/stages/{id}")
    public ResponseEntity<StageDTO> updateStage(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody StageDTO stageDTO
    ) throws URISyntaxException {
        log.debug("REST request to update Stage : {}, {}", id, stageDTO);
        if (stageDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, stageDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!stageRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        StageDTO result = stageService.update(stageDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, stageDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /stages/:id} : Partial updates given fields of an existing stage, field will ignore if it is null
     *
     * @param id the id of the stageDTO to save.
     * @param stageDTO the stageDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated stageDTO,
     * or with status {@code 400 (Bad Request)} if the stageDTO is not valid,
     * or with status {@code 404 (Not Found)} if the stageDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the stageDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/stages/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<StageDTO> partialUpdateStage(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody StageDTO stageDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update Stage partially : {}, {}", id, stageDTO);
        if (stageDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, stageDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!stageRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<StageDTO> result = stageService.partialUpdate(stageDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, stageDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /stages} : get all the stages.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of stages in body.
     */
    @GetMapping("/stages")
    public List<StageDTO> getAllStages() {
        log.debug("REST request to get all Stages");
        return stageService.findAll();
    }

    /**
     * {@code GET  /stages/:id} : get the "id" stage.
     *
     * @param id the id of the stageDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the stageDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/stages/{id}")
    public ResponseEntity<StageDTO> getStage(@PathVariable Long id) {
        log.debug("REST request to get Stage : {}", id);
        Optional<StageDTO> stageDTO = stageService.findOne(id);
        return ResponseUtil.wrapOrNotFound(stageDTO);
    }

    /**
     * {@code DELETE  /stages/:id} : delete the "id" stage.
     *
     * @param id the id of the stageDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/stages/{id}")
    public ResponseEntity<Void> deleteStage(@PathVariable Long id) {
        log.debug("REST request to delete Stage : {}", id);
        stageService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}

package bf.personnel.com.web.rest;

import bf.personnel.com.repository.AvancementCategorieRepository;
import bf.personnel.com.service.AvancementCategorieService;
import bf.personnel.com.service.dto.AvancementCategorieDTO;
import bf.personnel.com.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link bf.personnel.com.domain.AvancementCategorie}.
 */
@RestController
@RequestMapping("/api")
public class AvancementCategorieResource {

    private final Logger log = LoggerFactory.getLogger(AvancementCategorieResource.class);

    private static final String ENTITY_NAME = "avancementCategorie";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final AvancementCategorieService avancementCategorieService;

    private final AvancementCategorieRepository avancementCategorieRepository;

    public AvancementCategorieResource(
        AvancementCategorieService avancementCategorieService,
        AvancementCategorieRepository avancementCategorieRepository
    ) {
        this.avancementCategorieService = avancementCategorieService;
        this.avancementCategorieRepository = avancementCategorieRepository;
    }

    /**
     * {@code POST  /avancement-categories} : Create a new avancementCategorie.
     *
     * @param avancementCategorieDTO the avancementCategorieDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new avancementCategorieDTO, or with status {@code 400 (Bad Request)} if the avancementCategorie has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/avancement-categories")
    public ResponseEntity<AvancementCategorieDTO> createAvancementCategorie(@RequestBody AvancementCategorieDTO avancementCategorieDTO)
        throws URISyntaxException {
        log.debug("REST request to save AvancementCategorie : {}", avancementCategorieDTO);
        if (avancementCategorieDTO.getId() != null) {
            throw new BadRequestAlertException("A new avancementCategorie cannot already have an ID", ENTITY_NAME, "idexists");
        }
        AvancementCategorieDTO result = avancementCategorieService.save(avancementCategorieDTO);
        return ResponseEntity
            .created(new URI("/api/avancement-categories/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /avancement-categories/:id} : Updates an existing avancementCategorie.
     *
     * @param id the id of the avancementCategorieDTO to save.
     * @param avancementCategorieDTO the avancementCategorieDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated avancementCategorieDTO,
     * or with status {@code 400 (Bad Request)} if the avancementCategorieDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the avancementCategorieDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/avancement-categories/{id}")
    public ResponseEntity<AvancementCategorieDTO> updateAvancementCategorie(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody AvancementCategorieDTO avancementCategorieDTO
    ) throws URISyntaxException {
        log.debug("REST request to update AvancementCategorie : {}, {}", id, avancementCategorieDTO);
        if (avancementCategorieDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, avancementCategorieDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!avancementCategorieRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        AvancementCategorieDTO result = avancementCategorieService.update(avancementCategorieDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, avancementCategorieDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /avancement-categories/:id} : Partial updates given fields of an existing avancementCategorie, field will ignore if it is null
     *
     * @param id the id of the avancementCategorieDTO to save.
     * @param avancementCategorieDTO the avancementCategorieDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated avancementCategorieDTO,
     * or with status {@code 400 (Bad Request)} if the avancementCategorieDTO is not valid,
     * or with status {@code 404 (Not Found)} if the avancementCategorieDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the avancementCategorieDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/avancement-categories/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<AvancementCategorieDTO> partialUpdateAvancementCategorie(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody AvancementCategorieDTO avancementCategorieDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update AvancementCategorie partially : {}, {}", id, avancementCategorieDTO);
        if (avancementCategorieDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, avancementCategorieDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!avancementCategorieRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<AvancementCategorieDTO> result = avancementCategorieService.partialUpdate(avancementCategorieDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, avancementCategorieDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /avancement-categories} : get all the avancementCategories.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of avancementCategories in body.
     */
    @GetMapping("/avancement-categories")
    public List<AvancementCategorieDTO> getAllAvancementCategories() {
        log.debug("REST request to get all AvancementCategories");
        return avancementCategorieService.findAll();
    }

    /**
     * {@code GET  /avancement-categories/:id} : get the "id" avancementCategorie.
     *
     * @param id the id of the avancementCategorieDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the avancementCategorieDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/avancement-categories/{id}")
    public ResponseEntity<AvancementCategorieDTO> getAvancementCategorie(@PathVariable Long id) {
        log.debug("REST request to get AvancementCategorie : {}", id);
        Optional<AvancementCategorieDTO> avancementCategorieDTO = avancementCategorieService.findOne(id);
        return ResponseUtil.wrapOrNotFound(avancementCategorieDTO);
    }

    /**
     * {@code DELETE  /avancement-categories/:id} : delete the "id" avancementCategorie.
     *
     * @param id the id of the avancementCategorieDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/avancement-categories/{id}")
    public ResponseEntity<Void> deleteAvancementCategorie(@PathVariable Long id) {
        log.debug("REST request to delete AvancementCategorie : {}", id);
        avancementCategorieService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}

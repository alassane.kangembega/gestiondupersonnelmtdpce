package bf.personnel.com.web.rest;

import bf.personnel.com.repository.TypeDecorationRepository;
import bf.personnel.com.service.TypeDecorationService;
import bf.personnel.com.service.dto.TypeDecorationDTO;
import bf.personnel.com.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link bf.personnel.com.domain.TypeDecoration}.
 */
@RestController
@RequestMapping("/api")
public class TypeDecorationResource {

    private final Logger log = LoggerFactory.getLogger(TypeDecorationResource.class);

    private static final String ENTITY_NAME = "typeDecoration";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final TypeDecorationService typeDecorationService;

    private final TypeDecorationRepository typeDecorationRepository;

    public TypeDecorationResource(TypeDecorationService typeDecorationService, TypeDecorationRepository typeDecorationRepository) {
        this.typeDecorationService = typeDecorationService;
        this.typeDecorationRepository = typeDecorationRepository;
    }

    /**
     * {@code POST  /type-decorations} : Create a new typeDecoration.
     *
     * @param typeDecorationDTO the typeDecorationDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new typeDecorationDTO, or with status {@code 400 (Bad Request)} if the typeDecoration has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/type-decorations")
    public ResponseEntity<TypeDecorationDTO> createTypeDecoration(@RequestBody TypeDecorationDTO typeDecorationDTO)
        throws URISyntaxException {
        log.debug("REST request to save TypeDecoration : {}", typeDecorationDTO);
        if (typeDecorationDTO.getId() != null) {
            throw new BadRequestAlertException("A new typeDecoration cannot already have an ID", ENTITY_NAME, "idexists");
        }
        TypeDecorationDTO result = typeDecorationService.save(typeDecorationDTO);
        return ResponseEntity
            .created(new URI("/api/type-decorations/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /type-decorations/:id} : Updates an existing typeDecoration.
     *
     * @param id the id of the typeDecorationDTO to save.
     * @param typeDecorationDTO the typeDecorationDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated typeDecorationDTO,
     * or with status {@code 400 (Bad Request)} if the typeDecorationDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the typeDecorationDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/type-decorations/{id}")
    public ResponseEntity<TypeDecorationDTO> updateTypeDecoration(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody TypeDecorationDTO typeDecorationDTO
    ) throws URISyntaxException {
        log.debug("REST request to update TypeDecoration : {}, {}", id, typeDecorationDTO);
        if (typeDecorationDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, typeDecorationDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!typeDecorationRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        TypeDecorationDTO result = typeDecorationService.update(typeDecorationDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, typeDecorationDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /type-decorations/:id} : Partial updates given fields of an existing typeDecoration, field will ignore if it is null
     *
     * @param id the id of the typeDecorationDTO to save.
     * @param typeDecorationDTO the typeDecorationDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated typeDecorationDTO,
     * or with status {@code 400 (Bad Request)} if the typeDecorationDTO is not valid,
     * or with status {@code 404 (Not Found)} if the typeDecorationDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the typeDecorationDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/type-decorations/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<TypeDecorationDTO> partialUpdateTypeDecoration(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody TypeDecorationDTO typeDecorationDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update TypeDecoration partially : {}, {}", id, typeDecorationDTO);
        if (typeDecorationDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, typeDecorationDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!typeDecorationRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<TypeDecorationDTO> result = typeDecorationService.partialUpdate(typeDecorationDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, typeDecorationDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /type-decorations} : get all the typeDecorations.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of typeDecorations in body.
     */
    @GetMapping("/type-decorations")
    public List<TypeDecorationDTO> getAllTypeDecorations() {
        log.debug("REST request to get all TypeDecorations");
        return typeDecorationService.findAll();
    }

    /**
     * {@code GET  /type-decorations/:id} : get the "id" typeDecoration.
     *
     * @param id the id of the typeDecorationDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the typeDecorationDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/type-decorations/{id}")
    public ResponseEntity<TypeDecorationDTO> getTypeDecoration(@PathVariable Long id) {
        log.debug("REST request to get TypeDecoration : {}", id);
        Optional<TypeDecorationDTO> typeDecorationDTO = typeDecorationService.findOne(id);
        return ResponseUtil.wrapOrNotFound(typeDecorationDTO);
    }

    /**
     * {@code DELETE  /type-decorations/:id} : delete the "id" typeDecoration.
     *
     * @param id the id of the typeDecorationDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/type-decorations/{id}")
    public ResponseEntity<Void> deleteTypeDecoration(@PathVariable Long id) {
        log.debug("REST request to delete TypeDecoration : {}", id);
        typeDecorationService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}

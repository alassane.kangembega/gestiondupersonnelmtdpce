package bf.personnel.com.web.rest;

import bf.personnel.com.repository.NaturePositionIrreguliereRepository;
import bf.personnel.com.service.NaturePositionIrreguliereService;
import bf.personnel.com.service.dto.NaturePositionIrreguliereDTO;
import bf.personnel.com.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link bf.personnel.com.domain.NaturePositionIrreguliere}.
 */
@RestController
@RequestMapping("/api")
public class NaturePositionIrreguliereResource {

    private final Logger log = LoggerFactory.getLogger(NaturePositionIrreguliereResource.class);

    private static final String ENTITY_NAME = "naturePositionIrreguliere";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final NaturePositionIrreguliereService naturePositionIrreguliereService;

    private final NaturePositionIrreguliereRepository naturePositionIrreguliereRepository;

    public NaturePositionIrreguliereResource(
        NaturePositionIrreguliereService naturePositionIrreguliereService,
        NaturePositionIrreguliereRepository naturePositionIrreguliereRepository
    ) {
        this.naturePositionIrreguliereService = naturePositionIrreguliereService;
        this.naturePositionIrreguliereRepository = naturePositionIrreguliereRepository;
    }

    /**
     * {@code POST  /nature-position-irregulieres} : Create a new naturePositionIrreguliere.
     *
     * @param naturePositionIrreguliereDTO the naturePositionIrreguliereDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new naturePositionIrreguliereDTO, or with status {@code 400 (Bad Request)} if the naturePositionIrreguliere has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/nature-position-irregulieres")
    public ResponseEntity<NaturePositionIrreguliereDTO> createNaturePositionIrreguliere(
        @RequestBody NaturePositionIrreguliereDTO naturePositionIrreguliereDTO
    ) throws URISyntaxException {
        log.debug("REST request to save NaturePositionIrreguliere : {}", naturePositionIrreguliereDTO);
        if (naturePositionIrreguliereDTO.getId() != null) {
            throw new BadRequestAlertException("A new naturePositionIrreguliere cannot already have an ID", ENTITY_NAME, "idexists");
        }
        NaturePositionIrreguliereDTO result = naturePositionIrreguliereService.save(naturePositionIrreguliereDTO);
        return ResponseEntity
            .created(new URI("/api/nature-position-irregulieres/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /nature-position-irregulieres/:id} : Updates an existing naturePositionIrreguliere.
     *
     * @param id the id of the naturePositionIrreguliereDTO to save.
     * @param naturePositionIrreguliereDTO the naturePositionIrreguliereDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated naturePositionIrreguliereDTO,
     * or with status {@code 400 (Bad Request)} if the naturePositionIrreguliereDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the naturePositionIrreguliereDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/nature-position-irregulieres/{id}")
    public ResponseEntity<NaturePositionIrreguliereDTO> updateNaturePositionIrreguliere(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody NaturePositionIrreguliereDTO naturePositionIrreguliereDTO
    ) throws URISyntaxException {
        log.debug("REST request to update NaturePositionIrreguliere : {}, {}", id, naturePositionIrreguliereDTO);
        if (naturePositionIrreguliereDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, naturePositionIrreguliereDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!naturePositionIrreguliereRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        NaturePositionIrreguliereDTO result = naturePositionIrreguliereService.update(naturePositionIrreguliereDTO);
        return ResponseEntity
            .ok()
            .headers(
                HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, naturePositionIrreguliereDTO.getId().toString())
            )
            .body(result);
    }

    /**
     * {@code PATCH  /nature-position-irregulieres/:id} : Partial updates given fields of an existing naturePositionIrreguliere, field will ignore if it is null
     *
     * @param id the id of the naturePositionIrreguliereDTO to save.
     * @param naturePositionIrreguliereDTO the naturePositionIrreguliereDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated naturePositionIrreguliereDTO,
     * or with status {@code 400 (Bad Request)} if the naturePositionIrreguliereDTO is not valid,
     * or with status {@code 404 (Not Found)} if the naturePositionIrreguliereDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the naturePositionIrreguliereDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/nature-position-irregulieres/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<NaturePositionIrreguliereDTO> partialUpdateNaturePositionIrreguliere(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody NaturePositionIrreguliereDTO naturePositionIrreguliereDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update NaturePositionIrreguliere partially : {}, {}", id, naturePositionIrreguliereDTO);
        if (naturePositionIrreguliereDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, naturePositionIrreguliereDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!naturePositionIrreguliereRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<NaturePositionIrreguliereDTO> result = naturePositionIrreguliereService.partialUpdate(naturePositionIrreguliereDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, naturePositionIrreguliereDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /nature-position-irregulieres} : get all the naturePositionIrregulieres.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of naturePositionIrregulieres in body.
     */
    @GetMapping("/nature-position-irregulieres")
    public List<NaturePositionIrreguliereDTO> getAllNaturePositionIrregulieres() {
        log.debug("REST request to get all NaturePositionIrregulieres");
        return naturePositionIrreguliereService.findAll();
    }

    /**
     * {@code GET  /nature-position-irregulieres/:id} : get the "id" naturePositionIrreguliere.
     *
     * @param id the id of the naturePositionIrreguliereDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the naturePositionIrreguliereDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/nature-position-irregulieres/{id}")
    public ResponseEntity<NaturePositionIrreguliereDTO> getNaturePositionIrreguliere(@PathVariable Long id) {
        log.debug("REST request to get NaturePositionIrreguliere : {}", id);
        Optional<NaturePositionIrreguliereDTO> naturePositionIrreguliereDTO = naturePositionIrreguliereService.findOne(id);
        return ResponseUtil.wrapOrNotFound(naturePositionIrreguliereDTO);
    }

    /**
     * {@code DELETE  /nature-position-irregulieres/:id} : delete the "id" naturePositionIrreguliere.
     *
     * @param id the id of the naturePositionIrreguliereDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/nature-position-irregulieres/{id}")
    public ResponseEntity<Void> deleteNaturePositionIrreguliere(@PathVariable Long id) {
        log.debug("REST request to delete NaturePositionIrreguliere : {}", id);
        naturePositionIrreguliereService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}

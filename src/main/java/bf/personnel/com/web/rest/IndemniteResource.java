package bf.personnel.com.web.rest;

import bf.personnel.com.repository.IndemniteRepository;
import bf.personnel.com.service.IndemniteService;
import bf.personnel.com.service.dto.IndemniteDTO;
import bf.personnel.com.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link bf.personnel.com.domain.Indemnite}.
 */
@RestController
@RequestMapping("/api")
public class IndemniteResource {

    private final Logger log = LoggerFactory.getLogger(IndemniteResource.class);

    private static final String ENTITY_NAME = "indemnite";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final IndemniteService indemniteService;

    private final IndemniteRepository indemniteRepository;

    public IndemniteResource(IndemniteService indemniteService, IndemniteRepository indemniteRepository) {
        this.indemniteService = indemniteService;
        this.indemniteRepository = indemniteRepository;
    }

    /**
     * {@code POST  /indemnites} : Create a new indemnite.
     *
     * @param indemniteDTO the indemniteDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new indemniteDTO, or with status {@code 400 (Bad Request)} if the indemnite has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/indemnites")
    public ResponseEntity<IndemniteDTO> createIndemnite(@RequestBody IndemniteDTO indemniteDTO) throws URISyntaxException {
        log.debug("REST request to save Indemnite : {}", indemniteDTO);
        if (indemniteDTO.getId() != null) {
            throw new BadRequestAlertException("A new indemnite cannot already have an ID", ENTITY_NAME, "idexists");
        }
        IndemniteDTO result = indemniteService.save(indemniteDTO);
        return ResponseEntity
            .created(new URI("/api/indemnites/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /indemnites/:id} : Updates an existing indemnite.
     *
     * @param id the id of the indemniteDTO to save.
     * @param indemniteDTO the indemniteDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated indemniteDTO,
     * or with status {@code 400 (Bad Request)} if the indemniteDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the indemniteDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/indemnites/{id}")
    public ResponseEntity<IndemniteDTO> updateIndemnite(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody IndemniteDTO indemniteDTO
    ) throws URISyntaxException {
        log.debug("REST request to update Indemnite : {}, {}", id, indemniteDTO);
        if (indemniteDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, indemniteDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!indemniteRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        IndemniteDTO result = indemniteService.update(indemniteDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, indemniteDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /indemnites/:id} : Partial updates given fields of an existing indemnite, field will ignore if it is null
     *
     * @param id the id of the indemniteDTO to save.
     * @param indemniteDTO the indemniteDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated indemniteDTO,
     * or with status {@code 400 (Bad Request)} if the indemniteDTO is not valid,
     * or with status {@code 404 (Not Found)} if the indemniteDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the indemniteDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/indemnites/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<IndemniteDTO> partialUpdateIndemnite(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody IndemniteDTO indemniteDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update Indemnite partially : {}, {}", id, indemniteDTO);
        if (indemniteDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, indemniteDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!indemniteRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<IndemniteDTO> result = indemniteService.partialUpdate(indemniteDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, indemniteDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /indemnites} : get all the indemnites.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of indemnites in body.
     */
    @GetMapping("/indemnites")
    public List<IndemniteDTO> getAllIndemnites() {
        log.debug("REST request to get all Indemnites");
        return indemniteService.findAll();
    }

    /**
     * {@code GET  /indemnites/:id} : get the "id" indemnite.
     *
     * @param id the id of the indemniteDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the indemniteDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/indemnites/{id}")
    public ResponseEntity<IndemniteDTO> getIndemnite(@PathVariable Long id) {
        log.debug("REST request to get Indemnite : {}", id);
        Optional<IndemniteDTO> indemniteDTO = indemniteService.findOne(id);
        return ResponseUtil.wrapOrNotFound(indemniteDTO);
    }

    /**
     * {@code DELETE  /indemnites/:id} : delete the "id" indemnite.
     *
     * @param id the id of the indemniteDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/indemnites/{id}")
    public ResponseEntity<Void> deleteIndemnite(@PathVariable Long id) {
        log.debug("REST request to delete Indemnite : {}", id);
        indemniteService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}

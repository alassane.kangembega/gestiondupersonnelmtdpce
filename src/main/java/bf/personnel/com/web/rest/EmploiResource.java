package bf.personnel.com.web.rest;

import bf.personnel.com.repository.EmploiRepository;
import bf.personnel.com.service.EmploiService;
import bf.personnel.com.service.dto.EmploiDTO;
import bf.personnel.com.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link bf.personnel.com.domain.Emploi}.
 */
@RestController
@RequestMapping("/api")
public class EmploiResource {

    private final Logger log = LoggerFactory.getLogger(EmploiResource.class);

    private static final String ENTITY_NAME = "emploi";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final EmploiService emploiService;

    private final EmploiRepository emploiRepository;

    public EmploiResource(EmploiService emploiService, EmploiRepository emploiRepository) {
        this.emploiService = emploiService;
        this.emploiRepository = emploiRepository;
    }

    /**
     * {@code POST  /emplois} : Create a new emploi.
     *
     * @param emploiDTO the emploiDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new emploiDTO, or with status {@code 400 (Bad Request)} if the emploi has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/emplois")
    public ResponseEntity<EmploiDTO> createEmploi(@RequestBody EmploiDTO emploiDTO) throws URISyntaxException {
        log.debug("REST request to save Emploi : {}", emploiDTO);
        if (emploiDTO.getId() != null) {
            throw new BadRequestAlertException("A new emploi cannot already have an ID", ENTITY_NAME, "idexists");
        }
        EmploiDTO result = emploiService.save(emploiDTO);
        return ResponseEntity
            .created(new URI("/api/emplois/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /emplois/:id} : Updates an existing emploi.
     *
     * @param id the id of the emploiDTO to save.
     * @param emploiDTO the emploiDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated emploiDTO,
     * or with status {@code 400 (Bad Request)} if the emploiDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the emploiDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/emplois/{id}")
    public ResponseEntity<EmploiDTO> updateEmploi(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody EmploiDTO emploiDTO
    ) throws URISyntaxException {
        log.debug("REST request to update Emploi : {}, {}", id, emploiDTO);
        if (emploiDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, emploiDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!emploiRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        EmploiDTO result = emploiService.update(emploiDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, emploiDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /emplois/:id} : Partial updates given fields of an existing emploi, field will ignore if it is null
     *
     * @param id the id of the emploiDTO to save.
     * @param emploiDTO the emploiDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated emploiDTO,
     * or with status {@code 400 (Bad Request)} if the emploiDTO is not valid,
     * or with status {@code 404 (Not Found)} if the emploiDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the emploiDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/emplois/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<EmploiDTO> partialUpdateEmploi(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody EmploiDTO emploiDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update Emploi partially : {}, {}", id, emploiDTO);
        if (emploiDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, emploiDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!emploiRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<EmploiDTO> result = emploiService.partialUpdate(emploiDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, emploiDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /emplois} : get all the emplois.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of emplois in body.
     */
    @GetMapping("/emplois")
    public List<EmploiDTO> getAllEmplois() {
        log.debug("REST request to get all Emplois");
        return emploiService.findAll();
    }

    /**
     * {@code GET  /emplois/:id} : get the "id" emploi.
     *
     * @param id the id of the emploiDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the emploiDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/emplois/{id}")
    public ResponseEntity<EmploiDTO> getEmploi(@PathVariable Long id) {
        log.debug("REST request to get Emploi : {}", id);
        Optional<EmploiDTO> emploiDTO = emploiService.findOne(id);
        return ResponseUtil.wrapOrNotFound(emploiDTO);
    }

    /**
     * {@code DELETE  /emplois/:id} : delete the "id" emploi.
     *
     * @param id the id of the emploiDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/emplois/{id}")
    public ResponseEntity<Void> deleteEmploi(@PathVariable Long id) {
        log.debug("REST request to delete Emploi : {}", id);
        emploiService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}

package bf.personnel.com.web.rest;

import bf.personnel.com.repository.AvancementClasseRepository;
import bf.personnel.com.service.AvancementClasseService;
import bf.personnel.com.service.dto.AvancementClasseDTO;
import bf.personnel.com.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link bf.personnel.com.domain.AvancementClasse}.
 */
@RestController
@RequestMapping("/api")
public class AvancementClasseResource {

    private final Logger log = LoggerFactory.getLogger(AvancementClasseResource.class);

    private static final String ENTITY_NAME = "avancementClasse";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final AvancementClasseService avancementClasseService;

    private final AvancementClasseRepository avancementClasseRepository;

    public AvancementClasseResource(
        AvancementClasseService avancementClasseService,
        AvancementClasseRepository avancementClasseRepository
    ) {
        this.avancementClasseService = avancementClasseService;
        this.avancementClasseRepository = avancementClasseRepository;
    }

    /**
     * {@code POST  /avancement-classes} : Create a new avancementClasse.
     *
     * @param avancementClasseDTO the avancementClasseDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new avancementClasseDTO, or with status {@code 400 (Bad Request)} if the avancementClasse has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/avancement-classes")
    public ResponseEntity<AvancementClasseDTO> createAvancementClasse(@RequestBody AvancementClasseDTO avancementClasseDTO)
        throws URISyntaxException {
        log.debug("REST request to save AvancementClasse : {}", avancementClasseDTO);
        if (avancementClasseDTO.getId() != null) {
            throw new BadRequestAlertException("A new avancementClasse cannot already have an ID", ENTITY_NAME, "idexists");
        }
        AvancementClasseDTO result = avancementClasseService.save(avancementClasseDTO);
        return ResponseEntity
            .created(new URI("/api/avancement-classes/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /avancement-classes/:id} : Updates an existing avancementClasse.
     *
     * @param id the id of the avancementClasseDTO to save.
     * @param avancementClasseDTO the avancementClasseDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated avancementClasseDTO,
     * or with status {@code 400 (Bad Request)} if the avancementClasseDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the avancementClasseDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/avancement-classes/{id}")
    public ResponseEntity<AvancementClasseDTO> updateAvancementClasse(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody AvancementClasseDTO avancementClasseDTO
    ) throws URISyntaxException {
        log.debug("REST request to update AvancementClasse : {}, {}", id, avancementClasseDTO);
        if (avancementClasseDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, avancementClasseDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!avancementClasseRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        AvancementClasseDTO result = avancementClasseService.update(avancementClasseDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, avancementClasseDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /avancement-classes/:id} : Partial updates given fields of an existing avancementClasse, field will ignore if it is null
     *
     * @param id the id of the avancementClasseDTO to save.
     * @param avancementClasseDTO the avancementClasseDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated avancementClasseDTO,
     * or with status {@code 400 (Bad Request)} if the avancementClasseDTO is not valid,
     * or with status {@code 404 (Not Found)} if the avancementClasseDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the avancementClasseDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/avancement-classes/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<AvancementClasseDTO> partialUpdateAvancementClasse(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody AvancementClasseDTO avancementClasseDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update AvancementClasse partially : {}, {}", id, avancementClasseDTO);
        if (avancementClasseDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, avancementClasseDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!avancementClasseRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<AvancementClasseDTO> result = avancementClasseService.partialUpdate(avancementClasseDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, avancementClasseDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /avancement-classes} : get all the avancementClasses.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of avancementClasses in body.
     */
    @GetMapping("/avancement-classes")
    public List<AvancementClasseDTO> getAllAvancementClasses() {
        log.debug("REST request to get all AvancementClasses");
        return avancementClasseService.findAll();
    }

    /**
     * {@code GET  /avancement-classes/:id} : get the "id" avancementClasse.
     *
     * @param id the id of the avancementClasseDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the avancementClasseDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/avancement-classes/{id}")
    public ResponseEntity<AvancementClasseDTO> getAvancementClasse(@PathVariable Long id) {
        log.debug("REST request to get AvancementClasse : {}", id);
        Optional<AvancementClasseDTO> avancementClasseDTO = avancementClasseService.findOne(id);
        return ResponseUtil.wrapOrNotFound(avancementClasseDTO);
    }

    /**
     * {@code DELETE  /avancement-classes/:id} : delete the "id" avancementClasse.
     *
     * @param id the id of the avancementClasseDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/avancement-classes/{id}")
    public ResponseEntity<Void> deleteAvancementClasse(@PathVariable Long id) {
        log.debug("REST request to delete AvancementClasse : {}", id);
        avancementClasseService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}

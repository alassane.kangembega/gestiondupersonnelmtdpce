package bf.personnel.com.web.rest;

import bf.personnel.com.repository.EchelonRepository;
import bf.personnel.com.service.EchelonService;
import bf.personnel.com.service.dto.EchelonDTO;
import bf.personnel.com.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link bf.personnel.com.domain.Echelon}.
 */
@RestController
@RequestMapping("/api")
public class EchelonResource {

    private final Logger log = LoggerFactory.getLogger(EchelonResource.class);

    private static final String ENTITY_NAME = "echelon";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final EchelonService echelonService;

    private final EchelonRepository echelonRepository;

    public EchelonResource(EchelonService echelonService, EchelonRepository echelonRepository) {
        this.echelonService = echelonService;
        this.echelonRepository = echelonRepository;
    }

    /**
     * {@code POST  /echelons} : Create a new echelon.
     *
     * @param echelonDTO the echelonDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new echelonDTO, or with status {@code 400 (Bad Request)} if the echelon has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/echelons")
    public ResponseEntity<EchelonDTO> createEchelon(@RequestBody EchelonDTO echelonDTO) throws URISyntaxException {
        log.debug("REST request to save Echelon : {}", echelonDTO);
        if (echelonDTO.getId() != null) {
            throw new BadRequestAlertException("A new echelon cannot already have an ID", ENTITY_NAME, "idexists");
        }
        EchelonDTO result = echelonService.save(echelonDTO);
        return ResponseEntity
            .created(new URI("/api/echelons/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /echelons/:id} : Updates an existing echelon.
     *
     * @param id the id of the echelonDTO to save.
     * @param echelonDTO the echelonDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated echelonDTO,
     * or with status {@code 400 (Bad Request)} if the echelonDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the echelonDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/echelons/{id}")
    public ResponseEntity<EchelonDTO> updateEchelon(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody EchelonDTO echelonDTO
    ) throws URISyntaxException {
        log.debug("REST request to update Echelon : {}, {}", id, echelonDTO);
        if (echelonDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, echelonDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!echelonRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        EchelonDTO result = echelonService.update(echelonDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, echelonDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /echelons/:id} : Partial updates given fields of an existing echelon, field will ignore if it is null
     *
     * @param id the id of the echelonDTO to save.
     * @param echelonDTO the echelonDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated echelonDTO,
     * or with status {@code 400 (Bad Request)} if the echelonDTO is not valid,
     * or with status {@code 404 (Not Found)} if the echelonDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the echelonDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/echelons/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<EchelonDTO> partialUpdateEchelon(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody EchelonDTO echelonDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update Echelon partially : {}, {}", id, echelonDTO);
        if (echelonDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, echelonDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!echelonRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<EchelonDTO> result = echelonService.partialUpdate(echelonDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, echelonDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /echelons} : get all the echelons.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of echelons in body.
     */
    @GetMapping("/echelons")
    public List<EchelonDTO> getAllEchelons() {
        log.debug("REST request to get all Echelons");
        return echelonService.findAll();
    }

    /**
     * {@code GET  /echelons/:id} : get the "id" echelon.
     *
     * @param id the id of the echelonDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the echelonDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/echelons/{id}")
    public ResponseEntity<EchelonDTO> getEchelon(@PathVariable Long id) {
        log.debug("REST request to get Echelon : {}", id);
        Optional<EchelonDTO> echelonDTO = echelonService.findOne(id);
        return ResponseUtil.wrapOrNotFound(echelonDTO);
    }

    /**
     * {@code DELETE  /echelons/:id} : delete the "id" echelon.
     *
     * @param id the id of the echelonDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/echelons/{id}")
    public ResponseEntity<Void> deleteEchelon(@PathVariable Long id) {
        log.debug("REST request to delete Echelon : {}", id);
        echelonService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
